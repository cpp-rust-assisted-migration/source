(defpackage :lilac-rust/rust-common
  (:documentation "Helper common to lenses and translations.")
  (:use :gt/full
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/cpp
        :software-evolution-library/software/rust
        :lilac-commons/ast-fragment)
  (:import-from :software-evolution-library/software/c-cpp-project
                :project-dependency-tree
                :file-dependency-tree)
  (:import-from :software-evolution-library
                :genome)
  (:local-nicknames
   (:ast :software-evolution-library/software/parseable)
   (:rustfmt :lilac-rust/rustfmt)
   (:sel :software-evolution-library))
  (:export
    :rust-type-initializer
    :maybe-wrap-parens
    :maybe-unwrap-parens
    :annotate
    :get-annotation
    :get-all-annotations
    :uid
    :add-uid
    :find-uid
    :assign-uid
    :assign-uid-from
    :fixup-impl-item
    :make-self-parameter
    :copy-ast
    :ensure-expression-statement
    :format-source-text
    :constant-fold-rust))
(in-package :lilac-rust/rust-common)

(defun rust-type-initializer (type)
  "Construct an initializer for a Rust type."
  (check-type type type-ast)
  (if (and (typep type 'rust-generic-type)
           (not (rust-turbofish-operator type)))
      (rust-type-initializer
       (copy type
             :rust-turbofish-operator
             (make '|RUST-::|)))
      (make 'rust-call-expression
            :rust-arguments
            (make 'rust-arguments :children nil)
            :rust-function
            (make 'rust-scoped-identifier
                  :rust-name (make 'rust-identifier :text "new")
                  :rust-path (make 'rust-identifier
                                   :text (ast:source-text type))))))

(defun maybe-wrap-parens (ast &key (class 'rust-parenthesized-expression))
  (assert (subtypep class 'parenthesized-expression-ast))
  (if (wrap-parens? ast)
      (make class :children (list ast))
      ast))

(defgeneric wrap-parens? (ast)
  (:method ((ast t)) t)
  (:method ((ast literal-ast)) nil)
  (:method ((ast call-ast)) nil)
  (:method ((ast parenthesized-expression-ast)) nil)
  (:method ((ast identifier-ast)) nil)
  (:method ((ast subscript-ast)) nil))

(defun maybe-unwrap-parens (ast)
  (match ast
    ((rust-parenthesized-expression :children (list child))
     (typecase child
       (literal-ast child)
       (call-ast child)
       (parenthesized-expression-ast
        (maybe-unwrap-parens child))
       (identifier-ast child)
       (rust-type-cast-expression child)
       (otherwise ast)))))

(-> get-annotation (sel:software ast:ast symbol)
  (values t list &optional))
(defloop get-annotation (obj ast key)
  "Lookup KEY in the annotations of OBJ and its ancestors, returning
the cdr of the first match.

Also return the entire match as a second value."
  (if-let (hit (assoc key (ast:ast-annotations ast)))
    (values (cdr hit) hit)
    (if-let (parent (ast:get-parent-ast obj ast))
      (get-annotation obj parent key)
      (values nil nil))))

(-> get-all-annotations (sel:software ast:ast symbol)
  (soft-alist-of ast:ast t))
(defun get-all-annotations (obj ast key)
  "Lookup KEY in the annotations of OBJ and its ancestors, returning
an alist of from ancestor to value for the ancestors that have a
value."
  (nlet get-all-annotations ((ast ast)
                             (results nil))
    (if (null ast) (nreverse results)
        (let ((parent (ast:get-parent-ast obj ast)))
          (if-let ((result (assoc key (ast:ast-annotations ast))))
            (get-all-annotations parent (cons (cons ast (cdr result)) results))
            (get-all-annotations parent results))))))

(defun annotate (ast key value)
  (if (typep ast 'ast-fragment)
      (copy ast
            :children
            (mapcar (op (annotate _ key value))
                    (children ast)))
      (copy ast
            :annotations
            (acons key value
                   (ast:ast-annotations ast)))))

(defconst +uid+ 'uid)

(defgeneric uid (ast)
  (:method ((ast ast:ast))
    (ast:ast-annotation ast +uid+)))

(let ((i 0))
  (defun add-uid (ast)
    (assign-uid ast (incf i))))

(defgeneric assign-uid (ast uid)
  (:method (ast uid)
    (annotate ast +uid+ uid)))

(defun assign-uid-from (old ast)
  (if old
      (assign-uid ast (uid old))
      ast))

(defun copy-ast (ast &rest args &key &allow-other-keys)
  (assign-uid-from
   ast
   (multiple-value-call #'copy ast
     (values-list args)
     :before-text (before-text ast)
     :after-text (after-text ast))))

(defun find-uid (uid ast)
  (find uid ast :key #'uid))

(defun make-self-parameter (&key (reference t) mutable)
  "Make a Rust self parameter."
  ;; A template would parse to a reference expression.
  (make 'rust-self-parameter
        :rust-borrow
        (and reference (make 'rust-&))
        :children
        (append
         (when mutable
           (list (make 'rust-mutable-specifier :text "mut")))
         (list (make 'rust-self :text "self")))))

(defun fixup-impl-item (ast)
  "Make sure functions in impl items have a &self parameter."
  (match ast
    ((rust-impl-item
      :rust-body
      (and body (rust-declaration-list
                 :children children)))
     (nest
      (copy ast :rust-body)
      (copy body :children)
      (iter (for fn in children))
      (collect)
      (match fn
        ((rust-function-item
          :rust-name name
          :rust-parameters
          (and params (rust-parameters :children children)))
         (cond ((typep (first children) 'rust-self-parameter)
                fn)
               ((ast:source-text= name "new")
                fn)
               (t
                (copy fn
                      :rust-parameters
                      (copy params
                            :children
                            (cons (make-self-parameter)
                                  children))))))
        (otherwise fn))))))

(defun ensure-expression-statement (ast)
  (etypecase ast
    (statement-ast ast)
    (rust-ast (make 'rust-expression-statement :children (list ast)))
    (cpp-ast (make 'cpp-expression-statement :children (list ast)))))

(defun format-source-text (ast &key verbose)
  (let* ((source (ast:source-text ast)))
    (if (typep ast 'rust-ast)
        (let ((rustfmt (rustfmt:rustfmt source :verbose verbose)))
          (if (equal source rustfmt)
              (ast:source-text
               (patch-whitespace
                (tree-copy ast)
                :prettify t))
              rustfmt))
        source)))

(defun constant-fold-rust (rust-ast)
  "Try to constant-fold RUST-AST, returning a Rust AST."
  (if-let (value (constant-fold rust-ast))
    (convert 'rust-ast value)
    rust-ast))
