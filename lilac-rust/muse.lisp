(defpackage :lilac-rust/muse
  (:use
    :gt/full
    :argot/argot
    :argot/readtable
    :gt/full
    :argot/argot
    :argot/readtable
    :lsp-server
    :lsp-server/protocol
    :argot-server/utility
    :argot-server/lsp-diff
    :argot-server/muses/muse
    :argot-server/muses/code-actor
    :argot-server/muses/project-ast-muse
    :software-evolution-library
    :software-evolution-library/software/parseable
    :software-evolution-library/software/project
    :software-evolution-library/software/tree-sitter
    :software-evolution-library/software/rust
    ;; TODO
    ;; :software-evolution-library/software/rust-project
    :software-evolution-library/software/cpp
    :software-evolution-library/software/cpp-project
    :lilac-commons/refactoring
    :lilac-commons/commons
    :lilac-rust/rust-common)
  (:import-from :lilac-rust/traversal :*uri*)
  (:import-from :lsp-server/utilities/lsp-utilities
                :file-uri-pathname)
  (:local-nicknames
   (:attrs :functional-trees/attrs)
   (:analysis :lilac-commons/analysis)
   (:dir :software-evolution-library/software/directory)
   (:rustrans :lilac-rust/rust-translation)
   (:choice :argot-server/choices)
   (:range :software-evolution-library/utility/range))
  (:shadowing-import-from :argot/readtable :argot-readtable)
  (:shadowing-import-from :serapeum :~> :~>>)
  (:import-from :lilac-rust/rustfmt :rustfmt)
  (:import-from :software-evolution-library/components/file
                :original-path)
  (:export :lilac-rust-muse
           :defclass
           :rust-translation))
(in-package :lilac-rust/muse)
(in-readtable argot-readtable)

(defclass rust-translation (one-shot-refactoring)
  ()
  (:default-initargs
   :name :lilac.cpp2rust
   :needs-confirmation nil
   :kind CodeActionKind.Source))

(defmethod refactoring-description ((self rust-translation))
  (ecase-of scope *scope*
    (:unit "Translate compilation unit to Rust")
    (:project "Translate project to Rust")))

(defun remote-project-name (uri)
  (when (stringp uri)
    (let* ((root (project-root-for uri))
           (root-path (file-uri-pathname root))
           (root-path (ensure-directory-pathname root-path)))
      (lastcar (pathname-directory root-path)))))

(defmethod apply-refactoring ((r rust-translation) (p project) (targets list))
  (flet ((path-of (target)
           (namestring
            (pathname
             (etypecase target
               (dir:file-ast (dir:full-pathname target))
               (software (original-path target)))))))
    (analysis:with-aggressive-analysis ()
      (let ((rustrans:*resolve-choice-function* #'muse-resolve-choice-function))
        (multiple-value-call #'rustrans:translate-files-partially
          p targets
          :strip-comments t
          (if-let (project-name (remote-project-name *uri*))
            (values :project-name project-name)
            (values))
          (if (single targets)
              (values :entry-point (path-of (only-elt targets)))
              (values)))))))

(defclass lilac-rust-muse (code-actor project-ast-muse)
  ((refactorings :initarg :refactorings :reader refactorings))
  (:default-initargs
   :name "LiLaC C++ -> Rust translation"
   :force-two-round-code-actions t
   :lang 'cpp
   :refactorings (list (make 'rust-translation))))

(defmethod muse-code-action-kinds append ((muse lilac-rust-muse))
  (~>> muse
       refactorings
       (mapcar #'refactoring-kind)
       nub
       (mapcar #'string)))

(defmethod muse-commands append ((muse lilac-rust-muse))
  (mapcar #'refactoring-name (refactorings muse)))

(defmethod applicable-commands ((muse lilac-rust-muse)
                                &key start end text-document)
  (when (inhibit-code-actions?)
    (return-from applicable-commands nil))
  (let ((uri (slot-value text-document '|uri|)))
    (unless (ensure-project-ast uri)
      (return-from applicable-commands nil)))
  (with-document-software ((start end)
                           :software software)
    (mapcar
     (lambda (refactoring)
       (make 'Command
             :title (refactoring-description refactoring)
             :command (string (refactoring-name refactoring))
             :arguments
             (list (slot-value text-document 'URI)
                   (text-document-version text-document))))
     (refactorings muse))))

(defun get-original-line-number ()
  (when-let (range (ast-source-range
                    rustrans:*original-software*
                    rustrans:*original-ast*))
    (range:line (range:begin range))))

(defun choice-description (choice)
  (fmt "~@[~a: ~]~a"
       (when-let (loop (find-if (of-type 'loop-ast) choice))
         (drop-prefix "rust-"
                      (substitute #\Space #\-
                                  (string-capitalize
                                   (tree-sitter-class-name
                                    (class-of loop))))
                      :test #'char-equal))
       (source-text choice)))

(defun muse-resolve-choice-function (choices)
  (let* ((choice
           (choice:present-choices
            (mapcar #'choice-description choices)
            :prompt (fmt "Choose replacement strategy~@[ for line ~a~]: ~a"
                         (get-original-line-number)
                         (ellipsize
                          (source-text rustrans:*original-ast*)
                          40)))))
    (when (emptyp choice)
      (error "Translation aborted"))
    (let* ((choice
             (find choice choices
                   :test #'equal :key #'choice-description))
           (approve/reject
             (choice:present-choices
              (list (source-text choice))
              :open t
              :prompt "Approve/reject replacement template"
              :multi-line t
              :default-text (source-text choice))))
      (when (emptyp approve/reject)
        (error "Translation aborted"))
      choice)))

(defun apply-refactoring-command (refactoring arguments)
  (destructuring-bind (uri version) arguments
    (declare (ignore version))
    (let* ((*uri* uri)
           (project
             (ensure-project-ast uri))
           (file
             (lret ((file (lookup-project-uri uri)))
               (assert (attrs:reachable? project file))))
           (files
             (with-thread-name (:name "Getting dependencies")
               (attrs:with-attr-table project
                 (get-files-in-scope file))))
           (translated-project
             (with-no-code-actions ()
               (with-thread-name (:name "Translating")
                 (apply-refactoring refactoring project files)))))
      (make 'ApplyWorkspaceEditParams
            :label (string (refactoring-name refactoring))
            :edit
            (annotate-workspace-edit
             (refactoring-description refactoring)
             (make 'WorkspaceEdit
                   :document-changes
                   (lsp-diff-projects uri project translated-project))
             :needs-confirmation (needs-confirmation? refactoring))))))

(defmethod apply-command ((muse lilac-rust-muse)
                          (cmd symbol)
                          arguments)
  (unless (keywordp cmd)
    (return-from apply-command (call-next-method)))
  (let ((r (find cmd (refactorings muse) :key #'refactoring-name)))
    (if r
        (apply-refactoring-command r arguments)
        (return-from apply-command (call-next-method)))))
