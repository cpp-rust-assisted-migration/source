(defpackage :lilac-rust/traversal
  (:use :gt/full
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/cpp
        :software-evolution-library/software/rust
        :lilac-commons/ast-fragment
        :lilac-rust/translated-ast
        :lilac-rust/rust-common
        :lilac-commons/database)
  (:local-nicknames
   (:alt :lilac-rust/translated-ast)
   (:ast :software-evolution-library/software/parseable)
   (:attrs :functional-trees/attrs)
   (:choice :argot-server/choices)
   (:it :iterate)
   (:l :fresnel/lens)
   (:rust :lilac-rust/rust-common)
   (:rust-lens :lilac-rust/rust-lens)
   (:sel :software-evolution-library))
  (:import-from :argot-server/utility :goto-position)
  (:import-from :lsp-server/utilities/lsp-utilities
                :current-connection)
  (:shadowing-import-from :trivia.fail :fail)
  (:export
    :traversal-lift-lower
    :substitute-iterators
    :handle-block-children
    :*interactive*
    :*input-script*
    :*input-mode*
    :*uri*
    :template
    :template-plist
    :read-string
    :y/n
    :choose-from-alist))
(in-package :lilac-rust/traversal)


;;; Environment (types, constants, vars).

(defconst +directions+ '(:l2r :r2l))

(defconst +modifying+ '(:mut :const))

(defconst +iterators+ '(:i1 :i2))

(deftype direction ()
  (cons 'member +directions+))

(deftype modifying ()
  (cons 'member +modifying+))

(deftype iterators ()
  (cons 'member +iterators+))

(deftype input-mode ()
  '(member :repl :argot :script :best-guess))

(defvar *input-script* '())

(declaim (type input-mode *input-mode*))
(defvar *input-mode* :argot)

(defvar *interactive* t)


;;; Generic functions.

(defgeneric traversal-fill-holes (traversal &key &allow-other-keys)
  (:documentation "Fill the holes in a traversal template.")
  (:method ((traversal t) &key)
    nil))

(defgeneric traversal-lower (traversal &key block-children &allow-other-keys)
  (:documentation "Lower to Rust from a traversal template.")
  (:method-combination standard/context)
  (:method :context ((traversal t) &key &allow-other-keys)
    (annotate (call-next-method)
              'template traversal)))


;;; Traversal classes (generated).

(eval-always
  (defparameter *traversal-properties*
    '((traversal0 :l2r :const :i1)
      (traversal1 :r2l :const :i1)
      (traversal2 :l2r :mut :i1)
      (traversal3 :l2r :const :i2)
      (traversal4 :r2l :mut :i1)
      (traversal5 :r2l :const :i2)
      (traversal6 :l2r :mut :i2)
      (traversal7 :r2l :mut :i2)))

  (assert (every (distinct :key #'cdr) *traversal-properties*)))

(defclass traversal ()
  ((ast :initarg :ast :type (or null ast:ast))
   (direction :type direction)
   (modifying :type modifying)
   (iterators :type iterators)
   (template-plist
    :initform nil
    :accessor template-plist)))

(defmethod print-object ((self traversal) stream)
  (print-unreadable-object (self stream :type t)
    (with-slots (self direction modifying iterators) self
      (format stream "~a,~a,~a" direction modifying iterators))))

(defmethod traversal-lower :around ((self traversal) &rest kwargs &key)
  (multiple-value-call #'call-next-method
    self
    (values-list (slot-value self 'template-plist))
    (values-list kwargs)))

(defmacro generate-mixin-classes ()
  `(progn
     ,@(iter (for slot in '(direction modifying iterators))
             (appending
              (iter (for value in
                         (ecase slot
                           (direction +directions+)
                           (modifying +modifying+)
                           (iterators +iterators+)))
                    (collecting
                      `(defclass ,(symbolicate 'traversal- (princ-to-string value))
                           (traversal)
                         ((,slot :initform ,value)))))))))

(generate-mixin-classes)

(defmacro generate-traversal-classes ()
  `(progn
     ,@(iter (for (name direction modifying iterators)
                  in *traversal-properties*)
             (collecting
               `(defclass ,name (,(symbolicate 'traversal- (princ-to-string direction))
                                 ,(symbolicate 'traversal- (princ-to-string modifying))
                                 ,(symbolicate 'traversal- (princ-to-string iterators)))
                  ())))))

(generate-traversal-classes)

(defmacro generate-traversal-class-dispatch ()
  `(defun traversal-class-dispatch (direction modifying iterators
                              &rest args
                              &key &allow-other-keys)
     (dispatch-case ((direction direction)
                     (modifying modifying)
                     (iterators iterators))
       ,@(iter (for (class direction modifying iterators)
                    in *traversal-properties*)
               (collect `(((eql ,direction) (eql ,modifying) (eql ,iterators))
                          (apply #'make ',class args)))))))

(generate-traversal-class-dispatch)


;;; API.

(defun traversal-lift (&rest kwargs &key uri &allow-other-keys)
  (when-let (traversal (apply #'read-traversal-class :uri uri kwargs))
    (apply #'traversal-fill-holes traversal kwargs)
    traversal))

(defun traversal-lift-lower (&rest kwargs &key uri &allow-other-keys)
  (when-let (traversal (apply #'traversal-lift :uri uri kwargs))
    (apply #'traversal-lower traversal kwargs)))

(def +left-to-right+ "Left-to-right")
(def +right-to-left+ "Right-to-left")
(def +yes+ "Yes")
(def +no+ "No")

(defun guess-direction (ast)
  ;; TODO Counting down.
  (if (some (op (source-text= "rbegin" _)) ast)
      +right-to-left+
      +left-to-right+))

(-> guess-modifying (ast) (values string &optional))
(defun guess-modifying (ast)
  (match ast
    ((cpp-for-statement
      (cpp-initializer init))
     (cond ((find-if (lambda (ast)
                       (match ast
                         ((cpp* "$_.cbegin")
                          t)
                         ((cpp* "const $_ $_ = $_.begin()") t)
                         ((type cpp-ast)
                          (when-let (type (slot-value-safe ast 'cpp-type))
                            (find-if (op (source-text= _ "const_iterator"))
                                     type)))))
                     init)
            +no+)
           (t +yes+)))
    (otherwise +yes+)))

(-> guess-iterator-count (ast) (values (integer 0) &optional))
(defun guess-iterator-count (ast)
  (match ast
    ((cpp-for-statement
      (cpp-initializer init))
     (count-if (of-type 'cpp-init-declarator) init))
    (otherwise 1)))

(defvar *uri* nil)

(defun read-traversal-class (&rest kwargs
                             &key line direction modifying iterators
                               ast (uri *uri*)
                             &allow-other-keys)
  ;; NB The line we are receiving is zero-based, which is how VS Code
  ;; handles line numbers internally, but the way it displays line
  ;; numbers is 1-based.
  (check-type line (integer 0 *))
  (when (and uri
             (ignore-errors
              (current-connection)))
    ;; We want the line to be visible below the dropdown.
    (goto-position uri :line (max 0 (- line 3))))
  (when (not *interactive*)
    (return-from read-traversal-class nil))
  (let ((direction
          (or direction
              (let ((default
                      (or (guess-direction ast)
                          +left-to-right+)))
                (choose-from-alist
                 `((,+left-to-right+ . :l2r)
                   (,+right-to-left+ . :r2l))
                 :prompt
                 (fmt "Which direction does the loop on line ~a (`~a`) go in?~@[ (Default: ~a)~]"
                      (+ line 2)
                      (take 20 (first (source-text-take-lines 1 ast)))
                      default)
                 :default-text default))))
        (modifying
          (or modifying
              (let ((default (guess-modifying ast)))
                (if (y/n (fmt "Is the container modified?~@[ (Default: ~a)~]"
                              default)
                         :default default)
                    :mut
                    :const))))
        (iterators
          (or iterators
              (let ((default (guess-iterator-count ast)))
                (choose-from-alist
                 '(("1" . :i1)
                   ("2" . :i2))
                 :prompt (fmt "How many iterators?~@[ (Default: ~a)~]"
                              default)
                 :default :i1
                 :default-text (princ-to-string default))))))
    (declare (direction direction)
             (modifying modifying)
             (iterators iterators))
    (apply #'traversal-class-dispatch
           direction modifying iterators
           :allow-other-keys t
           kwargs)))


;;; Input handling.

(defun read-string (&key (prompt "Say something:") (default-text ""))
  (assure string
    (ecase-of input-mode *input-mode*
      (:best-guess default-text)
      (:script (pop *input-script*))
      (:repl
       (print prompt)
       (let ((input (read-line)))
         (if (emptyp input) default-text input)))
      (:argot
       (choice:read-string
        :prompt prompt
        :default-text default-text)))))

(defun y/n (prompt &key (default nil default-supplied?))
  (assure boolean
    (ecase-of input-mode *input-mode*
      (:best-guess
       (if default-supplied?
           (string-ecase default
             ("Yes" t)
             ("No" nil))
           t))
      (:repl
       (y-or-n-p "~a" prompt))
      (:argot
       (multiple-value-call #'choice:yes-or-no?
         prompt
         (if default-supplied?
             (values :default default)
             (values))))
      (:script
        (pop *input-script*)))))

(defun choose-from-alist (alist &key (prompt "Choose one: ") (default-text "")
                                  (default nil default-supplied?))
  (ecase-of input-mode *input-mode*
    (:best-guess
     (or (aget default-text alist :test #'equal)
         default))
    ((:repl :script)
     (let ((key (read-string :prompt prompt :default-text default-text)))
       (if-let (match (assoc key alist :test #'equal))
         (cdr match)
         (if default-supplied? default
             (error "Not in alist: ~a" key)))))
    (:argot
     (choice:present-choice-from-alist
      alist
      :prompt prompt
      :default-text default-text))))


;;; Traversal mixin methods.

;;; TODO Candidates should be variables in scope?

(defun guess-container-name (ast)
  (match ast
    ((cpp-for-statement
      (cpp-initializer (and init (cpp-ast))))
     (iter (for ast in-tree init)
           (thereis
            (match ast
              ((or (cpp* "$1.begin()" container)
                   (cpp* "$1.cbegin()" container)
                   (cpp* "$1.rbegin()" container))
               (source-text container))))))))

(defun guess-iterator-name (ast &key (offset 0))
  (match ast
    ((cpp-for-statement
      (cpp-initializer (and init (cpp-ast))))
     (let ((candidates (collect-if (of-type 'cpp-init-declarator) init)))
       (unless (>= offset (length candidates))
         (when-let (iterator (elt candidates offset))
           (source-text (lhs iterator))))))))

(defun read-container-name-for (ast)
  (assure string
    (let ((default
            (or (guess-container-name ast)
                "UNKNOWN")))
      (read-string
       :prompt (fmt "Name of the container?~@[ Default: ~a~]" default)
       :default-text default))))

(defun read-sole-iterator-name-for (ast)
  (assure string
    (let ((default (or (guess-iterator-name ast)
                       "UNKNOWN")))
      (read-string
       :prompt (fmt "Name of the iterator?~@[ Default: ~a~]" default)
       :default-text default))))

(defun read-nth-iterator-name-for (ast offset)
  (assure string
    (let ((default (or (guess-iterator-name ast :offset offset)
                       "UNKNOWN")))
      (read-string
       :prompt (fmt "Name of the ~:r iterator?~@[ Default: ~a~]"
                    (1+ offset)
                    default)
       :default-text default))))

(defmethod traversal-fill-holes
    ((self traversal-i1)
     &key
       ast
       (container-name (read-container-name-for ast))
       (iterator-name (read-sole-iterator-name-for ast)))
  (assert (not (emptyp iterator-name)))
  (unless (emptyp container-name)
    (with-slots (template-plist) self
      (setf template-plist
            (list* :container-name container-name
                   :iterator-name iterator-name
                   :iterator-replacement
                   (make 'rust-identifier :text iterator-name)
                   template-plist)))))

(defmethod traversal-fill-holes
    ((self traversal-i2)
     &key
       ast
       (container-name (read-container-name-for ast))
       (iterator-1 (read-nth-iterator-name-for ast 0))
       (iterator-2 (read-nth-iterator-name-for ast 1)))
  (with-slots ((plist template-plist)) self
    (unless (emptyp container-name)
      (setf plist (list* :container-name container-name plist))
      (unless (emptyp iterator-1)
        (setf plist (list*
                     :iterator-1 iterator-1
                     :iterator-1-replacement (rust* "$1[i]" container-name)
                     plist)))
      (unless (emptyp iterator-2)
        (setf plist (list*
                     :iterator-2 iterator-2
                     :iterator-2-replacement (rust* "$1[i+1]" container-name)
                     plist))))))


;;; Generation methods.

(defun substitute-iterators (for-loop ast table)
  "Turn dereferences of OLD-NAME in AST into simple variable accesses
of NEW-NAME."
  (declare (hash-table table)
           (ast ast))
  #+debug
  (do-hash-table (k v table)
    (assert (stringp k))
    (assert (typep v 'ast)))
  (labels ((iterator-use? (var)
             (and (gethash (source-text var) table)
                  (not (find (get-declaration-id :variable var)
                             (cpp-body for-loop)))))
           (handle (ast)
             (match ast
               ((cpp-pointer-expression
                 :cpp-operator (cpp-*)
                 :cpp-argument (and arg (cpp-identifier :text text)))
                (if-let (new-name (gethash text table))
                  (if (iterator-use? arg)
                      (make-rust-wrapper
                       (assign-uid-from ast (tree-copy new-name)))
                      ast)
                  (fail)))
               ((cpp-field-expression
                 :cpp-operator (cpp-->)
                 :cpp-argument (and arg (cpp-identifier :text text)))
                (if-let (new-name (gethash text table))
                  (if (iterator-use? arg)
                      (copy ast
                            :cpp-argument (make-rust-wrapper
                                           (assign-uid-from
                                            arg
                                            (tree-copy new-name))))
                      ast)
                  (fail)))
               ((cpp-identifier :text text)
                (if-let (new-name (gethash text table))
                  (if (iterator-use? ast)
                      (make-rust-wrapper
                       (assign-uid-from ast (tree-copy new-name)))
                      ast)
                  (fail)))
               ;; Handle a pointer expression based on its argument.
               ((cpp-pointer-expression
                 :cpp-operator (cpp-&)
                 :cpp-argument arg)
                (let ((r (handle arg)))
                  (if (eql r arg)
                      ast
                      (assign-uid-from ast r))))
               ;; Handle a reference declarator based on the value of
               ;; the surrounding init declarator.
               ;; ((cpp-init-declarator
               ;;   :cpp-declarator (cpp-reference-declarator :children (list id))
               ;;   :cpp-value value)
               ;;  (let ((r (handle value)))
               ;;    (if (eql value r)
               ;;        ast
               ;;        (copy ast
               ;;              :cpp-value r
               ;;              :cpp-declarator id))))
               (otherwise ast))))
    (mapcar (lambda (ast)
              (let ((result (handle ast)))
                (assert (find-uid (uid ast) result))
                result))
            ast)))

(defun handle-block-children (children for-loop iterator-names-alist)
  (let ((ht (alist-hash-table
             (mapcar (op (cons (source-text (car _1)) (cdr _1)))
                     iterator-names-alist)
             :test #'equal)))
    (mapcar (lambda (child)
              (make-untranslated-ast
               (substitute-iterators
                for-loop
                child
                ht)))
            children)))

(defmethod traversal-lower ((self traversal0)
                               &key container-name iterator-name block-children
                                 iterator-replacement)
  (assert (notany (of-type 'translated-ast) block-children))
  (with-slots (ast) self
    (ensure-expression-statement
     (rust* "for $I in $ITER.iter() { @BODY }"
            :i (make 'rust-identifier :text (assure string iterator-name))
            :iter (make 'rust-identifier :text (assure string container-name))
            :body
            (handle-block-children
             block-children
             ast
             (list (cons iterator-name iterator-replacement)))))))

(defmethod traversal-lower ((self traversal2)
                               &key container-name iterator-name block-children
                                 iterator-replacement)
  (assert (notany (of-type 'translated-ast) block-children))
  (with-slots (ast) self
    (ensure-expression-statement
     (rust* "for $I in $ITER.iter() { @BODY }"
            :i (make 'rust-identifier :text (assure string iterator-name))
            :iter (make 'rust-identifier :text (assure string container-name))
            :body
            (handle-block-children
             block-children
             ast
             (list (cons iterator-name iterator-replacement)))))))

(defmethod traversal-lower
    ((self traversal6) &key container-name iterator-1 iterator-2 block-children
                         iterator-1-replacement iterator-2-replacement)
  (declare (string container-name iterator-1 iterator-2))
  (with-slots (ast) self
    (ensure-expression-statement
     (rust* "for i in 0..($CONTAINER.len()-1) { @BODY }"
            :container container-name
            :body
            (handle-block-children
             block-children
             ast
             (list (cons iterator-1 iterator-1-replacement)
                   (cons iterator-2 iterator-2-replacement)))))))
