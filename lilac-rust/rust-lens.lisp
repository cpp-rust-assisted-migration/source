(defpackage :lilac-rust/rust-lens
  (:documentation "Lens for translating C++ to Rust.

Lenses are used for convenience; not meant to be bidirectional.")
  (:use :gt/full
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/cpp
        :software-evolution-library/software/rust
        :argot-server/lens/ast
        :lilac-rust/translated-ast
        :lilac-rust/rust-common
        :lilac-commons/commons
        :lilac-commons/ast-fragment)
  (:import-from :trivia.fail :fail)
  (:local-nicknames (:l :fresnel/lens)
                    (:ast :software-evolution-library/software/parseable)
                    (:sel :software-evolution-library)
                    (:ts :software-evolution-library/software/tree-sitter))
  (:export :rust-lens :to-rust
           :rust-lens-raw
           :camel/snake
           :undeclared-import))
(in-package :lilac-rust/rust-lens)

;;; TODO Preserve before, after text.

(defun to-rust (x)
  (etypecase x
    (string (to-rust (convert 'cpp-ast x :deepest t :tolerant t)))
    (t (l:forward (rust-lens) x))))


;;; Supporting macros.

(defmacro match-asts ((&rest  bindings) &body clauses)
  (with-unique-names (annotations
                      before-text after-text
                      before-asts after-asts
                      indent-adjustment indent-children)
    `(l:match-constructors
         (,@bindings ,before-text ,after-text
                     (,indent-adjustment (l:of-type '(or null integer)))
                     (,indent-children (l:of-type '(or null (integer 0))))
                     (,before-asts (l:mapcar (comment-lens)))
                     (,after-asts (l:mapcar (comment-lens)))
                     (,annotations (l:of-type 'list)))
       ,@(mapcar (lambda (clause)
                   (match clause
                     ((list* 'make _)
                      (append clause
                              `(:annotations ,annotations
                                :before-text ,before-text
                                :after-text ,after-text
                                :before-asts ,before-asts
                                :after-asts ,after-asts
                                :indent-children ,indent-children
                                :indent-adjustment ,indent-adjustment)))
                     (otherwise clause)))
                 clauses))))

(defmacro match-templates ((&rest bindings) &body clauses)
  (let ((clauses (batches clauses 2 :even t))
        (args (iter (for var in (mapcar #'car bindings))
                    (appending (list (make-keyword var)
                                     var)))))
    `(l:match ,bindings
       ,@(iter (for (from to) in clauses)
               (collect (append from args))
               (collect (append to args))))))

(defun comment-lens ()
  "Lens for comments. This is only used when translating before/after-asts."
  (match-asts ((text (l:of-type 'string)))
    (make 'cpp-comment :text text)
    (make 'rust-comment :text text)))

(defmacro with-natural-match* (matcher bindings &body body)
  "Match two constructors on the same initargs."
  (let ((new-args
          (iter (for (name . nil) in bindings)
                (appending
                 `(,(make-keyword name)
                   ,name)))))
    `(,matcher ,bindings
               ,@(iter (for form in body)
                       (collecting
                         (ematch form
                           ((list* 'make class args)
                            `(make ,class ,@args ,@new-args))
                           ((list* lang (and template (type string)) args)
                            `(,lang ,template ,@args ,@new-args))))))))

(defmacro with-natural-ast-match (bindings &body body)
  "Match two constructors on the same initargs."
  `(with-natural-match* match-asts ,bindings
     ,@body))

(defmacro with-natural-match (bindings &body body)
  "Match two constructors on the same initargs."
  `(with-natural-match* l:match ,bindings
     ,@body))

(eval-always
  (defun parse-sliced-template (template)
    (let ((count 0)
          (substrings '()))
      (values
       (regex-replace-all "(\\{\\{.*?\\}\\})"
                          template
                          (lambda (string &rest args)
                            (declare (ignore args))
                            (push (slice (copy-seq string) 2 -2) substrings)
                            (string+ "$" (incf count)))
                          :simple-calls t)
       (nreverse substrings))))

  (defmacro scaffold ((fn template))
    (multiple-value-bind (template args)
        (parse-sliced-template template)
      `(ast-from-template* ,template
                           ',(language-ast-class fn)
                           ,@args)))

  (defpattern scaffold (call)
    (convert 'match (eval `(scaffold ,call)))))


;;; Lens for translated ASTs.

(defun translated-ast-lens (&key (rust-type t) (satisfies (constantly t)))
  (flet ((rust-ok? (rust)
           (and (typep rust rust-type)
                (funcall satisfies rust))))
    (l:make-lens
     (lambda (x)
       (let ((rust
               (match x
                 ((translated-ast (rust (list rust)))
                  rust))))
         ;; NB While this a Rust AST, it is not guaranteed to only
         ;; contain other Rust ASTs. That's taken care of by
         ;; `translate/bottom-up` in `lilac-rest/rust-translation`.
         (and (rust-ok? rust) rust)))
     (lambda (x)
       (match x
         ((and rust (type rust-ast))
          (when (rust-ok? rust)
            (make-rust-wrapper rust)))))
     (lambda (ast translated)
       (match* (ast translated)
         (((and rust (type rust-ast))
           (translated-ast (cpp (list cpp))))
          (when (rust-ok? rust)
            (make-partial-translation cpp rust))))))))


;;; Terminals.

(defun boolean-lit-term ()
  (match-templates ()
    (cpp* "true")
    (rust* "true")

    (cpp* "false")
    (rust* "false")))

(defun integer? (string)
  ;; TODO Hex, octal syntax for C++. (Use with-c-syntax?) What syntax
  ;; does Rust use?
  (ignore-some-conditions (parse-error)
    (parse-integer string)))

(defun float? (string)
  (when (scan "[\\.df]" string)
    (floatp (parse-float string))))

(defun cpp-float+suffix (string)
  (let ((base (chomp string '("f" "F" "l" "L"))))
    (values (remove #\' base)
            (drop (length base) string))))

(defun rust-float+suffix (string)
  (let ((base (chomp string '("f32" "f64"))))
    (values base (drop (length base) string))))

(defun float-string-lens ()
  (l:make-lens
   (lambda (string)
     (multiple-value-bind (base suffix)
         (cpp-float+suffix string)
       (when (float? base)
         (string-ecase suffix
           ("" base)
           (("l" "L") base)
           (("f" "F") (string+ base "f32"))))))
   (lambda (string)
     (multiple-value-bind (base suffix)
         (rust-float+suffix string)
       (when (float? base)
         (string-case suffix
           ("" base)
           ("f64" base)
           ("f32" (string+ base "f"))))))))

(defun int-lit-term ()
  (match-asts ((int (l:satisfies #'integer?)))
    (make 'cpp-number-literal :text int)
    (make 'rust-integer-literal :text int)))

(defun float-lit-term ()
  (match-asts ((float (float-string-lens)))
    (make 'cpp-number-literal :text float)
    (make 'rust-float-literal :text float)))

(defun number-lit-term ()
  (l:or! (float-lit-term)
         (int-lit-term)))

(defun string-lit-term ()
  (match-asts ((text (l:identity)))
    (make 'cpp-string-literal :text text)
    (make 'rust-string-literal :text text)))

(defun char-lit-term ()
  (match-asts ((text (l:identity)))
    (make 'cpp-char-literal :text text)
    (make 'rust-char-literal :text text)))

(defun lit-term ()
  (l:or (number-lit-term)
        (boolean-lit-term)
        (string-lit-term)
        (char-lit-term)
        (translated-ast-lens :rust-type 'literal-ast)))

(defun camel-case->snake-case (s)
  "Convert S from camel case to snake case."
  (if (notany #'upper-case-p s) s
      (mapconcat #'string-downcase
                 (runs (string-upcase-initials s)
                       :test (lambda (c1 c2)
                               (and (upper-case-p c1)
                                    (lower-case-p c2))))
                 "_")))

(defun snake-case->camel-case (s)
  "Convert S from snake case to camel case."
  (mapconcat #'string-upcase-initials
             (split-sequence #\_ s)
             ""))

(defun camel/snake ()
  (l:guard #'stringp
           (l:make-lens #'camel-case->snake-case
                        #'snake-case->camel-case)))

(defun name-term (&key (name-lens (l:of-type 'string)))
  (l:or
   (match-asts ((name name-lens))
     ;; TODO Since C++ references are implicitly dereferenced, C++
     ;; identifers may be too be translated into Rust dereferences.
     (make 'cpp-identifier :text name)
     (make 'rust-identifier :text name)

     ;; TODO x.y vs x->y
     (make 'cpp-field-identifier :text name)
     (make 'rust-field-identifier :text name)

     (make 'cpp-namespace-identifier :text name)
     ;; The Rust grammar doesn't have an equivalent.
     (make 'rust-identifier :text name)

     (make 'cpp-module-name :text name)
     (make 'rust-identifier :text name))
   (translated-ast-lens :rust-type
                        '(or rust-identifier rust-field-identifier))))

(defun qname-term (&key function want-type)
  "Work around the fact that C++ qualified identifiers are right-recursive,
while Rust scoped identifiers are left-recursive."
  (declare (optimize (debug 0)))
  (labels
      ((cpp->parts (cpp &optional parts)
         "Split a C++ qualified identifier into parts."
         (match cpp
           ((cpp-qualified-identifier
             (cpp-scope scope)
             (cpp-name name))
            (cpp->parts name (cons scope parts)))
           (otherwise
            (reverse (cons cpp parts)))))
       (parts->cpp (parts)
         "Assemble PARTS into a C++ qualified identifier."
         (reduce (lambda (part cpp)
                   (make 'cpp-qualified-identifier
                         :cpp-scope part
                         :cpp-name cpp))
                 parts
                 :from-end t))
       (rust->parts (rust &optional parts)
         "Split a Rust scoped identifier into its parts."
         (match rust
           ((rust-scoped-identifier
             (rust-path path)
             (rust-name name))
            (cons name (rust->parts path)))
           ((rust-scoped-type-identifier
             (rust-path path)
             (rust-name name))
            (cons name (rust->parts path)))
           (otherwise
            (reverse (cons rust parts)))))
       (parts->rust (parts)
         "Assemble PARTS into a Rust scoped identifier."
         ;; This is ugly, but maybe not as ugly as an ad-hoc parser.
         ;; The Rust grammar is very picky: if it has types in it
         ;; anywhere, it has to be a type identifier; scopes fold
         ;; right, except if the last one has type arguments, in which
         ;; case it is a generic with with a scoped type as its path.
         ;; And if it's in a function call it has to be a scoped
         ;; identifier, but use the turbofish.
         (let ((string (fmt "~{~a~^::~}" (mapcar #'ast:source-text parts))))
           (assure rust-ast
             (if (some (of-type 'type-ast) parts)
                 (if function
                     (let* ((string-with-turbofish (string-replace-all ":<" string "::<"))
                            (call-string (fmt "~a();" string-with-turbofish)))
                       (find-if (of-type 'rust-scoped-identifier)
                                (convert 'rust-ast call-string :deepest t)))
                     (let ((string (fmt "let x: ~a;" string)))
                       (find-if (of-type 'rust-scoped-type-identifier)
                                (convert 'rust-ast string :deepest t))))
                 (find-if (of-type 'rust-scoped-identifier)
                          (convert 'rust-ast (string+ string ";")
                                   :deepest t))))))
       (qname-term ()
         "The actual lens."
         (l:make-lens
          (lambda (cpp)
            (when (typep cpp 'cpp-qualified-identifier)
              (let* ((cpp-parts (cpp->parts cpp)))
                (unless (and want-type
                             (not (typep (lastcar cpp-parts) 'cpp-type-identifier)))
                  (let ((rust-parts
                          (mapcar (op (l:forward (rust-lens) _))
                                  cpp-parts)))
                    (unless (some #'null rust-parts)
                      (parts->rust rust-parts)))))))
          (lambda (rust)
            (when (typep rust 'rust-scoped-identifier)
              (let* ((rust-parts (rust->parts rust))
                     (cpp-parts
                       (mapcar (op (l:backward (rust-lens) _))
                               rust-parts)))
                (unless (some #'null cpp-parts)
                  (parts->cpp cpp-parts)))))
          (lambda (rust cpp)
            (when (typep rust 'rust-scoped-identifier)
              (let* ((rust-parts (rust->parts rust))
                     (old-cpp-parts (cpp->parts cpp))
                     (cpp-parts
                       (mapcar (op (l:backward (rust-lens) _ _))
                               rust-parts
                               old-cpp-parts)))
                (unless (some #'null cpp-parts)
                  (parts->cpp cpp-parts))))))))
    (l:or (qname-term)
          (translated-ast-lens :rust-type 'rust-scoped-identifier))))

(defconst +operator-names+
  '("+" "-" "*" "/" "%" "||" "&&" "|" "^" "&"
    "==" "!=" ">" ">=" "<=" "<" "<<" ">>"
    "+=" "-=" "*=" "/=" "%=" "&=" "|=" "^=" "<<=" ">>="))

(defun op-term ()
  (macrolet ((match-operators ()
               `(l:match-constructors ()
                  ,@(iter (for name in +operator-names+)
                          (appending
                           `((make ',(find-external-symbol
                                      (string+ 'cpp- name)
                                      :sel/sw/ts
                                      :error t)
                                   :text ,name)
                             (make ',(find-external-symbol
                                      (string+ 'rust- name)
                                      :sel/sw/ts
                                      :error t)
                                   :text ,name)))))))
    (l:or (match-operators)
          (translated-ast-lens :rust-type 'terminal-symbol))))

(defun this-term ()
  (l:match () (cpp* "this") (rust* "self")))

(defun export-term ()
  (l:match ()
    (make 'cpp-export-specifier :text "export")
    (make 'rust-visibility-modifier :text "pub")))

(defun term (&key function)
  (l:or (lit-term)
        (this-term)
        (export-term)
        (qname-term :function function)
        (name-term)
        (op-term)
        (translated-ast-lens
         :rust-type '(or literal-ast identifier-ast terminal-symbol))))


;;; Expressions.

(defun constant-folding ()
  (l:canonize-after
   (lambda (ast)
     (let ((folded (constant-fold-rust ast)))
       (if (eql ast folded) nil
           folded)))))

(defun wrap-rust-parens ()
  (l:canonize-after
   (lambda (ast)
     (let ((wrapped (maybe-wrap-parens ast)))
       (if (eql ast wrapped) nil
           wrapped)))))

(defun unwrap-rust-parens ()
  (l:canonize-after
   (lambda (ast)
     (let ((wrapped (maybe-unwrap-parens ast)))
       (if (eql ast wrapped) nil
           wrapped)))))

(defun unwrap-cpp-parens ()
  (l:canonize-before
   (lambda (ast)
     (loop (match ast
             ((cpp-parenthesized-expression :children (list child))
              (setf ast child))
             (otherwise (return ast)))))))

(defun wrap-clone ()
  (l:canonize-after
   (lambda (ast)
     (when (typep ast '(or identifier-ast subscript-ast field-ast))
       (rust* "$EXPR.clone()" :expr ast)))))

(defun type-id-to-id ()
  (l:canonize-after
   (lambda (ast)
     (when (typep ast 'rust-type-identifier)
       (make 'rust-identifier :text (ast:source-text ast))))))

(defun unop ()
  (match-templates ((val (expression)))
    (cpp* "-$VAL")
    (rust* "-$VAL")

    (cpp* "!$VAL")
    (rust* "!$VAL")))

(defun binop ()
  (match-asts ((left (expression))
               (right (expression))
               (operator (op-term)))
    (make 'cpp-binary-expression
          :cpp-left left
          :cpp-right right
          :cpp-operator operator)
    (make 'rust-binary-expression
          :rust-left left
          :rust-right right
          :rust-operator operator)))

(defun field-expression ()
  "Translate a field expression.

This doesn't distinguish between x.y and x->y, but Rust doesn't
either (it uses \"automatic dereferencing\" instead."
  (match-templates ((expr (expression))
                    (field (name-term :name-lens (camel/snake))))
    (cpp* "$EXPR.$FIELD")
    (rust* "$EXPR.$FIELD")

    (cpp* "$EXPR->$FIELD")
    (rust* "$EXPR.$FIELD")))

(defun ref ()
  "Translate a reference wrapper."
  (match-templates ((expr (expression)))
    (cpp* "std::cref($EXPR)")
    (rust* "&$EXPR")

    (cpp* "__CRAM__CREF($EXPR)")
    (rust* "&$EXPR")

    (cpp* "std::ref($EXPR)")
    (rust* "&mut $EXPR")

    (cpp* "__CRAM__REF($EXPR)")
    (rust* "&mut $EXPR")))

(defun std-name ()
  (l:match ()
    "string" "String"
    "sqrt" "sqrt"
    "sqrtf" "sqrt"
    "pow" "powf"
    "powf" "powf"
    "min" "min"))

(defun std-id ()
  (match-asts ((name (std-name)))
    (make 'cpp-qualified-identifier
          :cpp-scope (make 'cpp-namespace-identifier :text "std")
          :cpp-name (make 'cpp-identifier :text name))
    (make 'rust-field-identifier :text name)

    (make 'cpp-identifier :text name)
    (make 'rust-field-identifier :text name)

    (make 'cpp-field-identifier :text name)
    (make 'rust-field-identifier :text name)))

(defun std-field ()
  (match-asts ((name (std-name)))
    (make 'cpp-field-identifier :text name)
    (make 'rust-field-identifier :text name)))

(defun std-call-to-method-call ()
  "Convert std::fn(x) to x.fn()."
  (match-asts ((function (std-id))
               (arg (l:after (expression) (wrap-rust-parens)))
               (more-args (l:mapcar (expression))))
    (make 'cpp-call-expression
          :cpp-function function
          :cpp-arguments
          (make 'cpp-argument-list
                :children (list* arg more-args)))
    (rust* "$VAL.$FIELD(@ARGS)"
           :val arg
           :field function
           :args more-args)))

(defun std-method-call ()
  (match-templates ((arg
                     (l:after (expression)
                              (wrap-rust-parens)))
                    (field (std-field))
                    (arguments (l:mapcar (expression))))
    (cpp* "$ARG.$FIELD(@ARGUMENTS)")
    (rust* "$ARG.$FIELD(@ARGUMENTS)")))

(defun static-cast ()
  (l:or
   (match-templates ((arg (l:after (expression) (wrap-rust-parens))))
     (cpp* "static_cast<std::string>($ARG)")
     (rust* "String::from($ARG)"))
   (match-templates ((type (type-lens))
                     (arg (l:after (expression)
                                   (wrap-rust-parens))))
     (cpp* "static_cast<$TYPE>($ARG)")
     (rust* "($ARG as $TYPE)"))))

(defun throw-statement ()
  "Translate errors."
  (match-templates ((arg (string-lit-term)))
    (cpp "throw std::logic_error($ARG);")
    (rust "panic!(@ARG);")))

(defun call ()
  (match-templates ((fn
                     (l:or (name-term :name-lens (camel/snake))
                           (expression :function t)))
                    (args (l:mapcar (expression))))
    (cpp* "$FN(@ARGS)")
    (rust* "$FN(@ARGS)")))

(defun args ()
  (match-asts ((args (l:mapcar (expression))))
    (make 'cpp-argument-list :children args)
    (make 'rust-arguments :children args)))

(defun parenthesized-expression ()
  (match-templates ((expr (expression)))
    (cpp* "($EXPR)")
    (rust* "($EXPR)")))

(defun cast ()
  (match-asts ((type (type-lens))
               (value (expression)))
    (make 'cpp-cast-expression
          :cpp-type type
          :cpp-value value)
    (make 'rust-type-cast-expression
          :rust-type type
          :rust-value value)))

(defun dereference ()
  (l:or
   (match-templates ((value (expression)))
     (cpp* "*$VALUE")
     (rust* "*$VALUE"))
   (translated-ast-lens
    :rust-type 'rust-unary-expression)))

(defun lvalue ()
  (l:before (unwrap-cpp-parens)
            (l:or (name-term)
                  (dereference))))

(defun preincrement-expression ()
  (match-templates ((var (lvalue)))
    (cpp* "++$VAR")
    (rust* "{$VAR += 1; $VAR}")

    (cpp* "--$VAR")
    (rust* "{$VAR -= 1; $VAR}")))

(defun postincrement-expression ()
  (match-templates ((var (lvalue)))
    (cpp* "$VAR++")
    (rust* "{let temp = $VAR; $VAR += 1; temp}")

    (cpp* "$VAR--")
    (rust* "{let temp = $VAR; $VAR -= 1; temp}")))

(defun new-lens ()
  (match-asts ((name (l:of-type 'string))
               (args (args)))
    (make 'cpp-new-expression
          :cpp-type (make 'cpp-type-identifier :text name)
          :cpp-arguments args)
    (make 'rust-call-expression
          :rust-arguments args
          :rust-function (make 'rust-scoped-identifier
                               :rust-name (rust* "new")
                               :rust-path (make 'rust-identifier
                                                :text name)))))

(defun subscript ()
  (match-templates ((arg (expression))
                    (idx (expression)))
    (cpp* "$ARG[$IDX]")
    (rust* "$ARG[$IDX]")))

(defun compound-literal-expression ()
  ;; Note we want we want a type identifier with a turbofish, but a
  ;; simple identifier otherwise.
  (l:or
   (match-templates ((type (l:after (type-lens)))
                     (values (l:mapcar (expression)))
                     (args (l:mapcar (type-lens)) nil))
     (cpp* "$TYPE<@ARGS>{@VALUES}")
     (rust* "$TYPE::<@ARGS>::new(@VALUES)"))
   (match-templates ((type (l:after (type-lens)
                                    (type-id-to-id)))
                     (values (l:mapcar (expression))))
     (cpp* "$TYPE{@VALUES}")
     ;; Why not a struct literal? You need to know the slot names.
     ;; \(This can be and is handled by top-down translation).
     (rust* "$TYPE::new(@VALUES)"))))

(defun pointer-expression ()
  (match-templates ((arg (expression)))
    (cpp* "&$ARG")
    (rust* "&$ARG")))

(defun conditional-expression ()
  (match-templates ((condition (expression))
                    (consequence (expression))
                    (alternative (expression)))
    (cpp* "$CONDITION ? $CONSEQUENCE : $ALTERNATIVE")
    (rust* "if $CONDITION { $CONSEQUENCE } else { $ALTERNATIVE }")))

(defun iterator-expr ()
  (l:after
   (l:match ((expr (l:optional (expression))))
     (cpp* "std::next($EXPR)" :expr expr)
     (rust* "$EXPR + 1" :expr expr)

     (cpp* "$EXPR.begin()" :expr expr)
     (rust* "0")

     (cpp* "$EXPR.cbegin()" :expr expr)
     (rust* "0")

     (cpp* "$EXPR.rbegin()" :expr expr)
     (rust* "$EXPR.len()" :expr expr)

     (cpp* "$EXPR.crbegin()" :expr expr)
     (rust* "$EXPR.len()" :expr expr)

     (cpp* "$EXPR.end()" :expr expr)
     (rust* "$EXPR.len()" :expr expr)

     (cpp* "$EXPR.cend()" :expr expr)
     (rust* "$EXPR.len()" :expr expr)

     (cpp* "$EXPR.rend()" :expr expr)
     (rust* "0")

     (cpp* "$EXPR.crend()" :expr expr)
     (rust* "0"))
   (constant-folding)))

(defun iterator-decl ()
  (match-templates ((var (name-term))
                    (expr (iterator-expr)))
    (cpp "$_ $VAR = $EXPR;")
    (rust "let mut $VAR = $EXPR;")

    (cpp "const $_ $VAR = $EXPR;")
    (rust "let $VAR = $EXPR;")

    (cpp "$_ const $VAR = $EXPR;")
    (rust "let $VAR = $EXPR;")))

(defun expression (&key function)
  (l:or (term :function function)
        (dereference)
        (unop)
        (binop)
        (field-expression)
        (ref)
        (static-cast)
        (throw-statement)
        (iterator-expr)
        (std-call-to-method-call)
        (std-method-call)
        (call)
        (assignment)
        (parenthesized-expression)
        (cast)
        (preincrement-expression)
        (postincrement-expression)
        (new-lens)
        (subscript)
        (compound-literal-expression)
        (pointer-expression)
        (conditional-expression)
        (translated-ast-lens :rust-type 'expression-ast)))


;;; Structures.

(defun flatten-struct-declarations ()
  "Flatten declarations to one declarator per field declaration.
Also introduce rust `pub' terminals for public members."
  (l:canonize-before
   (lambda (ast)
     (nest
      (when (typep ast 'cpp-field-declaration-list))
      (let ((private nil)))
      (labels ((publicize (ast)
                 "Insert a `pub` AST into AST."
                 (copy ast
                       :children
                       (cons
                        (make-rust-wrapper
                         (make 'rust-visibility-modifier))
                        (direct-children ast))))
               (maybe-publicize (ast)
                 (if private
                     ast
                     (publicize ast)))))
      (let ((new-decls
              (block failure
                (mappend (lambda (field-decl)
                           (match field-decl
                             ((cpp-field-declaration
                               :cpp-declarator (list (type cpp-field-identifier)))
                              (list
                               (if private
                                   field-decl
                                   (publicize (tree-copy field-decl)))))
                             ((cpp-field-declaration
                               :cpp-declarator (and children (type list)))
                              (unless (every (of-type 'cpp-field-identifier)
                                             children)
                                (fail))
                              (mapcar (lambda (child)
                                        (maybe-publicize
                                         (copy (tree-copy field-decl)
                                               :cpp-declarator
                                               (list child))))
                                      children))
                             ((cpp-access-specifier
                               :cpp-keyword (cpp-private))
                              (setf private t)
                              nil)
                             ((cpp-access-specifier
                               :cpp-keyword (cpp-public))
                              (setf private nil)
                              nil)
                             (otherwise
                              (return-from failure))))
                         (children ast))))))
      (if (or (null new-decls)
              (equal new-decls (children ast)))
          ast
          (copy ast :children new-decls))))))

(defun field-declaration ()
  (match-asts ((type (type-lens))
               (name (name-term))
               (children (l:mapcar (rust-lens))))
    (make 'cpp-field-declaration
          :children children
          :cpp-type type
          :cpp-declarator (list name))
    (make 'rust-field-declaration
          :children children
          :rust-name name
          :rust-type type)))

(defun field-declaration-list ()
  (l:after
   (l:before
    (flatten-struct-declarations)
    (match-asts ((decls (l:mapcar (field-declaration))))
      (make 'cpp-field-declaration-list :children decls)
      (make 'rust-field-declaration-list :children decls)))
   (ensure-before-space)))

(defun struct-specifier ()
  (match-asts ((name (type-lens))
               ;; export/pub
               (children (l:mapcar (term)))
               (field-declaration-list (field-declaration-list)))
    (make 'cpp-struct-specifier
          :children children
          :cpp-name name
          :cpp-body field-declaration-list)
    (make 'rust-struct-item
          :children children
          :rust-name name
          :rust-body field-declaration-list)))

(defun enum-variant ()
  (match-asts ((name (name-term))
               ;; TODO constexpr?
               (value (l:optional (expression))))
    (make 'cpp-enumerator
          :cpp-name name
          :cpp-value value)
    (make 'rust-enum-variant
          :rust-name name
          :rust-value value)))

(defun enum-specifier ()
  (match-templates ((name (type-identifier :name-lens (l:opp (camel/snake))))
                    (variants (l:mapcar (enum-variant))))
    (cpp* "enum class $NAME { @VARIANTS }")
    (rust* "enum $NAME { @VARIANTS }")

    (cpp* "enum struct $NAME { @VARIANTS }")
    (rust* "enum $NAME { @VARIANTS }")

    (cpp* "enum $NAME { @VARIANTS }")
    (rust* "enum $NAME { @VARIANTS }")))


;;; Statements.

(defun expression-statement ()
  (match-asts ((child (expression)))
    (make 'cpp-expression-statement
          :children (list child))
    (make 'rust-expression-statement
          :children (list child))))

(defun return-statement ()
  (l:or (match-templates ((expr (expression)))
          (cpp "return $EXPR;")
          (rust "return $EXPR;"))
        (match-templates ()
          (cpp "return;")
          (rust "return;"))))

(defun primitive-type-name ()
  ;; See https://doc.rust-lang.org/std/os/raw/.
  (l:match-constructors ()
    "double" "f64"
    "float" "f32"
    "int" "i32"
    "short" "i16"
    "bool" "bool"
    ;; Right? https://github.com/rust-lang/rust-bindgen/issues/1671
    "size_t" "usize"
    ;; See https://en.cppreference.com/w/cpp/types/integer
    "int8_t" "i8"                       ;?
    "int16_t" "i16"
    "int32_t" "i32"
    "int64_t" "i64"
    "uint8_t"  "u8"                       ;?
    "uint16_t" "u16"
    "uint32_t" "u32"
    "uint64_t" "u64"))

(defun void ()
  (l:match ()
    (cpp* "void")
    (make 'rust-unit-type)))

(defun primitive-type ()
  (l:or (void)
        (match-asts ((name (primitive-type-name)))
          (make 'cpp-primitive-type :text name)
          (make 'rust-primitive-type :text name))))

(defun type-identifier (&key (name-lens (l:of-type 'string)))
  (match-asts ((name name-lens))
    (make 'cpp-type-identifier :text name)
    (make 'rust-type-identifier :text name)))

(defun sized-type ()
  (l:match ()
    ;; See https://doc.rust-lang.org/std/os/raw/.
    ;; (make 'cpp-sized-type-specifier
    ;;       :cpp-modifiers (list (make 'cpp-unsigned :text "unsigned"))
    ;;       :cpp-type (make 'cpp-primitive-type :text "int"))
    ;; (make 'rust-primitive-type :text "u32")

    (scaffold (cpp "{{short}} var"))
    (make 'rust-primitive-type :text "i16")

    (scaffold (cpp "{{short int}} var"))
    (make 'rust-primitive-type :text "i16")

    (scaffold (cpp "{{signed short}} var"))
    (make 'rust-primitive-type :text "i16")

    (scaffold (cpp "{{signed short int}} var"))
    (make 'rust-primitive-type :text "i16")

    (scaffold (cpp "{{unsigned short}} var"))
    (make 'rust-primitive-type :text "u16")

    (scaffold (cpp "{{unsigned short int}} var"))
    (make 'rust-primitive-type :text "u16")

    (scaffold (cpp "{{int}} var"))
    (make 'rust-primitive-type :text "i32")

    (scaffold (cpp "{{signed}} var"))
    (make 'rust-primitive-type :text "i32")

    (scaffold (cpp "{{signed int}} var"))
    (make 'rust-primitive-type :text "i32")

    (scaffold (cpp "{{unsigned}} var"))
    (make 'rust-primitive-type :text "u32")

    (scaffold (cpp "{{unsigned int}} var"))
    (make 'rust-primitive-type :text "u32")

    ;; Assuming Linux and LP64.

    (scaffold (cpp "{{long}} var"))
    (make 'rust-primitive-type :text "i64")

    (scaffold (cpp "{{long int}} var"))
    (make 'rust-primitive-type :text "i64")

    (scaffold (cpp "{{signed long}} var"))
    (make 'rust-primitive-type :text "i64")

    (scaffold (cpp "{{signed long int}} var"))
    (make 'rust-primitive-type :text "i64")

    (scaffold (cpp "{{unsigned long}} var"))
    (make 'rust-primitive-type :text "u64")

    (scaffold (cpp "{{unsigned long int}} var"))
    (make 'rust-primitive-type :text "u64")

    (scaffold (cpp "{{long long}} var"))
    (make 'rust-primitive-type :text "i64")

    (scaffold (cpp "{{long long int}} var"))
    (make 'rust-primitive-type :text "i64")

    (scaffold (cpp "{{signed long long}} var"))
    (make 'rust-primitive-type :text "i64")

    (scaffold (cpp "{{signed long long int}} var"))
    (make 'rust-primitive-type :text "i64")

    (scaffold (cpp "{{unsigned long long}} var"))
    (make 'rust-primitive-type :text "u64")

    (scaffold (cpp "{{unsigned long long int}} var"))
    (make 'rust-primitive-type :text "u64")))

(defun type-descriptor ()
  "Translate a trivial type descriptor."
  (l:match ((type (type-lens)))
    (make 'cpp-type-descriptor
          :cpp-type type
          :cpp-pre-type-qualifiers nil
          :cpp-post-type-qualifiers nil)
    type))

(defun std-type ()
  (match-asts ((name (std-name)))
    (make 'cpp-qualified-identifier
          :cpp-scope (make 'cpp-namespace-identifier :text "std")
          :cpp-name (make 'cpp-type-identifier :text name))
    (make 'rust-type-identifier :text name)))

(defun collection-name ()
  (l:match ()
    "vector" "Vec"
    "deque" "VecDeque"
    "list" "LinkedList"))

(defun collection-type ()
  (match-asts ((collection-name (collection-name))
               (element-type (type-lens)))
    (make 'cpp-qualified-identifier
          :cpp-scope (make 'cpp-namespace-identifier :text "std")
          :cpp-name (make 'cpp-template-type
                          :cpp-name
                          (make 'cpp-type-identifier :text collection-name)
                          :cpp-arguments
                          (make 'cpp-template-argument-list
                                :children (list element-type))))
    (make 'rust-generic-type
          :rust-type
          (make 'rust-type-identifier
                :text collection-name)
          :rust-type-arguments
          (make 'rust-type-arguments
                :children
                (list element-type)))))

(defun auto-lens ()
  (l:match ()
    (make 'cpp-placeholder-type-specifier
          :cpp-constraint nil
          :children (list (make 'cpp-auto :text "auto")))
    (list)))

(defun template-type ()
  (match-asts ((type (type-lens))
               (args (template-args)))
    (make 'cpp-template-type
          :cpp-name type
          :cpp-arguments args)
    (make 'rust-generic-type
          :rust-type type
          :rust-type-arguments args)))

(defun template-type/turbofish ()
  (match-asts ((type (type-lens))
               (args (template-args)))
    (make 'cpp-template-type
          :cpp-name type
          :cpp-arguments args)
    (make 'rust-generic-type-with-turbofish
          :rust-type type
          :rust-type-arguments args)))

(Defun template-args ()
  (match-asts ((args (l:mapcar (type-lens))))
    (make 'cpp-template-argument-list
          :children args)
    (make 'rust-type-arguments
          :children args)))

(defun type-lens ()
  (l:or (collection-type)
        (std-type)
        (primitive-type)
        (sized-type)
        (type-descriptor)
        (type-identifier)
        (qname-term :want-type t)
        (auto-lens)
        (template-type)
        (translated-ast-lens :rust-type 'type-ast)))

(defun const-lens ()
  (l:match ()
    (list)
    (make 'rust-mutable-specifier :text "mut")

    (list (make 'cpp-type-qualifier :text "const"))
    (list)))

;;; TODO Can let-decl-lens and let-init-lens be merged? Would need to
;;; extent lenses to allow elements to be omitted in the forward
;;; direction.

(defun maybe-add-initializer ()
  "Canonizer to synthesize an appropriate initializer on the Rust side."
  (l:canonize-after
   (lambda (ast)
     (match (rust-type ast)
       ;; Don't add an initializer for primitive types.
       ((type rust-primitive-type) ast)
       ((and type (type rust-type-identifier))
        (copy ast :rust-value (rust-type-initializer type)))
       ((make 'rust-generic-type :rust-type type)
        (copy ast :rust-value (rust-type-initializer type)))
       (otherwise ast)))))

(defun let-decl-lens ()
  "Lenses for bare declarations (no initializers)."
  (l:after
   (match-asts ((name
                 (l:after (name-term)
                          (drop-after-text)))
                (type
                 (l:after (type-lens)
                          (drop-before-text)))
                (mut (const-lens)))
     (make 'cpp-declaration
           :cpp-pre-specifiers mut
           :cpp-type type
           :cpp-declarator (list name))
     (make 'rust-let-declaration
           :rust-pattern name
           :rust-type type
           :rust-mutable-specifier mut))
   (maybe-add-initializer)))

(defun maybe-add-init-cast ()
  "Canonizer to synthesize an appropriate cast to an initializer."
  (l:canonize-after
   (lambda (ast)
     (match ast
       ((rust-ast
         (rust-type
          (or nil
              (make 'rust-type-identifier :text "String")))
         (rust-value
          ;; TODO All types?
          (and value (type rust-string-literal))))
        (copy ast
              :rust-type nil
              :rust-value (rust* "String::from($1)" value)))
       (otherwise ast)))))

(defun let-init-lens ()
  (l:after
   (match-asts ((name
                 (l:after (name-term)
                          (drop-after-text)))
                (type
                 (l:after (type-lens)
                          (drop-before-text)))
                ;; Rust prints a warning if the RHS of an
                ;; assignment/declaration has parens.
                (value (l:after (expression)
                                (unwrap-rust-parens)))
                (mut (const-lens)))
     (make 'cpp-declaration
           :cpp-pre-specifiers mut
           :cpp-type type
           :cpp-declarator
           (list
            (make 'cpp-init-declarator
                  :cpp-declarator name
                  :cpp-value value)))
     (make 'rust-let-declaration
           :rust-pattern name
           :rust-type type
           :rust-value value
           :rust-mutable-specifier mut))
   (maybe-add-init-cast)))

(defun normalize-mut ()
  (l:canonize-after
   (lambda (ast)
     (ematch ast
       ((rust-let-declaration
         (rust-type
          (rust-reference-type
           (rust-mutable-specifier nil)))
         (rust-value
          (rust-reference-expression
           (rust-mutable-specifier (rust-mutable-specifier)))))
        (copy ast
              :rust-type
              (copy (rust-type ast)
                    :rust-mutable-specifier
                    (make 'rust-mutable-specifier :text "mut"))))
       (otherwise ast)))))

(defun let-reference-lens ()
  (l:after
   (match-asts ((name
                 (l:after (name-term)
                          (drop-after-text)))
                (type
                 (l:after (type-lens)
                          (drop-before-text)))
                (value
                 (l:after (expression)
                          (drop-before-text)))
                (mut (const-lens)))
     (make 'cpp-declaration
           :cpp-pre-specifiers mut
           :cpp-type type
           :cpp-declarator
           (list
            (make 'cpp-init-declarator
                  :cpp-declarator
                  (make 'cpp-reference-declarator
                        :cpp-valueness (make 'cpp-& :text "&")
                        ;; TODO Name slot?
                        :children (list name))
                  :cpp-value value)))
     (make 'rust-let-declaration
           :rust-mutable-specifier nil
           :rust-pattern name
           :rust-type (make 'rust-reference-type
                            :rust-type type)
           :rust-value (make 'rust-reference-expression
                             :rust-mutable-specifier mut
                             :rust-value value)))
   (normalize-mut)))

(defun let-auto-lens ()
  (match-templates ((name
                     (l:after (name-term)
                              (drop-after-text)))
                    (value
                     (l:after (expression)
                              (drop-before-text))))
    (cpp* "const auto $NAME = $VALUE")
    (rust* "let $NAME = $VALUE")

    (cpp* "auto $NAME = $VALUE")
    (rust* "let mut $NAME = $VALUE")

    (cpp* "const auto& $NAME = $VALUE")
    (rust* "let &$NAME = $VALUE")

    (cpp* "auto& $NAME = $VALUE")
    (rust* "let &mut $NAME = $VALUE")))

(defun let-lens ()
  (l:or
   (let-auto-lens)
   (let-reference-lens)
   (let-init-lens)
   (let-decl-lens)))

(defun constexpr-var-lens ()
  (match-templates ((name
                     (l:after (name-term)
                              (drop-after-text)))
                    (type
                     (l:after (type-lens)
                              (drop-before-text)))
                    (value
                     (l:after (expression)
                              (drop-before-text))))
    (cpp* "constexpr $TYPE $NAME = $VALUE")
    (rust* "const $NAME:$TYPE = $VALUE")))

(defun basic-assignment ()
  (match-templates ((lhs (rust-lens))
                    (rhs (expression)))
    (cpp* "$LHS = $RHS")
    (rust* "$LHS = $RHS")))

(defun augmented-assignment ()
  (with-natural-ast-match ((operator (op-term))
                           (lhs (rust-lens))
                           (rhs (expression)))
    (make 'cpp-assignment-expression)
    (make 'rust-compound-assignment-expr)))

(defun assignment ()
  (l:or (basic-assignment)
        (augmented-assignment)))

(defun block-lens ()
  (l:or
   (match-asts ((children (l:mapcar (statement))))
     (make 'cpp-compound-statement :children children)
     (make 'rust-block :children children))
   (translated-ast-lens :rust-type 'rust-block)))

(defun if-else ()
  (match-asts ((condition (expression))
               (consequence (l:after (rust-lens) (ensure-block)))
               (alternative (l:after (rust-lens) (ensure-block))))
    (make 'cpp-if-statement
          :condition (make 'cpp-condition-clause
                           :cpp-value condition)
          :consequence consequence
          :alternative alternative)
    (make 'rust-expression-statement
          :children (list
                     (make 'rust-if-expression
                           :condition condition
                           :consequence consequence
                           :alternative
                           (make 'rust-else-clause
                                 :children (list alternative)))))))

(defun if-then ()
  (match-asts ((condition (expression))
               (consequence (l:after (rust-lens)
                                     (ensure-block)))
               (alternative (l:of-type 'null)))
    (make 'cpp-if-statement
          :condition (make 'cpp-condition-clause
                           :cpp-value condition)
          :consequence consequence
          :alternative alternative)
    (make 'rust-expression-statement
          :children (list
                     (make 'rust-if-expression
                           :condition condition
                           :consequence consequence)))))

(defun ensure-block ()
  "Canonizer to ensure a Rust block."
  (l:canonize-after
   (lambda (c)
     (and (typep c 'rust-ast)
          (not (typep c 'rust-block))
          (make 'rust-block
                :before-text (before-text c)
                :after-text (after-text c)
                :children
                (list (strip-surrounding-text c)))))))

(defun ensure-statement ()
  (l:canonize-after
   (lambda (c)
     (typecase c
       (statement-ast c)
       (ast:ast
        (make 'rust-expression-statement
              :children (list c)))))))

(defun if-lens ()
  (l:or (if-else)
        (if-then)))

(defun params ()
  (l:or
   (match-asts ((params (l:mapcar (param))))
     (make 'cpp-parameter-list :children params)
     (make 'rust-parameters :children params))
   (translated-ast-lens :rust-type 'rust-parameters)))

(defun template-params ()
  (l:match ((params (l:mapcar (template-param))))
    (make 'cpp-template-parameter-list :children params)
    (make 'rust-type-parameters :children params)))

(defun template-param ()
  (l:or (param)
        (match-asts ((type (type-lens)))
          (make 'cpp-type-parameter-declaration
                :children (list type))
          type)))

(defun cpp-gensym ()
  (make 'cpp-identifier :text (symbol-name (gensym))))

(defun ensure-parameter-name ()
  (l:canonize-before
   (lambda (ast)
     (when (typep ast 'cpp-parameter-declaration)
       (match ast
         ((cpp-parameter-declaration
           (cpp-declarator nil))
          (copy ast
                :cpp-declarator
                (cpp-gensym)))
         ((cpp-parameter-declaration
           (cpp-declarator
            (cpp-abstract-reference-declarator)))
          (copy ast
                :cpp-declarator
                (make 'cpp-reference-declarator
                      :cpp-valueness (make 'cpp-&)
                      :children (list (cpp-gensym))))))))))

(defun value-param ()
  (l:match ((type (type-lens))
            (name (l:after (name-term)
                           (drop-before-text)))
            (mut (const-lens)))
    (make 'cpp-parameter-declaration
          :cpp-pre-specifiers mut
          :cpp-type type
          :cpp-declarator name)
    (make 'rust-parameter
          :rust-mutable-specifier mut
          :rust-pattern name
          :rust-type type)))

(defun reference-param ()
  (l:match ((type (l:after (type-lens)
                           (drop-before-text)))
            (name (l:after (name-term)
                           (drop-before-text)))
            (mut (const-lens)))
    (make 'cpp-parameter-declaration
          :cpp-pre-specifiers mut
          :cpp-type type
          :cpp-declarator
          (make 'cpp-reference-declarator
                :cpp-valueness (make 'cpp-&)
                :children (list name)))
    (make 'rust-parameter
          :rust-pattern name
          :rust-type
          (make 'rust-reference-type
                :rust-mutable-specifier mut
                :rust-type type))))

(defun param ()
  (l:or
   (l:before
    (ensure-parameter-name)
    (l:or (reference-param)
          (value-param)))
   (translated-ast-lens :rust-type '(or rust-parameter rust-self-parameter))))

(defun implicit-return-lens ()
  (match-asts ((children (l:identity)))
    (make 'rust-expression-statement
          :children
          (list (make 'rust-return-expression :children children)))
    (make 'rust-implicit-return-expression :children children)))

(defun fixup-return ()
  "Canonizer that replaces a return statement at the end of a block
with an implicit return."
  (l:canonize-after
   (lambda (block)
     (match (last (children block))
       ((list (and ret
                   (rust-expression-statement
                    (children (list (rust-return-expression))))))
        (copy block
              :children (append1 (butlast (children block))
                                 (l:forward (implicit-return-lens)
                                            ret))))))))

(defun cleanup-name ()
  "Make sure the name of a C++ function is a cpp-identifier.
It can be parsed as a field identifier in methods."
  (l:canonize-before
   (lambda (fn)
     (match fn
       ((cpp-function-definition
         :cpp-declarator
         (or
          (cpp-reference-declarator
           :children
           (list
            (cpp-function-declarator
             :cpp-declarator (and id (cpp-field-identifier)))))
          (cpp-function-declarator
           :cpp-declarator (and id (cpp-field-identifier)))))
        (with fn id (change-class (copy id) 'cpp-identifier)))))))

(defun fixup-function-name ()
  (l:canonize-after
   (lambda (fn)
     (match fn
       ((rust-function-item
         :rust-name (and name (rust-field-identifier)))
        (copy fn
              :name (make 'rust-identifier :text (sel:text name))))))))

(defun function-name-term ()
  (name-term :name-lens (camel/snake)))

(defun function-declaration ()
  (l:before
   (cleanup-name)
   (match-asts ((name (function-name-term))
                (params (l:after (params)
                                 (drop-around-text)))
                (type (l:after (l:optional (type-lens))
                               (drop-before-text))))
     (make 'cpp-declaration
           :cpp-type type
           :cpp-value nil
           :cpp-declarator
           (list
            (make 'cpp-function-declarator
                  :cpp-declarator name
                  :cpp-parameters params)))
     (make 'rust-function-signature-item
           :rust-name name
           :rust-parameters params
           :rust-return-type type))))

(defun function-item ()
  (l:before
   (cleanup-name)
   (match-asts ((name (function-name-term))
                (params (l:after (params)
                                 (drop-around-text)))
                (body (l:after (block-lens)
                               (fixup-return)))
                (type (l:after (l:optional (type-lens))
                               (drop-before-text)))
                (children (l:mapcar (export-term)))
                (mut (const-lens) nil))
     (make 'cpp-function-definition
           :children children
           :cpp-type type
           :cpp-declarator
           ;; TODO before-text, after-text
           (make 'cpp-function-declarator
                 :cpp-declarator name
                 :cpp-parameters params)
           :cpp-body body)
     (make 'rust-function-item
           :children children
           :rust-name name
           :rust-parameters params
           :rust-body body
           :rust-return-type type)

     (make 'cpp-function-definition
           :cpp-pre-specifiers mut
           :children children
           :cpp-type type
           :cpp-declarator
           (make 'cpp-reference-declarator
                 :children
                 (list
                  (make 'cpp-function-declarator
                        :cpp-declarator name
                        :cpp-parameters params)))
           :cpp-body body)
     (make 'rust-function-item
           :children children
           :rust-name name
           :rust-parameters params
           :rust-body body
           :rust-return-type
           (make 'rust-reference-type
                 :rust-mutable-specifier mut
                 :rust-type type)))))

(defun preincrement-statement ()
  (match-templates ((var (lvalue)))
    (cpp "++$VAR;")
    (rust* "$VAR += 1;")

    (cpp "--$VAR;")
    (rust* "$VAR -= 1;")))

(defun postincrement-statement ()
  (match-templates ((var (lvalue)))
    (cpp "$VAR++;")
    (rust* "$VAR += 1;")

    (cpp "$VAR--;")
    (rust "$VAR -= 1;")))

(defun fixup-impl-item-lens ()
  "Make sure functions in impl items have a &self parameter."
  (l:canonize-after
   (lambda (ast)
     (fixup-impl-item ast))))

(defun break/continue ()
  (match-templates ()
    (cpp* "break;")
    (rust* "break;")

    (cpp* "continue;")
    (rust* "continue;")))

(defun while-statement ()
  (l:after
   (l:or
    (match-templates ((body (l:mapcar (statement))))
      (cpp* "while (1) { @BODY }")
      (rust* "loop { @BODY }")

      (cpp* "while (true) { @BODY }")
      (rust* "loop { @BODY }"))
    (match-templates ((test (expression))
                      (body (l:mapcar (statement))))
      (cpp* "while ($TEST) { @BODY }")
      (rust* "while $TEST { @BODY }")))
   (ensure-statement)))

(defun infinite-for-loop ()
  "Translate `for(;;) {}' into `loop {}'."
  (l:after
   (match-templates ((body (l:mapcar (statement))))
     (cpp* "for (;;) { @BODY }")
     (rust* "loop { @BODY }"))
   (ensure-statement)))

(defun infinite-do-while-loop ()
  "Translate infinite do-while loops into `loop'."
  (l:after
   (match-templates ((body (l:mapcar (statement))))
     (cpp* "do { @BODY } while (1)")
     (rust* "loop { @BODY }")

     (cpp* "do { @BODY } while (true)")
     (rust* "loop { @BODY }"))
   (ensure-statement)))

(defun for-range-loop ()
  (l:after
   ;; TODO What about for loops with an explicit type? No Rust
   ;; equivalent.
   (match-templates ((name (name-term))
                     (expr (expression))
                     (body (l:mapcar (statement))))
     (cpp* "for (auto $NAME : $EXPR) { @BODY }")
     (rust* "for $NAME in $EXPR.iter() { @BODY }"))
   (ensure-statement)))

(defun namespace/mod ()
  ;; TODO This can result in things being shoved into internal ASTs.
  ;; (match-templates ((name (name-term))
  ;;                   (items (l:mapcar (rust-lens))))
  ;;   (cpp* "namespace $NAME { @ITEMS }")
  ;;   (rust* "pub mod $NAME { @ITEMS }"))
  (l:or
   ;; Eliminate empty namespace forms to prevent ambiguous names.
   (l:match ((name (name-term)))
     (cpp* "namespace $NAME { @BODY }" :name name :body nil)
     (make 'ast-fragment)

     (cpp* "export namespace $NAME { @BODY }" :name name :body nil)
     (make 'ast-fragment))
   (match-asts ((name (name-term))
                (items (l:mapcar (rust-lens))))
     (make 'cpp-namespace-definition
           :cpp-name name
           :cpp-body
           (make 'cpp-declaration-list
                 :children items))
     (make 'rust-mod-item
           :children (list (make 'rust-visibility-modifier))
           :rust-name name
           :rust-body (make 'rust-declaration-list :children items)))))

(defun export-namespace/mod ()
  (match-asts ((name (name-term))
               (items (l:mapcar (l:after (rust-lens) (publicize-definitions)))))
    (make 'cpp-namespace-definition
          :children (list (make 'cpp-export-specifier))
          :cpp-name name
          :cpp-body
          (make 'cpp-declaration-list
                :children items))
    (make 'rust-mod-item
          :children (list (make 'rust-visibility-modifier))
          :rust-name name
          :rust-body (make 'rust-declaration-list :children items))))

(defun preproc-const ()
  (match-asts ((name (name-term))
               (value
                (l:bij (op (values (parse-integer _ :junk-allowed t)))
                       #'princ)))
    (make 'cpp-preproc-def
          :cpp-name name
          :cpp-value (make 'cpp-preproc-arg :text value))
    ;; TODO Should be const. Analyze the minimum type for the value.
    (rust "let $NAME = $VALUE;" :name name :value value)))

(defun using-all-declaration ()
  (match-templates ((name (qname-term)))
    (cpp* "using namespace $NAME")
    (rust* "use $NAME::*")))

(defun pointer-decl ()
  "Translate a pointer declaration.
Note pointers can be declared in safe code, they just can't be
dereferenced."
  (l:or (const-pointer-to-const-decl)
        (const-pointer-to-mut-decl)
        (mut-pointer-to-const-decl)
        (mut-pointer-to-mut-decl)))

(defun const-pointer-to-const-decl ()
  "Const pointer to const."
  (match-templates ((name (name-term))
                    (type (type-lens))
                    (expr (expression)))
    ;; West const.
    (cpp* "const $TYPE * const $NAME = &$EXPR")
    (rust* "let $NAME = &$EXPR as *const $TYPE")
    ;; East const.
    (cpp* "$TYPE const * const $NAME = &$EXPR")
    (rust* "let $NAME = &$EXPR as *const $TYPE")))

(defun const-pointer-to-mut-decl ()
  "Const pointer to mut."
  (match-templates ((name (name-term))
                    (type (type-lens))
                    (expr (expression)))
    (cpp* "$TYPE * const $NAME = &$EXPR")
    (rust* "let $NAME = &mut $EXPR as *mut $TYPE")))

(defun mut-pointer-to-const-decl ()
  (match-templates ((name (name-term))
                    (type (type-lens))
                    (expr (expression)))
    ;; West const.
    (cpp* "const $TYPE * $NAME = &$EXPR")
    (rust* "let mut $NAME = &$EXPR as *const $TYPE")
    ;; East const.
    (cpp* "$TYPE const * $NAME = &$EXPR")
    (rust* "let mut $NAME = &$EXPR as *const $TYPE")))

(defun mut-pointer-to-mut-decl ()
  (match-templates ((name (name-term))
                    (type (type-lens))
                    (expr (expression)))
    ;; West const.
    (cpp* "$TYPE * $NAME = &$EXPR")
    (rust* "let mut $NAME = &mut $EXPR as *mut $TYPE")
    ;; East const.
    (cpp* "$TYPE * $NAME = &$EXPR")
    (rust* "let mut $NAME = &mut $EXPR as *mut $TYPE")))

(defun module-import ()
  (l:after
   (l:match ((module (name-term)))
     (cpp "import $MODULE;" :module module)
     (rust "#[allow(unused_imports)]
use $MODULE::*;"
           :module module)

     (cpp "import :$MODULE;" :module module)
     ;; `pub use' doesn't match semantically, but we can't assume
     ;; interface/implementation distinctions in Rust.
     (rust "#[allow(unused_imports)]
pub use $MODULE::*;"
           :module module))
   (fragmentize)))

(defun module-reexport ()
  (l:match ((module (name-term)))
    (cpp "export import $MODULE;" :module module)
    #1=(rust "pub use $MODULE::*;" :module module)
    (cpp "export import :$MODULE;" :module module)
    #1#))

(defun header-unit-import ()
  (l:match ()
    (make 'cpp-import-declaration
          :cpp-name (make 'cpp-system-lib-string))
    (make 'ast-fragment)))

(defun module-declaration ()
  ;; Delete module declarations.
  (l:match ()
    (make 'cpp-module-declaration)
    (make 'ast-fragment)

    (make 'cpp-module-fragment-declaration)
    (make 'ast-fragment)))

(defun undeclared-import ()
  "Rewrite an undeclared import to be a relative or absolute import.
This lens is meant to be called directly, not as part of the bottom-up
translation."
  (match-templates ((name (name-term)))
    (cpp* "export import :$NAME")
    (rust* "pub use super::$NAME::*")

    (cpp* "import :$NAME")
    ;; No declarations in Rust.
    (rust* "pub use super::$NAME::*")

    (cpp* "export import $NAME")
    (rust* "pub use crate::$NAME::*")

    (cpp* "import $NAME")
    (rust* "use crate::$NAME::*")))

(defun module-statement ()
  (l:or (module-reexport)
        (module-import)
        (module-declaration)
        (header-unit-import)))

(defun publicize-definitions ()
  (l:canonize-after
   (lambda (ast)
     (if (typep ast 'definition-ast)
         (if (find-if (of-type 'rust-visibility-modifier)
                      (children ast))
             ast
             (copy ast
                   :children
                   (cons
                    (make 'rust-visibility-modifier)
                    (direct-children ast))))
         ast))))

(defun export-block ()
  (l:match ((children (l:mapcar
                       (l:after
                        (rust-lens)
                        (publicize-definitions)))))
    (cpp* "export { @CHILDREN }" :children children)
    (make 'ast-fragment :children children)))

(defun statement ()
  (l:or (preincrement-statement)
        (postincrement-statement)
        (break/continue)
        (iterator-decl)
        (expression-statement)
        (let-lens)
        (constexpr-var-lens)
        (return-statement)
        (function-item)
        (function-declaration)
        (pointer-decl)
        (block-lens)
        (if-lens)
        (while-statement)
        (infinite-for-loop)
        (infinite-do-while-loop)
        (for-range-loop)
        (struct-specifier)
        (enum-specifier)
        (namespace/mod)
        (export-namespace/mod)
        (preproc-const)
        (using-all-declaration)
        (module-statement)
        (export-block)
        (l:after
         (translated-ast-lens
          :rust-type '(or
                       statement-ast
                       declaration-ast))
         (fixup-impl-item-lens))))


;;; Files.

(defun rust-file ()
  (match-asts ((children (l:mapcar (statement))))
    (make 'cpp-translation-unit :children children)
    (make 'rust-source-file :children children)))


;;; Utilities

(defun prettify ()
  (l:canonize-after
   (lambda (x)
     (patch-whitespace x))))

(defun drop-after-text ()
  (l:canonize-after
   (lambda (x)
     (if x
         (copy x :after-text "")
         (values x t)))))

(defun drop-before-text ()
  (l:canonize-after
   (lambda (x)
     (if x (copy x :before-text "")
         (values x t)))))

(defun drop-around-text ()
  (l:compose (drop-before-text)
             (drop-after-text)))

(Defun ensure-before-space ()
  (l:canonize-after
   (lambda (x)
     (and x
          (emptyp (before-text x))
          (copy x :before-text " ")))))

(defun fragmentize ()
  "Canonizer to convert an AST to a fragment with the same children.
This can be used to write \"lenses\" (obviously not bidirectional)
that translate from one AST to multiple ASTs."
  (l:canonize-after
   (lambda (x)
     (make 'ast-fragment :children (children x)))))


;;; Final lens

(defun rust-lens-raw ()
  (l:or (rust-file) (statement) (expression)
        ;; For partial translation.
        (params) (template-params) (param) (template-param) (type-lens)
        (translated-ast-lens)))

(defun rust-lens ()
  (l:after (rust-lens-raw)
           (prettify)))
