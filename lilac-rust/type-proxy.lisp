(uiop:define-package :lilac-rust/type-proxy
  (:documentation "Reproduce C++ and Rust type hierarchies using Lisp
  classes as proxies.")
  (:use :gt/full
        :software-evolution-library/software/rust
        :software-evolution-library/software/cpp)
  (:local-nicknames (:it :iterate)
                    (:ts :software-evolution-library/software/tree-sitter)
                    (:ast :software-evolution-library/software/parseable)
                    (:abstract :org.tfeb.hax.abstract-classes)
                    (:l :fresnel/lens)
                    (:rust :lilac-rust/rust-common)
                    (:rust-lens :lilac-rust/rust-lens)
                    (:sel :software-evolution-library)
                    (:choice :argot-server/choices)
                    (:alt :lilac-rust/translated-ast))
  (:shadowing-import-from :trivia.fail :fail)
  (:shadow :type)
  (:export
    :type-proxy
    :translate-method-call))
(in-package :lilac-rust/type-proxy)

;;; TODO Rust types are simple enough we should have no problem
;;; modeling them. C++ types may be harder since C++ and CL use
;;; different superclass linearization algorithms. However a C++
;;; compatible ordering should still be possible.

(defclass type ()
  ()
  (:metaclass abstract:abstract-class))

(defclass c-type (type)
  ()
  (:metaclass abstract:abstract-class))


;;; Rust types

(defclass rust-type (type)
  ()
  (:metaclass abstract:abstract-class))

(defclass trait (rust-type)
  ()
  (:metaclass abstract:abstract-class))

(defgeneric trait-methods (obj)
  (:method-combination append)
  (:method ((obj t)) nil))

(eval-always
  (defun intern* (x)
    (intern (string x) :lilac-rust/type-proxy))

  (defun parse-trait-definition (string)
    (let* ((rust (sel:genome (sel:from-string 'ts:rust string)))
           (trait (assure ast:ast
                    (find-if (of-type 'ts:rust-trait-item) rust))))
      (values (ast:source-text (ts:rust-name trait))
              (mapcar (op (ast:source-text (ts:rust-name _)))
                      (ast:collect-if (of-type
                                       '(or ts:rust-function-item
                                         ts:rust-function-signature-item))
                                      trait))))))

(defmacro define-trait (string)
  (multiple-value-bind (trait-name method-names)
      (parse-trait-definition string)
    (let ((trait-name (intern* trait-name))
          (method-names (mapcar #'intern* method-names)))
      `(progn
         (export-always '(,trait-name ,@method-names))
         (defclass ,trait-name (trait)
           ()
           (:metaclass abstract:abstract-class))
         (defmethod trait-methods append ((obj ,trait-name))
           ',method-names)))))

(define-trait "pub trait Clone {
    fn clone(&self) -> Self;

    fn clone_from(&mut self, source: &Self) { ... }
}")

(define-trait |Index|)

(defmethod trait-methods append ((obj |Index|))
  '|index|)

(define-trait |Default|)

(defmethod trait-methods append ((obj |Clone|)))

(define-trait |Clone|)

(defclass |Vec| (|Index| |Default| |Clone|)
  ())
