(defpackage :lilac-rust/valhalla
  (:use :gt/full
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/project
        :lilac-commons/analysis
        :cmd)
  (:import-from :lilac-cpp/refactorings
                :*refactoring-pipeline*)
  (:import-from :lilac-rust/test
                :translate-string)
  (:import-from :lilac-commons/commons
                :project+file)
  (:local-nicknames
   (:analysis :lilac-commons/analysis)
   (:ast :software-evolution-library/software/parseable)
   (:attrs :functional-trees/attrs)
   (:cpp :software-evolution-library/software/cpp-project)
   (:dbg :software-evolution-library/utility/debug)
   (:dir :software-evolution-library/software/directory)
   (:l :fresnel/lens)
   (:project :software-evolution-library/software/project)
   (:refactor :lilac-cpp/refactorings)
   (:rust :lilac-rust/rust-lens)
   (:rustran :lilac-rust/rust-translation)
   (:sel :software-evolution-library)
   (:htree :lilac-rust/translated-ast)
   (:traversal :lilac-rust/traversal)
   (:tg :trivial-garbage)
   (:ts :software-evolution-library/software/tree-sitter))
  (:import-from :software-evolution-library/software/rust)
  (:import-from :lilac-rust/traversal
                :*interactive*)
  (:export
    :load-valhalla
    :load-valhalla/cache
    :translate-random-file
    :skip))
(in-package :lilac-rust/valhalla)

(defparameter *valhalla-path*
  (asdf:system-relative-pathname
   :lilac-rust
   "lilac-grist/valhalla/"))

(defvar *valhalla*)

(defun load-valhalla (&key (cache t)
                        (path *valhalla-path*))
  (flet ((load-valhalla ()
           (format *error-output* "~&Loading project~%")
           (when (bound-value '*valhalla*)
             (makunbound '*valhalla*)
             (tg:gc :full t))
           (prog1 (sel:from-file
                   'cpp:cpp-project
                   path)
             (tg:gc :full t)
             (format *error-output* "~&Project loaded~%"))))
    (if cache
        (ensure *valhalla* (load-valhalla))
        (setf *valhalla* (load-valhalla)))))

(declaim (string *random-file*))
(defvar-unbound *random-file*
  "The file picked for this session.")

(defun translate-random-file (&key (project (load-valhalla)) force
                                (refactor t))
  (flet ((random-file ()
           (car (random-elt (project:evolve-files project)))))
    (if force
        (setf *random-file* (random-file))
        (ensure *random-file* (random-file)))
    (rustran::translate-file project *random-file* :refactor refactor :verbose t)))

(defun list-valhalla-files ()
  (mapcar #'cdr
          (remove-if (op (string*= "third_party" (car _)))
                     (project:evolve-files *valhalla*))))

(defun bestn-files (n pred)
  (bestn n
         (list-valhalla-files)
         pred
         :key (op (length (ast:source-text _)))
         :memo t))

(defun shortest-files (&key (n 10))
  (bestn-files n #'<))

(defun longest-files (&key (n 10))
  (bestn-files n #'>))

(defun percent-translated (ast)
  "Percentage on non-whitepace characters translated."
  (flet ((stripped-length (ast)
           (length (collapse-whitespace (source-text ast)))))
    (let* ((length (stripped-length ast)))
      (if (typep ast 'rust-ast)
          1.0
          (let* ((full-translations (collect-if (of-type 'htree:full-translation) ast))
                 (rust-len
                   (reduce #'+ full-translations :key #'stripped-length)))
            (assert (< rust-len length))
            (float (/ rust-len length)))))))

(defun percent-translated/tokens (ast)
  (flet ((token-length (ast)
           (length (tokens (source-text ast)))))
    (let* ((length (token-length ast)))
      (if (typep ast 'rust-ast)
          1.0
          (let* ((full-translations (collect-if (of-type 'htree:full-translation) ast))
                 ;; TODO Should this be the length, not of the
                 ;; translation, but of the C++ tokens in the
                 ;; translation?
                 (rust-len (reduce #'+ full-translations :key #'token-length)))
            (assert (< rust-len length))
            (float (/ rust-len length)))))))

(defun percent-translated/ast-type (ast-type ast)
  (if (typep ast 'rust-ast)
      1.0
      (let* ((full-translations (collect-if (of-type 'htree:full-translation) ast))
             ;; TODO Should this be the length, not of the
             ;; translation, but of the C++ tokens in the
             ;; translation?
             ;; (rust-wrappers (collect-if (of-type 'htree:rust-wrapper) ast))
             (translated-asts-of-type
               (mappend (op (collect-if (of-type ast-type) _))
                        (mapcar #'htree:get-cpp-child
                                full-translations)))
             (untranslated-asts-of-type
               (set-difference
                (collect-if (of-type ast-type) ast)
                translated-asts-of-type)))
        (let ((divisor
                (+ (length translated-asts-of-type)
                   (length untranslated-asts-of-type))))
          (unless (zerop divisor)
            (float (/ (length translated-asts-of-type)
                      divisor)))))))

(defvar *percentages* (make-hash-table))

(defun record-percentages (files &key (ast-type 'statement-ast)
                                   (handle t)
                                   (project *valhalla*))
  (unless (boundp 'rustran::*failure-frequencies*)
    (setf rustran:*failure-frequencies* (make-hash-table)))
  (let ((*interactive* nil)
        (lilac-rust/traversal:*input-mode* :best-guess)
        (percentages-file
          (pathname (fmt "~~/percentages-~a" (get-universal-time))))
        (failures-file
          (pathname (fmt "~~/failures-~a" (get-universal-time)))))
    (labels ((write-percentages (file)
               (with-output-to-file (out file :if-exists :rename-and-delete)
                 (with-standard-io-syntax
                   (write (hash-table-alist *percentages*)
                          :stream out
                          :readably nil
                          :pretty t))))
             (write-failures (file)
               (with-output-to-file (out file :if-exists :rename-and-delete)
                 (with-standard-io-syntax
                   (write (hash-table-alist rustran:*failure-frequencies*)
                          :stream out
                          :readably nil
                          :pretty t)))))
      (unwind-protect
           (do-each (file files)
             (tg:gc :full t)
             (format *error-output* "~&File: ~a~%" file)
             (force-output *error-output*)
             (write-percentages percentages-file)
             (write-failures failures-file)
             (let ((success nil))
               (unwind-protect
                    (progn
                      (handler-bind ((error
                                       (lambda (e)
                                         (when handle
                                           (maybe-invoke-restart 'rustran::ignore-iterator)
                                           (maybe-invoke-restart 'rustran::record-failure)
                                           (maybe-invoke-restart 'rustran::skip-refactoring)
                                           (continue e)
                                           (invoke-restart 'ignore-errors)))))
                        (restart-case
                            (progn
                              (ensure-gethash file *percentages*
                                              (percent-translated/ast-type
                                               ast-type
                                               (mvlet ((project file
                                                        (if project
                                                            (values project file)
                                                            (project+file (source-text file)))))
                                                 (rustran:translate-file project file
                                                                         :refactor t
                                                                         :verbose t))))
                              (setf success t))
                          (ignore-errors ()
                            nil)))
                      (format *error-output* "~&Finished ~a (~a)" file success)
                      (force-output *error-output*))
                 (unless success
                   (setf (gethash file *percentages*) :error)))))
        (write-percentages percentages-file)
        (write-failures failures-file)))))

(defun project-ast-type-distribution (project)
  (file-list-ast-type-distribution
   (mapcar #'cdr (evolve-files project))))

(defun file-list-ast-type-distribution (files)
  (lret ((table (make-hash-table)))
    (dolist (file files)
      (file-ast-type-distribution file :table table))))

(defun file-ast-type-distribution (file &key (table (make-hash-table)))
  (iter (for ast in-tree (sel:genome file))
        (let ((class (tree-sitter-class-name (type-of ast))))
          (incf (gethash class table 0))))
  table)

(defun hist (table)
  (sort (hash-table-alist table)
        #'>
        :key #'cdr))

(defun list-by-extension (dir ext)
  (split-sequence
   #\Nul
   ;; Run in dir to get relative file names.
   ($cmd "find" (list dir) "-print0 -name" (list (fmt "*.~a" ext)))))

(defun dir-rust-files-type-distribution (dir)
  (file-list-ast-type-distribution
   (mapcar (op (sel:from-file 'rust _))
           (list-by-extension dir "rs"))))

(defun translate-util.cc-as-string ()
  (let* ((lilac-rust/translated-ast:*translated-source-text-delimiters* '("«" . "»"))
         (lilac-rust/traversal:*input-mode* :best-guess)
         (translation
           (handler-bind ((error (lambda (e)
                                   (declare (ignore e))
                                        ;(maybe-invoke-restart 'rustran::record-failure)
                                        ;(maybe-invoke-restart 'rustran:ignore-iterator)
                                   (asdf:test-system "lilac-rust")                   )))
             (translate-string
              (read-file-into-string #p"/home/prodriguez/Documents/valhalla/src/midgard/util.cc")
              :refactor nil))))
    (values translation (source-text translation))))
