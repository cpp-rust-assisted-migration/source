(defpackage :lilac-rust/rust-translation
  (:use :gt/full
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/cpp
        :software-evolution-library/software/cpp-project
        :software-evolution-library/software/c-cpp-project
        :software-evolution-library/software/rust
        :lilac-commons/ast-fragment
        :lilac-rust/translated-ast
        :lilac-rust/rust-common
        :lilac-commons/database
        :lilac-commons/analysis
        :lilac-commons/commons)
  (:import-from :lilac-commons/refactoring :project-relative-uri)
  (:local-nicknames
   (:attrs :functional-trees/attrs)
   (:c2rust :lilac-rust/c2rust)
   (:dir :software-evolution-library/software/directory)
   (:dbg :software-evolution-library/utility/debug)
   (:file :software-evolution-library/components/file)
   (:it :iterate)
   (:l :fresnel/lens)
   (:project :software-evolution-library/software/project)
   (:rust :lilac-rust/rust-common)
   (:rust-lens :lilac-rust/rust-lens)
   (:sel :software-evolution-library)
   (:simple :software-evolution-library/software/simple)
   (:range :software-evolution-library/utility/range)
   (:traversal :lilac-rust/traversal)
   (:ts :software-evolution-library/software/tree-sitter))
  (:import-from :cl-interpol)
  (:import-from :software-evolution-library :software)
  (:import-from :software-evolution-library/software/directory :file-ast)
  (:import-from :lilac-cpp/refactorings
                :apply-refactoring*
                :*refactoring-pipeline*)
  (:shadowing-import-from :serapeum :~> :~>>)
  (:import-from :lilac-cpp/refactorings
                :const-method?
                :const-declaration?
                :evaluated?
                :infer-type*)
  (:import-from :lilac-cpp/refactorings
                :get-fallthrough-sets)
  (:import-from :argot-server/utility
                :with-thread-name
                :project-root-for)
  (:shadowing-import-from :trivia.fail :fail)
  (:export
    :translate-partially
    :try-translate
    :*resolve-choice-function*
    :*original-ast*
    :*original-software*
    :*original-attrs*
    :*original-dependencies*
    :ignore-iterator
    :translate-file
    :*force-associated-types*
    :skip-refactoring
    :*failure-frequencies*
    :translate-files-partially))
(in-package :lilac-rust/rust-translation)
(in-readtable :interpol-syntax)


;;; Partial translation.

(defparameter *resolve-choice-function* #'first)

(defparameter *original-ast* nil)
(defparameter *original-software* nil)

(defparameter *force-associated-types* nil
  "If non-nil, always generate associated types (even when Self would do)."
  )

(defparameter *use-c2rust* nil
  "Flag to control c2rust integration.")

(defvar *original-attrs*)
(defvar-unbound *original-dependencies*
  "Original dependency tree of project.")

(defvar-unbound *new-imports*
  "List of new Rust imports synthesized during translation.")

(defvar-unbound *new-traits*
  "List of new Rust traits synthesized during translation.")

(defvar-unbound *constraints-db*
  "Database used for tracking type constraints on templates.")

(define-table type-traits
  ((:type-def :type ast)
   (:trait :type string)
   (:method :type ast)
   (:method-name :type string)
   (:template :type ast)))

(define-table template-param-traits
  ((:template :type cpp-template-declaration)
   (:position :type (integer 0))
   (:trait :type (or string null))
   (:method :type (or ast null))))

;;; TODO Is this redundant with the trait dictionary?
(defvar *method-trait-overrides* '())

(defvar-unbound *norecurse*
  "Track UIDs of ASTs that we should not recurse on, lest they diverge.

This can happen when expanding an AST into several ASTs (through an
AST fragment): we don't want to descend again into the original AST if
it itself included in the expansion.")

(defun norecurse! (ast)
  (withf *norecurse* (uid ast)))

(defparameter *internal-refactorings*
  '()
  "A list of internal refactorings.
Internal refactorings map C++ trees to other C++ trees, but in
unidiomatic ways we don't want to expose to the end user.

Refactorings will be applied in the given order.")

(defun get-same-by-uid (ast software)
  (find-uid (uid ast) software))

(defun get-original (ast &key error)
  "Look for an AST having the same UID as AST in SOFTWARE."
  (or (get-same-by-uid ast *original-software*)
      (when error
        (cerror "Return nil"
                "No original for ~a"
                ast)
        nil)))

(deftype cpp-ast* ()
  '(or c/cpp-root cpp-ast))

(deftype top-down-translation ()
  '(or rust-ast cpp-ast text-fragment ast-fragment
    untranslated-ast
    rust-wrapper
    (eql :remove)
    ;; NB List can be null (no translation) or non-null (multiple
    ;; translations).
    list))

(deftype translation-target ()
  '(or cpp-ast rust-ast
    c/cpp-root
    (subclass-union translated-ast)
    file-ast))

(deftype bottom-up-translation ()
  '(or rust-ast text-fragment null ast-fragment file-ast))

(defun assert-path-from (root ast)
  (unless (or (eql root ast)
              (typep ast 'cpp-empty-statement)
              (eql ast (lookup root nil)))
    (assert (or (ast-path root ast)))))

(defun parameters+return-type (fn)
  "Return the parameters (without any &self parameters) of FN, and the
return type of FN as a second value."
  (attrs:with-attr-table *original-attrs*
    (let* ((parameters (function-parameters fn))
           (parameters
             (if (find-if (of-type 'rust-self-parameter) fn)
                 (rest parameters)
                 parameters))
           (return-type
             (etypecase fn
               (cpp-ast (infer-type fn))
               (rust-ast (rust-return-type fn)))))
      (values parameters return-type))))

(defun argument-types+return-type+object-type (template fn)
  "Return three values.

1. The types of the arguments of FN in TEMPLATE.
2. The return type of its calls.
3. The type of the object it is invoked on, if any."
  (attrs:with-attr-table *original-attrs*
    (labels ((collect-fun-uses-by-name (ast fn-name)
               "Collect uses of FN-NAME by name (not scope) in AST."
               (collect-if (lambda (ast)
                             (and (typep ast 'call-ast)
                                  (match (call-function ast)
                                    ((and callfn (identifier-ast))
                                     (source-text= callfn fn-name))
                                    ((cpp-field-expression
                                      (cpp-field field))
                                     (source-text= field fn-name)))))
                           ast))
             (simplify-type (type)
               (etypecase type
                 (null type)
                 (ts:cpp-qualified-identifier
                  (simplify-type (cpp-name type)))
                 (ts:identifier-ast type))))
      (flet ((infer-common-type (asts)
               ;; TODO Possible conflicts between
               ;; argument/object/return types on the "same" method?
               ;; To a first approximation, partition by arity?

               ;; TODO Compute an upper bound, or disjunction?
               (some #'infer-type* asts)))
        (mvlet* ((fn-name (function-name fn))
                 (calls
                  (collect-fun-uses-by-name template fn-name))
                 (method-invocations
                  (filter (op (typep (call-function _) 'cpp-field-expression))
                          calls))
                 (object-type
                  (infer-common-type
                   (filter-map (op (get-declaration-ast
                                    :variable
                                    (cpp-argument (call-function _))))
                               method-invocations)))
                 (calls-with-arguments
                  (filter #'call-arguments calls))
                 (position-arguments
                  (when calls-with-arguments
                    (apply #'mapcar #'list
                           (mapcar #'call-arguments calls))))
                 (argument-types
                  (mapcar (lambda (args)
                            (infer-common-type args))
                          position-arguments))
                 (call-return-type
                  (infer-common-type calls)))
          (values (mapcar #'simplify-type argument-types)
                  (simplify-type call-return-type)
                  object-type))))))

(-> function-name-associated-type-prefix (ast)
  (values string &optional))
(defun function-name-associated-type-prefix (fn)
  (~> fn
      function-name
      source-text
      string-upcase-initials
      (escape (dict #\- "_"))))

(defun void? (type)
  "Is TYPE a void (unit) type?"
  (member type '("void" "()")
          :test #'source-text=))

(defgeneric template-type-constraints (template type)
  (:method (template type)
    (let* ((data (analyze-template-params/attr template))
           (param
             (first
              (select-results data
                              'param-ids
                              t
                              :param-type-string
                              (source-text type)))))
      param))
  (:method (template (type cpp-type-descriptor))
    (template-type-constraints template (cpp-type type))))

(defun extract-associated-type-names (template fn &key no-prefix)
  "Extract the names of associated types for FN, as strings."
  (attrs:with-attr-table *original-attrs*
    (mvlet* ((parameters return-type
              (parameters+return-type fn))
             (prefix
              (cond
                (*force-associated-types*
                 (function-name-associated-type-prefix fn))
                (no-prefix "")
                (t (function-name-associated-type-prefix fn))))
             (argument-types call-return-type object-type
              (argument-types+return-type+object-type template fn)))
      (flet ((self-type? (type)
               (and object-type type
                    (source-text= object-type type))))
        (let ((want-return-type? (not (void? return-type)))
              (associated-types
                (iter (iter:with arg-types = argument-types)
                      (for i from 1)
                      (repeat (length parameters))
                      (for argument-type = (pop arg-types))
                      (let ((prefixed (string+ prefix i)))
                        (collecting
                          (cond (*force-associated-types*
                                 prefixed)
                                ((self-type? argument-type) "Self")
                                (t prefixed)))))))
          (values (append
                   associated-types
                   (if want-return-type?
                       (cond (*force-associated-types*
                              #1=(list (string+ prefix "Output")))
                             ((self-type? call-return-type)
                              (list "Self"))
                             (t #1#))
                       nil))
                  (append
                   argument-types
                   (if want-return-type?
                       (list call-return-type)
                       nil))))))))

(defun type-name->associated-type (name)
  (declare (string name))
  (ematch name
    ;; ((equal "Self") (rust* "Self"))
    ((type string)
     (assure rust-associated-type
       (rust* "type $NAME" :name name)))))

(defun extract-associated-types (template fn &key no-prefix)
  "Extract the associated types (as Rust ASTs) from FN.
As a second value, return the type variables from TEMPLATE that they
correspond to."
  (receive (type-names arg-types)
      (extract-associated-type-names template fn :no-prefix no-prefix)
    (values (mapcar #'type-name->associated-type
                    (remove "Self" type-names :test #'equal))
            arg-types)))

(defun wrap-for-rust (ast)
  "Wrap AST as a child of a Rust AST."
  (etypecase ast
    (cpp-ast (make-untranslated-ast ast))
    (rust-ast ast)))

(defun wrap-for-cpp (ast)
  "Wrap AST as a child of a C++ AST."
  (etypecase ast
    (rust-ast (make-rust-wrapper ast))
    (cpp-ast ast)))

(defgeneric make-associated-type (type type-name)
  (:documentation "Make a `rust-type-item' instance from TYPE and
  TYPE-NAME.")
  (:method ((type rust-reference-type) type-name)
    (make-associated-type (rust-type type) type-name))
  (:method (type type-name)
    (assure rust-type-item
      (rust* "type $NAME = $TYPE"
             :name type-name
             :type
             ;; XXX Translation shouldn't be needed.
             (let ((type2 (translate/bottom-up type)))
               (if (eql type type2)
                   (tree-copy type)
                   type2))))))

(defun extract-associated-types/defaults (template fn &key no-prefix)
  (mvlet ((parameters return-type
           (parameters+return-type fn))
          (type-names
           (extract-associated-type-names template fn
                                          :no-prefix no-prefix)))
    (append
     (remove nil
             (mapcar (lambda (type-name param)
                       (unless (source-text= type-name "Self")
                         (make-associated-type
                          (parameter-type param)
                          type-name)))
                     type-names
                     parameters))
     (and (length> type-names parameters)
          (let ((last-type-name (lastcar type-names)))
            (unless (source-text= last-type-name "Self")
              (list
               (make-associated-type
                return-type
                last-type-name))))))))

(defun make-new-traits ()
  (labels ((method-enclosing-function (method-name)
             "Find the enclosing function for METHOD-NAME."
             (find-enclosing-declaration
              :function
              (attrs-root*)
              method-name))
           (make-self-type (name)
             (if (source-text= name "Self")
                 (make 'rust-type-identifier :text "Self")
                 (make 'rust-scoped-type-identifier
                       :rust-path (rust* "Self")
                       :rust-name
                       (make 'rust-type-identifier
                             :text name))))
           (anonymize-parameter (fn param type)
             "Replace name in PARAM with an underscore."
             (let* ((name (only-elt (parameter-names param)))
                    (param-type (parameter-type param))
                    (offset (position param (function-parameters fn)))
                    (prefix (function-name-associated-type-prefix fn))
                    (type-name
                      (cond (*force-associated-types*
                             #1=(string+ prefix (1+ offset)))
                            ((equal type "Self")
                             (rust* "Self"))
                            (t #1#))))
               (~> param
                   (with name (make 'cpp-identifier :text "_"))
                   (with param-type
                         (make-rust-wrapper
                          (make-self-type type-name))))))
           (make-trait-signature (template fn &key no-prefix)
             "Make a signature item from FN."
             (mvlet* ((return-type (infer-type fn))
                      (prefix
                       (if no-prefix ""
                           (function-name-associated-type-prefix fn)))
                      (atypes (extract-associated-type-names template fn :no-prefix no-prefix))
                      (params (function-parameters fn))
                      (atypes generic-return-type
                       (values (take (length params) atypes)
                               (car (drop (length params) atypes))))
                      (params
                       (cons
                        (make-self-parameter)
                        (mapcar #'make-untranslated-ast
                                (progn
                                  (assert (length>= atypes params))
                                  (mapcar (op (anonymize-parameter fn _ _))
                                          params
                                          atypes))))))
               (assure rust-function-signature-item
                 (rust* "fn $NAME(@PARAMS) -> $TYPE"
                        :name (definition-name fn)
                        :params params
                        :type
                        (if (void? return-type)
                            (make 'rust-unit-type)
                            (if (source-text= generic-return-type "Self")
                                (make 'rust-type-identifier :text "Self")
                                (make-self-type (string+ prefix "Output"))))))))
           (make-new-trait (trait method-names templates)
             ;; TODO Should use AST template directly. But body ends
             ;; up in internal ASTs.
             (assert (length= method-names templates))
             (let* ((fns (mapcar #'method-enclosing-function method-names))
                    (single (single fns)))
               (copy (rust* "pub trait $TRAIT { }"
                            :trait trait)
                     :rust-body
                     (make 'rust-declaration-list
                           :children
                           (append (mappend (op (extract-associated-types _ _ :no-prefix single))
                                            templates
                                            fns)
                                   (mapcar (op (make-trait-signature _ _ :no-prefix single))
                                           templates
                                           fns)))))))
    (let ((traits (nub (mapcar #'car (select-results *constraints-db* 'type-traits '(:trait))))))
      (iter (for trait in traits)
            (let ((methods-and-templates
                    (select-results *constraints-db*
                                    'type-traits
                                    '(:method :template)
                                    :trait trait)))
              (collecting (make-new-trait trait
                                          (mapcar #'first methods-and-templates)
                                          (mapcar #'second methods-and-templates))))))))

(defun apply-internal-refactorings (project
                                    &key (refactorings *internal-refactorings*)
                                      (root-path '()))
  (dbg:note 1 "Applying internal refactorings")
  (labels ((apply-refactoring (ast refactoring &key project)
             (attrs:with-attr-session (project :shadow nil)
               (funcall refactoring ast))))
    (symbol-macrolet ((genome (lookup project root-path)))
      (reduce (lambda (project fn)
                (dbg:note 2 "Applying refactoring ~a" fn)
                (with project
                      genome
                      (apply-refactoring genome fn
                                         :project project)))
              refactorings
              :initial-value project))))

(defun translate-files-partially (project files &rest args &key (cargo t) entry-point project-name
                                  &allow-other-keys)
  "Translate all the files in FILES, insert the translations as Rust
files, and return the resulting polyglot project."
  (setf files (mapcar #'sel:genome files))
  (assert (every (op (ast-path project _)) files))
  ;; NB We first translate the files individually, then add them into
  ;; the project all at once. If we tried to add them incrementally we
  ;; might try to compute symbol tables for heterogeneous trees.
  (let* ((projects
           (reduce (lambda (projects root)
                     (assert (ast-path project root))
                     (cons (apply #'translate-partially project
                                  :allow-other-keys t
                                  :root root args)
                           projects))
                   files
                   :initial-value '()))
         (project
           (reduce (lambda (project new-file)
                     (with project (file:original-path new-file) new-file))
                   (mapcar #'cdr
                           (filter (op (string^= "rust/" (car _)))
                                   (mappend #'dir:evolve-files
                                            projects)))
                   :initial-value project)))
    (if (not cargo) project
        (copy project
              :other-files
              (let* ((cargo-rel-path "rust/Cargo.toml")
                     (project-dir (project:project-dir project))
                     (cargo-abs-path
                       (namestring (path-join project-dir "rust/Cargo.toml")))
                     (entry-point-path
                       (and entry-point
                            (enough-namestring entry-point project-dir)))
                     (cargo-obj
                       (copy (sel:from-string
                              'simple:simple
                              (generate-cargo.toml
                               (or project-name
                                   (guess-project-name project))
                               :entry-point entry-point-path))
                             :original-path cargo-abs-path)))
                (acons cargo-rel-path
                       cargo-obj
                       (dir:other-files project)))))))

(defgeneric translate-partially (project &key prettify bottom-up interactive
                                           root strip-comments format)
  (:method ((project cpp-project)
            &key (prettify t) (bottom-up t)
              (root (sel:genome project))
              ((:interactive traversal:*interactive*) traversal:*interactive*)
              ((:refactorings refactorings) *internal-refactorings*)
              (strip-comments t)
              (format t))
    (setf root (sel:genome root))
    (nest
     (let ((original-project project)
           (root-path (ast-path project root)))
       (assert-path-from project root))
     (symbol-macrolet ((genome (lookup project root-path))))
     (let* ((file (find-enclosing 'file-ast project genome))
            (file-path (and file (ast-path project file))))
       ;; Assign a unique ID to every AST. This differs from the
       ;; serial number in that it is preserved across calls to
       ;; tree-copy.
       (dbg:note 1 "Adding UIDs")
       (let ((target (or file genome)))
         (withf project target
                (mapcar #'add-uid
                        (if strip-comments
                            (remove-if (of-type 'comment-ast) target)
                            target)))))
     (symbol-macrolet ((file (lookup project file-path))))
     (mvlet* ((*new-imports* nil)
              (*original-software* (or file genome))
              (*original-attrs*
               *original-dependencies*
               (attrs:with-attr-table project
                 (values attrs:*attrs*
                         (project-dependency-tree project))))
              (*constraints-db*
               (ensure-tables (empty-database)
                              'type-traits
                              'template-param-traits))
              (*method-trait-overrides* *method-trait-overrides*)))
     (let* ((project
              (with-thread-name (:name "Internal refactorings")
                (apply-internal-refactorings
                 project
                 :refactorings refactorings
                 :root-path root-path)))
            (path-restarts* (empty-map)))
       (declare (special path-restarts*)))
     (flet ((ast-path (obj ast)
              (assert (typep (sel:genome obj) 'ast))
              (lret ((path (ast-path obj ast)))
                (unless (or (typep ast 'root-ast)
                            (and (typep ast 'translated-ast)
                                 (some (of-type 'root-ast)
                                       (children ast))))
                  (assert-path-from obj ast))))
            ;; These two functions (along with the following macro)
            ;; allow for backing up in the translation and picking up
            ;; from a parent AST.
            (call/path-restart (path fn)
              (nlet retry ()
                (flet ((retry () (retry)))
                  (declare (dynamic-extent #'retry))
                  (let ((path-restarts*
                          (with path-restarts*
                                path
                                #'retry)))
                    (declare (special path-restarts*))
                    (funcall fn
                             ;; Being able to specify the AST is
                             ;; important as it may have been
                             ;; replaced.
                             (lookup project path))))))
            (invoke-path-restart (path)
              "Resume the translation at PATH."
              (if-let (fn (lookup path-restarts* path))
                (funcall fn)
                (error "~&No restart for path ~a" path)))))
     (macrolet ((with-path-restart ((path ast &key) &body body)
                  (with-thunk (body ast)
                    `(call/path-restart ,path ,body)))))
     (let ((*norecurse* (empty-set))
           ;; This contains the set of ASTs to be removed after
           ;; top-down translation. They aren't removed right away,
           ;; however, since they include preprocessor includes we
           ;; need to do type inference.
           (to-remove (empty-set))))
     (labels
         ((remove-marked-asts! ()
            (withf project genome
                   (remove-if (op (contains? to-remove _))
                              genome)))
          (top-down! (ast &optional parent)
            (assert-path-from project genome)
            (nest
             (when (typep ast 'ast))
             (let* ((path (ast-path project ast))
                    (parent-path (butlast path))))
             (with-path-restart (path ast))
             (nlet handle ((translation
                            (if (contains? *norecurse* (uid ast))
                                nil
                                (attrs:with-attr-session (project :shadow nil)
                                  (translate/top-down ast)))))
               (dispatch-case ((parent (or null translation-target))
                               (translation top-down-translation))
                 ;; No translation, recurse.
                 ((* null)
                  (mapc (op (top-down! _ ast))
                        (children ast)))
                 ;; This AST should be removed.
                 ((* (eql :remove))
                  ;; Less on an AST without a path would set the
                  ;; genome slot of the software to null, leading
                  ;; to confusing errors later on.
                  (assert-path-from project ast)
                  (withf to-remove ast))
                 ;; A method could return multiple translations,
                 ;; but has returned only one, so use that.
                 ((* (tuple t))
                  (handle (assure atom (car translation))))
                 ;; Invoke `*resolve-choice-function*' on a list of
                 ;; possible translations.
                 ((* list)
                  (let ((*original-ast* ast))
                    (handle
                     ;; The result must not be another list.
                     (assure atom
                       (funcall *resolve-choice-function*
                                translation)))))
                 ;; If only one child is valid, try to replace the AST
                 ;; with the translation of that child.
                 ((* untranslated-ast)
                  (handle (get-cpp-child translation)))
                 ((* rust-wrapper)
                  (handle (get-rust-child translation)))
                 ;; An AST fragment.
                 ((* ast-fragment)
                  (let ((valid-children
                          ;; We need to ensure that any translated
                          ;; children are wrapped as appropriate
                          ;; translated-ast instances.
                          (mappend
                           (lambda (child)
                             (dispatch-case ((parent (or null translation-target))
                                             (child top-down-translation))
                               ((* (eql :remove))
                                nil)
                               ((file-ast *)
                                (list child))
                               ((null *)
                                (list child))
                               ((rust-ast rust-ast)
                                (list child))
                               ((rust-ast cpp-ast)
                                (list (make-untranslated-ast child)))
                               ((rust-ast rust-wrapper)
                                (list (get-rust-child child)))
                               ((rust-ast untranslated-ast)
                                (list child))
                               ((cpp-ast* cpp-ast)
                                (list child))
                               ((cpp-ast* rust-ast)
                                (list (make-rust-wrapper child)))
                               ((cpp-ast* untranslated-ast)
                                (list (get-cpp-child child)))
                               (((subclass-union translated-ast) *)
                                (error "Attempt to insert fragment as child of translated AST. "))
                               ((* (or text-fragment ast-fragment list
                                       untranslated-ast rust-wrapper))
                                (error "Invalid"))))
                           (children translation))))
                    ;; Make sure we don't recurse on the AST itself.
                    (norecurse! ast)
                    (withf project path (ast-fragment valid-children))
                    ;; Back up to the parent to recurse on the
                    ;; new children now that they are in the
                    ;; tree. Note that it is important that this
                    ;; be done through the control stack: any
                    ;; active invocation of mapc by the parent on
                    ;; its children is now invalid, because the
                    ;; tree has been changed under it.
                    (invoke-path-restart parent-path)))
                 ;; Top-down translation is allowed to hand back a
                 ;; C++ AST. This means that the AST has been
                 ;; simplified for further translation later.
                 (((or null cpp-ast*) (or cpp-ast text-fragment))
                  (unless (eql translation ast)
                    (withf project path translation))
                  (let ((ast (assure ast (lookup project path))))
                    (top-down! ast parent)))
                 ((rust-ast cpp-ast)
                  (withf project path (make-untranslated-ast translation))
                  (let ((ast (assure ast (lookup project path))))
                    (cl:mapc (op (top-down! _ ast))
                             (children (get-cpp-child ast)))))
                 ;; We got a Rust AST back.
                 ((cpp-ast* rust-ast)
                  (withf project path
                         (make-rust-wrapper translation))
                  (let ((ast (assure ast (lookup project path))))
                    (cl:mapc (op (top-down! _ ast))
                             (children (get-rust-child ast)))))
                 ((rust-ast (or rust-ast text-fragment))
                  (withf project path translation)
                  (let ((ast (assure ast (lookup project path))))
                    (cl:mapc (op (top-down! _ ast))
                             (get-rust-children ast))))
                 ((rust-wrapper rust-ast)
                  (withf project path translation)
                  (let ((ast (assure ast (lookup project path))))
                    (cl:mapc (op (top-down! _ ast))
                             (get-rust-children ast))))
                 ((* (or rust-ast cpp-ast text-fragment))
                  (error "Invalid translation: ~a (in ~a) -> ~a"
                         ast parent translation))))))
          (wrap-bottom-up-translation (ast orig translation)
            (etypecase-of bottom-up-translation translation
              ((or rust-ast text-fragment)
               (if (typep (get-parent-ast project ast) 'rust-ast)
                   translation
                   (etypecase orig
                     (rust-ast
                      translation)
                     ((or cpp-source-text-fragment
                          cpp-source-text-fragment-variation-point)
                      (make-rust-wrapper translation))
                     (cpp-ast
                      (make-partial-translation
                       orig translation)))))
              (ast-fragment
               (make 'ast-fragment
                     :children
                     (mapcar (op (wrap-bottom-up-translation ast orig _))
                             (children translation))))
              (file-ast translation)
              (null translation)))
          (bottom-up! (ast)
            (when (typep ast 'ast)
              (let ((path (ast-path project ast))
                    (orig
                      (if (typep ast 'translated-ast)
                          (get-cpp-child ast)
                          ast)))
                (let ((translation
                        (translate/bottom-up
                         (if (typep ast '(or rust-wrapper
                                          partial-translation
                                          full-translation))
                             (get-rust-child ast)
                             orig))))
                  (etypecase-of bottom-up-translation translation
                    ((or rust-ast text-fragment ast-fragment file-ast)
                     (withf project path
                            (wrap-bottom-up-translation ast orig translation)))
                    (null (mapc #'bottom-up! (children ast)))))))))
       (dbg:note 1 "Performing top-down translation")
       (with-thread-name (:name "Context sensitive phase")
         (top-down! genome))
       (dbg:note 3 "Adding imports")
       (setf genome
             (copy genome
                   :children
                   (append
                    (mapcar #'make-rust-wrapper
                            (reverse *new-imports*))
                    (mapcar #'make-rust-wrapper
                            (attrs:with-attr-table *original-attrs*
                              (make-new-traits)))
                    (children genome))))
       (dbg:note 3 "Removing marked ASTs")
       (remove-marked-asts!)
       (dbg:note 1 "Performing bottom-up translation")
       (with-thread-name (:name "Context free phase")
         (when bottom-up
           ;; Compute a fixed point. This is necessary because lenses do
           ;; not descend into Rust ASTs, only C++ ASTs, although they
           ;; can recognize when a Rust AST is of the correct class for
           ;; a given slot. The tricky part is when a lens matches
           ;; against a Rust AST that itself has C++ children that in
           ;; turn has Rust children. In this case the translation is
           ;; actually working from both directions and multiple passes
           ;; may be needed to make both directions "meet".
           (nlet fix ((old genome)
                      (limit 100))
             (when (< limit 0)
               (error "Unstable translation"))
             (bottom-up! genome)
             (unless (equal? genome old)
               (fix genome (1- limit)))))))
     (with-thread-name (:name "Final fixups"))
     (let* ((final
              ;; TODO This should happen earlier.
              (fixup-signatures
               (fixup-templated-arguments
                (merge-impl-items
                 (if (typep genome 'translated-ast)
                     (get-rust-child genome)
                     ;; TODO Why is this necessary?
                     (or (and bottom-up (translate/bottom-up genome))
                         genome))))))
            (final
              (patch-whitespace final
                                :prettify prettify))
            (final (eif format (format-genome final) final))
            ;; Assign new serial numbers. Otherwise if there are
            ;; untranslated nodes we could end up with interval
            ;; collisions from trying to have them in two places at
            ;; once in the project tree.
            (final (tree-copy final))
            (orig-source
              (lookup original-project root-path))
            (orig-source
              (if (typep orig-source 'file-ast)
                  (only-elt (dir:contents orig-source))
                  orig-source))
            (old-path
              (car (rassoc orig-source
                           (project:evolve-files original-project)
                           :key (op (sel:genome _)))))
            (new-path
              (namestring (translated-path old-path)))
            (final-project
              (with original-project new-path
                    (make 'rust
                          :genome final
                          :original-path new-path))))
       (assert (not (eql final-project original-project)))
       (values final-project
               new-path)))))

(defun format-genome (ast &key verbose)
  (if (typep ast 'rust-ast)
      (sel:genome
       (sel:from-string 'rust
                        (format-source-text ast :verbose verbose)))
      ast))


;;; Common.

(defgeneric flatten-ast (ast)
  (:documentation "Flatten AST into a list of ASTs.")
  (:method ((ast t))
    (list ast))
  (:method ((ast compound-ast))
    (children ast)))


;;; Context-free translation.

;;; Context-free translation is mostly driven by lenses.

(defvar-unbound *failure-frequencies*
  "When bound, use to track failures by class.")

(defun record-failure (ast)
  (when (boundp '*failure-frequencies*)
    (let ((class-name (tree-sitter-class-name (type-of ast))))
      (incf (gethash class-name *failure-frequencies* 0))))
  (dbg:note 2 "Could not translate ~a"
            (handler-case (princ-to-string ast)
              (error ()
                (with-output-to-string (s)
                  (print-unreadable-object (ast s :type t :identity t))))))
  nil)

(defgeneric translate/bottom-up (ast)
  (:method ((ast text-fragment)) ast)
  (:method ((ast file-ast))
    (copy ast
          :contents (mapcar (op (or (translate/bottom-up _1)
                                    _1))
                            (dir:contents ast))))
  (:method ((ast rust-ast))
    (let ((children (children ast)))
      (cond ((no children) ast)
            ((every (of-type '(or rust-ast text-fragment)) ast) ast)
            (t
             (let ((translations (mapcar #'translate/bottom-up children)))
               (if (equal? children translations) ast
                   (reduce
                    (lambda (ast child.translation)
                      (destructuring-bind (child . translation)
                          child.translation
                        (if (typep translation
                                   '(and rust-ast (not source-text-fragment)))
                            (with ast child translation)
                            ast)))
                    (mapcar #'cons children translations)
                    :initial-value ast)))))))
  (:method :around ((ast rust-ast))
    "Final fixups."
    (match (call-next-method)
      ((and expr
            (rust-struct-expression
             (rust-name
              (and type (rust-generic-type)))))
       (copy expr
             :rust-name
             (make 'rust-generic-type-with-turbofish
                   :rust-type (rust-type type)
                   :rust-type-arguments (rust-type-arguments type))))
      (result result)))
  (:method ((ast cpp-ast))
    (restart-case
        (if-let (result (l:forward (rust-lens:rust-lens) ast))
          (assign-uid-from ast result)
          (record-failure ast))
      (record-failure ()
        (record-failure ast))))
  (:method ((ast rust-impl-item))
    (split-impl-item (call-next-method ast)))
  (:method ((ast rust-wrapper))
    (translate/bottom-up (get-rust-child ast)))
  (:method ((ast untranslated-ast))
    (translate/bottom-up (get-cpp-child ast)))
  (:method ((ast partial-translation))
    (translate/bottom-up (get-rust-child ast)))
  (:method ((ast full-translation))
    (get-rust-child ast))
  (:method ((ast rust-function-item))
    (let ((result (call-next-method)))
      (or (match result
            ((rust-function-item
              (rust-name (source-text= "fmt"))
              (rust-body body))
             ;; TODO Handle non-writes in the body (conditions?). This
             ;; may require user assistance.
             (when (and (every (of-type 'rust-expression-statement) (children body))
                        (every (of-type 'rust-macro-invocation)
                               (mappend #'children
                                        (children body))))
               (mvlet* ((writes (collect-if
                                 (of-type 'rust-macro-invocation)
                                 body))
                        (writes last-writes
                         (halves writes -1))
                        (changes
                         (append
                          (mapcar (lambda (write)
                                    (cons write
                                          (rust* "$1.unwrap()"
                                                 ;; Prevent recursion.
                                                 (copy write))))
                                  writes)
                          (let ((last-write (only-elt last-writes)))
                            (list
                             (cons last-write
                                   (rust* "return $1"
                                          ;; Prevent recursion.
                                          (copy last-write))))))))
                 (mapcar (hash-table-function
                          (alist-hash-table changes))
                         result)))))
          result))))

(defmethod flatten-ast ((ast cpp-comma-expression))
  (nlet rec ((ast ast)
             (acc '()))
    (etypecase ast
      (cpp-comma-expression
       (rec (cpp-right ast)
            (cons (cpp-left ast) acc)))
      (cpp-ast (nreverse (cons ast acc))))))

(defmethod translate/bottom-up ((ast cpp-comma-expression))
  (let ((expressions (mapcar #'translate/bottom-up (flatten-ast ast))))
    (when (every (of-type '(or rust-ast text-fragment)) expressions)
      (mvlet* ((expressions last-expressions
                (halves expressions -1))
               (expressions
                (append1 (mapcar (op (make 'rust-expression-statement
                                           :children (list _)))
                                 expressions)
                         (make 'rust-implicit-return-expression
                               :children last-expressions))))
        (make 'rust-block
              :children expressions)))))

(defmethod translate/bottom-up ((template cpp-template-declaration))
  "Convert the children of TEMPLATE into generics."
  (ematch template
    ((cpp-template-declaration
      (cpp-parameters params)
      (children (list* d _)))
     (parameterize-ast (translate/bottom-up d) params
                       (get-original template)))))

(defun parameterize-rust-type (ast params template)
  "Parameterize the `rust-type' slot of AST according to PARAMS and
TEMPLATE."
  (copy ast :rust-type
        (parameterize-ast (rust-type ast) params template)))

(-> param-constraints (ast) vector)
(defun param-constraints (template)
  "Return a vector of parameter constraints for TEMPLATE.
Each element of the vector corresponds to a template parameter, and
contains an alist from traits to methods on that trait."
  (let* ((data
           (select-results *constraints-db*
                           'template-param-traits
                           '(:position :trait :method)
                           :template template))
         (len (1+ (reduce #'max data :key #'first)))
         (array (map-into (make-array len)
                          (op (make-hash-table :size 0 :test 'equal)))))
    (iter (for (position trait method) in data)
          (when (and method trait)
            (pushnew method (href (aref array position) trait))))
    (map-into array #'hash-table-alist array)))

(defun constrain-type-parameters (template type-parameters)
  (let ((param-constraints (param-constraints template)))
    (if (some #'emptyp param-constraints)
        type-parameters
        (progn
          (assert (length= type-parameters param-constraints))
          (map 'list
               (op (constrain-type-parameter template _ _))
               type-parameters
               param-constraints)))))

(defun get-param-associated-types (template constraints param)
  "Get the associated types for PARAM of TEMPLATE."
  (let ((only-param?
          (~> template
              cpp-parameters
              children
              single)))
    (~>> (get-associated-types-from-constraints template constraints :no-prefix only-param?)
         (filter (op (source-text= param (cdr _))))
         (mapcar #'car))))

(defun get-associated-types-from-constraints (template constraints &key no-prefix)
  "Use CONSTRAINTS to derive the associated types for TEMPLATE."
  (mappend (lambda (method)
             (receive (atypes types)
                 (extract-associated-type-names template method :no-prefix no-prefix)
               (list atypes types)
               (mapcar #'cons atypes types)))
           (attrs:with-attr-table *original-attrs*
             (nub
              (remove nil
                      (mapcar (op (find-enclosing-declaration :function (attrs-root*) _))
                              (mappend #'cdr constraints)))))))

(defun constrain-type-parameter (template param constraints)
  "Constrain PARAM of TEMPLATE according to CONSTRAINTS.
That is, if CONSTRAINTS is not empty, return a
`rust-constrained-type-parameter'."
  (labels ((trait-type-bound (associated-types trait)
             (declare (string trait))
             (let ((type-id
                     (make 'rust-type-identifier :text trait)))
               (if (no associated-types) type-id
                   (make 'rust-generic-type
                         :rust-type type-id
                         :rust-type-arguments
                         (make 'rust-type-arguments
                               :children
                               (mapcar
                                (lambda (atype)
                                  (declare (string atype))
                                  (make 'rust-type-binding
                                        :rust-name
                                        (make 'rust-type-identifier :text atype)
                                        :rust-type
                                        (tree-copy param)))
                                associated-types)))))))
    (if (no constraints) param
        (mvlet* ((traits (sort-new (mapcar #'car constraints) #'string<))
                 (associated-types
                  (get-param-associated-types template constraints param))
                 (associated-types
                  (remove "Self" associated-types :test #'source-text=)))
          (make 'rust-constrained-type-parameter
                :rust-left param
                :rust-bounds
                (make 'rust-trait-bounds
                      :children
                      (map 'list
                           (op (trait-type-bound associated-types _))
                           traits)))))))

(defgeneric parameterize-ast (ast params template)
  (:documentation "Make AST, a non-generic Rust AST, generic using
  PARAMS, a Rust type parameters AST.")
  (:method (ast (params t) (template t))
    ast)
  (:method ((ast full-translation) params template)
    (make-full-translation
     (get-cpp-child ast)
     (parameterize-ast (get-rust-child ast)
                       params
                       template)))
  (:method ((ast rust-wrapper) params template)
    (make-rust-wrapper
     (parameterize-ast (get-rust-child ast)
                       params
                       template)))
  (:method (ast (params cpp-template-parameter-list) template)
    (if-let (rust-params
             (translate/bottom-up params))
      (parameterize-ast ast rust-params template)
      (call-next-method)))
  (:method ((ast rust-struct-item) (params rust-type-parameters) (template t))
    (copy ast
          :rust-type-parameters params))
  (:method ((fn rust-function-item) (params rust-type-parameters) template)
    (match fn
      ((rust-function-item
        (rust-name (source-text= "new"))
        (rust-return-type
         (and return-type (rust-type-identifier))))
       (copy fn
             :rust-return-type
             (parameterize-ast return-type params template)))
      (otherwise
       (let* ((type-parameters (children params))
              (constrained-type-parameters
                (constrain-type-parameters template type-parameters)))
         (if (equal type-parameters constrained-type-parameters) fn
             (copy fn
                   :rust-type-parameters
                   (make 'rust-type-parameters
                         :children
                         constrained-type-parameters)))))))
  (:method ((ast rust-type-identifier) (params rust-type-parameters) (template t))
    (make 'rust-generic-type
          :rust-type ast
          :rust-type-arguments
          (make 'rust-type-arguments
                :children (mapcar #'tree-copy (children params)))))
  (:method ((ast rust-declaration-list) (params rust-type-parameters) template)
    (copy ast
          :children (mapcar (op (parameterize-ast _ params template))
                            (children ast))))
  (:method ((ast rust-impl-item) (params rust-type-parameters) template)
    "Parameterize the impl block.
E.g. with one type parameter T, `impl Sub for Point` becomes

    impl <T:Sub<Output=T>> Sub for Point<T>"
    (flet ((constrain-type (trait type-param-ast &key output)
             "Constrain a type with a trait and the requirement that it return itself.

E.g. `T` and trait `Sub` becomes `T:Sub<Output=T>`."
             (match type-param-ast
               ((rust-type-identifier)
                (let ((actual-type
                        (if output
                            (make 'rust-type-binding
                                  :rust-name
                                  (make 'rust-type-identifier
                                        :text "Output")
                                  :rust-type
                                  (copy type-param-ast))
                            (copy type-param-ast))))
                  (make 'rust-constrained-type-parameter
                        :rust-left type-param-ast
                        :rust-bounds
                        (make 'rust-trait-bounds
                              :children
                              (list
                               (make 'rust-generic-type
                                     :rust-type (trait-type-id (find-trait trait))
                                     :rust-type-arguments
                                     (make 'rust-type-arguments
                                           :children (list actual-type))))))))
               (otherwise type-param-ast))))
      (parameterize-rust-type
       (match ast
         ((rust-impl-item
           :rust-trait (and trait-ast (rust-ast)))
          (let ((params (tree-copy params)))
            (if (typep (find-trait trait-ast) 'op-trait)
                (copy ast
                      :rust-trait (tree-copy trait-ast)
                      :rust-type-parameters
                      (tree-copy
                       (make 'rust-type-parameters
                             :children
                             (mapcar (op (constrain-type trait-ast _ :output t))
                                     (children params)))))
                (copy ast
                      :rust-trait (parameterize-ast trait-ast params template)))))
         (otherwise
          (copy ast :rust-type-parameters
                (make 'rust-type-parameters
                      :children
                      (constrain-type-parameters template (children params))))))
       params
       template)))
  (:method :around ((ast rust-impl-item) (params rust-type-parameters) (template t))
    "If we have an impl for Display, make sure any type parameters are constrained to Display."
    ;; Is this a safe assumption?
    (match (call-next-method)
      ((and impl
            (rust-impl-item :rust-trait trait
                            :rust-type-parameters nil
                            :rust-type (rust-generic-type
                                        (rust-type-arguments
                                         (and type-args-ast
                                              (rust-type-arguments))))))
       (unless (display-trait? trait)
         (fail))
       (let ((type-args (children type-args-ast)))
         (copy
          impl
          :rust-type-parameters
          (make
           'rust-type-parameters
           :children
           (mapcar
            (lambda (type-id)
              (make 'rust-constrained-type-parameter
                    :rust-left (tree-copy type-id)
                    :rust-bounds
                    (make 'rust-trait-bounds
                          :children
                          (list
                           (make 'rust-type-identifier
                                 :text (source-text trait))))))
            type-args)))))
      (result result)))
  (:method ((ast rust-type-identifier) (params rust-type-parameters) (template t))
    (assign-uid-from
     ast
     (make 'rust-generic-type
           :rust-type ast
           :rust-type-arguments
           (make 'rust-type-arguments
                 :children (children params))))))

(defun display-trait? (trait)
  ;; We're generating these ourselves, so the options are just in case we
  ;; change how we import it.
  (member trait
          '("Display" "fmt::Display" "std::fmt::Display")
          :test #'source-text=))

(defun constrained-by-trait? (trait impl)
  (some (op (source-text= trait _))
        (mappend #'children
                 (collect-if (of-type 'rust-trait-bounds) impl))))

(defun fixup-signatures (root)
  "Remove Rust function signatures outside of the few locations Rust allows them"
  (nest
   (if (typep root 'cpp-ast) root)
   (flet ((valid-signature-location? (ast)
            "Is AST in a valid location for a function signature item?"
            (find-enclosing '(or rust-trait-item rust-foreign-mod-item)
                            root
                            ast))))
   (remove-if (lambda (ast)
                (and (typep ast 'rust-function-signature-item)
                     (not (valid-signature-location? ast))))
              root)))

(defun fixup-templated-arguments (root)
  "Fix up arguments to functions called in templates in ROOT according
to the declared signatures of the traits those templates specialize."
  ;; TODO This should happen much earlier (maybe on the C++ side)?
  (flet ((call-function* (call)
           (nlet rec ((fn (call-function call)))
             (typecase fn
               (rust-field-expression
                (rec (rust-field fn)))
               (t fn)))))
    (let* ((traits (collect-if (of-type 'rust-trait-item) root))
           (trait-names (mapcar #'rust-name traits))
           (impl-items (collect-if (of-type 'rust-impl-item) root))
           (trait->impl-items
             (iter (for trait in trait-names)
                   (collecting
                     (cons trait
                           (filter (op (constrained-by-trait? trait _))
                                   impl-items)))))
           (impl-item->trait
             (iter outer
                   (for (trait . impl-items) in trait->impl-items)
                   (iter (for item in impl-items)
                         (in outer
                             (collecting (cons item trait))))))
           (old-call->new-call
             (iter outer
                   (for (nil . trait-name) in impl-item->trait)
                   (let* ((trait (find trait-name traits :key #'rust-name))
                          (sigs
                            (collect-if
                             (of-type 'rust-function-signature-item)
                             trait))
                          (sig->call
                            (filter-map
                             (lambda (call)
                               (when-let (sig
                                          (find-if (lambda (sig)
                                                     (source-text= (call-function* call)
                                                                   (rust-name sig)))
                                                   sigs))
                                 (cons sig call)))
                             (collect-if (of-type 'call-ast)
                                         root))))
                     (iter (for (sig . call) in sig->call)
                           (let ((args (call-arguments call))
                                 ;; TODO
                                 ;; (params (function-parameters sig))
                                 (params
                                   (remove-if (of-type 'rust-self-parameter)
                                              (children (rust-parameters sig)))))
                             (when (length= args params)
                               (let ((new-args
                                       (iter (for arg in args)
                                             (for param in params)
                                             (collecting
                                               (if (and (typep (rust-type param) 'rust-reference-type)
                                                        (not (typep arg 'rust-reference-expression)))
                                                   (match arg
                                                     ((rust* "$1.clone()" arg)
                                                      (rust* "&$1" arg))
                                                     (otherwise (rust* "&$1" arg)))
                                                   arg)))))
                                 (unless (equal new-args args)
                                   (in outer
                                       (collecting
                                         (cons call
                                               (copy call
                                                     :rust-arguments
                                                     (make 'rust-arguments
                                                           :children new-args))))))))))))))
      (if old-call->new-call
          (mapcar (hash-table-function
                   (alist-hash-table
                    old-call->new-call))
                  root)
          root))))

(defun implements-same-trait-for-same-type? (item1 item2)
  (match item1
    ((rust-impl-item
      (rust-trait trait1)
      (rust-type type1))
     (match item2
       ((rust-impl-item
         (rust-trait trait2)
         (rust-type type2))
        (and (equal? trait1 trait2)
             (equal? type1 type2)))))))

(defun merge-impl-items (ast)
  "Merge impl items in AST with the same trait and type.
This can happen when there are method definitions in the class and
separate friend definitions."
  (let ((impls (collect-if (of-type 'rust-impl-item) ast)))
    (if (no impls) ast
        (let ((groups
                (assort impls :test #'implements-same-trait-for-same-type?)))
          (if (every #'single groups) ast
              (let* ((to-remove (mappend #'cdr groups))
                     (ast (reduce #'less to-remove :initial-value ast)))
                (flet ((sort-methods-last (children)
                         "Put the methods last."
                         (multiple-value-bind (fns other)
                             (partition (of-type 'function-declaration-ast)
                                        children)
                           (append other fns))))
                  (reduce (lambda (ast group)
                            (let ((target (car group)))
                              (with ast target
                                    (copy target
                                          :rust-body
                                          (copy (rust-body target)
                                                :children
                                                (sort-methods-last
                                                 (mappend (op (children (rust-body _)))
                                                          group)))))))
                          groups
                          :initial-value ast))))))))


;;; Context-sensitive translation.

(defgeneric translate/top-down (ast)
  (:documentation "Attempt to translate AST, working top-down.")
  (:method-combination standard/context)
  (:method ((ast ast)) nil)
  (:method :around ((ast structured-text))
    (let ((result (call-next-method)))
      (if result
          (if (and (typep ast 'structured-text)
                   (typep result 'structured-text))
              (copy result
                    :before-text (before-text ast)
                    :after-text (after-text ast))
              result)
          result)))
  (:method ((ast partial-translation))
    ;; There may be more ASTs requiring translation internally.
    (translate/top-down (get-rust-child ast)))
  (:method ((ast rust-wrapper))
    (get-rust-child ast))
  (:method ((ast untranslated-ast))
    (translate/top-down (get-cpp-child ast))))

(defun get-modules-to-declare (ast)
  "Return the names of any Rust modules AST's file will need to declare.

- Non-module files \(or the \"main\" module) declare their non-partition
  dependencies.
- A \"primary module interface unit\" declares its partitions.
- No other files declare modules."
  (let* ((file (find-enclosing 'dir:file-ast (attrs-root*) ast))
         (tree
           (aget (namestring (dir:full-pathname file))
                 *original-dependencies*
                 :test #'equal))
         (dependent-modules
           (~> tree
               flatten
               (filter #'module-path? _)
               ;; Drop any circular dependency.
               (remove (dir:full-pathname file)
                       _
                       :test #'equal)
               nub))
         (module-kind (module? ast))
         (module-files
           (if (or (no module-kind)
                   (equal "main" (module-unit-full-name module-kind)))
               ;; This is not a module, or it's the main module.
               ;; Declare all non-partitions.
               (remove-if (op (find #\- _))
                          dependent-modules)
               ;; This is the primary module unit of a partition.
               (and (typep module-kind 'primary-module-interface-unit)
                    (let ((prefix (string+ (module-unit-module-name module-kind) "-")))
                      (mapcar (op (drop-prefix prefix _))
                              (filter (op (string^= prefix _))
                                      dependent-modules)))))))
    (mapcar (op (drop-suffix ".cppm" _))
            module-files)))

(defun build-module-declaration (name)
  "Return a declaration AST for a module named NAME."
  (declare (string name))
  (match name
    ;; Drop extensions.
    ((ppcre "^(.*?)\\." name)
     (build-module-declaration name))
    (otherwise
     (make-rust-wrapper
      (assure rust-mod-item
        (rust* "pub mod $NAME" :name name))))))

(defun imported-modules (ast)
  "Collect the import declarations from AST."
  (collect-if (of-type 'cpp-import-declaration) ast))

(defun undeclared-imports (ast modules-to-declare)
  "Return any undeclared imports in AST."
  (remove-if (lambda (import)
               (let ((name (drop-prefix ":" (source-text (cpp-name import)))))
                 (member name modules-to-declare :test #'equal)))
             (imported-modules ast)))

(defun rewrite-undeclared-import (import)
  "Translate an undeclared import to its RUst equivalent.
Undeclared imports will always either be sibling partitions or
top-level modules, so the Rust equivalents will start with `crate::'
or `super::'."
  (l:forward (rust-lens:undeclared-import) import))

(defun rewrite-undeclared-imports (ast undeclared-imports)
  "Rewrite any undeclared imports in in AST."
  (mapcar (lambda (ast)
            (when (member ast undeclared-imports)
              (let ((rewrite (rewrite-undeclared-import ast)))
                (if (no rewrite) ast
                    (make 'ast-fragment
                          :children
                          (list (make-rust-wrapper
                                 (rust* "#[allow(unused_imports)]"))
                                (make-full-translation ast rewrite)))))))
          ast))

(defun fixup-rust-imports (ast next)
  (unless (find-if (of-type 'rust-ast) ast)
    (let* ((modules-to-declare (get-modules-to-declare ast))
           (new-module-declarations
             (mapcar #'build-module-declaration modules-to-declare))
           (undeclared-imports
             (undeclared-imports ast modules-to-declare)))
      (declare ((soft-list-of string) modules-to-declare))
      (if (nor new-module-declarations undeclared-imports)
          next
          (let ((ast (or next ast)))
            (norecurse! ast)
            (rewrite-undeclared-imports
             (copy ast
                   :children
                   (append new-module-declarations
                           (children ast)))
             undeclared-imports))))))

(defmethod translate/top-down ((ast cpp-module-declaration))
  :remove)

(defun pruneable-namespace? (ast)
  "Return T if AST is a pruneable namespace.
A namespace is pruneable if it only contains declarations (not
definitions)."
  ;; TODO Consider pruning the namespace selectively. Removing
  ;; function declarations, keeping type declarations, etc.
  (ematch ast
    ((cpp* "namespace $NAME { @CHILDREN }" :name _ :children children)
     ;; TODO Are there type declartions?
     (every (lambda (child)
              (and (typep child 'declaration-ast)
                   (not (typep child 'definition-ast))))
            children))))

(defmethod translate/top-down ((ast cpp-namespace-definition))
  (if (pruneable-namespace? ast)
      :remove
      (call-next-method)))

(defmethod translate/top-down ((ast cpp-translation-unit))
  "Add module imports as necessary."
  (fixup-rust-imports ast (call-next-method)))

;;; Are there any circumstances we would want to keep it?
(defmethod translate/top-down ((ast cpp-empty-statement))
  :remove)

(defmethod translate/top-down ((ast cpp-preproc-function-def))
  :remove)

(defmethod translate/top-down ((ast cpp-preproc-call))
  (match ast
    ((cpp-preproc-call
      (cpp-directive (cpp-preproc-directive :text "#pragma")))
     :remove)
    (otherwise (call-next-method))))

;;; Rust always imports the prelude.
(defmethod translate/top-down ((ast cpp-using-declaration))
  (when (source-text= (first (children ast)) "std")
    :remove))

(defmethod flatten-ast ((declaration cpp-declaration))
  "Flatten a declaration with multiple declarators into a list of declarations."
  (ematch declaration
    ((cpp-declaration
      :cpp-declarator declarators)
     (if (single declarators)
         (list declaration)
         (mapcar (lambda (declarator)
                   (tree-copy
                    (copy declaration
                          :cpp-declarator (list declarator))))
                 declarators)))))

(defun infinite-loop? (cpp-ast)
  "Is CPP-AST an infinite loop?"
  (match cpp-ast
    ((cpp-for-statement
      :cpp-initializer nil
      :cpp-condition nil
      :cpp-update nil)
     t)
    ((or (cpp-do-statement :condition cond)
         (cpp-while-statement :condition cond))
     (match cond
       ((or (cpp* "1") (cpp* "true"))
        t)))))

(defun add-continue-preamble (loop body preamble)
  "In BODY, insert (copies of) PREAMBLE before continue statements."
  (let ((update-statements
          (mapcar #'ensure-expression-statement
                  (flatten-ast preamble)))
        (continue-asts
          (filter (op (member loop (exit-control-flow _)))
                  (collect-if (of-type 'continue-ast) body))))
    (if (no continue-asts) body
        (reduce
         (lambda (body continue)
           (with body
                 continue
                 (make 'ast-fragment
                       :children
                       (append1 (mapcar #'tree-copy update-statements)
                                continue))))
         continue-asts
         :initial-value body))))

(defun reset-body-formatting (body)
  "Remove any formatting from the children of BODY."
  (copy body
        :children
        (mapcar (op (copy-ast _ :before-text "" :after-text ""))
                (children body))))

(defun translate-for-loop-interactively (cpp)
  "Translate a for loop interactively.
NB This is currently only used for testing."
  (nest
   (when (eql traversal:*interactive* :force))
   (with-simple-restart (ignore-iterator "Give up on translating iterator"))
   (match cpp
     ((cpp-for-statement
       :cpp-initializer initializer
       :cpp-body body)
      (let* ((file (find-enclosing 'file-ast
                                   (attrs-root*)
                                   cpp))
             (original-file
               (assure file-ast
                 (get-original file)))
             (original-ast
               (assure cpp-ast
                 (get-original cpp)))
             (line (range:line
                    (range:begin
                     (ast-source-range
                      original-file
                      original-ast))))
             (uri
               (and traversal:*uri*
                    (string+ (project-root-for traversal:*uri*)
                             "/"
                             (string-left-trim "/"
                                               (namestring
                                                (dir:full-pathname file)))))))
        (traversal:traversal-lift-lower
         :uri uri
         :line line
         :ast cpp
         :block-children
         (if (typep body 'cpp-compound-statement)
             (children body)
             (list body))
         :iter-var-1
         (find-if (of-type 'cpp-identifier)
                  initializer)))))))

(deftype iterator-direction ()
  '(member :up :down))

(defun translate-index-for-loop-to-rust-range (cpp)
  "Translate a for loop with an index variable to a Rust range."
  (labels ((loop-direction (comparator increment-operator)
             "From COMPARATOR and INCREMENT-OPERATOR,
              tell if the loop is going up or down."
             (assure iterator-direction
               (etypecase comparator
                 ((or c/cpp-< c/cpp-<=) :up)
                 ((or c/cpp-> c/cpp->=) :down)
                 (c/cpp-!=
                  (etypecase increment-operator
                    (c/cpp-++ :up)
                    (c/cpp--- :down))))))
           (make-range (start to direction inclusive)
             "Make a Rust range expression AST."
             (let ((start
                     (make-untranslated-ast
                      (copy start :after-text "")))
                   (to
                     (make-untranslated-ast
                      (copy to :before-text ""))))
               (dispatch-case ((direction iterator-direction)
                               (inclusive boolean))
                 (((eql :up) null) (rust* "$1..$2" start to))
                 (((eql :up) true) (rust* "$1..=$2" start to))
                 (((eql :down) true) (rust* "($1..=$2).rev()" to start))
                 (((eql :down) null)
                  (rust* "($1..=$2).rev()"
                         (make-increment-expr to)
                         start)))))
           (make-block (ast)
             "Wrap AST as a Rust block."
             (let* ((statement
                      (if (typep ast 'statement-ast)
                          (make-untranslated-ast ast)
                          (make 'rust-expression-statement
                                :children (list (make-untranslated-ast ast))))))
               (make 'rust-block :children (list statement))))
           (constant-fold-increment (ast)
             "Try to constant-fold incrementing AST. Otherwise return nil."
             (typecase ast
               (untranslated-ast
                (when-let (const (constant-fold-increment (get-cpp-child ast)))
                  (make-untranslated-ast const)))
               (cpp-number-literal
                (when-let (n (ignore-some-conditions (parse-error)
                               (parse-integer (source-text ast))))
                  (copy ast :text (fmt "~a" (1+ n)))))))
           (make-increment-expr (ast)
             "Return a Rust expression incrementing AST."
             (or (constant-fold-increment ast)
                 (rust* "{$1 + 1}" ast))))
    (match cpp
      ((cpp-for-statement
        :cpp-initializer
        (cpp "$TYPE $VAR = $NUM;" :type _ :var init-var :num start)
        :cpp-condition
        (cpp-binary-expression
         :lhs binop-left
         :rhs binop-right
         :operator
         (and comp
              (or (cpp-<=) (cpp->=)
                  (cpp-<) (cpp->) (cpp-!=))))
        :cpp-update
        (and update-expr
             (or (cpp* "$VAR++" :var inc-var)
                 (cpp* "$VAR--" :var inc-var)
                 (cpp* "++$VAR" :var inc-var)
                 (cpp* "--$VAR" :var inc-var)))
        :body body)
       ;; The variables being defined, incremented, and compared
       ;; against must the same variable.
       (unless (same #'source-text (list init-var binop-left inc-var)
                     :test #'equal)
         (fail))
       ;; We don't want to handle iterators here.
       (when (or (find-if #'iterator? update-expr)
                 (find-if #'iterator? start))
         (fail))
       (let* ((direction (loop-direction comp (cpp-operator update-expr)))
              (inclusive (typep comp '(or cpp-<= cpp->=)))
              (rust-body
                (if (typep body 'cpp-compound-statement)
                    (make-untranslated-ast body)
                    ;; Handle for without brackets.
                    (make-block body))))
         (ensure-expression-statement
          (make 'rust-for-expression
                :rust-pattern (make-untranslated-ast init-var)
                :rust-value (make-range start
                                        binop-right
                                        direction
                                        inclusive)
                :rust-body rust-body)))))))

(defun translate-for-loop-iterator-idioms (cpp)
  "Translate various idiomatic for loops using iterators."
  (match cpp
    ;; Forward mutable loops.
    ((or (cpp #.(fmt "~
for ($_ $VAR = $CONTAINER.begin(); $VAR != $CONTAINER.end(); $VAR++) {
  @BODY
}")
              :var var
              :container container
              :body children)
         (cpp #.(fmt "~
for ($_ $VAR = $CONTAINER.begin(); $VAR != $CONTAINER.end(); ++$VAR) {
  @BODY
}")
              :var var
              :container container
              :body children))
     (let ((replacement (l:forward (rust-lens:rust-lens) var)))
       (ensure-expression-statement
        (rust* "for $VAR in $CONTAINER.iter() { @BODY }"
               :var replacement
               :container (make-untranslated-ast container)
               :body
               (traversal:handle-block-children
                children cpp
                (list (cons var replacement)))))))
    ;; Forward constant loops.
    ((or
      ;; Valhalla uses this style.
      (cpp #.(fmt "~
for ($_ $VAR = $CONTAINER.cbegin();
    $VAR != $CONTAINER.cend();
    $VAR++) {
  @BODY
}")
           :var var
           :container container
           :body children)
      (cpp #.(fmt "~
for ($_ $VAR = $CONTAINER.cbegin();
    $VAR != $CONTAINER.cend();
    ++$VAR) {
  @BODY
}")
           :var var
           :container container
           :body children)
      ;; Openuxas uses this style.
      (cpp #.(fmt "~
for ($_ $VAR = $CONTAINER.cbegin(), $END_VAR = $CONTAINER.cend();
    $VAR != $END_VAR;
    $VAR++) {
  @BODY
}")
           :var var
           :end-var end-var
           :container container
           :body children))
     (progn end-var)                    ;ignore
     (let ((replacement (l:forward (rust-lens:rust-lens) var)))
       (ensure-expression-statement
        (rust* "for $VAR in $CONTAINER.iter() { @BODY }"
               :var replacement
               :container (make-untranslated-ast container)
               :body
               (traversal:handle-block-children
                children cpp
                (list (cons var replacement)))))))
    ;; Backward mutable loops.
    ((cpp #.(fmt "~
for ($_ $VAR = $CONTAINER.rbegin(); $VAR != $CONTAINER.rend(); $VAR++) {
  @BODY
}")
          :var var
          :container container
          :body children)
     (let ((replacement (l:forward (rust-lens:rust-lens) var)))
       (ensure-expression-statement
        (rust "for $VAR in $CONTAINER.iter().rev() { @BODY }"
              :var replacement
              :container (make-untranslated-ast container)
              :body
              (traversal:handle-block-children
               children cpp
               (list (cons var replacement)))))))))

(defun translate-any-for-loop-to-while (cpp)
  "Translate a C++ for loop to a Rust while loop.
If there are continue statements in the body, arrange to insert
copies of the update expression before them."
  (labels ((while-body-children (body update)
             "List of children for the Rust while AST."
             (nest
              (mapcar #'ensure-expression-statement)
              (append
               (flatten-ast (reset-body-formatting body))
               (flatten-ast update))))
           (build-while-body (body update)
             "Body AST for the Rust while AST."
             (make 'cpp-compound-statement
                   :children (while-body-children body update)))
           (extract-initializer-statements (initializer)
             (when initializer
               (iter (for ast in (flatten-ast initializer))
                     (collecting
                       (make-untranslated-ast
                        (ensure-expression-statement ast))))))
           (build-while (condition update body)
             (ensure-expression-statement
              (copy (rust* #?"while $CONDITION {\n}"
                           :condition (make-untranslated-ast condition))
                    :body
                    (make-untranslated-ast
                     (build-while-body body update)))))
           (wrap-block (asts)
             "If needed, wrap ASTs in a block for scoping."
             (if (single asts) (car asts)
                 (ensure-expression-statement
                  (make 'rust-block :children asts)))))
    (declare (ftype (-> (list) (values ast &optional)) wrap-block))
    (match cpp
      ((cpp-for-statement
        :cpp-initializer initializer
        :cpp-condition condition
        :cpp-update update
        :cpp-body body)
       (let ((body (add-continue-preamble cpp body update)))
         (wrap-block
          (append1
           (extract-initializer-statements initializer)
           (build-while condition update body))))))))

(defmethod translate/top-down ((cpp cpp-for-statement))
  (cond
    ;; Leave translating for-range loops to lenses.
    ((typep cpp 'cpp-for-range-loop) nil)
    ;; Leave infinite loops for lenses.
    ((infinite-loop? cpp) nil)
    ;; Try translating interactively (only used for testing).
    ((translate-for-loop-interactively cpp))
    ;; Translate imperative for loop idioms.
    ((translate-index-for-loop-to-rust-range cpp))
    ;; Handle iterator idioms.
    ((translate-for-loop-iterator-idioms cpp))
    ;; If we can't do better, translate as a while loop.
    ((translate-any-for-loop-to-while cpp))))

(defmethod translate/top-down ((cpp cpp-do-statement))
  "Translate a C++ do statement to a Rust while loop.
If there are `continue' statements in the body, arrange to insert
copies of the exit test before them."
  (when (infinite-loop? cpp)
    (return-from translate/top-down nil))
  (match cpp
    ((cpp-do-statement
      :body body
      :condition maybe-break)
     (let* ((body (reset-body-formatting body))
            (maybe-break
              (cpp* "if (!$CONDITION) { break; }"
                    :condition maybe-break)))
       (ensure-expression-statement
        (copy (rust* "loop {}")
              :body
              (make-untranslated-ast
               (copy body
                     :children
                     (append1 (children
                               (add-continue-preamble cpp body maybe-break))
                              maybe-break)))))))))

(defun strip-cram-annotations (ast)
  "Strip any __CRAM___ preprocessor annotations around AST."
  (if (and (typep ast 'call-ast)
           (string^= "__CRAM__" (source-text (call-function ast))))
      (strip-cram-annotations
       (only-elt (call-arguments ast)))
      ast))

(-> iterator (ast) (values (or null ast) (or null iterator-direction)
                           &optional))
(defun iterator? (var)
  "If AST is an iterator, return the container it iterates.
As a second value, return the direction (or nil)."
  (nest
   (let ((var (strip-cram-annotations var))
         (direction nil))
     (declare ((or null iterator-direction) direction)))
   (attrs:with-attr-table *original-attrs*)
   (labels ((iterator? (var)
              (when-let* ((orig (get-original var))
                          (decl (get-declaration-ast :variable orig)))
                (or
                 ;; Get the container name from the initializer.
                 (iter (for init-decl in
                            (collect-if (of-type 'cpp-init-declarator) decl))
                       (thereis (some #'iter-expr-container (rhs init-decl))))
                 ;; Get the container name from comparisons.
                 (iter (for usage in (collect-var-uses (attrs-root*) orig))
                       (thereis
                        (match (get-parent-ast (attrs-root*) usage)
                          ((cpp* "$VAR != $CONTAINER.end()" :var v :container c)
                           (setf direction :up)
                           (and (eql v usage) c))
                          ((cpp* "$VAR != $CONTAINER.rend()" :var v :container c)
                           (setf direction :down)
                           (and (eql v usage) c))))))))
            (iter-expr-container (expr)
              (match expr
                ((or (cpp* "$CONTAINER.begin()" :container container)
                     (cpp* "$CONTAINER.cbegin()" :container container)
                     (cpp* "$CONTAINER.end()" :container container)
                     (cpp* "$CONTAINER.cend()" :container container))
                 (setf direction :up)
                 container)
                ((or (cpp* "$CONTAINER.rbegin()" :container container)
                     (cpp* "$CONTAINER.crbegin()" :container container)
                     (cpp* "$CONTAINER.rend()" :container container)
                     (cpp* "$CONTAINER.crend()" :container container))
                 (setf direction :down)
                 container)
                ((identifier-ast)
                 (iterator? expr))))))
   (let ((container (iterator? var)))
     (when container
       (assert direction))
     (values container direction))))

(defun reverse-iterator? (var)
  (eql :down (nth-value 1 (iterator? var))))

(defun translate-arg-to-array-dereference (ast arg)
  "Translate the argument of a dereference to an array lookup."
  (declare (ignore ast))
  (nest
   (mvlet ((container direction (iterator? arg))))
   (when container)
   (let ((index
           (ecase direction
             (:up arg)
             (:down
              (constant-fold-rust
               (cpp* "$INDEX - 1" :index arg)))))))
   (cpp* "$CONTAINER[$INDEX]"
         :container (tree-copy (copy-ast container))
         :index index)))

(defmethod translate/top-down ((ast cpp-pointer-expression))
  "Rewrite pointer dereferences on iterators into list access."
  (match ast
    ((cpp-pointer-expression
      :operator (cpp-*)
      :argument arg)
     (translate-arg-to-array-dereference ast arg))))

(defmethod translate/top-down ((ast cpp-field-expression))
  "Translate -> field expressions on iterators to array index dereferences."
  (match ast
    ((cpp-field-expression
      :operator (cpp-->)
      :argument arg)
     (when-let (arg (translate-arg-to-array-dereference ast arg))
       (copy-ast ast :argument arg)))))

(defmethod translate/top-down ((ast cpp-update-expression))
  "Rewrite increments of reverse iterators into decrements."
  (flet ((increment->decrement (ast)
           (match ast
             ((cpp* "$VAR++" :var var)
              (cpp* "$VAR--" :var var))
             ((cpp* "++$VAR" :var var)
              (cpp* "--$VAR" :var var)))))
    (match ast
      ((or (cpp* "$VAR++" :var var)
           (cpp* "++$VAR" :var var))
       (when (reverse-iterator? var)
         (increment->decrement ast))))))

(defmethod translate/top-down ((ast cpp-preproc-include))
  ;; TODO This needs to be a top-down include since later we will want
  ;; to look at what is actually being used in the file.
  (let ((path (cpp-path ast)))
    (string-case (source-text path)
      ;; ("<iostream>" (rust "use std::io;"))
      (t :remove))))

(defun flatten-shl (ast)
  (nlet rec ((ast ast)
             (acc nil))
    (ematch ast
      ((cpp-binary-expression
        (cpp-left left)
        (cpp-operator (cpp-<<))
        (cpp-right right))
       (rec left (cons right acc)))
      ((type expression-ast)
       (cons ast acc)))))

(defun drop-deref (x)
  (match x
    ((cpp* "__CRAM__DEREF($1)" x) x)
    (otherwise x)))

(defun drop-clone (x)
  (match x
    ((cpp* "__CRAM__CLONE($1)" x) x)
    (otherwise x)))

(deftype dest-kind ()
  '(member :stdout :stderr :ostream))

(defun std? (name ast)
  "Is AST equivalent to std::$NAME?"
  (let ((ast (drop-clone ast)))
    (or
     (match ast
       ((cpp* "std::$1" std-name)
        (source-text= std-name name)))
     (and (source-text= ast name)
          (let ((qs (ts::namespace-qualifiers ast)))
            (or (no qs)
                (equal (mapcar #'source-text qs)
                       '("std"))))))))

(defun translate-shl-to-print (ast)
  (labels ((ostream? (ast)
             "In AST an ostream?"
             (when-let (orig (get-original ast))
               (attrs:with-attr-table *original-attrs*
                 (source-text= "std::ostream"
                               (infer-type* orig)))))
           (endl? (ast) (std? "endl" (drop-clone ast)))
           (send-to (macro dest)
             "Rewrite MACRO and EXPRS to write to DEST."
             (if (no dest)
                 (values macro nil)
                 (ecase-of dest-kind (dest-kind dest)
                   (:stdout
                    (values macro nil))
                   (:stderr
                    (values
                     (string-ecase macro
                       ("print" "eprint")
                       ("println" "eprintln"))
                     nil))
                   (:ostream
                    (values
                     (string-ecase macro
                       ("print" "write")
                       ("println" "writeln"))
                     dest)))))
           (control-string+args (exprs)
             "Build a control string from EXPRs, concatenating string
literals with {} holes for other expressions."
             (let ((s (make-string-output-stream))
                   (args (queue)))
               (dolist (expr exprs)
                 (match expr
                   ((cpp-string-literal :text text)
                    (write-string (string-trim '(#\") text) s))
                   (otherwise
                    (write-string "{}" s)
                    (enq expr args))))
               (values
                (prog1 (get-output-stream-string s)
                  (close s))
                (qlist args))))
           (macro-name+exprs (exprs)
             "Return the macro to use to print EXPRS, and the arguments to it.
E.g. if `std::endl' is among EXPRS, then the macro will be `println!'
or `writeln!' and `std::endl' will be removed from the exprs."
             (multiple-value-bind (exprs-butlast last-exprs)
                 (halves exprs -1)
               (if (notany #'endl? exprs)
                   (values "print" exprs)
                   (if (and (endl? (car last-exprs))
                            (not (find-if #'endl? exprs-butlast)))
                       (values "println" exprs-butlast)
                       (values "print"
                               (mapcar (lambda (expr)
                                         (if (endl? expr)
                                             (make 'cpp-string-literal
                                                   :text "\\n")
                                             expr))
                                       exprs))))))
           (build-macro-invocation (macro dest control-string args)
             (make 'rust-macro-invocation
                   :rust-macro (make 'rust-identifier :text macro)
                   :children
                   (list
                    (make 'rust-token-tree
                          :children
                          ;; TODO Shouldn't need these text fragments.
                          `(,(make 'text-fragment :text "(")
                             ,@(and dest
                                    (list dest
                                          (make 'text-fragment :text ", ")))
                             ,@(if (emptyp control-string)
                                   nil
                                   (list (make 'rust-string-literal
                                               :text (string+ #\" control-string #\"))))
                             ,@(when args
                                 (list (make 'text-fragment :text ", ")))
                             ,@(iter (for (arg . more?) on args)
                                     (collecting
                                         (etypecase arg
                                           (rust-wrapper (get-rust-child arg))
                                           (rust-ast arg)
                                           (cpp-ast
                                            (make-untranslated-ast arg))))
                                     (when more?
                                       (collecting
                                         (make 'text-fragment :text ", "))))
                             ,(make 'text-fragment :text ")"))))))
           (dest-kind (dest)
             (assure dest-kind
               (econd
                 ((std? "cout" dest) :stdout)
                 ((std? "cerr" dest) :stderr)
                 ((std? "clog" dest) :stderr)
                 ((ostream? dest) :ostream)))))
    (let ((flat (flatten-shl ast)))
      (when (some (lambda (ast)
                    (or (some  (op (std? _ ast)) '("cout" "cerr"))
                        (endl? ast)
                        (ostream? ast)
                        (typep ast 'string-ast)))
                  flat)
        (destructuring-bind (dest . exprs) flat
          (mvlet* ((dest (drop-deref dest))
                   (macro exprs (macro-name+exprs exprs))
                   (control-string args (control-string+args exprs))
                   (macro dest (send-to macro dest))
                   ;; Args are implicitly borrowed.
                   (args (mapcar #'drop-clone args)))
            (build-macro-invocation macro dest control-string args)))))))

(defmethod translate/top-down ((ast cpp-binary-expression))
  (when (typep (cpp-operator ast) 'cpp-<<)
    (translate-shl-to-print ast)))

(defmethod translate/top-down ((template cpp-template-declaration))
  template
  (when (string^= "friend "
                  (source-text
                   (first
                    (remove-if (of-type 'cpp-template-parameter-list)
                               (children template)))))
    (return-from translate/top-down
      :remove))
  ;; Template declaration nodes can only have one definition child. So
  ;; we first expand the given definition child. If it expands into
  ;; multiple ASTs (as an AST fragment) then we need to wrap copies of
  ;; the template declaration around any new definition ASTs, such
  ;; that each new template-declaration instance is valid in having
  ;; only one child. Template parameters are ultimately propagated
  ;; into the child ASTs by `parameterize-ast' during the context-free
  ;; translation phase.
  (labels ((wrap-new-definition (template new-definition)
             (etypecase new-definition
               ((or cpp-ast translated-ast)
                (let* ((template (tree-copy template)))
                  (with template
                        (assure ast
                          (find-if (of-type 'definition-ast)
                                   template))
                        new-definition)))
               (rust-ast
                (wrap-new-definition template (make-rust-wrapper new-definition))))))
    (if-let* ((definitions
               (filter (of-type 'definition-ast)
                       (direct-children template)))
              (child (only-elt definitions))
              (translation (translate/top-down child)))
      (if (typep translation 'ast-fragment)
          (progn
            (dolist (nd (children translation))
              (norecurse! nd))
            (make 'ast-fragment
                  :children
                  (mapcar (lambda (child)
                            (let ((wrapped (wrap-new-definition template child)))
                              (if (ignore-some-conditions (rule-matching-error)
                                    (output-transformation wrapped))
                                  wrapped
                                  child)))
                          (children translation))))
          (wrap-new-definition template translation))
      (call-next-method))))

(defun param-methods (db param)
  "Return methods invoked on PARAM."
  (labels ((call-fn (call) (car call))
           (method-call? (call)
             (eql :this (second call))))
    (mapcar #'call-fn
            (filter #'method-call?
                    (select-results
                     db
                     'invoked-methods
                     '(:method :offset)
                     :param param)))))

(defun register-type-requirements (ast)
  (attrs:with-attr-table *original-attrs*
    (let ((root (attrs:attrs-root*)))
      (labels ((get-type-def (type)
                 (with-aggressive-analysis ()
                   (get-declaration-ast :type type)))
               (sorted-template-params (db template)
                 "Get TEMPLATE's params from DB, sorting by position."
                 (sort (mapply #'cons
                               (select-results db
                                               'param-ids
                                               '(:param :param-position)
                                               :template template))
                       #'<
                       :key #'cdr))
               (record-constraint (db template type param param-position)
                 "Record constraints on PARAM in TEMPLATE according to DB."
                 (let ((methods (param-methods db param)))
                   (if (no methods)
                       (record-template-param-trait template param-position nil nil)
                       (dolist (method methods)
                         (and-let* ((trait (get-method-trait type method))
                                    (type-def (get-type-def type))
                                    ;; It's not a constraint if it's a parameter.
                                    ((not (typep type-def 'parameter-ast)))
                                    (method (canonicalize-method-name type-def method)))
                           (record-type-trait template type method trait)
                           (record-template-param-trait template
                                                        param-position
                                                        trait method))))))
               (record-constraints (db template types params.positions)
                 "Record template parameter constraints from DB."
                 (iter (for type in types)
                       (for (param . param-position) in params.positions)
                       (record-constraint db template type param param-position))
                 (values types params.positions)))
        (match (get-original ast)
          ((cpp-template-type
            (cpp-name type)
            (cpp-arguments
             (cpp-template-argument-list
              (children type-args))))
           ;; For each parameter of the template, record the invoked
           ;; methods as constraints on the parameter type.
           (when-let* ((type-def
                        (with-aggressive-analysis ()
                          (get-declaration-ast :type type)))
                       (template (find-enclosing 'cpp-template-declaration
                                                 root
                                                 type-def)))
             (let* ((params-db (analyze-template-params/attr template))
                    (params (sorted-template-params params-db template)))
               (when
                   ;; There is a constraint for every index.
                   (set-equal (iota (length type-args))
                              (nub (mapcar #'cdr params)))
                 (record-constraints params-db template type-args params)))))
          ;; ((cpp* "$FN<@TARGS>(@ARGS)"
          ;;        :fn fn
          ;;        :targs targs
          ;;        :args _)
          ;;  (let ((attrs:*attrs* *original-attrs*))
          ;;    (when-let* ((fn (get-original fn))
          ;;                (fn-def (get-declaration-ast :function fn))
          ;;                (template (find-enclosing 'cpp-template-declaration
          ;;                                          (attrs-root*) fn-def)))
          ;;      (register-requirements-from-template targs template))))
          ((cpp* "$FN(@ARGS)"
                 :fn fn
                 :args args)
           ;; If FN is a function, register the inferred types of ARGS
           ;; as constraints on the corresponding template parameters.
           (and-let* ((fn-def
                       (lastcar
                        (get-declaration-asts :function fn)))
                      ((typep fn-def 'function-declaration-ast))
                      (fn-params (function-parameters fn-def))
                      (template
                       (find-enclosing 'cpp-template-declaration
                                       (attrs-root*)
                                       fn-def)))
             (let* ((arg-types (mapcar #'infer-type* args))
                    (params-db (analyze-template-params/attr template)))
               (record-constraints params-db template
                                   arg-types
                                   (sorted-template-params params-db template))))))))))

(defun record-template-param-trait (template param-position trait method)
  "Record the fact that TEMPLATE needs in param PARAM (in PARAMS) to
implement the trait TRAIT, because of calling METHOD."
  (dbg:note 2 "Template `~a` needs trait ~a for method ~a on parameter ~a"
            (apply #'string+ (take 2 (lines (source-text template))))
            (source-text trait)
            (source-text method)
            param-position)
  (upsertf *constraints-db*
           'template-param-traits
           :template template
           :position param-position
           :trait trait
           :method method))

(defun record-type-trait (template type method trait)
  "Record the fact that TYPE must implement METHOD for TRAIT."
  (dbg:note 2 "Need impl ~a for ~a with method ~a"
            (source-text trait)
            (source-text type)
            (source-text method))
  (when-let (type-def (get-declaration-ast :type type))
    (upsertf *constraints-db*
             'type-traits
             :type-def type-def
             :trait trait
             :method method
             :method-name (source-text method)
             :template template)))

(defun group-methods-into-impl-items (name struct methods &key from)
  "Group METHODS, methods of STRUCT (named NAME), into impl blocks.
If FROM is provided, it is another impl block to use as a protoype."
  (assert (typep struct 'cpp-ast))
  (let* ((methods (filter #'function-body methods))
         ;; NB This will eliminate stub methods in classes. Is this
         ;; always what we want?
         (methods (remove-if (op (no (children (function-body _))))
                             methods))
         (methods-by-trait
           (mapcar (op (cons (field-trait struct (car _1)) _1))
                   (assort methods :key (op (field-trait struct _)))))
         (no-trait-methods
           (assoc nil methods-by-trait))
         (methods-by-trait (remove no-trait-methods methods-by-trait))
         (no-trait-methods (cdr no-trait-methods))
         (trait-impl-blocks
           (iter (for (trait . methods) in methods-by-trait)
                 (let ((single (single methods)))
                   (collecting
                     (make-impl-item
                      name
                      (append
                       (mappend (lambda (method)
                                  (when-let (template (field-template struct method))
                                    (extract-associated-types/defaults
                                     template
                                     method
                                     :no-prefix single)))
                                methods)
                       (mapcar (op (add-self-parameter _ :trait trait))
                               methods))
                      :trait trait
                      :original struct
                      :from from))))))
    (if no-trait-methods
        (cons
         (make-impl-item name
                         (mapcar #'add-self-parameter
                                 (mapcar #'patch-export no-trait-methods))
                         :original struct
                         :from from)
         trait-impl-blocks)
        trait-impl-blocks)))

(defun split-impl-item (item)
  "Split an impl item into multiple impl items based on method
traits.

If ITEM already implements a trait, it is left alone."
  (labels ((split-impl-item (item)
             ;; There may not be an original if we are translating an
             ;; external definition.
             (if-let (struct (get-original item :error nil))
               (group-methods-into-impl-items
                (rust-type item)
                struct
                (children (rust-body item))
                :from item)
               (list item))))
    (if (rust-trait item) item
        (list-fragment (split-impl-item item)))))

(defun list-fragment (asts)
  "Wrap ASTs in an AST fragment if necessary."
  (ematch asts
    (nil asts)
    ((list ast) ast)
    ((list* _ _)
     (make 'ast-fragment
           :children asts))))

(defun canonicalize-method-name (type-def method-name)
  "Canonicalize METHOD-NAME to the exact AST that appears in TYPE-DEF's definition."
  (or (find method-name (extract-type-methods type-def) :test #'source-text=)
      (error "~a is not a declared method of ~a"
             (source-text method-name)
             type-def)))

(defun get-method-trait (type method)
  "Get the trait that METHOD on TYPE belongs to."
  (or (aget method *method-trait-overrides* :test #'source-text=)
      (let ((default-text (string-capitalize (source-text method)))
            (prompt
              (fmt "What trait should ~a::~a implement?"
                   (source-text type)
                   (source-text method))))
        (declare (ignorable prompt))
        (if traversal:*interactive*
            (setf *method-trait-overrides*
                  (acons method
                         (traversal:read-string :prompt prompt
                                               :default-text default-text)
                         *method-trait-overrides*))
            default-text))))

(defmethod translate/top-down :before ((ast cpp-template-type))
  "Correlate template arguments with parameters, look up their type
definitions, and apply trait constraints."
  (register-type-requirements ast))

(defun make-derive (traits)
  (rust (fmt "#[derive(~{~a~^, ~})]"
             (mapcar #'source-text traits))))

(defmethod flatten-ast ((ast cpp-field-declaration))
  (let ((ids (cpp-declarator ast)))
    (if (and (listp ids)
             (every (of-type 'cpp-field-identifier) ids))
        (mapcar
         (lambda (id)
           (tree-copy
            (copy ast :cpp-declarator (list id))))
         ids)
        (list ast))))

(defgeneric add-self-parameter (fn &key trait mut)
  (:documentation "Add a SELF parameter to FN")
  (:method :around ((fn t) &key trait mut)
    (declare (ignore trait mut))
    (if (find-if (of-type 'rust-self-parameter) fn)
        fn
        (call-next-method)))
  (:method ((fn rust-function-item) &key trait mut)
    (declare (ignore trait mut))
    fn)
  (:method ((fn cpp-function-definition) &key trait mut)
    (if-let (params
             (find-if (of-type 'cpp-parameter-list) fn))
      ;; Insert a self parameter.
      (with fn
            params
            (copy params
                  :children (cons (make-rust-wrapper
                                   ;; TODO Conditionalize based on the trait.
                                   (make-self-parameter
                                    :reference
                                    (trait-wants-reference? trait)
                                    :mutable
                                    (or mut
                                        (trait-wants-mutable? trait)
                                        (not (const-method? fn)))))
                                  (children params))))
      fn)))

(defun lift-operator-overload (method type trait)
  (labels ((use-trait-method-name (method op-trait)
             "Replace the name of METHOD with the name of OP-TRAIT's method."
             (let ((name-ast (assure ast (definition-name-ast method))))
               (with method
                     name-ast
                     (make-rust-wrapper
                      (assign-uid-from
                       name-ast
                       (make 'rust-identifier :text
                             (trait-method-name op-trait)))))))
           (make-output-type ()
             (make 'rust-scoped-type-identifier
                   :rust-name (make 'rust-type-identifier :text "Output")
                   :rust-path (rust* "Self")))
           (return-associated-type (method)
             "Specify that METHOD returns the Output associated
type (this will be added elsewhere to the impl block)."
             (let ((output-type (make-output-type)))
               (copy method
                     :cpp-type
                     (make-rust-wrapper
                      (assign-uid-from
                       (cpp-type method)
                       output-type)))))
           (with/self (method type-id self)
             "If type-id is in a template type, replace it."
             (let ((target
                     (or (when-let ((parent (get-parent-ast method type-id)))
                           (when (typep parent 'cpp-template-type)
                             parent))
                         type-id)))
               (with method target (assign-uid-from target (tree-copy self)))))
           (replace-types-with-self (method type)
             "Replace TYPE in method with Self."
             (let ((self
                     (assign-uid-from type
                                      (make-rust-wrapper
                                       (make 'rust-type-identifier :text "Self")))))
               (reduce (lambda (method type-id)
                         (match (find-enclosing 'parameter-ast method type-id)
                           ;; If this a reference declarator parameter, but
                           ;; we don't want a reference, drop the
                           ;; reference.
                           ((and param
                                 (cpp-parameter-declaration
                                  (cpp-declarator
                                   (cpp-reference-declarator
                                    (children (list id))))))
                            (if (trait-wants-reference? trait :rhs t)
                                (with/self method type-id self)
                                (with/self method
                                           param
                                           (assign-uid-from
                                            param
                                            (copy (with/self param type-id self)
                                                  :cpp-declarator id)))))
                           (otherwise (with/self method type-id self))))
                       ;; The type ASTs to replace with `Self'.
                       (collect-if (op (and (typep _1 'type-identifier-ast)
                                            (source-text= _1 type)))
                                   method)
                       :initial-value method))))
    (~> method
        (use-trait-method-name trait)
        (return-associated-type)
        (replace-types-with-self type))))

(defun make-display-item (cpp-fn)
  (pushnew (rust* "use std::fmt") *new-imports* :test #'source-text=)
  (when (and (typep cpp-fn 'cpp-function-definition)
             (string$= (definition-name cpp-fn) "operator<<")
             (string*= "ostream" (source-text (cpp-type cpp-fn))))
    (destructuring-bind (ostream-param printee-param)
        (function-parameters cpp-fn)
      (let* ((type (cpp-type printee-param))
             (name (parameter-name printee-param))
             (dest-name (parameter-name ostream-param))
             (cpp-body
               (mapcar (lambda (ast)
                         (match ast
                           ((cpp-identifier :text (equal name))
                            (make-rust-wrapper
                             (make 'rust-identifier :text "self")))))
                       (find-if (of-type 'cpp-compound-statement)
                                cpp-fn))))
        (rust* "impl fmt::Display for $TYPE {
    fn fmt(&self, $DEST: &mut fmt::Formatter<'_>) -> fmt::Result {
        @BODY
    }
}"
               :type (make-untranslated-ast type)
               :dest dest-name
               :body
               ;; TODO Retain side-effectful expressions?
               (mapcar #'make-untranslated-ast
                       (mapcar (op (make 'cpp-expression-statement
                                         :children (list _)))
                               (extract-writers cpp-body))))))))

(defun extract-writers (ast)
  (labels ((extract-writers (ast)
             (if (typep ast 'binary-ast)
                 (list ast)
                 (mappend #'extract-writers
                          (children ast)))))
    (extract-writers ast)))

(defun make-impl-item (type methods &key trait original from)
  "Make an impl item for CLASS with METHODS."
  (labels ((maybe-wrap-ast (ast)
             (if (typep ast 'cpp-ast)
                 (make-untranslated-ast ast)
                 ast))
           (make-rust-impl-item (&rest kwargs &key &allow-other-keys)
             (if from
                 (apply #'copy from kwargs)
                 (apply #'make 'rust-impl-item kwargs)))
           (make-impl-item (type methods &key (trait trait))
             (etypecase trait
               (null
                (make-rust-impl-item
                 :rust-type (maybe-wrap-ast type)
                 :rust-trait trait
                 :rust-body
                 (make 'rust-declaration-list
                       :children
                       (mapcar #'maybe-wrap-ast methods))))
               (ast
                (make-impl-item type methods :trait (find-trait trait)))
               (string
                (if-let (trait-data (find-trait trait))
                  (make-impl-item type methods :trait trait-data)
                  (make-rust-impl-item
                   :rust-type (maybe-wrap-ast type)
                   :rust-trait
                   (make 'rust-type-identifier :text trait)
                   :rust-body
                   (make 'rust-declaration-list
                         :children
                         (mapcar #'maybe-wrap-ast methods)))))
               (trait
                (let ((name (trait.name trait)))
                  (pushnew (rust* "use std::ops::$1" name)
                           *new-imports*)
                  (let ((base-impl-item
                          ;; NB Type parameters will be added later by
                          ;; parameterize-ast.
                          (rust* "impl $TRAIT for $TYPE { type Output = Self; } "
                                 :trait name
                                 :type (maybe-wrap-ast type)))
                        (lifted-methods
                          (mapcar (lambda (method)
                                    (maybe-wrap-ast
                                     (if (typep method 'cpp-ast)
                                         (lift-operator-overload method type trait)
                                         method)))
                                  methods)))
                    (copy base-impl-item
                          :rust-body
                          (copy (rust-body base-impl-item)
                                :children
                                (append (children (rust-body base-impl-item))
                                        lifted-methods)))))))))
    (assign-uid-from original (make-impl-item type methods))))

(defun wrap-traits (traits &rest asts)
  (make 'ast-fragment
        :children
        (cons (make-derive traits)
              asts)))

(defun field-trait (struct field)
  (match field
    ((and (cpp-function-definition)
          (access #'definition-name-ast
                  (and name (cpp-operator-name))))
     (operator-trait name field))
    ((and (cpp-function-definition)
          (access #'definition-name-ast name))
     (caar
      (select-results *constraints-db*
                      'type-traits
                      '(:trait)
                      :method-name (source-text name)
                      :type-def struct)))
    ((rust-function-item
      (rust-name name))
     (let ((name (l:backward (rust-lens:rust-lens) name)))
       (caar
        (select-results *constraints-db*
                        'type-traits
                        '(:trait)
                        :method-name (source-text name)
                        :type-def struct))))))

(defun field-template (struct field)
  (flet ((field-template (struct name)
           (caar
            (select-results *constraints-db*
                            'type-traits
                            '(:template)
                            :type-def struct
                            :method-name (source-text name)))))
    (match field
      ((and (cpp-function-definition)
            (access #'definition-name-ast name))
       (field-template struct name))
      ((rust-function-item
        (rust-name name))
       (field-template struct name)))))

(defun make-new-method (name member-decls)
  "Create a new method for type NAME with members MEMBER-DECLS."
  (let ((names.types
          (filter-map (lambda (decl)
                        (match decl
                          ((cpp-field-declaration
                            :cpp-declarator
                            (list (and name (cpp-field-identifier)))
                            :cpp-type type)
                           (cons name type))))
                      member-decls)))
    (rust "fn new(@PARAMS) -> $TYPE { $TYPE{@ARGS}}"
          :type (make-untranslated-ast name)
          :params
          (mapcar (op (make 'rust-parameter
                            :rust-pattern
                            (make 'rust-identifier
                                  :text (source-text (car _1)))
                            :rust-type (make-untranslated-ast (cdr _1))))
                  names.types)
          :args
          (mapcar
           (lambda (name.type)
             (make 'rust-shorthand-field-initializer
                   :children (list
                              (make 'rust-identifier
                                    :text
                                    (source-text (car name.type))))))
           names.types))))

(defun constructor? (method)
  (match method
    ((cpp-function-definition)
     (find-if (of-type 'cpp-field-initializer-list)
              (direct-children method)))))

(defgeneric normalize-field (field)
  (:documentation "Normalize FIELD, a function definition or
declaration, into a list of function definitions (with dummy bodies in
the case of declarations).")
  (:method ((field cpp-function-definition))
    (if (no (cpp-body field))
        (list
         (copy field
               :cpp-body (make 'cpp-compound-statement
                               :children nil)))
        (list field)))
  (:method ((field cpp-field-declaration))
    (let-match (((cpp-field-declaration
                   :cpp-type type
                   :cpp-declarator
                   (and declarators (type list)))
                 field))
      (mapcar
       (lambda (decl)
         (ematch decl
           ((cpp-function-declarator
             :cpp-declarator
             (cpp-field-identifier :text name-string)
             :cpp-parameters params)
            (make 'cpp-function-definition
                  :cpp-type type
                  :cpp-declarator
                  (copy decl
                        :cpp-declarator
                        (make 'cpp-identifier :text name-string)
                        :cpp-parameters
                        (copy params))
                  :cpp-body
                  (make 'cpp-compound-statement
                        :children nil)))))
       declarators)))
  (:method ((field cpp-declaration))
    (match field
      ((cpp-declaration
        :cpp-type type
        :cpp-declarator decls)
       (unless (every (of-type 'cpp-function-declarator) decls)
         (fail))
       (mappend (lambda (decl)
                  (normalize-field
                   (make 'cpp-function-definition
                         :cpp-type (or type (make 'cpp-primitive-type :text "int"
                                                  :after-text " "))
                         :cpp-declarator decl)))
                decls))
      (otherwise (call-next-method)))))

(defun translate-struct-specifier (struct &aux (traits '("Clone")))
  "If AST has fields that are function declarators, move them into a
succeeding impl block."
  ;; TODO Split out decls that are function prototypes. Translate them
  ;; into an impl block. Add something (here or as a lens?) that
  ;; translates function prototypes into Rust function stubs.
  (labels ((declares-function? (field-decl)
             (or (typep field-decl 'cpp-function-definition)
                 (and (slot-exists-p field-decl 'cpp-declarator)
                      (some (of-type 'cpp-function-declarator)
                            (cpp-declarator field-decl)))))
           (compute-impl-blocks (name function-decls other-decls)
             (declare (ignorable other-decls))
             (group-methods-into-impl-items
              name
              struct
              (mappend #'normalize-field function-decls))))
    (ematch struct
      ;; Ignore a struct (declaration) without a body.
      ((cpp-struct-specifier :cpp-body nil))
      ((cpp-struct-specifier
        :cpp-name name
        :cpp-body
        (and field-decl-list (cpp-field-declaration-list)))
       (mvlet* ((flat-decls
                 ;; Make sure no declaration declares more than one
                 ;; member/method.
                 (remove-if (of-type 'comment-ast)
                            (mappend #'flatten-ast
                                     (children field-decl-list))))
                (function-decls other-decls
                 (partition #'declares-function? flat-decls))
                ;; TODO Should we always add a new method, even if there are
                ;; no other function methods?
                (member-only-struct
                 (copy struct
                       :cpp-body
                       (copy field-decl-list
                             :children other-decls))))
         (if (no function-decls)
             (if (typep (predecessor (sel:genome (attrs-root*)) struct) 'rust-attribute-item)
                 member-only-struct
                 (wrap-traits traits member-only-struct))
             (apply #'wrap-traits
                    traits
                    member-only-struct
                    (compute-impl-blocks name function-decls other-decls))))))))

(defmethod translate/top-down ((ast cpp-struct-specifier))
  (if (no (cpp-body ast))
      :remove
      (translate-struct-specifier ast)))

(defmethod translate/top-down ((class cpp-class-specifier))
  (assign-uid-from class
                   (make 'cpp-struct-specifier
                         :children (direct-children class) ;conserve export
                         :cpp-name (cpp-name class)
                         :cpp-body
                         (when (cpp-body class)
                           (copy (cpp-body class)
                                 :children
                                 (cons (make 'cpp-access-specifier
                                             :cpp-keyword
                                             (make 'cpp-private))
                                       (children (cpp-body class))))))))

(defmethod translate/top-down ((ast cpp-access-specifier))
  :remove)

(defmethod translate/top-down ((ast cpp-friend-declaration))
  "Remove friend declarations, since we are treating all members as
public anyway."
  :remove)

(defmethod translate/top-down ((ast cpp-enum-specifier))
  (norecurse! ast)
  (wrap-traits '("PartialEq" "Copy" "Clone") ast))

(defmethod translate/top-down :before ((call cpp-call-expression))
  (register-type-requirements call))

(defmethod translate/top-down ((call cpp-call-expression))
  (match call
    ;; ((cpp* "std::move(__CRAM__DEREF($1))" ast)
    ;;  (make 'rust-unary-expression
    ;;        :rust-operator (make 'rust-*)
    ;;        :children (list (make-untranslated-ast ast))))
    ((cpp* "__CRAM__DEREF($1)" ast)
     (make 'rust-unary-expression
           :operator (make 'rust-*)
           :argument (make-untranslated-ast ast)))
    ((cpp* "__CRAM__CLONE($1)" (and ast (cpp-ast)))
     (rust* "$1.clone()"
            (make-untranslated-ast ast)))
    ((cpp* "__CRAM__CLONE($1)" ast)
     (rust* "$1.clone()" ast))
    ;; TODO These need to take types into account. Or can we assume
    ;; everything that uses STL-compatible method names uses
    ;; STL-compatible signatures?
    ((cpp* "$X.size()" :x x)
     (rust* "$X.len()"
            :x (make-untranslated-ast x)))
    ((cpp* "$X.front()" :x x)
     ;; C++ arrays cannot contain references; they can only contain
     ;; (1) values, (2) pointers, and (3) reference wrappers. Since we
     ;; are only supporting reference-like pointers at the moment, we
     ;; only need to consider references and reference wrappers.
     (if (evaluated? call)
         (rust* "$X[0].clone()" :x (make-untranslated-ast x))
         (rust* "$X[0]" :x (make-untranslated-ast x))))
    ((cpp* "$X.push_back($Y)" :x x :y y)
     (cpp* "$X.push($Y)"
           :x x
           :y y))
    ((cpp* "$X.erase($FROM, $TO)" :x x :from from :to to)
     (when (or (iterator? from) (iterator? to))
       (when-let ((original-from (strip-cram-annotations (get-original from)))
                  (original-to (strip-cram-annotations (get-original to))))
         (rust* "$X.drain($FROM..$TO)"
                :x (make-untranslated-ast x)
                :from
                (assign-uid-from original-from
                                 (make 'rust-integer-literal :text "0"))
                :to
                (make-untranslated-ast (copy-ast original-to))))))
    ;; Strip move calls; they are meant as annotations.
    ((cpp* "std::move($X)" :x x) x)
    ((cpp* "__CRAM__MOVE($X)" :x x) x)))

(defun initializer-list->struct-expression
    (ast type &aux (orig *original-software*))
  (declare (cpp-initializer-list ast)
           (ast type))
  (when-let (result
             (or
              ;; Ignore errors in case TYPE is not part of the tree.
              (ignore-errors
               (get-declaration-ast :type type))
              (when-let (orig-type (get-original type))
                (attrs:with-attr-table *original-attrs*
                  (get-declaration-ast :type orig-type)))))
    (when (typep result '(or cpp-class-specifier cpp-struct-specifier))
      (when-let* ((table (sel/sw/ts::field-table result)))
        (let* ((args
                 ;; XXX This mapcar should not be necessary.
                 (mapcar (op (or (translate/top-down _1) _1))
                         (children ast)))
               (data-member-table
                 (or (@ table :variable)
                     (empty-map)))
               (data-member-ids
                 (mapcar #'only-elt
                         (convert 'list
                                  (or (range data-member-table)
                                      nil))))
               (data-member-ids.args
                 (sort (mapcar #'cons data-member-ids args)
                       (lambda (x y)
                         (path-later-p orig y x))
                       :key #'car)))
          (when (length= args data-member-ids)
            (rust* "$TYPE {@FIELDS}"
                   :type
                   (etypecase type
                     (rust-wrapper type)
                     (cpp-ast (make-untranslated-ast type)))
                   :fields
                   (iter (for (id . arg) in data-member-ids.args)
                         (collecting
                           (make 'rust-field-initializer
                                 :rust-name
                                 (make-untranslated-ast id)
                                 :rust-value
                                 (if (typep arg 'rust-ast) arg
                                     (make-untranslated-ast arg))))))))))))

(defmethod translate/top-down ((ast cpp-initializer-list)
                               &aux (attrs:*attrs* *original-attrs*)
                                 (orig-obj *original-software*))
  (when-let* ((orig-ast (get-original ast))
              (type (infer-type (get-original ast))))
    (or
     ;; Special handling to translate vector template types into
     ;; `vec!'.
     (when (scan "^(?:std::)?vector<.*?>$" (source-text type))
       (rust* "vec![@EXPRS]" :exprs
              ;; TODO How to handle macro token trees gracefully?
              (rest
               (mappend (op (list (make 'text-fragment :text ", ") _))
                        (mapcar #'make-untranslated-ast
                                (direct-children ast))))))
     ;; TODO This may be redundant.
     ;; Try to translate to a struct expression.
     (unless (typep (get-parent-ast orig-obj orig-ast)
                    'cpp-compound-literal-expression)
       (initializer-list->struct-expression ast type)))))

(defmethod translate/top-down ((ast cpp-compound-literal-expression))
  "Try to translate initializer lists into struct expressions."
  (match ast
    ((cpp-compound-literal-expression
      (cpp-type type)
      (cpp-value
       (and init (cpp-initializer-list))))
     (initializer-list->struct-expression init type))))

(defun prepare-main-function (fn)
  "Prepare a MAIN function for translation.
This involves at least:
- Avoiding integer return values
- Introducing Rust equivalents for argc and argv."
  (flet ((wrap-arg-vars (params statements)
           "If PARAM contains argc and argv bindings,
            add Rust equivalents to STATEMENTS."
           (ematch params
             ((list) statements)
             ((list argc-param argv-param)
              (let ((argv (convert 'rust-identifier
                                   (only-elt
                                    (parameter-names argv-param))))
                    (argc (convert 'rust-identifier
                                   (only-elt
                                    (parameter-names argc-param)))))
                (list*
                 (make-rust-wrapper
                  (rust "let $ARGV: Vec<String> = std::env::args().collect();"
                        :argv argv))
                 (make-rust-wrapper
                  (rust "let $ARGC = $ARGV.len();"
                        :argc argc
                        :argv argv))
                 statements))))))
    (match fn
      ;; Already fine.
      ((and (cpp-function-definition
             (cpp-body _)
             (cpp-declarator
              (cpp-function-declarator
               (cpp-parameters
                (cpp-parameter-list
                 (children nil)))))
             (cpp-type
              (or nil
                  (cpp-placeholder-type-specifier)
                  (cpp-primitive-type
                   :text "void")))
             (function-parameters _))
            (access #'definition-name-ast (source-text= "main")))
       nil)
      ;; Needs rewriting.
      ((and (cpp-function-definition
             (cpp-body body)
             (cpp-declarator decl)
             (cpp-type (type ast))
             (function-parameters params))
            (access #'definition-name-ast (source-text= "main")))
       (let ((cpp-type
               (if (exported? fn)
                   (make 'cpp-primitive-type :text "void")
                   nil))
             (cpp-parameters
               (make 'cpp-parameter-list :children nil))
             (cpp-body
               (copy body
                     :children
                     (wrap-arg-vars
                      params
                      (remove-if (of-type 'return-ast) (children body))))))

         (copy fn
               :cpp-type cpp-type
               :cpp-declarator
               (copy decl
                     :cpp-parameters cpp-parameters)
               :cpp-body cpp-body))))))

(defun make-pub-super ()
  (make 'rust-visibility-modifier
        ;; TODO Computed text?
        :children (list (make 'rust-super :text "super"))))

(defun patch-export (fn)
  "Add export to FN when needed.

If FN is a definition of a function declared exported, add an export
keyword."
  (flet ((make-export-specifier ()
           ;; TODO Should be computed text.
           (make 'cpp-export-specifier
                 :text "export")))
    (if (find-if (of-type 'cpp-export-specifier)
                 (direct-children fn))
        fn
        (if (typep fn 'cpp-function-definition)
            (attrs:with-attr-table *original-attrs*
              (let* ((orig-fn (get-original fn)))
                (if (exported? orig-fn)
                    (copy fn
                          :children (list (make-export-specifier)))
                    fn)))
            fn))))

(defmethod translate/top-down ((fn cpp-function-definition))
  (or (and *use-c2rust*
           (c2rust:c-like-function? fn)
           (make-rust-wrapper
            (c2rust:transpile-function
             (get-original fn))))
      (prepare-main-function fn)
      (match fn
        ((satisfies constructor?)
         :remove)
        ;; Special handling for translating `operator<<' print methods to
        ;; `impl fmt::Display' blocks.
        ((and (cpp-function-definition)
              (access #'definition-name-ast
                      (or (source-text= "operator<<")
                          (source-text= "std::operator<<"))))
         (make-display-item fn))
        ;; Special handling for friend functions.
        ((cpp-function-definition
          (cpp-declarator
           (cpp-function-declarator
            (cpp-declarator
             (and id
                  (cpp-qualified-identifier
                   (cpp-name name)
                   (cpp-scope class-name)))))))
         (when-let (type
                    (attrs:with-attr-table *original-attrs*
                      (get-declaration-ast :type
                                           (get-original class-name))))
           (let ((new-fn
                   (with fn
                         id
                         name)))
             (list-fragment
              (group-methods-into-impl-items
               (definition-name-ast type)
               type
               (list new-fn))))))
        (otherwise
         (let ((patched (patch-export fn)))
           (if (eql fn patched)
               (call-next-method)
               patched))))))

(defmethod translate/top-down ((ast cpp-declaration))
  "Handle `constexpr auto' (as Rust requires an explicit type)."
  (match ast
    ((cpp* "constexpr auto $VAR = $VALUE"
           :var _
           :value value)
     (if-let ((type (infer-type value)))
       (copy ast :cpp-type type)
       (fail)))
    (otherwise (call-next-method))))

(defun translate-switch-statement (switch)
  (labels ((collect-break-statements (ast switch-ast)
             (filter (op (member switch-ast (exit-control-flow _)))
                     (collect-if (of-type 'break-ast) ast)))
           (remove-break-statements (body switch-ast)
             "Remove break statements from BODY which apply directly to
              SWITCH-AST."
             ;; while, do-while, for, and switch.
             ;; NOTE: body will likely be a list.
             (reduce (op (remove _2 _1))
                     (collect-break-statements body switch-ast)
                     :initial-value body))
           (to-or-pattern (values)
             (cond
               ((atom values)
                (wrap-for-rust values))
               ((single values)
                (wrap-for-rust (car values)))
               (t
                (make 'rust-or-pattern
                      :children (mapcar #'to-or-pattern
                                        (list (butlast values)
                                              (lastcar values)))))))
           (patch-default-case (case-values)
             ;; NOTE: NIL will indicate the default case in VALUES. This should
             ;;       be replaced directly with a rust wildcard pattern.
             (mapcar (op (or _ (make 'rust-_ :text "_"))) case-values))
           (to-match-pattern (values)
             (make 'rust-match-pattern :children (list (to-or-pattern values))))
           (wrap-maybe/compound-statement (case-body-asts)
             "Wrap CASE-BODY-ASTS in an untranslated-ast. If CASE-BODY-ASTS is
             not a list of a single compound statement, wrap it in a compound
             statement for easier Rust translation."
             (if (and (single case-body-asts)
                      (typep case-body-asts 'cpp-compound-statement))
                 (make-untranslated-ast case-body-asts)
                 (make-untranslated-ast
                  (make 'cpp-compound-statement :children case-body-asts))))
           (fallthrough-set->match-arm (fallthrough-set)
             (let ((case-values
                     (patch-default-case (mapcar #'cpp-value fallthrough-set)))
                   (case-body (cpp-statements (lastcar fallthrough-set))))
               (make 'rust-match-arm
                     :rust-pattern (to-match-pattern case-values)
                     :rust-value (wrap-maybe/compound-statement case-body))))
           (collapse-case-statements (switch-ast)
             (make 'rust-match-block
                   :children
                   (mapcar #'fallthrough-set->match-arm
                           (mapcar (lambda (set)
                                     (mapcar (op (remove-break-statements _ switch-ast))
                                             set))
                                   (get-fallthrough-sets switch-ast))))))
    (ematch switch
      ((cpp-switch-statement
        (cpp-condition (cpp-condition-clause (cpp-value value))))
       (make 'rust-expression-statement
             :children (list
                        (make 'rust-match-expression
                              :rust-value (make-untranslated-ast value)
                              :rust-body (collapse-case-statements switch))))))))

(defmethod translate/top-down ((switch cpp-switch-statement))
  (translate-switch-statement switch))


;;; File translation

(defgeneric translate-file (project file &key refactor verbose aggressive)
  (:method ((project project:project)
            (file string)
            &rest kwargs &key refactor verbose aggressive)
    (declare (ignore refactor verbose aggressive))
    (apply #'translate-file project
           (aget file
                 (project:evolve-files project)
                 :test #'equal)
           kwargs))
  (:method ((project project:project)
            (software software)
            &key (refactor t) (verbose t) (aggressive t))
    (assert (find software (mapcar #'cdr (project:evolve-files project))))
    (when verbose
      (format *error-output* "~&Translating file: ~a~%"
              (sel/cp/file:original-path software)))
    (translate-file project (sel:genome software)
                    :refactor refactor
                    :verbose verbose
                    :aggressive aggressive))
  (:method ((project project:project)
            (genome ast)
            &key (refactor t)
              (verbose t)
              (aggressive t))
    (let* ((traversal:*input-mode* :argot) ;TODO: REPL?
           (path (assure (not null)
                   (ast-path project genome))))
      (flet ((translate-file ()
               (let ((project
                       (reduce
                        (lambda (project refactoring)
                          (when verbose
                            (format t "~&Applying ~a~%" refactoring))
                          (assure sel/sw/cpp-project:cpp-project
                            (attrs:with-attr-table project
                              (dbg:note 1 "Applying external refactoring ~a"
                                        refactoring)
                              (nlet retry ()
                                (restart-case
                                    (let ((new (apply-refactoring*
                                                refactoring
                                                (assure ast
                                                  (lookup project path)))))
                                      (with project path new))
                                  (skip-refactoring ()
                                    :report "Skip this refactoring"
                                    project)
                                  (retry-refactoring ()
                                    :report "Retry the refactoring"
                                    (retry)))))))
                        (if refactor
                            *refactoring-pipeline*
                            nil)
                        :initial-value project)))
                 (translate-partially project :root (lookup project path)))))
        (if aggressive
            (with-aggressive-analysis (:more-restarts '(ignore-iterator))
              (translate-file))
            (translate-file))))))
