(defpackage :lilac-rust/translated-ast
  (:use :gt/full
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/cpp
        :software-evolution-library/software/rust
        :lilac-rust/rust-common)
  (:local-nicknames
   (:abstract :org.tfeb.hax.abstract-classes)
   (:ts :software-evolution-library/software/tree-sitter)
   (:term :software-evolution-library/terminal))
  (:export
    :*translated-source-text-delimiters*
    :translated-ast
    :get-cpp-children
    :get-rust-children
    :make-untranslated-ast
    :make-rust-wrapper
    :rust-wrapper
    :untranslated-ast
    :get-cpp-child
    :get-rust-child
    :partial-translation
    :make-partial-translation
    :make-full-translation
    :full-translation
    :colorize-source-text
    :colorize-source-text/html))
(in-package :lilac-rust/translated-ast)


;;; Alternative ASTs

(defvar *translated-source-text-delimiters* '("" . ""))

(define-node-class translated-ast (alternative-ast structured-text)
  ((cpp :initarg :cpp-children :initarg :cpp :reader get-cpp-children
        :initform nil
        :type (soft-list-of cpp-ast))
   (rust :initarg :rust-children :initarg :rust :reader get-rust-children
         :initform nil
         :type (soft-list-of rust-ast))
   (child-slots
    :initform '((cpp . 0) (rust . 0))
    :allocation :class))
  (:metaclass abstract:abstract-class))

(defmethod initialize-instance :after ((self translated-ast) &key)
  (with-slots (cpp rust) self
    (assert (and cpp rust))
    (assert (every (of-type 'cpp-ast) cpp))
    (assert (every (of-type 'rust-ast) rust)))
  (slot-makunbound self 'child-slot-specifiers))

(defmethod get-rust-children ((ast rust-ast))
  (children ast))

(defmethod get-cpp-children ((ast cpp-ast))
  (children ast))

(defun get-cpp-child (ast)
  (only-elt (get-cpp-children ast)))

(defun get-rust-child (ast)
  (only-elt (get-rust-children ast)))

(define-node-class partial-translation (translated-ast)
  ())

(defmethod uid ((ast partial-translation))
  (uid (get-rust-child ast)))

(defmethod assign-uid ((ast partial-translation) uid)
  (copy ast
        :rust-children
        (mapcar (op (assign-uid _ uid))
                (get-rust-children ast))))

(define-node-class full-translation (translated-ast)
  ())

(defmethod uid ((ast full-translation))
  (uid (get-rust-child ast)))

(defmethod assign-uid ((ast full-translation) uid)
  (copy ast
        :rust-children
        (mapcar (op (assign-uid _ uid))
                (get-rust-children ast))))

(define-node-class untranslated-ast (translated-ast)
  ())

(defmethod uid ((ast untranslated-ast))
  (uid (get-cpp-child ast)))

(defmethod assign-uid ((ast untranslated-ast) uid)
  (copy ast
        :cpp-children
        (mapcar (op (assign-uid _ uid))
                (get-cpp-children ast))))

(define-node-class rust-wrapper (translated-ast)
  ())

(defmethod uid ((ast rust-wrapper))
  (uid (get-rust-child ast)))

(defmethod assign-uid ((ast rust-wrapper) uid)
  (copy ast
        :rust-children
        (mapcar (op (assign-uid _ uid))
                (get-rust-children ast))))

(defun make-source-fragment (ast)
  (receive (vp-class frag-class tree-class)
      (etypecase ast
        ;; NB Criss-cross!
        (rust-ast
         (values (find-class 'cpp-source-text-fragment-variation-point)
                 (find-class 'cpp-source-text-fragment)
                 (find-class 'cpp-source-text-fragment-tree)))
        (cpp-ast
         (values (find-class 'rust-source-text-fragment-variation-point)
                 (find-class 'rust-source-text-fragment)
                 (find-class 'rust-source-text-fragment-tree))))
    (make vp-class
          :source-text-fragment
          (make frag-class :text (fmt "/* ~a */" (source-text ast)))
          :source-text-fragment-tree
          (make tree-class :children (list ast)))))

(defun make-translated-ast (cpp rust &optional (class 'partial-translation))
  (assert (or cpp rust))
  (let ((cpp (ensure-list (or cpp (make-source-fragment rust))))
        (rust (ensure-list (or rust (make-source-fragment cpp)))))
    (assert (every (of-type 'cpp-ast) cpp))
    (assert (every (of-type 'rust-ast) rust))
    (make class
          :cpp-children (mapcar #'tree-copy cpp)
          :rust-children (mapcar #'tree-copy rust))))

(defun make-untranslated-ast (cpp)
  (check-type cpp cpp-ast)
  (assure untranslated-ast
    (make-translated-ast cpp nil 'untranslated-ast)))

(defun make-rust-wrapper (rust)
  (check-type rust rust-ast)
  (assure rust-wrapper
    (make-translated-ast nil rust 'rust-wrapper)))

(defun make-partial-translation (cpp rust)
  (assure (or partial-translation full-translation)
    (ematch* (cpp rust)
      (((type (and cpp-ast (not (or source-text-fragment variation-point))))
        (type (and rust-ast (not (or source-text-fragment variation-point)))))
       (make-translated-ast cpp rust
                            (if (every (of-type '(or rust-ast text-fragment))
                                       rust)
                                'full-translation
                                'partial-translation))))))

(defun make-full-translation (cpp rust)
  (check-type cpp cpp-ast)
  (assert (every (of-type '(or rust-ast text-fragment)) rust))
  (assure full-translation
    (make-translated-ast cpp rust 'full-translation)))

(defmethod source-text ((ast translated-ast) &rest args &key stream)
  (destructuring-bind (before . after) *translated-source-text-delimiters*
    (write-string before stream)
    (multiple-value-prog1
        (apply #'source-text
               (only-elt (get-rust-children ast))
               args))
    (write-string after stream)))

(defun colorize-source-text (source-text &key
                                           (start (car *translated-source-text-delimiters*))
                                           (end (cdr *translated-source-text-delimiters*)))
  (assert (and start end))
  (serapeum:~>
   source-text
   (string-replace-all (string+ end start) _ "")
   (string-replace-all start _ term:+color-grn+)
   (string-replace-all end _ term:+color-rst+)))

(defun colorize-source-text/html (source-text &key
                                                (start (car *translated-source-text-delimiters*))
                                                (end (cdr *translated-source-text-delimiters*))
                                                (rust-color "green")
                                                (cpp-color "333333")
                                                )
  (assert (and start end))
  (let* ((escaped
           (escape source-text
                   (dict #\& "&amp;"
                         #\< "&lt;"
                         #\> "&gt;"
                         ;; Rust orange is# f6834d
                         (character start)
                         (fmt "<span style='color: ~a'>" rust-color)
                         (character end) "</span>"))))
    (with-output-to-string (out)
      (format out "<html><body style='color: ~a'><pre>" cpp-color)
      (write-string escaped out)
      (write-string "</pre></body></html>" out))))
(defmethod get-representative-ast ((ast translated-ast) (parent rust-ast))
  (get-rust-child ast))

(defmethod get-representative-ast ((ast translated-ast) (parent cpp-ast))
  (get-cpp-child ast))

(defgeneric get-whitespace-representative (ast parent)
  (:method ((ast translated-ast) (parent t))
    (get-representative-ast ast parent))
  ;; Special cases.
  (:method ((ast rust-wrapper) (parent cpp-ast))
    (get-rust-child ast))
  (:method ((ast untranslated-ast) (parent rust-ast))
    (get-cpp-child ast)))

(defmethod ts::whitespace-between/parent
    :around ((parent t) style (ast1 translated-ast) ast2)
  (ts::whitespace-between/parent parent style
                                 (get-whitespace-representative ast1 parent)
                                 ast2))

(defmethod ts::whitespace-between/parent
    :around ((parent t) style ast1 (ast2 translated-ast))
  (ts::whitespace-between/parent parent style
                                 ast1
                                 (get-whitespace-representative ast2 parent)))

(defmethod (setf before-text) ((value t) (ast translated-ast))
  (multiple-value-prog1 (call-next-method)
    (setf (before-text (get-cpp-child ast)) value)
    (setf (before-text (get-rust-child ast)) value)))

(defmethod (setf after-text) ((value t) (ast translated-ast))
  (multiple-value-prog1 (call-next-method)
    (setf (after-text (get-cpp-child ast)) value)
    (setf (after-text (get-rust-child ast)) value)))

(defmethod ast-language-class ((ast rust-wrapper))
  (ast-language-class (get-rust-child ast)))

(defmethod ast-language-class ((ast untranslated-ast))
  (ast-language-class (get-cpp-child ast)))
