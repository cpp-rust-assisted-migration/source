(defpackage :lilac-rust/c2rust
  (:documentation "Wrapper for c2rust invocation")
  (:use :gt/full :cmd)
  (:import-from :yason)
  (:shadowing-import-from :serapeum :~>)
  (:use
    :software-evolution-library
    :software-evolution-library/software/c
    :software-evolution-library/software/cpp
    :software-evolution-library/software/rust
    :software-evolution-library/software/tree-sitter
    :software-evolution-library/software/parseable)
  (:export :transpile-fragment
           :transpile-tree
           :transpile-function
           :c-like-function?))
(in-package :lilac-rust/c2rust)

(defparameter *c2rust* "c2rust"
  "c2rust executable")

(defparameter *source.c*
  #p"source.c")

(defclass c2rust-env ()
  ((dir :initarg :dir :type directory-pathname :reader dir)
   (file :initarg :file :type (and relative-pathname file-pathname)
         :reader file)
   (fragment :initarg :fragment :type string :reader fragment)
   (path :type (and absolute-pathname file-pathname) :reader path))
  (:default-initargs
   :file *source.c*))

(defmethod initialize-instance :after ((env c2rust-env) &key)
  (with-slots (path) env
    (setf path (path-join (dir env) (file env)))))

(defmethod write-fragment ((env c2rust-env))
  (with-slots (dir file fragment) env
    (lret ((path (path-join dir *source.c*)))
      (write-string-into-file
       (string-replace-all "this->"
                           fragment
                           "this_")
       path))))

(defmethod compilation-database-string ((env c2rust-env))
  (let ((full-path (shlex:quote (namestring (path env)))))
    (with-output-to-string (out)
      (yason:encode
       (vector
        (dict "directory" (namestring (dir env))
              "command"
              (fmt "/usr/bin/cc -o CMakeFiles/~a.dir/~a.o -c ~a"
                   (string-upcase-initials (pathname-name (file env)))
                   (namestring (file env))
                   full-path)
              "file" full-path))
       out))))

(defun generate-compilation-database (env)
  (write-string-into-file
   (assure string
     (compilation-database-string env))
   (path-join (dir env) #p"compile_commands.json")))

(defun transpile-env (env)
  (cmd :in (dir env)
       (list *c2rust*)
       "transpile compile_commands.json"))

(defun get-rust (env)
  (let ((rust-file (make-pathname
                    :type "rs"
                    :defaults (path env))))
    (assert (file-exists-p rust-file))
    (~> rust-file
        read-file-into-string
        (string-replace-all "this_" _ "self.")
        ;; TODO Fix this in the Rust grammar
        (string-replace-all "{,}" _ "{}"))))

(defun transpile-fragment (source &rest kwargs
                           &key (file *source.c*))
  (with-temporary-directory (:pathname dir)
    (cmd:with-working-directory (dir)
      (let ((env (apply #'make 'c2rust-env
                        :dir (pathname dir)
                        :fragment source
                        :file file
                        kwargs)))
        (write-fragment env)
        (generate-compilation-database env)
        (transpile-env env)
        (get-rust env)))))

(Defun transpile-tree (source &key (cleanup t))
  (flet ((maybe-cleanup (tree)
           (if cleanup
               (cleanup-c2rust-output tree)
               tree)))
    (etypecase source
      (string
       (maybe-cleanup
        (from-string 'rust
                     (transpile-fragment source))))
      ((or ast software)
       (if (find-if (of-type 'function-declaration-ast)
                    source)
           (transpile-tree
            (source-text source))
           (let ((result
                   (transpile-tree
                    (string+ "int temp_transpiler_function() { "
                             (source-text source)
                             " return 0; }"))))
             (let ((fn (find-if (of-type 'rust-function-item) result)))
               (assert fn)
               (make 'rust-unsafe-block
                     :children
                     (list
                      (make 'rust-block
                            :children
                            (butlast (children (rust-body fn)))))))))))))

(defgeneric class-has-c-equivalent? (class)
  (:documentation "Is there a C equivalent to CLASS?")
  (:method ((class class))
    (class-has-c-equivalent? (class-name class)))
  (:method ((class-name symbol))
    (assert (string^= 'cpp- class-name))
    (let* ((symbol-name
             (regex-replace "(-\\d+)$"
                            (string+ 'c-
                                     (drop-prefix (string 'cpp-)
                                                  (string class-name)))
                            ""))
           (cpp-symbol
             (find-symbol symbol-name #.(find-package :ts))))
      (and cpp-symbol
           (find-class cpp-symbol nil)))))

(defgeneric ast-can-be-c? (ast)
  (:documentation "Does this single AST have a C equivalent?
Note this does not check the children of the AST.")
  (:method ((ast c-ast)) t)
  (:method ((ast cpp-ast))
    (class-has-c-equivalent? (type-of ast))))

(defun c-like-subtrees (tree)
  (let ((cache (make-hash-table))
        (subtrees '()))
    (labels ((can-be-c? (ast)
               (ensure2 (href cache ast)
                 (if (no (children ast))
                     (ast-can-be-c? ast)
                     (and (ast-can-be-c? ast)
                          (every #'can-be-c?
                                 (children ast))))))
             (collect-c-like-subtrees (tree)
               (if (can-be-c? tree)
                   (push tree subtrees)
                   (dolist (child (children tree))
                     (collect-c-like-subtrees child)))))
      (collect-c-like-subtrees tree)
      (nreverse subtrees))))

(defgeneric subtree-interesting? (tree)
  (:method ((tree t)) t)
  (:method ((tree identifier-ast))
    nil)
  (:method ((tree terminal-symbol))
    nil))

(defun cleanup-c2rust-output (output)
  (labels ((simplify-output (output)
             (nest
              (copy output :genome)
              (copy (sel:genome output) :children)
              (filter-map
               (lambda (child)
                 (typecase child
                   (rust-inner-attribute-item nil)
                   (rust-attribute-item nil)
                   (rust-function-item
                    (match child
                      ((and fn
                            (rust-function-item)
                            (access #'direct-children
                                    (list (rust-visibility-modifier)
                                          (and mods
                                               (rust-function-modifiers
                                                (rust-modifiers
                                                 (list (and unsafe
                                                            (rust-unsafe))
                                                       _)))))))
                       (copy fn
                             :children
                             (list (copy mods
                                         :rust-modifiers
                                         (list unsafe)))))
                      (otherwise child)))
                   (t child))))
              (children (sel:genome output))))
           (cleanup-int-decls (output)
             (mapcar (lambda (ast)
                       (when (source-text= ast "libc::c_int")
                         (make 'rust-primitive-type
                               :text "i32")))
                     output)))
    (~> output
        simplify-output
        cleanup-int-decls
        genome)))

(defun c-like-function? (ast)
  (and (typep ast 'function-declaration-ast)
       (let ((fn ast))
         (find-if (of-type '(or cpp-pointer-expression
                             cpp-array-declarator))
                  fn))))

(defun transpile-function (fn)
  (assert (c-like-function? fn))
  (let* ((rust (transpile-fragment (source-text fn)))
         (output
           (cleanup-c2rust-output
            (from-string 'rust rust))))
    (assert (not (search "this_" rust)))
    (or (find-if (of-type 'function-declaration-ast)
                 output)
        output)))
