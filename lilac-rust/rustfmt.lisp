(defpackage :lilac-rust/rustfmt
  (:documentation "Interface for rustfmt")
  (:use :gt/full
        :cmd)
  (:export :rustfmt))
(in-package :lilac-rust/rustfmt)

(declaim (type string *rustfmt*))
(defparameter *rustfmt* "rustfmt"
  "Name of the rustfmt executable.")

(defgeneric rustfmt (source &key verbose)
  (:documentation "Format SOURCE using rustfmt, if it is available.
If `verbose' is non-nil, direct rustfmt's stderr to `*error-output*`.")
  (:method :around (source &key verbose)
    (declare (ignore verbose))
    (if (resolve-executable *rustfmt*)
        (call-next-method)
        source))
  (:method ((source string) &key verbose)
    (let ((output
            ($cmd (list *rustfmt*)
                  :<<< source
                  :2> (and verbose *error-output*)
                  :ignore-error-status t)))
      (if (emptyp output) source output)))
  (:method ((source pathname) &key verbose)
    (let ((output
            ($cmd (list *rustfmt*)
                  "--emit stdout" source
                  :2> (and verbose *error-output*)
                  :ignore-error-status t)))
      (if (emptyp output) source output))))
