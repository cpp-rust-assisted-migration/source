(defpackage :lilac-rust/test
  (:use :gt/full
        :stefil+
        :lilac-rust/rust-common
        :lilac-commons/commons
        :lilac-commons/database
        :lilac-rust/muse
        :lilac-rust/rust-lens
        :lilac-commons/ast-fragment
        :lilac-rust/translated-ast
        :software-evolution-library
        :software-evolution-library/utility/range
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/cpp-project
        :cmd)
  (:import-from :cl-interpol)
  (:import-from :lilac-commons/commons
                :get-compilation-unit
                :header-path?
                :translated-path :project+file)
  (:import-from :lilac-cpp/refactorings :*refactoring-pipeline*)
  (:import-from :lilac-cpp/test-utils
                :refactor-raw-cpp
                :assert-source-text=)
  (:shadowing-import-from :serapeum :~>)
  (:local-nicknames
   (:analysis :lilac-commons/analysis)
   (:ast :argot-server/lens/ast)
   (:attrs :functional-trees/attrs)
   (:c2rust :lilac-rust/c2rust)
   (:dir :software-evolution-library/software/directory)
   (:it :iterate)
   (:l :fresnel/lens)
   (:project :software-evolution-library/software/project)
   (:refactorings :lilac-cpp/refactorings)
   (:rust :lilac-rust/rust-lens)
   (:rustrans :lilac-rust/rust-translation)
   (:rustran :lilac-rust/rust-translation)
   (:traversal :lilac-rust/traversal)
   (:ts :software-evolution-library/software/tree-sitter))
  (:import-from :lilac-rust/rustfmt :rustfmt)
  (:import-from :lilac-commons/refactoring
                :apply-refactoring)
  (:import-from :trivial-file-size
                :file-size-in-octets)
  #+sbcl
  (:import-from :argot-server/test/utility
                :define-timed-test
                :rerun-baseline)
  (:import-from :lilac-cpp/muse
                :lilac-cpp-muse)
  (:export :test :run-batch))
(in-package :lilac-rust/test)
(in-readtable :interpol-syntax)

(eval-always
  (when (equal (uiop:getenv "CI") "true")
    (pushnew :ci *features*)))

(defroot root)

(defsuite lilac-rust-tests "Tests for the LiLaC rust muse")

(defparameter *grist-dir*
  (asdf:system-relative-pathname :lilac-rust "lilac-grist/"))


;;; Common tests

(deftest test-translated-path ()
  (is (equal #P"rust/src/a/impl_part.rs"
             (translated-path "a-impl_part.cppm"))))


;;; Relation tests.

(define-table x-to-y ((:x) (:y)))

(deftest test-tables ()
  (let ((d (empty-database)))
    (upsertf d 'x-to-y :x 1 :y 2)
    (upsertf d 'x-to-y :x 2 :y 3)
    (let ((result (select-results d 'x-to-y t :x 1)))
      (is (length= result 1))
      (is (equal '(2)
                 (get-column-values
                  'x-to-y
                  (first result)
                  :y))))
    (is (equal '((2))
               (select-results
                d 'x-to-y
                '(:y)
                :x 1)))
    (is (equal
         '((x-to-y (:x 1 :y 2) (:x 2 :y 3)))
         (dump-database d)))))


;;; AST fragment tests.

(deftest test-ast-fragment ()
  (flet ((int (x)
           (make 'rust-integer-literal :text (princ-to-string x))))
    (is (equal '(1 2 3)
               (mapcar (compose #'parse-integer #'source-text)
                       (children
                        (make 'ast-fragment
                              :children
                              (make 'ast-fragment
                                    :children
                                    (mapcar #'int (list 1 2 3))))))))
    (is (equal '(1 2 3 4)
               (mapcar (compose #'parse-integer #'source-text)
                       (children
                        (make 'ast-fragment
                              :children (list (int 1) (int 2)
                                              (make 'ast-fragment
                                                    :children
                                                    (list (int 3)
                                                          (int 4)))))))))))

(deftest translated-ast-test ()
  (let* ((cpp (make 'cpp-ast))
         (rust (make 'rust-ast))
         (trans (make-partial-translation cpp rust)))
    (is (equal? (children trans) (list cpp rust)))
    (destructuring-bind (cpp rust) (children trans)
      (is (ast-path trans cpp))
      (is (ast-path trans rust)))))


;;; Bottom-up (lens) translation tests.

(deftest test-camel/snake-case ()
  (is (null (l:forward (camel/snake) 1)))
  (is (equal (l:forward (camel/snake) "UpperCamelCase") "upper_camel_case"))
  (is (equal (l:backward (camel/snake) "snake_case") "SnakeCase"))
  (is (equal (l:forward (camel/snake) "foo") "foo"))
  (is (equal (l:backward (camel/snake) "foo") "Foo")))

(defun check-lens (cpp rust)
  "Check C++ to Rust translation (lens only)."
  (let* ((translation (to-rust
                       (if (typep cpp 'cpp-ast) cpp
                           (convert 'cpp-ast cpp :deepest t))))
         (translation-text
           (collapse-whitespace (source-text translation))))
    (is (equal translation-text rust)
        "Versions:~%Reference: ~a~%Produced:  ~a~2%Mismatch at ~a"
        rust
        translation-text
        (mismatch rust translation-text))))

(deftest test-translate-int ()
  (let ((cpp-int
          (find-if (of-type 'cpp-number-literal)
                   (cpp "1;"))))
    (typep (to-rust cpp-int) 'rust-integer-literal)
    (is (source-text= (to-rust cpp-int)
                      "1"))))

(deftest test-translate-float ()
  (let ((cpp-double
          (find-if (of-type 'cpp-number-literal)
                   (cpp "1.0;"))))
    (typep (to-rust cpp-double) 'rust-float-literal)
    (source-text= "1.0" (to-rust cpp-double))))

(deftest test-translate-single-float ()
  (let ((cpp-float (cpp* "3.0f;")))
    (typep (to-rust cpp-float) 'rust-float-literal)
    (source-text= "3.0f32" (to-rust cpp-float))))

(deftest test-translate-char ()
  (is (source-text= "'a'" (to-rust (cpp* "'a'")))))

(deftest test-translate-unop ()
  (check-lens (cpp* "!x") "!x")
  (check-lens (cpp* "-x") "-x"))

(deftest test-translate-binop ()
  (let* ((cpp (cpp "2 + 2;"))
         (rust (to-rust cpp)))
    (is (source-text= rust "2 + 2;"))
    (is (typep rust 'rust-ast))
    (iter (for ast in-tree rust)
          (always (typep ast 'rust-ast)))))

(deftest test-translate-return ()
  (check-lens "return 1;" "return 1;"))

(deftest test-translate-return/no-expression ()
  (check-lens "return;" "return;"))

(deftest test-primitive-type-lens ()
  (is (source-text=
       "i32"
       (l:forward (rust::type-lens)
                  (find-if (of-type 'cpp-primitive-type) (cpp "int x;"))))))

(deftest test-fixed-sized-integer-lens ()
  (check-lens "int8_t x;" "let mut x:i8;")
  (check-lens "uint8_t x;" "let mut x:u8;")
  (check-lens "int16_t x;" "let mut x:i16;")
  (check-lens "uint16_t x;" "let mut x:u16;")
  (check-lens "int32_t x;" "let mut x:i32;")
  (check-lens "uint32_t x;" "let mut x:u32;")
  (check-lens "int64_t x;" "let mut x:i64;")
  (check-lens "uint64_t x;" "let mut x:u64;"))

(deftest test-sized-type-lens ()
  (is (source-text=
       "u32"
       (l:forward (rust::type-lens)
                  (find-if (of-type 'cpp-sized-type-specifier)
                           (cpp "unsigned int x;")))))
  (is (source-text=
       "i16"
       (l:forward (rust::type-lens)
                  (find-if (of-type 'cpp-sized-type-specifier)
                           (cpp "short x;")))))

  (is (source-text=
       "u16"
       (l:forward (rust::type-lens)
                  (find-if (of-type 'cpp-sized-type-specifier)
                           (cpp "unsigned short x;"))))))

(deftest test-let-lens ()
  (source-text= "let mut x:u32 = 10;"
                (to-rust (cpp "unsigned int x = 10;")))
  (source-text= "let mut y:u32 = 11;"
                (to-rust (cpp "unsigned int y = 11;"))))

(deftest test-let-auto-lens ()
  (is (source-text= "let mut x = 10;"
                    (to-rust (cpp "auto x = 10;"))))
  (is (source-text= "let x = 10;"
                    (to-rust (cpp "const auto x = 10;")))))

(deftest test-let-constexpr-var-lens ()
  (is (source-text= "const x:f32 = 42.0;"
                    (to-rust (cpp* "constexpr float x = 42.0;")))))

(deftest test-block-lens ()
  (let ((rust
          (to-rust
           (cpp "{
  unsigned int x = 10;
  unsigned int y = 11;
  return y;
}"))))
    (is (typep rust 'rust-block))
    (is (length= (children rust) 3))
    rust))

(deftest test-empty-function ()
  (check-lens "int myfun () {}"
              "fn myfun() -> i32 {}"))

(deftest test-function-value-parameters ()
  (check-lens "int myfun (int x) {}"
              "fn myfun(mut x: i32) -> i32 {}")
  (check-lens "int myfun (int x, double y) {}"
              "fn myfun(mut x: i32, mut y: f64) -> i32 {}"))

(deftest test-function-reference-parameters ()
  (check-lens "int myfun (int& x) {}"
              "fn myfun(x: &mut i32) -> i32 {}")
  (check-lens "int myfun (const int& x) {}"
              "fn myfun(x: &i32) -> i32 {}"))

(deftest test-function-reference-return ()
  (check-lens "int& myfun () {}"
              "fn myfun() -> &mut i32 {}")
  (check-lens "const int& myfun () {}"
              "fn myfun() -> &i32 {}"))

(deftest test-translate-simple-example-function ()
  (let* ((cpp
           (cpp "int myfun() {
  unsigned int x = 10;
  const unsigned int y = 20;
  return y;
}"))
         (rust (to-rust cpp)))
    ;; TODO Whitespace.
    (is (equal
         (remove-if #'whitespacep (source-text rust))
         (remove-if #'whitespacep
                    "fn myfun() -> i32 {
  let mut x:u32 = 10;
  let y:u32 = 20;
  y
}")))
    rust))

(deftest test-translate-call ()
  (is (source-text= "foo(1);" (to-rust (cpp "foo(1);"))))
  (is (source-text= "x.foo(1);" (to-rust (cpp "x.foo(1);"))))
  (is (source-text= "foo();" (to-rust (cpp "foo();"))))
  (is (source-text= "x.foo();" (to-rust (cpp "x.foo();")))))

(deftest test-binop-over-call ()
  (let ((cpp (find-if (of-type 'binary-ast)
                      (cpp "pts.size() < 2;"))))
    (is (typep cpp 'binary-ast))
    (is (source-text= (to-rust cpp) "pts.size() < 2"))))

(deftest test-assignment ()
  ;; TODO Should this clone?
  (is (source-text= "x = y;" (to-rust (cpp "x = y;")))))

(deftest test-augmented-assignment ()
  (is (source-text= "x += y;" (to-rust (cpp "x += y;")))))

(deftest test-augmented-assignment-in-block ()
  (is (source-text= (fmt "{~%    x += 1;~%}")
                    (to-rust (cpp* "{ ++x; }")))))

(deftest test-string-literal ()
  (is (source-text= "\"hello, world\";" (to-rust (cpp "\"hello, world\";")))))

(deftest test-if-then ()
  (let* ((cpp (cpp "if (x > 2) { return 1; }"))
         (rust (to-rust cpp)))
    (is (typep rust 'rust-expression-statement))
    (is (typep (first (children rust))
               'rust-if-expression))))

(deftest test-if-without-block ()
  (is (equal
       (fmt "if x > 1 {~%~4treturn x;~%};")
       (source-text (to-rust (cpp "if (x > 1) return x;"))))))

(deftest test-if-else ()
  (let ((rust (to-rust (cpp "if (x > 2) { return 1; } else { return 2; }"))))
    (is (typep rust 'rust-expression-statement))
    (let ((child (first (children rust))))
      (is (typep child 'rust-if-expression))
      (is (typep (alternative child) 'rust-else-clause)))))

(deftest test-parenthesized-expression ()
  (is (source-text= "(d + segdist) > dist;"
                    (to-rust (cpp "(d + segdist) > dist;")))))

(deftest test-c-style-cast ()
  (let ((cpp (find-if (of-type 'cpp-cast-expression) (cpp "(double)dist;"))))
    (is (typep cpp 'cpp-cast-expression))
    (is (source-text= "dist as f64" (to-rust cpp)))))

(deftest test-static-cast ()
  (check-lens "static_cast<double>(dist);"
              "(dist as f64);"))

(deftest test-static-cast-parens ()
  "There aren't parentheses around the whole RHS, but there is one for
disambiguation. (To avoid operator precedence issues.)"
  (check-lens "double z = x + static_cast<double>(y);"
              "let mut z:f64 = x + (y as f64);")
  (check-lens "double z = static_cast<double>(x + y);"
              "let mut z:f64 = (x + y) as f64;"))

(deftest test-translate-type-identifier ()
  (is (typep
       (to-rust (cpp "const midpoint Point = other_point;"))
       'rust-let-declaration)))

(deftest translate-container-type/initializer ()
  (is (source-text=
       "let mut x:Vec<Point> = ys;"
       (to-rust (cpp "std::vector<Point> x = ys;"))))
  (is (source-text=
       "let mut x:LinkedList<Point> = ys;"
       (to-rust (cpp "std::list<Point> x = ys;")))))

(deftest test-field-case ()
  (is (source-text= "p.camel_case_method();"
                    (to-rust (cpp "p.CamelCaseMethod();")))))

(deftest test-uninitialized-declaration ()
  ;; TODO The translation assumed here is that an unitialized
  ;; primitive type can be left uninitialized, but uninitialized
  ;; declarations of non-primitive types on the C++ side are
  ;; implicitly invoking a default constructor. Is this true?

  ;; Primitive types.
  (is (source-text= "let mut x:i32;" (to-rust (cpp "int x;"))))
  (is (source-text= "let x:i32;" (to-rust (cpp "const int x;"))))

  ;; Type identifiers.
  (is (source-text= "let x:Point = Point::new();"
                    (to-rust (cpp "const Point x;"))))
  (is (source-text= "let mut x:Point = Point::new();"
                    (to-rust (cpp "Point x;"))))

  ;; Generic types.
  (is (source-text= "let mut xs:Vec<i32> = Vec::new();"
                    (to-rust (cpp "std::vector<int> xs;"))))
  (is (source-text= "let xs:Vec<i32> = Vec::new();"
                    (to-rust (cpp "const std::vector<int> xs;")))))

(deftest test-dereference ()
  (check-lens "int x = *y;"
              "let mut x:i32 = *y;")
  (check-lens "const int x = *y;"
              "let x:i32 = *y;"))

(deftest test-reference-declaration ()
  (check-lens "const Point& x = y;"
              "let x:&Point = &y;")
  (check-lens "Point& x = y;"
              "let x:&mut Point = &mut y;")
  (check-lens "const Point& next_point = *p2;"
              "let next_point:&Point = &*p2;"))

(deftest test-preincrement-statement ()
  (check-lens "++x;" "x += 1;")
  (check-lens "--x;" "x -= 1;")
  (check-lens "++(x);" "x += 1;")
  (check-lens "--(x);" "x -= 1;")
  (check-lens "--*x;" "*x -= 1;")
  (check-lens "++(*x);" "*x += 1;")
  (check-lens "--(*x);" "*x -= 1;"))

(deftest test-preincrement-expression ()
  (check-lens "y = ++x;" "y = { x += 1; x };")
  (check-lens "y = --x;" "y = { x -= 1; x };")
  (check-lens "y = ++(x);" "y = { x += 1; x };")
  (check-lens "y = --(x);" "y = { x -= 1; x };")
  (check-lens "y = ++*x;" "y = { *x += 1; *x };")
  (check-lens "y = ++(*x);" "y = { *x += 1; *x };")
  (check-lens "y = --(*x);" "y = { *x -= 1; *x };"))

(deftest test-postincrement-statement ()
  (check-lens "x++;" "x += 1;")
  (check-lens "x--;" "x -= 1;")
  (check-lens "(x)++;" "x += 1;")
  (check-lens "(x)--;" "x -= 1;")
  (check-lens "(*x)++;" "*x += 1;")
  (check-lens "(*x)--;" "*x -= 1;")
  (check-lens "*x++;" "*{ let temp = x; x += 1; temp };")
  (check-lens "*x--;" "*{ let temp = x; x -= 1; temp };"))

(deftest test-postincrement-expression ()
  (check-lens "y = x++;" "y = { let temp = x; x += 1; temp };")
  (check-lens "y = x--;" "y = { let temp = x; x -= 1; temp };")
  (check-lens "y = (x)++;" "y = { let temp = x; x += 1; temp };")
  (check-lens "y = (x)--;" "y = { let temp = x; x -= 1; temp };")
  (check-lens "y = (*x)++;" "y = { let temp = *x; *x += 1; temp };")
  (check-lens "y = (*x)--;" "y = { let temp = *x; *x -= 1; temp };")
  (check-lens "y = *x++;" "y = *{ let temp = x; x += 1; temp };")
  (check-lens "y = *x--;" "y = *{ let temp = x; x -= 1; temp };"))

(deftest test-new ()
  ;; TODO Are these really the same thing?
  (check-lens "new MyType();" "MyType::new();"))

(deftest test-std-string ()
  (check-lens "std::string s = \"hello\";"
              "let mut s = String::from(\"hello\");"))

(deftest test-simple-struct-fields ()
  (let* ((struct (cpp* "struct mystruct { double x; int y; }"))
         (field-decls (collect-if (of-type 'cpp-field-declaration) struct)))
    (is (every #'source-text=
               (mapcar (op (patch-whitespace
                            (l:forward (rust::field-declaration) _)
                            :prettify t))
                       field-decls)
               '("x: f64"
                 "y: i32")))))

(deftest test-simple-struct ()
  (check-lens (cpp* "struct MyStruct { double x; int y; }")
              "struct MyStruct { pub x: f64, pub y: i32 }"))

(deftest test-multi-struct ()
  (check-lens (cpp* "struct MyStruct { double x, y; }")
              ;; TODO Extra spaces?
              "struct MyStruct { pub x: f64 , pub y: f64 }"))

(deftest test-private-in-struct ()
  (check-lens (cpp* "struct MyStruct { double x, y; private: int z; }")
              ;; TODO Extra spaces?
              "struct MyStruct { pub x: f64, pub y: f64, z: i32 }"))

(deftest test-anonymous-value-param ()
  (is (scan "fn distance\\(mut G\\d+: f64\\) -> f64 \\{\\}"
            (source-text
             (to-rust  (cpp "double Distance(double){}"))))))

(deftest test-anonymous-reference-param ()
  (is (scan "fn distance\\(G\\d+: &Point\\) -> f64 \\{\\}"
            (source-text
             (to-rust (cpp "double Distance(const Point&){}"))))))

(deftest test-call-snake-case ()
  (check-lens (cpp "PointToSegment(x);") "point_to_segment(x);"))

(deftest test-call-method-snake-case ()
  (check-lens (cpp "x.PointToSegment(x);") "x.point_to_segment(x);")
  (check-lens (cpp "x->PointToSegment(x);") "x.point_to_segment(x);"))

(deftest test-std-call ()
  (check-lens "std::sqrt(x);" "x.sqrt();")
  (check-lens "std::sqrtf(x);" "x.sqrt();"))

(deftest test-std-method ()
  (check-lens "x.sqrtf();" "x.sqrt();"))

(deftest test-this-to-self ()
  (check-lens "this.height;" "self.height;")
  (check-lens "this->sth();" "self.sth();"))

(deftest test-sqrt-to-method ()
  (check-lens "std::sqrt(21);" "21.sqrt();")
  (check-lens "sqrt(21);" "21.sqrt();"))

(deftest test-sqrt-to-method/parens ()
  (check-lens "sqrt(a * a + b * b);" "(a * a + b * b).sqrt();")
  (check-lens "std::sqrt(a * a + b * b);" "(a * a + b * b).sqrt();"))

(deftest test-compound-literal ()
  (check-lens "Point{x, y};" "Point::new(x, y);"))

(deftest test-reference-wrapper ()
  (check-lens "std::cref(x);" "&x;")
  (check-lens "__CRAM__CREF(x);" "&x;")
  (check-lens "std::ref(x);" "&mut x;")
  (check-lens "__CRAM__REF(x);" "&mut x;"))

(deftest test-&expression ()
  (check-lens "int d = &c;" "let mut d:i32 = &c;"))

(deftest test-template-type ()
  (let* ((cpp (cpp* "Point<float>{1.0, 2.0}"))
         (type (find-if (of-type 'cpp-template-type) cpp)))
    (is (typep type 'cpp-template-type))
    (source-text= (to-rust type) "Point::<float>")))

(deftest test-new-with-turbofish ()
  (check-lens "Point<float>{1, 2};" "Point::<f32>::new(1, 2);"))

(deftest test-qname ()
  (is (equal? (rust* "x::y::z")
              (to-rust (cpp* "x::y::z"))))
  (is (equal? (rust* "x::y")
              (to-rust (cpp* "x::y")))))

(deftest test-translate-enum-class ()
  (check-lens (cpp* "enum class Unit { IN, CM }")
              "enum Unit { IN, CM }")
  (check-lens (cpp* "enum struct Unit { IN, CM}")
              "enum Unit { IN, CM }")
  (check-lens (cpp* "enum access_t { read = 1, write = 2, exec = 4 }")
              "enum AccessT { read = 1, write = 2, exec = 4 }"))

(deftest test-enum-class-preserves-camel-case ()
  (check-lens (cpp* "enum class MyFoo { X, Y }")
              "enum MyFoo { X, Y }"))

(deftest test-translate-ternary ()
  (check-lens (cpp* "x ? y : z")
              "if x { y } else { z }"))

;;; TODO Generalize this notation?
(defun cpp-tree (args)
  (match args
    ((list (and class (type symbol)) (and text (type string)))
     (convert 'cpp-ast
              `((:class . ,(make-keyword class))
                (:text . ,text))))
    ((list* (and class (type symbol)) args)
     (multiple-value-bind (kwargs children)
         (parse-leading-keywords args)
       (convert 'cpp-ast
                `((:class . ,(make-keyword class))
                  ,@(loop for (slot value) in (batches kwargs 2 :even t)
                          collect (cons slot (cpp-tree value)))
                  ,@(and children
                         (list (cons :children (mapcar #'cpp-tree children))))))))
    ((list* (and template (type string)) args)
     (apply #'ast-template 'cpp-ast template args))
    (otherwise args)))

(deftest test-translate-qualified-type ()
  (check-lens
   (cpp-tree
    `(qualified-identifier
      :name (type-identifier "T")
      :scope (template-type
              :name (type-identifier "Pair")
              :arguments
              (template-argument-list
               (type-descriptor
                :type (primitive-type "float"))))))
   "Pair<f32>::T"))

(deftest test-translate-auto-reference ()
  (check-lens
   (cpp "auto& p = x;")
   "let &mut p = x;")
  (check-lens
   (cpp "const auto& p = x;")
   "let &p = x;"))

(deftest test-while-loop ()
  (check-lens
   (cpp* "while (x) { y(x); }")
   "while x { y(x); };"))

(deftest test-ns-to-mod ()
  (check-lens
   (cpp* "namespace X { int myfun() {} }")
   "pub mod X { fn myfun() -> i32 {} }"))

(deftest test-simple-preproc-const ()
  (check-lens
   (cpp* "#define VALHALLA_VERSION_MAJOR 3")
   ;; TODO Should be const.
   "let VALHALLA_VERSION_MAJOR = 3;"))

(deftest test-simple-using-all ()
  (check-lens
   (cpp* "using namespace valhalla::sif;")
   ;; TODO Should be const.
   "use valhalla::sif::*;"))

(deftest test-qualified-type-in-reference-param ()
  (let ((p
          (find-if (of-type 'cpp-parameter-declaration)
                   (cpp "void fn(const boost::property_tree::ptree& pt);"))))
    (is (typep (to-rust p) 'rust-parameter))))

(deftest test-declaration-to-signature-item ()
  (let ((cpp (cpp "void fn();")))
    (is (typep cpp 'cpp-declaration))
    (is (typep (to-rust cpp) 'rust-function-signature-item))))

(deftest test-for-range-loop ()
  (check-lens "for (auto x : xs) { frob(x); }"
              "for x in xs.iter() { frob(x); };"))

(deftest test-const-pointer-to-const-declaration ()
  (check-lens "const int* const raw = &x;"
              "let raw = &x as *const i32;")
  (check-lens "const int * const raw = &x;"
              "let raw = &x as *const i32;")
  (check-lens "const int *const raw = &x;"
              "let raw = &x as *const i32;"))

(deftest test-const-pointer-to-mut-declaration ()
  (check-lens "int* const raw = &x;"
              "let raw = &mut x as *mut i32;")
  (check-lens "int * const raw = &x;"
              "let raw = &mut x as *mut i32;")
  (check-lens "int *const raw = &x;"
              "let raw = &mut x as *mut i32;"))

(deftest test-mut-pointer-to-const-declaration ()
  (check-lens "const int* raw = &x;"
              "let mut raw = &x as *const i32;")
  (check-lens "int const * raw = &x;"
              "let mut raw = &x as *const i32;"))

(deftest test-mut-pointer-to-mut-declaration ()
  (check-lens "int* raw = &x;"
              "let mut raw = &mut x as *mut i32;"))

(deftest test-negative-indent-declaration ()
  (let ((binop (cpp* "x + y")))
    (setf (indent-adjustment binop) -17)
    (is (typep (to-rust binop) 'rust-binary-expression))))

(deftest test-module-import ()
  (check-lens "import foo; x();"
              "#[allow(unused_imports)] use foo::*; x();")
  (check-lens "import :foo; x();"
              "#[allow(unused_imports)] pub use foo::*; x();"))

(deftest test-module-reexport ()
  (check-lens "export import foo; x();"
              #1="pub use foo::*; x();")
  (check-lens "export import :foo; x();"
              #1#))

(deftest test-export-struct-to-pub-struct ()
  (check-lens (cpp* "export struct A { int x; }")
              "pub struct A { pub x: i32 }"))

(deftest test-export-fn-to-pub-fn ()
  (check-lens "export int myfun() {}"
              "pub fn myfun() -> i32 {}"))

(deftest test-translate-infinite-for-loop ()
  (check-lens "for (;;) { }"
              "loop { };")
  (check-lens "for ( ; ; ) { }"
              "loop { };"))

(deftest test-translate-infinite-while-loop ()
  (check-lens "while (1) { }"
              "loop { };")
  (check-lens "while (true) { }"
              "loop { };"))

(deftest test-translate-infinite-do-while-loop ()
  (check-lens "do { } while (1);"
              "loop { };")
  (check-lens "do { } while (true);"
              "loop { };"))

(deftest test-infinite-for-loop ()
  "Translate an infinite for loop."
  (check-lens "for (;;) {}"
              "loop { };"))

(deftest test-cast-to-string ()
  (check-lens "static_cast<std::string>(x);"
              "String::from(x);"))


;;; c2rust integration test

(defun test-c2rust-transpile ()
  (when (resolve-executable "c2rust")
    (let ((in-file (path-join *grist-dir* "c2rust/qsort.c"))
          (out-file (path-join *grist-dir* "c2rust/qsort.rs")))
      (is (equal (source-text
                  (c2rust:transpile-tree (read-file-into-string in-file)))
                 (read-file-into-string out-file))))))


;;; Top-down translation tests.

(defun translate-file (file &key refactor)
  (translate-string (read-file-into-string file)
                    :refactor refactor))

(defun translate-string (string &key refactor interactive)
  "Translate STRING using the full translation pipeline.
If REFACTOR is non-nil, run through the refactoring pipeline as well."
  (analysis:with-aggressive-analysis ()
    (mvlet* ((traversal:*interactive* interactive)
             (string
              (if refactor
                  (refactor-raw-cpp string)
                  string))
             (project file (project+file string))
             (new-project new-path
              (rustrans:translate-partially project :root file)))
      (values (only-elt (dir:contents (lookup new-project new-path)))
              project
              file))))

(defun check-same-output (cpp-fragment rust-fragment)
  "Check CPP-FRAGMENT and RUST-FRAGMENT yield the same output.
Compile both fragments by embedding them in implicit main functions,
and check that running the resulting object files produces the same
output."
  (nest
   (let ((cpp (fmt "~
#include <iostream>
#include <iterator>
#include <vector>
int main () {
    ~a
    return 0;
}~%"
                   cpp-fragment))
         (rust (fmt "~
fn main() {
    ~a
}
"
                    rust-fragment))))
   (with-temporary-directory (:pathname d))
   (let* ((cpp-source-file (path-join d "cpp-example.cc"))
          (cpp-object-file (path-join d "cpp-example"))
          (rust-source-file (path-join d "rust-example.rs"))
          (rust-object-file (path-join d "rust-example")))
     (write-string-into-file cpp cpp-source-file)
     (write-string-into-file rust rust-source-file)
     (cmd "c++" cpp-source-file "-o" cpp-object-file)
     (cmd "rustc" rust-source-file "-o" rust-object-file))
   (let ((cpp-output ($cmd cpp-object-file))
         (rust-output ($cmd rust-object-file)))
     (is (not (emptyp cpp-output)))
     (is (not (emptyp rust-output)))
     (is (equal cpp-output rust-output)))))

(deftest test-check-same-output ()
  "Simple test that check-same-output works as intended."
  (check-same-output
   "std::cout << \"Hello, world\" << std::endl;"
   "println!(\"Hello, world\");"))

(defun check-full-translation (cpp rust &key same-output refactor)
  (let* ((cpp
           (is (null-if-empty
                (etypecase cpp
                  (string cpp)
                  (pathname
                   (read-file-into-string
                    (is (file-exists-p
                         (path-join *grist-dir* cpp)))))))))
         (rust
           (is (null-if-empty
                (etypecase rust
                  (string rust)
                  (pathname
                   (read-file-into-string
                    (is (file-exists-p
                         (path-join *grist-dir* rust)))))))))
         (translation (translate-string cpp :refactor refactor))
         (translated-string (source-text translation)))
    (is (equal rust translated-string))
    (when same-output
      (check-same-output cpp rust))))

(deftest test-translate-comma-expression ()
  (check-full-translation
   "x, y;"
   #?"{\n    x;\n    y\n};"))

(deftest test-for-loop-while ()
  "Test that we fall through to while if we can't do better."
  (check-full-translation
   "for (; opaque_test(); opaque_step()) { do_something(); }"
   #?"while opaque_test() {\n    do_something();\n    opaque_step();\n};"))

(deftest test-simple-for-loop-upward-exclusive ()
  (check-full-translation
   "for (int i = 0; i < 10; i++) { std::cout << i << std::endl; }"
   #?"for i in 0..10 {\n    println!(\"{}\", i);\n};"
   :same-output t)
  (check-full-translation
   "for (int i = 0; i != 10; i++) { std::cout << i << std::endl; }"
   #?"for i in 0..10 {\n    println!(\"{}\", i);\n};"
   :same-output t))

(deftest test-simple-for-loop-upward-inclusive ()
  (check-full-translation
   "for (int i = 0; i <= 10; i++) { std::cout << i << std::endl; }"
   #?"for i in 0..=10 {\n    println!(\"{}\", i);\n};"
   :same-output t))

(deftest test-simple-for-loop-downward-exclusive ()
  (check-full-translation
   "for (int i = 10; i > 0; i--) { std::cout << i << std::endl; }"
   #?"for i in (1..=10).rev() {\n    println!(\"{}\", i);\n};"
   :same-output t)
  (check-full-translation
   "for (int i = 10; i != 0; i--) { std::cout << i << std::endl; }"
   #?"for i in (1..=10).rev() {\n    println!(\"{}\", i);\n};"
   :same-output t)
  ;; Don't constant-fold if not a literal.
  (check-full-translation
   "int j = 0; for (int i = 10; i > j; i--) { std::cout << i << std::endl; }"
   #?"let mut j:i32 = 0;\nfor i in ({ j + 1 }..=10).rev() {\n    println!(\"{}\", i);\n};"
   :same-output t))

(deftest test-simple-for-loop-downward-inclusive ()
  (check-full-translation
   "for (int i = 10; i >= 0; i--) { std::cout << i << std::endl; }"
   #?"for i in (0..=10).rev() {\n    println!(\"{}\", i);\n};"
   :same-output t))

(deftest test-for-loop ()
  (check-full-translation
   "for (int i = 1; i < argc; i++) {}"
   "for i in 1..argc {};")
  (check-full-translation
   "for (int pointIdx = 0; pointIdx < poly1[0].n(); pointIdx++) {}"
   "for pointIdx in 0..poly1[0].n() {};")
  (check-full-translation
   "for (; n > d2; --n) {}"
   #?"while n > d2 {\n    n -= 1;\n};"))

(deftest test-for-loop-continue ()
  "Test that we emit the step before calls to continue in a while loop."
  (check-full-translation
   #p"test/loops/for-loop-continue.cc"
   #p"test/loops/for-loop-continue.rs"))

(deftest test-nested-for-loop-continue ()
  "Test that we don't touch continue statements in nested loops."
  (check-full-translation
   #p"test/loops/nested-for-loop-continue.cc"
   #p"test/loops/nested-for-loop-continue.rs"))

(deftest test-nested-complex-for-loop-continue ()
  "Test that we don't distinguish continue statements in nested while loops."
  (check-full-translation
   #p"test/loops/nested-complex-for-loop-continue.cc"
   #p"test/loops/nested-complex-for-loop-continue.rs"))

(deftest test-do-while ()
  (check-full-translation
   #p"test/loops/do-while.cc"
   ;; TODO extra space before final semicolon
   #p"test/loops/do-while.rs"))

(deftest test-do-while-continue ()
  (check-full-translation
   #p"test/loops/do-while-continue.cc"
   ;; TODO extra space before final semicolon
   #p"test/loops/do-while-continue.rs"))

(deftest test-mixed-ast-bottom-up-translation ()
  (let* ((translated-ast
           (make-partial-translation
            (cpp "x+y;")
            (to-rust (cpp "x+y;"))))
         (ast
           (make 'cpp-compound-statement
                 :children (list translated-ast))))
    (is (typep (to-rust ast) 'rust-block))))

(deftest test-aggregate-initialization ()
  (is (typep
       (to-rust (cpp "myType myfun() { return new myType(); }"))
       'rust-function-item))
  (is (equal
       (source-text
        (translate-string "std::vector<myType> myfun() { return {}; }"))
       (fmt "fn myfun() -> Vec<myType> {~%    vec![]~%}"))))

(deftest test-aggregate-struct-initialization ()
  (mvlet ((cpp file (project+file (fmt "~
struct Point {
  double x,y;
  double Distance(const Point & p) {
    auto a = this->x - p.x;
    auto b = this->y - p.y;
    return std::sqrt(a * a + b * b);
  }
  Point PointAlongSegment(const Point & p1, double distance) {
    return Point{this->x + distance * (p1.x - this->x), this->y + distance * (p1.y - this->y)};
  }
};

Point origin () {
    return {0.0, 0.0};
}"))))
    (let ((init (is (lastcar
                     (collect-if (of-type 'cpp-initializer-list)
                                 (genome cpp))))))
      (is (typep init 'cpp-initializer-list))
      (let* ((translation
               (rustrans:translate-partially cpp
                                             :interactive nil
                                             :root file))
             (rust-init
               (lastcar
                (collect-if (of-type 'rust-struct-expression) translation))))
        (is (typep rust-init 'rust-struct-expression))
        (is (source-text= "Point { x: 0.0, y: 0.0 }" rust-init))
        translation))))

(deftest test-make-derive ()
  (is (source-text=
       "#[derive(PartialEq, PartialOrd)]"
       (rustrans::make-derive '("PartialEq" "PartialOrd")))))

(deftest test-size-to-len-method-bottom-up ()
  "Test that translating size to len in a conditional doesn't break
  bottom-up translation."
  (is (typep
       (translate-string "if (x > y.size()) { }")
       'rust-ast)))

(deftest test-dont-clone-literals ()
  (is (source-text= (translate-string "x = 17;") "x = 17;")))

(deftest test-translate-reference-declaration ()
  (is (source-text= (translate-string "const int& d = c;")
                    "let d:&i32 = &c;")))

(deftest test-template-struct-to-generic-struct ()
  (is (equal "#[derive(Clone)]
struct MyType<T> {
    pub x: T,
    pub y: T,
}"
             (rustfmt
              (source-text
               (translate-string
                "template <typename T>
struct MyType {
  T x, y;
};"))))))

(deftest test-constexpr-auto-translation ()
  (is (source-text= "const x: f64 = 42.0;"
                    (translate-string "constexpr auto x = 42.0;"))))

(deftest test-display ()
  (is (equal
       (rustfmt
        (source-text
         (translate-string
          (fmt "~
struct Point {
  double x,y;
};

std::ostream& operator<<(std::ostream& os, const Point& p)
{
  return os << \"(\" << p.x << \", \" << p.y << \")\";
}
"))))
       "use std::fmt;
#[derive(Clone)]
struct Point {
    pub x: f64,
    pub y: f64,
}
impl fmt::Display for Point {
    fn fmt(&self, os: &mut fmt::Formatter<'_>) -> fmt::Result {
        return write!(os, \"({}, {})\", self.x, self.y);
    }
}")))

(deftest test-handle-field-identifier-in-reference-declarator ()
  (is (find-if (of-type 'rust-function-item)
               (translate-string "T& get_x() {}"))))

(deftest test-enum-derives-copy ()
  (is (equal "#[derive(PartialEq, Copy, Clone)]
enum Unit {
    IN,
    CM,
}"
             (rustfmt
              (source-text
               (translate-string "enum class Unit {IN, CM};"))))))

(deftest test-clone-in-struct-expression ()
  (is (string*= "x.clone()"
                (source-text
                 (translate-string "struct Foo { float x; }; Foo { __CRAM__CLONE(x) };")))))

(deftest test-normalize-declaration-without-type ()
  (every (of-type 'cpp-function-definition)
         (is (mappend #'rustran::normalize-field
                      (children
                       (cpp-body
                        (is (find-if (of-type 'cpp-class-specifier)
                                     (cpp* "class A { A(int, int); };")))))))))

(deftest test-class-specifier-to-struct-specifier ()
  (is (equal
       (source-text
        (translate-string "class myclass { int x; int y; int z; };"))
       "#[derive(Clone)]
struct myclass {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}")))

(deftest test-translate-exported-main ()
  (is (equal (source-text (translate-string "export int main() {}"))
             "pub fn main() -> () {}")))

(deftest test-export-block ()
  (is (equal (source-text (translate-string "export {
  int one() {
    return 1;
  }
  int two() {
    return 2;
  }
}"))
             "pub fn one() -> i32 {
    1
}
pub fn two() -> i32 {
    2
}")))

(deftest test-translate-namespace ()
  (is (equal (source-text (translate-string "namespace NS {
  int one() {
    return 1;
  }
  int two() {
    return 2;
  }
}"))
             "pub mod NS {
    fn one() -> i32 {
        1
    }
    fn two() -> i32 {
        2
    }
}")))

(deftest test-export-namespace ()
  (is (equal (source-text (translate-string "export namespace NS {
  int one() {
    return 1;
  }
  int two() {
    return 2;
  }
}"))
             "pub mod NS {
    pub fn one() -> i32 {
        1
    }
    pub fn two() -> i32 {
        2
    }
}")))

(deftest test-translate-switch-statement ()
  (is (equal (source-text (translate-string "
void x () {
  switch (value) {
    case 1:
      break;
    case 2:
    default:
      break;
  }
}"))
             "fn x() -> () {
    match value {
        1 => {}
        2 | _ => {}
    };
}")))


;;; Traversal template tests.

(deftest test-translate-simple-iterator ()
  (check-full-translation
   #p"test/loops/openuxas/simple-iterator.cc"
   #p"test/loops/openuxas/simple-iterator.rs"))

(deftest test-translate-simple-const-iterator/openuxas ()
  "A const iterator in OpenUXAs style (calls cend in initializer)."
  (check-full-translation
   #p"test/loops/openuxas/simple-const-iterator.cc"
   #p"test/loops/openuxas/simple-const-iterator.rs"))

(deftest test-translate-simple-const-iterator/valhalla ()
  "A const iterator in Valhalla's style (calls cend in condition)."
  (check-full-translation
   #p"test/loops/valhalla/simple-const-iterator.cc"
   #p"test/loops/valhalla/simple-const-iterator.rs"))

(def +backward-iterator+ (fmt "~
for (auto revIt = overlapping.rbegin(); revIt != overlapping.rend(); revIt++)
    {}"))

(deftest test-translate-backward-iterator ()
  (is (equal (source-text (translate-string +backward-iterator+))
             "for revIt in overlapping.iter().rev() {  };")))

(deftest test-reverse-iterator-output ()
  (check-full-translation
   #p"test/loops/reverse-iterator-output.cc"
   #p"test/loops/reverse-iterator-output.rs"
   :refactor t
   :same-output t))

(deftest test-reverse-iterator-output/no-init ()
  (check-full-translation
   #p"test/loops/no-init-reverse-iterator-output.cc"
   #p"test/loops/no-init-reverse-iterator-output.rs"
   :refactor t
   :same-output t))

(def +for-loop-no-init+
  (fmt "~
auto next = numbers.begin();
for (; next != numbers.end(); ++ii, ++next) {
  *next += *ii;
}"))

(deftest test-translate-for-loop-no-init ()
  (is (equal (source-text (translate-string +for-loop-no-init+))
             (fmt "~
let mut next = 0;
while next != numbers.len() {
    numbers[next] += *ii;
    ii += 1;
    next += 1;
};"))))

(deftest test-translate-for-loop-no-init/openuxas ()
  ;; Needs refactoring to handle casts.
  (check-full-translation
   #p"test/loops/openuxas/for-loop-no-init.cc"
   ;; TODO Why clone itWaypoint?
   #p"test/loops/openuxas/for-loop-no-init.rs"
   :refactor t))

(def +backward-for-loop-no-init+
  (fmt "~
auto w = mish->getWaypointList().rbegin();
for (; w != mish->getWaypointList().rend(); w++)
  {}"))

(deftest test-translate-backward-for-loop-no-init ()
  (is (equal (source-text (translate-string +backward-for-loop-no-init+))
             (fmt "~
let mut w = mish.get_waypoint_list().len();
while w != 0 {
    w -= 1;
};")
             )))

(deftest test-translate-two-iterators-no-init ()
  (check-full-translation
   #p"test/loops/openuxas/two-iterators-no-init.cc"
   #p"test/loops/openuxas/two-iterators-no-init.rs"))



;;; Template analysis tests.

(deftest test-extract-methods-and-operators-used ()
  (let* ((sw (cpp "void print_area(const T& object) {
  std::cout << \"The area of the given geometric object is \" << object.area() << \".\" << std::endl;
}"))
         (id (find-if (op (source-text= "object" _)) sw)))
    (attrs:with-attr-table sw
      (receive (methods operators)
          (analysis:invoked-methods+operators id sw)
        (declare (ignore operators))
        (is (length= methods 1))
        (is (source-text= (caar methods) "area"))))))


;;; Full translation tests.

(defun extract-untranslated-nodes (ast)
  (let ((asts
          (convert 'list
                   (remove-if (of-type '(or rust-wrapper full-translation))
                              ast))))
    (values asts
            (mapcar (op (ast-path ast _))
                    asts))))

(def +test-dir+
  (asdf:system-relative-pathname :lilac-rust "lilac-grist/trim-front/refactored/"))

(deftest test-translate-main-with-argc ()
  (test-rust-compiles
   (source-text
    (translate-string
     "int main(int argc, char** argv) {
  std::cout << \"argc == \" << argc << std::endl;
}"))
   :reference-output "argc == 1"))

(def +reference-output+
  (fmt "~
pts:
(0, 0)
(3, 4)
(7, 7)

result:
(0, 0)
(3, 4)
(5, 5.5)

new pts:
(5, 5.5)
(7, 7)~%"))

(defun test-rust-compiles (rust &key
                                  (check-output t)
                                  (reference-output +reference-output+))
  (when (resolve-executable "rustc")
    (with-temporary-directory (:pathname d)
      (with-working-directory (d)
        (let ((rust-file (path-join d #p"trim-front.rs")))
          (write-string-into-file rust rust-file)
          (let ((result (rustc rust-file)))
            (is (zerop result))
            (when check-output
              (is (equal
                   ($cmd (make-pathname :defaults rust-file :type nil))
                   reference-output))))))))
  rust)

(defun rustc (rust-file &rest rustc-args)
  "Compile RUST-file with rustc."
  (assert (resolve-executable "rustc"))
  (with-simple-restart (continue "Ignore rustc")
    (cmd "rustc -v --edition 2021" rustc-args rust-file)))

(defvar-unbound *last-example-translation*
  "If bound, use to store the last example translation for debugging.")

(defun test-example-translation
    (cpp rust-reference &key (compile t) refactor delimit modularize)
  (declare (optimize debug))
  (flet ((remove-blank-lines (text)
           (~> text
               lines
               (remove-if #'blankp _)
               (string-join #\Newline))))
    (let* ((rust-reference
             (~> (path-join *grist-dir* rust-reference)
                 read-file-into-string
                 remove-blank-lines
                 (regex-replace-all "(?m)( *//.*)$" _ "")))
           (cpp-source
             (~> (path-join *grist-dir* cpp)
                 read-file-into-string))
           (*refactoring-pipeline*
             (if modularize
                 *refactoring-pipeline*
                 (remove 'refactorings:modularize-project
                         *refactoring-pipeline*)))
           (cpp-source
             (if refactor
                 (analysis:with-aggressive-analysis ()
                   (refactor-raw-cpp cpp-source))
                 cpp-source))
           (rust-ast
             (analysis:with-aggressive-analysis ()
               (translate-string cpp-source)))
           (translation
             (let ((*translated-source-text-delimiters*
                     (if delimit
                         '("«RUST" . "»")
                         *translated-source-text-delimiters*)))
               (when (boundp '*last-example-translation*)
                 (setf *last-example-translation* rust-ast))
               (~> rust-ast
                   source-text
                   remove-blank-lines))))
      (assert-source-text= rust-reference translation
                           :header "Reference, Translation")
      (when compile
        (test-rust-compiles translation :check-output nil))
      (values rust-ast translation cpp-source))))

(deftest (test-trim-front-example :long-running)
    (&key (compile t) verbose source (check-output t))
  (test-example-translation "trim-front/raw/one-file/trim-front.cc"
                            "trim-front/translated/trim-front-translated.rs"
                            :refactor t))

(deftest (test-cast :long-running) ()
  (test-example-translation "test/cast.cc" "test/cast.rs"
                            :refactor t))

(deftest (test-const :long-running) (&key (compile t))
  (test-example-translation "test/const.cc" "test/const.rs"
                            :compile compile
                            :refactor t))

#-ci
(deftest (test-move-or-clone :long-running) (&key delimit)
  (test-example-translation
   "test/move_or_clone.cc"
   "test/move_or_clone.rs"
   :delimit delimit
   :refactor t))

(deftest (test-overload-minus :long-running) ()
  (test-example-translation
   #p"test/overload_minus.cc"
   #p"test/overload_minus.rs"
   :refactor t))

(deftest (test-simple-generic-method :long-running) ()
  (analysis:with-aggressive-analysis ()
    (test-example-translation
     #p"test/simple_generic_method.cc"
     #p"test/simple_generic_method.rs"
     :refactor t)))

(deftest (test-overload-minus-generic :long-running) ()
  (test-example-translation
   #p"test/overload_minus_generic.cc"
   #p"test/overload_minus_generic.rs"
   :refactor t
   :delimit t))

(deftest (test-switch-rewriting :long-running) ()
  (test-example-translation
   #p"test/switch/switch.cc"
   #p"test/switch/switch.rs"
   :refactor t
   :delimit t))

;;; Disabled until it can be fixed. See issue #28.
#+(or)
(deftest (test-generics-and-traits :long-running) ()
  (let ((traversal:*interactive* nil))
    (test-example-translation
     #p"test/generics_and_traits.cc"
     #p"test/generics_and_traits.rs"
     :refactor t
     :delimit t)))

#-ci
(deftest (test-generics-and-mixed-traits-print :long-running) ()
  (let ((traversal:*interactive* nil)
        (rustrans::*method-trait-overrides*
          '(("add" . "PairParam")
            ("print" . "PairParam"))))
    (test-example-translation
     #p"test/generics_and_mixed_traits_print.cc"
     #p"test/generics_and_mixed_traits_print.rs"
     :refactor t
     :delimit t)))

(deftest (test-generics-and-mixed-traits-print-verbose :long-running) ()
  (let ((rustrans:*force-associated-types* t)
        (traversal:*interactive* nil)
        (rustrans::*method-trait-overrides*
          '(("add" . "PairParam")
            ("print" . "PairParam"))))
    (test-example-translation
     #p"test/generics_and_mixed_traits_print.cc"
     #p"test/generics_and_mixed_traits_print_verbose.rs"
     :refactor t
     :delimit t)))

#-ci
(deftest (test-generics-and-mixed-traits :long-running) (&key (delimit t))
  (let ((rustrans::*method-trait-overrides*
          '(("add" . "PairParam"))))
    (test-example-translation
     #p"test/generics_and_mixed_traits.cc"
     #p"test/generics_and_mixed_traits.rs"
     :refactor t
     :delimit delimit)))

#-ci
(deftest (test-multiple-mutable-borrows :long-running) ()
  (let ((*refactoring-pipeline*
          (remove 'refactorings:add-move-and-clone
                  *refactoring-pipeline*)))
    (test-example-translation
     #p"test/multiple_mutable_borrows.cc"
     #p"test/multiple_mutable_borrows.rs"
     :refactor t
     :delimit t)))

(deftest test-translate-trim-front-header-only ()
  "Regression for failure in translating the header by itself."
  (let* ((raw-path (path-join *grist-dir* "trim-front-header/raw/"))
         ;; (reference-path (path-join *grist-dir* "trim-front-header/refactored"))
         (project (from-file 'cpp-project raw-path))
         (refactoring (make 'rust-translation))
         (translated-project
           (finishes
             (analysis:with-aggressive-analysis ()
               (apply-refactoring
                refactoring project
                (list (lookup project "trim-front.h")))))))
    (is (find-if (of-type 'rust-source-file)
                 (lookup translated-project
                         (namestring (translated-path "trim-front.h")))))))

(defun refactor-project/entry-point (project entry-point-name &key modularize)
  (nest
   (let* ((refactorings (refactorings:refactoring-pipeline))
          (m (find 'refactorings:modularize-project
                   refactorings
                   :key #'type-of))
          (refactorings
            (if modularize
                refactorings
                (remove m refactorings)))))
   (flet ((apply-one-refactoring (project refactoring)
            (attrs:with-attr-table project
              (let ((files
                      (get-compilation-units)))
                (analysis:with-aggressive-analysis ()
                  (restart-case
                      (apply-refactoring refactoring project files)
                    (skip-refactoring ()
                      project))))))
          (ensure-headers (project)
            (attrs:with-attr-table project
              (sel/sw/c-cpp-project:project-dependency-tree project))
            project)))
   (progn
     (let ((initial-source (source-text (ensure-headers project))))
       (dolist (r refactorings)
         (apply-one-refactoring project r))
       (assert-source-text= initial-source (source-text project)))
     (reduce #'apply-one-refactoring
             refactorings
             :initial-value project))))

(deftest test-translate-trim-front-with-header ()
  "Translate a file with a header."
  (let* ((raw-path (path-join *grist-dir* "trim-front-header/refactored"))
         (project (from-file 'cpp-project raw-path))
         (refactoring (make 'rust-translation))
         (files (collect-if (of-type 'dir:file-ast) project))
         (cc-files
           (serapeum:filter #'header-path? files :key #'dir:name))
         (files
           (append cc-files
                   (stable-set-difference files cc-files)))
         (translated-project
           (finishes
             (analysis:with-aggressive-analysis ()
               (lilac-commons/refactoring:apply-refactoring
                refactoring project
                (mappend #'dir:contents files)))))
         (rust-asts
           (collect-if (of-type 'rust-source-file) translated-project))
         (rust-files
           ;; The _header file should be first.
           (sort-new
            (mapcar (op (find-enclosing (of-type 'dir:file-ast) translated-project _))
                    rust-asts)
            #'length> :key #'dir:name)))
    (is (length= 2 rust-files))
    (test-rust-compiles
     (mapconcat (op (source-text (genome _))) rust-files #\Newline))))

(deftest test-simple-module-crate-compiles ()
  (let ((path (path-join *grist-dir* #p"test/modules/multi-file-simple-rs/src/")))
    (finishes
      (let ((output ($cmd :in path "cargo run")))
        (is (search "10 20" output))))))

(deftest test-private-module-crate-compiles ()
  (let ((path (path-join *grist-dir* #p"test/modules/multi-file-simple-private-rs/src/")))
    (finishes
      (let ((output ($cmd :in path "cargo -q run")))
        (is (string= output "10 20
Private: z = 13
Public component from 'main': 10"))))))

(defun translate-modules-to-crate (&key
                                     (project #p"test/modules/multi-file-simple_ref/")
                                     destination
                                     modularize
                                     (main "main.cc")
                                     (refactor t))
  (let* ((project-dir
           (path-join *grist-dir* project))
         (project
           (from-file 'cpp-project project-dir))
         (project
           (if refactor
               (refactor-project/entry-point project main :modularize modularize)
               project))
         ;; (main-software (dir:evolve-files-ref project main))
         (main-name (pathname-name main)))
    (let* ((files
             (attrs:with-attr-table project
               (get-compilation-units)))
           (project (rustran:translate-files-partially project files
                                                       :entry-point
                                                       (translated-path main))))
      (flet ((write-to-destination (d)
               (to-file project d)
               (is (file-exists-p (path-join d "rust/Cargo.toml")))
               (is (> (file-size-in-octets (path-join d "rust/Cargo.toml")) 0))
               (is (> (length (read-file-into-string (path-join d "rust/Cargo.toml"))) 0))
               (is (file-exists-p (path-join d (fmt "rust/src/~a.rs" main-name))))
               (with-simple-restart (continue "Return the project anyway")
                 (finishes ($cmd "cargo build"
                                 :2> *error-output*
                                 :in (path-join d "rust/"))))))
        (if destination
            (write-to-destination destination)
            (with-temporary-directory (:pathname d)
              (write-to-destination d))))
      project)))

(deftest (test-translate-modules-to-crate :long-running t) ()
  (translate-modules-to-crate))

(deftest (test-translate-modularized-trim-front-to-crate :long-running t)
    (&key destination)
  (translate-modules-to-crate :project (path-join *grist-dir* #p"trim-front/raw/C++20/")
                              :destination destination))

(deftest (test-modularize-and-translate-trim-front :long-running t) (&key destination)
  (translate-modules-to-crate :project (path-join *grist-dir* #p"trim-front/raw/C++14/")
                              :destination destination
                              :main "main.cc"
                              :modularize t))

(deftest (test-modularize-and-translate-trim-front/auto :long-running t) (&key destination)
  "Make sure we can translate a snapshot of the output of the automated refactoring."
  (translate-modules-to-crate :project (path-join *grist-dir* #p"test/modules/trim_front_auto/")
                              :destination destination
                              :main "main.cc"
                              :modularize nil
                              :refactor nil))

#+sbcl
(defun test-baseline-interactively (directory transcript)
  (with-temporary-file (:pathname f)
    (with-temporary-directory-of (:pathname d)
                                 directory
      (let ((string (read-file-into-string transcript)))
        (with-temporary-file (:pathname f)
          (write-string-into-file
           (string-replace-all "${PROJECT}" string (namestring d))
           f
           :if-exists :supersede)
          (rerun-baseline f
                          :enable '(lilac-cpp-muse lilac-rust-muse)
                          :format :vscode
                          :allow-local t))))))

#+sbcl
(define-timed-test test-interactively-translate-trim-front/single-file ()
  (setf traversal:*interactive* :force)
  (unwind-protect
       (test-baseline-interactively
        (path-join *grist-dir* "trim-front/raw/one-file/")
        (path-join *grist-dir* "mnemo/vscode-trim-front-one-file.json"))
    (setf traversal:*interactive* t)))

#+sbcl
(define-timed-test test-interactively-translate-trim-front/cpp-14 ()
  (setf traversal:*interactive* :force)
  (unwind-protect
       (test-baseline-interactively
        (path-join *grist-dir* "trim-front/raw/C++14/")
        (path-join *grist-dir* "mnemo/vscode-trim-front-cpp-14.json"))
    (setf traversal:*interactive* t)))
