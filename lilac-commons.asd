(defsystem "lilac-commons"
  :name "LiLaC C++/Rust common implementation"
  :pathname "lilac-commons"
  :author "GrammaTech"
  :license "MIT"
  :description "Common code for C++ and Rust CRAM muses"
  :class :package-inferred-system
  :depends-on ("lilac-commons/commons"))

(register-system-packages "trivia" '(:trivia.fail))
(register-system-packages "cl-mustache" '(:mustache))
