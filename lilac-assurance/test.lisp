(defpackage :lilac-assurance/test
  (:use :gt/full
        :stefil+
        :lilac-assurance/muse
        :software-evolution-library
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :cmd)
  (:export :test :run-batch))
(in-package :lilac-assurance/test)
(in-readtable :curry-compose-reader-macros)

(defroot root)

(defsuite lilac-assurance-tests "Tests for the LiLaC assurance muse")
