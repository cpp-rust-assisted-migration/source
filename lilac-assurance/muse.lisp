(defpackage :lilac-assurance/muse
  (:use
    :gt/full
    :argot/argot
    :argot/readtable
    :gt/full
    :argot/argot
    :argot/readtable
    :lsp-server
    :lsp-server/protocol
    :argot-server/utility
    :argot-server/lsp-diff
    :argot-server/muses/muse
    :argot-server/muses/code-actor
    :software-evolution-library
    :software-evolution-library/software/parseable
    :software-evolution-library/software/tree-sitter
    :dig
    :libfuzzer)
  (:shadowing-import-from :argot/readtable :argot-readtable)
  (:shadow :test)
  (:export :lilac-assurance-muse))
(in-package :lilac-assurance/muse)
(in-readtable argot-readtable)
