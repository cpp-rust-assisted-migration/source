# lilac-assurance

Assurance muse for LiLaC.

This material is based upon work supported by DARPA under Contract(s) No. HR001122C0025. Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of DARPA.
