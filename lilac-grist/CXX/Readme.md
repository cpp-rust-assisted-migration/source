# Example

This is a toy example to test the [CXX Rust library](https://cxx.rs) for Rust/C++ interoperability. It shows how to create an instance of a C++ class in Rust, and call a method on it.

To run, enter the `wrap` directory, then do `cargo run`. Compiling may take a couple of seconds, since cargo needs to download some crates (first use), compile a number of Rust and C++ files (both supplied here and auto-created), and link them.

Expected result: prints `0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16` to stdout.

# Resources on CXX:

- local file `rust_cpp_interop.pdf`
- Web: <https://tylerjw.dev/posts/rust-cpp-interop> and <https://tylerjw.dev/posts/rust-cmake-interop-part-3-cxx>.
