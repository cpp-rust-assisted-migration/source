#include <memory>

typedef unsigned short ushort;

// A toy class that stores an integer i and defines an operator () that prints 0..i-1 to stdout
class enumerate {
public:
  enumerate(const ushort& i): i(i) {}
  void operator()() const;

private:
  ushort i;
};

// C++ bindings for enumerate functionality that I want to export to Rust. Note that they can be inline

inline std::unique_ptr<enumerate> new_enumerate(const ushort& i) { return std::make_unique<enumerate>(i); }
inline void operator_paren(std::unique_ptr<enumerate> p) { (*p)(); }
