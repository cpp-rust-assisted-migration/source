fn main() {
    cxx_build::bridge("src/main.rs")
        .file("src/enumerate.cc")
        .compile("my_demo"); // I believe "my_demo" is just the name of the library with the C++ code that gets linked against Rust code. It's not the name of the final executable

}
