#[cxx::bridge]
mod ffi {
    unsafe extern "C++" {
        // optional. Used for some static checks, but not required for building
        include!("wrap/include/enumerate.hh");

        // The following types and functions must have the same name (and compatible signatures) as their C++ equivalents

        type enumerate; // declare enumerate as "some type"

        fn new_enumerate(_: &u16) -> UniquePtr<enumerate>;
        fn operator_paren(_: UniquePtr<enumerate>);
    }
}

fn main() {
    let e = ffi::new_enumerate(&17); // the & makes it a ref, which in turn is necessary since the function takes a ref in C++
    ffi::operator_paren(e);          // calls enumerate::operator()
}
