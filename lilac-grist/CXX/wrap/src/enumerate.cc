#include <iostream>

#include "wrap/include/enumerate.hh"

using namespace std;

void enumerate::operator()() const {
  for (ushort i = 0; i < this->i; ++i)
    cout << i << " ";
  cout << endl;
}
