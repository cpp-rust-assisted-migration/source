#include <cmath>
#include <iostream>
#include <list>

struct Point {
  double x, y;
  double Distance(const Point &p) {
    auto a = this->x - p.x;
    auto b = this->y - p.y;
    return std::sqrt(a * a + b * b);
  }
  Point PointAlongSegment(const Point &p1, double distance) {
    return Point{this->x + distance * (p1.x - this->x),
                 this->y + distance * (p1.y - this->y)};
  }
};
