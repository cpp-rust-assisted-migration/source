#include <cmath>
#include <iostream>
#include <vector>

struct Point {
  double x, y;
  double Distance(const Point &p) const {
    const double a = this->x - p.x;
    const double b = this->y - p.y;
    return std::sqrt(a * a + b * b);
  }
  Point PointAlongSegment(const Point &p1, const double distance) const {
    return Point{this->x + distance * (p1.x - this->x),
                 this->y + distance * (p1.y - this->y)};
  }
};
