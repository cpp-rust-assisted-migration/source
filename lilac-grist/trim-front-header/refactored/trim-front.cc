#define __CRAM__CLONE(x) x
#define __CRAM__MOVE(x) x
#define __CRAM__REF(x) x
#define __CRAM__CREF(x) x
#include <cmath>
#include <iostream>
#include <vector>

#include "trim-front.h"

std::vector<Point> trim_front(std::vector<Point> &pts, const float dist) {
  if (pts.size() < 2) {
    return {};
  }

  std::vector<Point> result;
  result.push_back(pts.front());
  double d = static_cast<double>(0.0f);
  for (std::vector<Point>::iterator p1 = pts.begin(), p2 = std::next(pts.begin()); p2 != pts.end();
       ++p1, ++p2) {
    const Point &next_point = *p2;
    const double segdist = p1->Distance(next_point);
    if ((d + segdist) > static_cast<double>(dist)) {
      const double frac = (static_cast<double>(dist) - d) / segdist;
      const Point midpoint = p1->PointAlongSegment(next_point, frac);
      result.push_back(__CRAM__CLONE(midpoint));

      pts.erase(pts.begin(), __CRAM__MOVE(p1));
      pts.front() = __CRAM__MOVE(midpoint);
      return result;
    } else {
      d += segdist;
      result.push_back(__CRAM__CLONE(*p2));
    }
  }

  pts.clear();
  return result;
}

// the rest is for testing and was added later. To keep it simple, the code is
// not particularly idiomatic

void print_point(const Point &p) {
  std::cout << "(" << p.x << ", " << p.y << ")";
}

void print_list_point(const std::vector<Point> &pts) {
  for (std::vector<Point>::const_iterator pp = pts.begin(); pp != pts.end();
       ++pp) {
    print_point(__CRAM__CREF(*pp));
    std::cout << std::endl;
  }
}

int main() {
  // for testing
  const Point p1{0.0, 0.0};
  const Point p2{3.0, 4.0}; // segdist sqrt(3^2 + 4^2) = 5
  const Point p3{7.0, 7.0}; // segdist sqrt(4^2 + 3^2) = 5, total dist 10

  std::vector<Point> pts = {__CRAM__MOVE(p1), __CRAM__MOVE(p2), __CRAM__MOVE(p3)};
  std::cout << "pts:" << std::endl;
  print_list_point(__CRAM__CREF(pts));
  std::cout << std::endl;

  const std::vector<Point> result = trim_front(__CRAM__REF(pts), 7.5);

  // expect: result = (0,0) -> (3,4) -> (5, 5.5) ; pts = (5, 5.5) -> (7,7) (the
  // rest)
  std::cout << "result:" << std::endl;
  print_list_point(__CRAM__CREF(__CRAM__MOVE(result)));
  std::cout << std::endl;
  std::cout << "new pts:" << std::endl;
  print_list_point(__CRAM__CREF(__CRAM__MOVE(pts)));
  std::cout << std::endl;

  return 0;
}
