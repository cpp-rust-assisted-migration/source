#[derive(Clone)]
struct QuickSortInt {}
impl QuickSortInt {
    unsafe fn swap(&mut self, mut a: *mut i32, mut b: *mut i32) {
        let mut t: i32 = *a;
        *a = *b;
        *b = t;
    }

    unsafe fn partition(&mut self, mut arr: *mut i32, mut low: i32, mut high: i32) -> i32 {
        let mut pivot: i32 = *arr.offset(high as isize);
        let mut i: i32 = low - 1 as i32;
        let mut j: i32 = low;
        while j <= high - 1 as i32 {
            if *arr.offset(j as isize) <= pivot {
                i += 1;
                self.swap(&mut *arr.offset(i as isize), &mut *arr.offset(j as isize));
            }
            j += 1;
        }
        self.swap(
            &mut *arr.offset((i + 1 as i32) as isize),
            &mut *arr.offset(high as isize),
        );
        return i + 1 as i32;
    }

    unsafe fn sort_array(&mut self, mut arr: *mut i32, mut low: i32, mut high: i32) {
        if low < high {
            let mut i: i32 = self.partition(arr, low, high);
            self.sort_array(arr, low, i - 1 as i32);
            self.sort_array(arr, i + 1 as i32, high);
        }
    }

    fn sort<'a>(&'a mut self, v: &'a mut Vec<i32>) -> () {
        unsafe {
            self.sort_array(v.as_mut_ptr(), 0, v.len() as i32 - 1);
        }
    }
}
fn main() {
    let argv: Vec<String> = std::env::args().collect();
    let argc = argv.len();
    let mut q: QuickSortInt = QuickSortInt {};
    let mut v: Vec<i32> = vec![9, 7, 8, 6];
    q.sort(&mut v);
    for n in v {
        print!("{} ", n);
    }
    println!();
}
