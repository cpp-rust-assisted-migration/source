#include <iostream>
#include <vector>

class QuickSortInt {
private:
  void swap(int *a, int *b) {
    int t = *a;
    *a = *b;
    *b = t;
  }

  int partition(int arr[], int low, int high) {
    int pivot = arr[high];
    int i = low - 1;

    for (int j = low; j <= high - 1; j++) {
      if (arr[j] <= pivot) {
        i++;
        swap(&arr[i], &arr[j]);
      }
    }
    swap(&arr[i + 1], &arr[high]);
    return i + 1;
  }

  void sort_array(int arr[], int low, int high) {
    if (low < high) {
      int i = partition(arr, low, high);
      sort_array(arr, low, i - 1);
      sort_array(arr, i + 1, high);
    };
  }

public:
  void sort(std::vector<int> &v) {
    this->sort_array(v.data(), 0, v.size() - 1);
  }
};

int main(int argc, char **argv) {
  QuickSortInt q{};
  std::vector<int> v{9, 8, 7, 6};
  q.sort(v);
  for (auto n : v) {
    std::cout << n << " ";
  }
  std::cout << std::endl;
  return 0;
}
