unsafe fn swap(mut a: *mut i64, mut b: *mut i64) {
    let mut t: i64 = *a;
    *a = *b;
    *b = t;
}

unsafe fn partition(
    mut arr: *mut i64,
    mut low: i64,
    mut high: i64,
) -> i64 {
    let mut pivot: i64 = *arr.offset(high as isize);
    let mut i: i64 = low - 1 as i64;
    let mut j: i64 = low;
    while j <= high - 1 as i64 {
        if *arr.offset(j as isize) <= pivot {
            i += 1;
            swap(&mut *arr.offset(i as isize), &mut *arr.offset(j as isize));
        }
        j += 1;
    }
    swap(
        &mut *arr.offset((i + 1 as i64) as isize),
        &mut *arr.offset(high as isize),
    );
    return i + 1 as i64;
}

unsafe fn quickSort(
    mut arr: *mut i64,
    mut low: i64,
    mut high: i64,
) {
    if low < high {
        let mut i: i64 = partition(arr, low, high);
        quickSort(arr, low, i - 1 as i64);
        quickSort(arr, i + 1 as i64, high);
    }
}
