// I have renamed some functions:
// Distance -> distance, PointAlongSegment -> point_along_segment
// This is the way Rust wants it, and it will create a more maintainable target program

// The Clone clause simply adds a clone (deep copy) method to the Point class. Whether we use it or not is up to us
#[derive(Clone)]
struct Point { x: f64, y: f64 }

impl Point {
  fn distance(& self, to: &Point) -> f64 { ((self.x - to.x).powi(2) + (self.y - to.y).powi(2)).sqrt() }
  fn point_along_segment(&self, to: &Point, d: f64) -> Point {
    Point { x: self.x + d * (to.x - self.x) , y: self.y + d * (to.y - self.y) }
  }
}

fn trim_front(pts: &mut Vec<Point>, dist: f32) -> Vec<Point> {
  if pts.len() < 2 {
    return Vec::new();
  }

  let mut result: Vec<Point> = Vec::new();
  result.push(pts[0].clone());
  let mut d: f64 = 0.0;
  let p1 = pts.iter();
  let mut p2 = pts.iter();
  p2.next();
  let pts_iter = p1.zip(p2);
  for (i, (p, next_point)) in pts_iter.enumerate() {
    let segdist = p.distance(&next_point);
    if d + segdist > (dist as f64) {
      let frac: f64 = ((dist as f64) - d) / segdist;
      let midpoint = p.point_along_segment(&next_point, frac); // point_along_segment produces a clone, which gets moved here into midpoint
      result.push(midpoint.clone());

      // this point will probably require human intervention
      pts.drain(..i); // remove the initial part up to i-1
      pts[0] = midpoint; // override the first point. This is an actual move: don't need midpoint anymore
      return result;
    } else {
      d += segdist;
      result.push((*next_point).clone());
    }
  }

  pts.clear();
  result
}

fn print_point(p: &Point) { print!("({}, {})", p.x, p.y); }

fn print_point_vec(pts: &Vec<Point>) {
  for p in pts.iter() {
    print_point(&p);
    println!("");
  }
}

fn main() {
  let p1 = Point { x: 0.0, y: 0.0 };
  let p2 = Point { x: 3.0, y: 4.0 }; // segdist sqrt(3^2 + 4^2) = 5
  let p3 = Point { x: 7.0, y: 7.0 }; // segdist sqrt(4^2 + 3^2) = 5, total dist 10

  let mut pts = vec![p1, p2, p3];
  println!("pts:"); print_point_vec(&pts); println!("");

  let result = trim_front(&mut pts, 7.5);

  // expect: result = (0,0) -> (3,4) -> (5, 5.5) ; pts = (5, 5.5) -> (7,7) (the rest)
  println!("result:");       print_point_vec(&result); println!("");
  println!("modified pts:"); print_point_vec(&pts);    println!("");
}
