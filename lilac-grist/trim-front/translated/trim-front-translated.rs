#[derive(Clone)]
struct Point {
    pub x: f64,
    pub y: f64,
}
impl Point {
    fn distance(&self, p: &Point) -> f64 {
        let a: f64 = self.x - p.x;
        let b: f64 = self.y - p.y;
        (a * a + b * b).sqrt()
    }

    fn point_along_segment(&self, p1: &Point, distance: f64) -> Point {
        Point {
            x: self.x + distance * (p1.x - self.x),
            y: self.y + distance * (p1.y - self.y),
        }
    }
}
fn trim_front(pts: &mut Vec<Point>, dist: f32) -> Vec<Point> {
    if pts.len() < 2 {
        return vec![];
    };
    let mut result: Vec<Point> = Vec::new();
    result.push(pts[0].clone());
    let mut d: f64 = 0.0f32 as f64;
    {
        let mut p1 = 0;
        let mut p2 = 1;
        while p2 != pts.len() {
            let next_point: &Point = &pts[p2];
            let segdist: f64 = pts[p1].distance(next_point);
            if (d + segdist) > (dist as f64) {
                let frac: f64 = ((dist as f64) - d) / segdist;
                let midpoint: Point = pts[p1].point_along_segment(next_point, frac);
                result.push(midpoint.clone());
                pts.drain(0..p1);
                pts[0] = midpoint;
                return result;
            } else {
                d += segdist;
                result.push(pts[p2].clone());
            };
            p1 += 1;
            p2 += 1;
        }
    };
    pts.clear();
    result
}

fn print_point(p: &Point) -> () {
    print!("({}, {})", p.x, p.y);
}

fn print_list_point(pts: &Vec<Point>) -> () {
    for pp in pts.iter() {
        print_point(&pp);
        println!();
    }
}

fn main() {
    let p1: Point = Point { x: 0.0, y: 0.0 };
    let p2: Point = Point { x: 3.0, y: 4.0 };
    let p3: Point = Point { x: 7.0, y: 7.0 };
    let mut pts: Vec<Point> = vec![p1, p2, p3];
    println!("pts:");
    print_list_point(&pts);
    println!();
    let result: Vec<Point> = trim_front(&mut pts, 7.5);
    println!("result:");
    print_list_point(&result);
    println!();
    println!("new pts:");
    print_list_point(&pts);
    println!();
}
