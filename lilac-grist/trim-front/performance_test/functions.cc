#include <math.h>
#include <cstring>
#include <unistd.h>

#include <fstream>
#include <string>
#include <set>

#include "functions.hh"

using namespace std;

namespace CONTROL {

  ssize_t getline(FILE* file, char** line_r, const char& eol) {
    *line_r = NULL;
    size_t* n = (size_t*)malloc(sizeof(size_t));
    ssize_t r = getdelim(line_r, n, (int)eol, file);
    __SAFE_ASSERT__ (r != 0); // I don't see how this can be zero
    free(n);
    if (r < 0)
      free(*line_r);
    else {
      // clean up the end of the string
      // the char at index r MUST be the NULL character
      __SAFE_ASSERT__ ((*line_r)[r] == '\0');
      // the char at index r-1 MAY be eol, in which case we delete it
      if ((*line_r)[r-1] == eol) {
	(*line_r)[--r] = '\0';
	__CERR__ ("(char " << ushort(eol) << " deleted)" << endl);
      }}
    return r; }

  ushort getchar() {
    const string s = system2string("/bin/bash -c 'read -n 1 c; echo -n $c'");
    return ushort( s.empty() ? 0 : s[0] ); }

  ushort getchar(const string& valids, const string& prompt) {
    ushort c;
    do {
      cout << prompt << flush; c = getchar(); if (c > 0) cout << endl; }
    while (valids.find(c) == string::npos);
    return c; }

  string getline_no_echo(const string& msg) {
    string cmd = "getline_no_echo '" + msg + "'";
    return system2string(cmd.c_str()); }

  string defline_no_echo(const string& first_msg, const string& again_msg) {
    string cmd = "defline_no_echo '" + first_msg + "' '" + again_msg + "'";
    return system2string(cmd.c_str()); }

  bool skip_line_comment(istream& is, string& word, const string& comment, const char& eol) {
    is >> word;
    if (is && word.substr(0,comment.length()) == comment) { // if is && word begins with comment
      skip_line(is, eol);
      return true; }
    return false; }

  int system(const char* const command) {
#ifdef __VERBOSE__
    wait_for_return(string("About to execute system call: '") + command + "'. Press <RETURN>, or <CTRL-C> to abort: ");
    printf("|%s|\n", command);
#endif
    return ::system(command); }

  FILE* popen(const char* const command, const char* const mode) {
#ifdef __VERBOSE__
    wait_for_return(string("About to execute system call via popen (mode \"") + string(mode) + "\"): '" + command + "'. Press <RETURN>, or <CTRL-C> to abort: ");
#endif
    return ::popen(command, mode); }

  string system2string(const char* const command, const string& line_sep) {
    string result;
    FILE* pipe = CONTROL::popen(command, "r");
    char** line_r = (char**) malloc(sizeof(char*));
    while (CONTROL::getline(pipe, line_r) >= 0) {
      result += string(*line_r) + line_sep;
      free(*line_r); }
    free(line_r);
    if (pclose(pipe) != 0)
      throw Exception("CONTROL::system2string: syscall returned non-zero");
    return result; }

  // a bit hacky, potentially slow, and side-effectful
  bool has_clipboard() {
    try                     { clear_clipboard(); return true; }
    catch(const Exception&) { return false; }
  }

  // We only want this function to cause the side effect of writing into the clipboard.
  // We never want any printed output, including error reports, hence redirect to stderr
  void to_clipboard(const string& s) {
    static const string clip_cmd = "clipboard 2> /dev/null";
    if (system(string("echo -n '" + s + "' | " + clip_cmd).c_str()) != 0)
      throw Exception("CONTROL::to_clipboard: syscall returned non-zero"); }

  // Function clear_clipboard clears the clipboard by overriding it with a sequence of spaces.
  // I found that overriding it with the empty string (to_clipboard("")) does *not*
  // clear the clipboard on all systems. E.g. emacs somehow managed to retain the string.
  // It is a mystery how it does that, but it is anyway likely safer to actually overwrite the clipboard

  bool received_yes(const string& message, ostream& out) {
    char c;
    do {
      out << message << flush; c = CONTROL::getchar(); out << endl;
    }
    while (c = toupper(c), c != 'Y' && c != 'N');
    return c == 'Y'; }
}

namespace COMPARE {
  short compare(const long& x, const long& y) {
    if (x == y)
      return 0;
    return ( x < y ? -1 : +1 ); }
}

namespace LOGARITHM {
  pair<ushort,ushort> floor_ceil_log(const ulong& i, const ushort& base) {
    __SAFE_ASSERT__ (  i  >= 1);
    __SAFE_ASSERT__ (base >= 2);
    ulong n = i;
    ushort floor = 0;
    bool is_exact_power = true;
    while (n >= base) {
      is_exact_power &= (n % base == 0);
      n /= base; // this is floor(n/base)
      ++floor; }
    is_exact_power &= (n == 1);
    ushort ceil = ( is_exact_power ? floor : floor + 1 );
    return pair<ushort,ushort>(floor, ceil); }

  ulong n_ary2decimal(const vector<ushort>& v, const ushort& base) {
    ulong m = 1;
    ulong result = 0;
    for (vector<ushort>::const_reverse_iterator digit = v.rbegin(); digit != v.rend(); ++digit) {
      __SAFE_ASSERT__ (*digit < base);
      result += (*digit) * m;
      m      *= base; }
    return result; }

  vector<ushort> decimal2n_ary(const ulong& l, const ushort& base, const ushort& size) {
    const ushort min_size = max(ceil_log(l + 1, base), ushort(1));
    __SAFE_ASSERT__ (size == 0 || size >= max(ceil_log(l + 1, 2), ushort(1)));
    const ushort vector_size = ( size == 0 ? min_size : size );
    vector<ushort> result; result.reserve(vector_size);
    ulong r = l;
    for (ushort b = 0; b < vector_size; ++b) { // just a counter: repeat vector_size times
      ushort digit = r % base;
      result.insert(result.begin(), digit);
      r /= base; } // = (r-digit)/base
    __SAFE_ASSERT__ (r == 0);
    return result; }

  bool increment(vector<bool>& bv) {
    for (ushort i = bv.size(); i >= 1; --i) { // process from right to left
      if ( (bv[i-1] = !bv[i-1]) )
    	return true; }
    return false;
  }
}

namespace PPRINT {
  string tabularize(const string& s, const string& sep, const unsigned short& tab) {
    string result;
    ushort n = s.size();
    for (int i = 0; i < n; ++i) {
      if (i > 0 && (n - i) % tab == 0)
	result += ( s[i - 1] == ' ' || s[i] == ' ' ? " " : sep );
      result += s[i]; }
    return result; }

  string hourize(const ulong& seconds) {
    if      (seconds < 60)
      return widthify(seconds) + "s";
    else if (seconds < 3600) { // 60 <= seconds < 3600
      ushort m = seconds / 60;
      ushort s = seconds % 60;
      return widthify(m) + ":" + widthify(s, 2, RIGHTJUST, '0') + "m"; }
    else {                     // 3600 <= seconds < infty
      ushort h = seconds / 3600;
      ushort s = seconds % 3600;
      ushort m = s       / 60;
      s        = s       % 60;
      return widthify(h) + ":" + widthify(m, 2, RIGHTJUST, '0') + ":" + widthify(s, 2, RIGHTJUST, '0') + "h"; }}

  string firstTimeOrNot(bool& firstTime, const string& connective, const string& common) {
    string result = ( firstTime ? common : connective + common );
    firstTime = false;
    return result; }}

namespace RANDOM {
  long integer(const long& min, const long& max) {
    __SAFE_ASSERT__ (max >= min);
    ulong size = max - min;
    long r = long(rint((float(rand()) / RAND_MAX) * size));
    return r + min; }

  float real(const float& min, const float& max) {
    __SAFE_ASSERT__ (max >= min);
    float size = std::max(max - min, float(0));
    float r = (float(rand()) / RAND_MAX) * size;
    return r + min; }

  vector<ushort> permutation(const ushort& n) {
    vector<ushort> result; result.reserve(n);
    set<ushort> S;
    for (ushort i = 0; i < n; ++i)
      S.insert(S.end(), i);
    while (! S.empty()) {
      ushort pos = integer(0,S.size() - 1);
      set<ushort>::iterator poss = S.begin();
      while (pos > 0) { ++poss; --pos; }
      result.push_back(*poss);
      S.erase(poss); }
    return result; }
}

namespace MISC {
  bool starts_with(const string& s, const string& prefix) {
    const ushort s_l = s.length(), pr_l = prefix.length();
    return ( pr_l <= s_l ? s.substr(0, pr_l) == prefix : false ); }

  bool ends_with(const string& s, const string& suffix) {
    const ushort s_l = s.length(), su_l = suffix.length();
    return ( su_l <= s_l ? s.substr(s_l-su_l, su_l) == suffix : false ); }

  void string2listofstring(const string& line, list<string>& wordlist, const char& cont) {
    istringstream iss(line);
    string word;
    while (iss >> word)
      if (wordlist.empty() ? false : wordlist.back().back() == cont) {
	// replace cont in previous word by new word, essentially appending the new word
	wordlist.back().back() = ' ';
	wordlist.back().append(word); }
      else
	// make new list entry
	wordlist.push_back(word); }
}
