#!/bin/bash
set -eu
for timings in ./*.timings; do
    echo "$timings"
    perl -ne '/(\d+\.\d+) old pts/ && print "$1"; /(\d+\.\d+) front/ && print ",$1\n"' "$timings" \
        | perl -ne '/(.*),(.*)/ && print "$2-$1\n"' | perl -pe 's/^00\.(\d+)-(59\.\d+)$/60.\1-\2/g' | tail -n +11 | bc -l | ministat
done
