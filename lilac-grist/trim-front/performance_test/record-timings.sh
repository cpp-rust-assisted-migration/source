#!/bin/bash

set -eux

if [[ "$(whoami)" != "root" ]]; then
    echo "Must be root"
    exit 1
fi

bash governor.sh performance

vmtouch -l trim-front-example.txt &
sleep 5

record_timings() {
    command time stdbuf -oL nice -20 "./$1" 2>&1 | ts "%b %d %H:%M:%.S" >> "$2"
}

inputs=("trim-front-gcc" "trim-front-clang" "trim-front-refactored-gcc" "trim-front-refactored-clang" "trim-front-rs" "trim-front-hand-rs")

for i in $(seq "$1"); do
    for input in "${inputs[@]}"; do
        echo "TIMING $input"
        record_timings "$input" "${input}.timings"
    done
done

bash governor.sh powersave

pkill vmtouch
