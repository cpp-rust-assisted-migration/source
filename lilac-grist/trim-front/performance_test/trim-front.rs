use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;

#[derive(Clone)]
struct Point {
    x: f64,
    y: f64,
}
impl Point {
    fn distance(&self, p: &Point) -> f64 {
        let a: f64 = self.x - p.x;
        let b: f64 = self.y - p.y;
        (a * a + b * b).sqrt()
    }

    fn point_along_segment(&self, p1: &Point, distance: f64) -> Point {
        Point {
            x: self.x + distance * (p1.x - self.x),
            y: self.y + distance * (p1.y - self.y),
        }
    }
}
fn trim_front(pts: &mut Vec<Point>, dist: f32) -> Vec<Point> {
    if pts.len() < 2 {
        return vec![];
    };
    let mut result: Vec<Point> = Vec::new();
    result.push(pts[0].clone());
    let mut d: f64 = 0.0f32 as f64;
    for i in 0..(pts.len() - 1) {
        let next_point: &Point = &pts[i + 1];
        let segdist: f64 = pts[i].distance(&next_point);
        if (d + segdist) > (dist as f64) {
            let frac: f64 = ((dist as f64) - d) / segdist;
            let midpoint: Point = pts[i].point_along_segment(&next_point, frac);
            result.push(midpoint.clone());
            pts.drain(0..i);
            pts[0] = midpoint;
            return result;
        } else {
            d += segdist;
            result.push(pts[i + 1].clone());
        };
    }
    pts.clear();
    result
}

fn read_i64_from_line(reader: &mut BufReader<File>) -> i64 {
  let mut line = String::new();
  reader.read_line(&mut line).expect("error reading line");
  let x: i64 = line.trim().parse().unwrap();
  x
}

fn read_f64_from_line(reader: &mut BufReader<File>) -> f64 {
  let mut line = String::new();
  reader.read_line(&mut line).expect("error reading line");
  let x: f64 = line.trim().parse().unwrap();
  x
}

#[allow(dead_code)]
fn read_string_from_line(reader: &mut BufReader<File>) -> String {
  let mut line = String::new();
  reader.read_line(&mut line).expect("error reading line");
  line.trim().to_string()
}

fn main() {
    let f = File::open("trim-front-example.txt").expect("couldn't open file");
    let mut reader = BufReader::new(f);

    let mut line: String;

    let sample_size = read_i64_from_line(&mut reader);

    let mut pts: Vec<Point> = Vec::new();

    for _i in 0..sample_size {
	let x = read_f64_from_line(&mut reader);
	let y = read_f64_from_line(&mut reader);
	// println!("Found point: ({},{})", x, y);
	pts.push(Point { x, y });
    }

    line = String::new(); reader.read_line(&mut line).expect("error reading line"); let cutoff: f32 = line.trim().parse().unwrap();

    println!("cutoff:            {}", cutoff);
    println!("old pts list size: {}", pts  .len());

    let front = trim_front(&mut pts, cutoff);

    println!("front   list size: {}", front.len());
    println!("new pts list size: {}", pts  .len());
}
