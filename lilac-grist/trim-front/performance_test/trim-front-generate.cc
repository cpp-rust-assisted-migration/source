#include <iostream>
#include <fstream>
#include <cmath>
#include <list>

#include "functions.hh"

using namespace std;

const short RANGE = 100;

int main(const int argc, const char * const * const argv) {

  if (argc == 1 || string(argv[1]) == "-h") {
    cout << argv[0] << ": usage: <sample size>" << endl
	 << "Generate a test suite of the given size, and store it in a file." << endl;
    exit(1);
  }

  const ulong sample_size = atoi(argv[1]);

  ofstream out("trim-front-example.txt");

  out << sample_size << endl;

  RANDOM::set_seed();

  float x_prev, y_prev;
  double d = 0.0f;
  for (ulong i = 0; i < sample_size; ++i) {
    float x = RANDOM::real(-RANGE, RANGE);
    float y = RANDOM::real(-RANGE, RANGE);
    // cout << "(" << x << "," << y << ")" << endl;
    if (i > 0) {
      auto xd = x - x_prev;
      auto yd = y - y_prev;
      d += std::sqrt(xd * xd + yd * yd);
    }
    out << x << endl
	<< y << endl; // storing each number in a separate line: easier to parse from Rust?
    x_prev = x;
    y_prev = y;
  }

  cout << "total distance:    " << d << endl;
  out << d << endl; // chosen such that cutoff is *likely* reached in the last segment, so we exercise the full loop

  return 0;
}
