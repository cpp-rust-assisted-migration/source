#ifndef __FUNCTIONS_HH__

#define __FUNCTIONS_HH__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

#include <string>
#include <sstream>
#include <iostream>
#include <istream>
#include <ostream>
#include <vector>
#include <list>
#include <utility>
#include <algorithm>

typedef unsigned short ushort;
typedef unsigned int   uint;
typedef unsigned long  ulong;

namespace CONTROL {
  // Append characters from current pointer to line, until but excluding eol or eof.
  // This is a wrapper around C's getline function but such that it mimics the behavior
  // of C++'s getline function. In particular, trailing newline characters are removed.
  // (Precisely, it calls C's getdelim and not getline.)
  ssize_t getline(FILE* file, char** line_r, const char& eol = '\n');

  // You can use this function to read and process a FILE* 'file' line by line:
  /* required: <stdio.h>, <stdlib.h>, <cstring> */
  // char** line_r = (char**) malloc(sizeof(char*));
  // while (CONTROL::getline(file, line_r) >= 0) {
  //   <process *line_r>;
  //   free(*line_r); // allocated by CONTROL::getline(): must free here
  // }

  // free(line_r);

  // You can read and process an input stream 'is' (as opposed to a FILE*) line by line like this
  // (note that we need the FILE* version above with functions like popen, which do not exist for streams):

  // /* required: <string>, <istream> */
  // std::string line;
  // while (std::getline(is, line = ""))
  //   <process line>

  // Make a system call that should reads 1 char, without waiting for
  // <RETURN>, and return it. The function below wraps system2string around
  // this script and returns the char IF it is "printable" (roughly, text),
  // otherwise 0.
  ushort getchar();

  // Read a single character (without waiting for <RETURN>) from valids and return.
  // Repeat on bad input. The second function uses a default prompt that depends on 'valids' (this function can therefore not be implemented by giving 'prompt' a default value in the first)
  ushort getchar(const std::string& valids, const std::string& prompt);
  ushort getchar(const std::string& valids);

  // echo-less line input
  std::string getline_no_echo(const std::string&       msg = "Please enter line (no echo): "); // just read a line
  std::string defline_no_echo(const std::string& first_msg = "Please enter line (no echo): ",
			      const std::string& again_msg = "Please enter line again: "); // read a line and then read it again, for verification

  // Skip forward until eol or eos. This is useful to delete trailing whitespace garbage in an istream.
  // Consider an ushort i and a string w:
  //   cin >> i;        // user types '123<RETURN>' : this reads the ushort, but the trailing <RETURN> remains on cin (or is in fact put back into it, God knows why)
  //   cout << "Enter new word, or <RETURN> to exit: "; getline(cin, w); // w = "", since the left-over <RETURN> terminates the getline immediately
  // The solution is to insert this call in between the two lines:
  //   ...
  //   CONTROL::skip_line(std::cin);
  //   ...
  // The issue here is of course that we mix stream input (cin >>) and low-level character input (getline). These aren't really compatible.
  // The above problem can also be fixed by doing "cin >> w", which consumes the garbage whitespace first.
  // However, using cin you cannot realize the "<RETURN> to exit" functionality.
  void skip_line(std::istream& is, const char& eol = '\n');

  // Read the next string. If it BEGINS with comment, skip the rest of the line and return true.
  // Otherwise store the string in word and return false.
  // Use this as follows: at a place in the stream where a comment is allowed (which does not have to be the beginning of the line), do:
  // std::string word;
  // while (skip_line_comment(is, word, comment, eol));
  // if (is)
  //   <use word> // (it is *not* a comment!)
  bool skip_line_comment(std::istream& is, std::string& word, const std::string& comment, const char& eol = '\n');

  int system(const char* const command);
  FILE* popen(const char* const command, const char* const mode);

  // Execute a system call and return the output (stdout) of the command as a string.
  // Throws if error.
  // If you expect a multi-line output, use line_sep to separate the lines
  std::string system2string(const char* const command, const std::string& line_sep = "");

  // Print message on out and wait for <RETURN>. Excess characters are removed
  void wait_for_return(const std::string& message = "Press <RETURN> to continue, or <CTRL-c> to abort: ", std::ostream& out = std::cerr);

  // Print message on out and wait for 'y' or 'n' answer. Return true iff 'y'
  bool received_yes(const std::string& message = "Please enter 'y' or 'n': ", std::ostream& out = std::cout);

  bool   has_clipboard();
  void    to_clipboard(const std::string& s);
  void clear_clipboard();

  void warning(const std::string& message = "", std::ostream& out = std::cerr);

  // This is a generic exception class with a self-identifying message and a code attribute
  // that can be use to transport information. The idea is to derive sub-classes from this class,
  // but in small applications it seems also fine to just throw exceptions of type CONTROL::Exception.
  struct Exception {
    const std::string message;
    const ushort code;
    Exception(const std::string& message = "", const ushort& code = 0);
    void print() const;
  };
}

// When writing a piece of software, distinguish between two types of runtime tests:

// 1. Those that check integrity of user input. Such tests should always be executed.
//    Generally, do not turn those checks off. They only happen during reading of input.
//    The reading is most likely slower than the checking. "Input" includes command line, stdin,
//    file input, etc. The program has no way of knowing whether these data are sane.

// 2. Those that check program invariants and thus verify whether the program is correct.
//    Use the __SAFE_... macros below for those tests. A simple crash due to a failed assert is enough.
//    Once the program is stable, these tests can be turned off by undefining the macro.

// Evaluate assertion if __SAFE_COMPUTATION__ is defined. If not, __SAFE_ASSERT__ results in an empty statement ";"
// Careful: in macro definition, NO SPACE between macro name and (...) !
#ifdef __SAFE_COMPUTATION__
#define __SAFE_COMPUTE__(stmt) stmt
#define __SAFE_ASSERT__(cond) assert(cond)  // short for: __SAFE_COMPUTE__ (assert cond)
#else
#define __SAFE_COMPUTE__(stmt)
#define __SAFE_ASSERT__(cond)
#endif

#ifdef __VERBOSE__
#define __VERBOSE_COMPUTE__(stmt) stmt
#define __CERR__(stuff) std::cerr << stuff  // print only in verbose mode. Use for debugging messages
#else
#define __VERBOSE_COMPUTE__(stmt)
#define __CERR__(stuff)
#endif

namespace COMPARE {
  short compare(const long& x, const long& y);

  // Returns sign(x-y): 0 if equal, 1 if x > y, -1 if x < y
  // Motivation: containers have "bool operator <" defined, but this only returns "<".
  // When you need to distinguish between all three possibilities, compare is more efficient.
  // This function was not called "compare" to avoid confusion with the above compare function
  template <class T>
  short compare_container(const T& x, const T& y) {
    typename T::const_iterator xi = x.begin(), yi = y.begin(), x_end = x.end(), y_end = y.end();
    while (true) {
      if      (xi == x_end && yi == y_end) // x == y
	return 0;
      else if (xi == x_end)                // x < y
	return -1;
      else if (yi == y_end)                // y < x
	return +1;
      else if (*xi < *yi)                  // x < y
	return -1;
      else if (*yi < *xi)                  // y < x
	return +1;
      ++xi, ++yi; }
    assert (false); }
}

namespace LOGARITHM {
  // Returns, in log(i) time, a pair that contains floor(log_b(i)) (first) and ceil(log_b(i)) (second).
  std::pair<ushort,ushort> floor_ceil_log(const ulong& i, const ushort& base);

  inline ushort floor_log(const ulong& i, const ushort& base) { return floor_ceil_log(i, base).first ; } // floor(log(i))
  inline ushort  ceil_log(const ulong& i, const ushort& base) { return floor_ceil_log(i, base).second; } // ceil (log(i))

  // Returns (log_base(i) \in N).
  inline bool is_power(const ulong& i, const ushort& base) { std::pair<ushort,ushort> p = floor_ceil_log(i, base); return p.first == p.second; }

  // Returns base^exp, for exp >= 0.
  // T can be an integral or floating type, for example
  // power((unsigned long long) 2, 10)
  // power((long double) 10.3, 20)
  template<class T>
  T power(const T& base, const ushort& exp) {
    T result = 1;
    for (ushort i = 0; i < exp; ++i)
      result *= base;
    return result; }

  // convert number in base b, given as vector of digits, into a decimal unsigned long
  ulong n_ary2decimal(const std::vector<ushort>& v, const ushort& base); // n-ary number to decimal
  std::vector<ushort> decimal2n_ary(const ulong& l, const ushort& base, const ushort& size = 0); // decimal to n-ary. Result is vector of given size (min if 0)

  // add 1 modulo the vector length. False iff overflow. For instance, to print all bitstrings of length 10:
  // vector<bool> v(10, false);
  // do PPRINT::print_all(v) << endl; while (LOGARITHM::increment(v));
  bool increment(std::vector<bool>& bv);
}

namespace PPRINT {
  typedef enum { LEFTJUST, RIGHTJUST, CENTERED } Alignment;

  // generic print function for containers that allow L2R iteration. Simplest use: PPRINT::print_all(li) << endl; // for a list of integers li
  template <class C>
  std::ostream& print_all(const C& c, std::ostream& out = std::cout, const std::string& sep = " ", const bool& frame = false) {
    for (typename C::const_iterator x = c.begin(); x != c.end(); ++x)
      out << ( x == c.begin() ? "" : sep ) << (frame ? "|" : "") << *x << (frame ? "|" : "");
    return out; }

  // Convert object x into a string, padded with fill character as necessary to achieve target width.
  // stringstream::operator<< must be defined for object x.
  // If width = 0, just return string version of x
  template <class T>
  std::string widthify(const T& x, const ushort& width = 0, const Alignment& c = CENTERED, const char& fill = ' ') {
    std::ostringstream os;
    os << x;
    std::string s = os.str();

    ushort n = s.size();
    if (n >= width)
      return s;
    ushort addlength = width - n;
    std::string result;
    switch(c) {
    case LEFTJUST:  result = s + std::string(addlength, fill); break;
    case RIGHTJUST: result = std::string(addlength, fill) + s; break;
    case CENTERED:  result = ( addlength % 2 == 0 ?
			       std::string( addlength      / 2, fill) + s + std::string( addlength      / 2, fill) :
			       std::string((addlength - 1) / 2, fill) + s + std::string((addlength + 1) / 2, fill) ); break; }
    return result; }

  // Output string s, but with <sep> inserted as tabulator every tab character. Never at left or right end of s.
  // Never if either left or right neighbor of tabulator position is ' '
  std::string tabularize(const std::string& s, const std::string& sep = ",", const ushort& tab = 3);

  // Convert object x into a string using sprintf and the format string supplied (which must include the %).
  // sprintf must be defined for object x (i.e. mainly basic types, such as numeric ones).
  // If output is expected to be very long, provide sufficient length argument
  // Example: formatString(10, "%d")
  template <class T>
  std::string formatString(const T& x, const std::string& format, const ushort& length = 10) {
    char* s = new char[length];
    snprintf(s, length, format.c_str(), x);
    std::string result(s);
    delete s;
    return result; }

  std::string hourize(const ulong& seconds); // given 20, produces "20s", given 80, produces "1:20m", given 3700, produces "1:01:40h"
  inline std::string varname(const std::string& prefix = "") { static ushort varname_counter = 0; return prefix + "_" + widthify(varname_counter++); }
  // examples for the next one: plural(1, tree) = 1 tree, plural(2, tree) = 2 trees, plural(1, entr, y, ies) = 1 entry, plural(2, entr, y, ies) = 2 entries
  inline std::string plural(const ulong& n, const std::string& base, const std::string& s_suffix = "", const std::string& p_suffix = "s") { return widthify(n) + " " + base + ( n == 1 ? s_suffix : p_suffix ); }
}

// The "main" function should call RANDOM::set_seed
namespace RANDOM {
  inline ulong set_seed() { ulong seed = time(0); srand(seed); return seed; } // seed varies each time
  inline void  set_seed(const ulong& seed) { srand(seed); }                   // repeat sequences by reusing the seed value

  long  integer(const  long& min, const  long& max); // in [min,max]. The bounds can be negative
  float real   (const float& min, const float& max); // in [min,max] or close to the bounds. Double precision is meaningless for a random number!
  inline bool bit() { return integer(0,1); }
  std::vector<ushort> permutation(const ushort& n);  // a random permutation of {0,...,n-1}
  template <class T>
  // Requires: T = container<B>, ushort T.size(), void T.reserve(ushort), void T.push_back(B), B T[ushort].
  // Examples: std::vector, std::string
  T permute(const T& in) {                           // permuting the elements of a container
    const ushort size = in.size();
    std::vector<ushort> perm = RANDOM::permutation(size);
    // PPRINT::print_all(perm) << endl;
    T out; out.reserve(size);
    for (ushort i = 0; i < size; ++i)
      out.push_back(in[perm[i]]);
    return out; }
}

// A simple class to measure wall clock time at various program points.
// You can declare several independent clocks.
// For after-the-fact memory usage, use something like "/usr/bin/time -v <cmd>"
class Clock {
public:
  typedef ulong Timestamp;

private:
  Timestamp stamp;

public:
  inline void start() { stamp = time(0); }
  inline void start(std::ostream& out) { out << "starting time ..." << std::endl; start(); }

  inline Timestamp current() const { return ::time(0) - stamp; }

  inline Clock() { start(); }
  inline Clock(std::ostream& out) { start(out); }

  static inline void wait(const ushort& seconds) { Clock c; do; while (c.current() < seconds); }
};

inline std::ostream& operator<<(std::ostream& out, const Clock& c) { out << PPRINT::hourize(c.current()); return out; }

namespace MISC {
  // Two simple utility functions for string processing
  bool starts_with(const std::string& s, const std::string& prefix);
  bool   ends_with(const std::string& s, const std::string& suffix);

  // line should be a single line of text. Extracts whitespace-separated substrings into a list of words.
  // If a substring ends in cont, the following whitespace is not interpreted as word separator.
  // Instead, the next substring is appended to the previous word.
  // Examples:    "a b c d" -> [a,b,c,d]       "a b\  c d" -> [a,b c,d]
  void string2listofstring(const std::string& line, std::list<std::string>& wordlist, const char& cont = '\\');
};

// inline function definitions

namespace CONTROL {
  inline ushort getchar(const std::string& valids) { return getchar(valids, std::string("Enter character from '") + valids + "': "); }
  inline void   skip_line(std::istream& is, const char& eol) { std::string line; std::getline(is, line, eol); }
  inline void   wait_for_return(const std::string& message, std::ostream& out) { out << message << std::flush; skip_line(std::cin, '\n'); }
  inline void   clear_clipboard() { to_clipboard("          "); }
  inline void   warning(const std::string& message, std::ostream& out) { out << "warning:" << message << std::endl; }

  inline      Exception::Exception(const std::string& message, const ushort& code): message(message), code(code) {}
  inline void Exception::print() const { if (! message.empty()) std::cerr << message << std::endl; }
}

#endif
