use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;

// I have renamed some functions:
// Distance -> distance, PointAlongSegment -> point_along_segment
// This is the way Rust wants it, and it will create a more maintainable target program

// The Clone clause simply adds a clone (deep copy) method to the Point class. Whether we use it or not is up to us
#[derive(Clone)]
struct Point { x: f64, y: f64 }

impl Point {
  fn distance(& self, to: &Point) -> f64 { ((self.x - to.x).powi(2) + (self.y - to.y).powi(2)).sqrt() }
  fn point_along_segment(&self, to: &Point, d: f64) -> Point {
    Point { x: self.x + d * (to.x - self.x) , y: self.y + d * (to.y - self.y) }
  }
}

fn trim_front(pts: &mut Vec<Point>, dist: f32) -> Vec<Point> {
  if pts.len() < 2 {
    return Vec::new();
  }

  let mut result: Vec<Point> = Vec::new();
  result.push(pts[0].clone());
  let mut d: f64 = 0.0;
  let p1 = pts.iter();
  let mut p2 = pts.iter();
  p2.next();
  let pts_iter = p1.zip(p2);
  for (i, (p, next_point)) in pts_iter.enumerate() {
    let segdist = p.distance(&next_point);
    if d + segdist > (dist as f64) {
      let frac: f64 = ((dist as f64) - d) / segdist;
      let midpoint = p.point_along_segment(&next_point, frac); // point_along_segment produces a clone, which gets moved here into midpoint
      result.push(midpoint.clone());

      // this point will probably require human intervention
      pts.drain(..i); // remove the initial part up to i-1
      pts[0] = midpoint; // override the first point. This is an actual move: don't need midpoint anymore
      return result;
    } else {
      d += segdist;
      result.push((*next_point).clone());
    }
  }

  pts.clear();
  result
}

fn read_i64_from_line(reader: &mut BufReader<File>) -> i64 {
  let mut line = String::new();
  reader.read_line(&mut line).expect("error reading line");
  let x: i64 = line.trim().parse().unwrap();
  x
}

fn read_f64_from_line(reader: &mut BufReader<File>) -> f64 {
  let mut line = String::new();
  reader.read_line(&mut line).expect("error reading line");
  let x: f64 = line.trim().parse().unwrap();
  x
}

#[allow(dead_code)]
fn read_string_from_line(reader: &mut BufReader<File>) -> String {
  let mut line = String::new();
  reader.read_line(&mut line).expect("error reading line");
  line.trim().to_string()
}

fn main() {
    let f = File::open("trim-front-example.txt").expect("couldn't open file");
    let mut reader = BufReader::new(f);

    let mut line: String;

    let sample_size = read_i64_from_line(&mut reader);

    let mut pts: Vec<Point> = Vec::new();

    for _i in 0..sample_size {
	let x = read_f64_from_line(&mut reader);
	let y = read_f64_from_line(&mut reader);
	// println!("Found point: ({},{})", x, y);
	pts.push(Point { x, y });
    }

    line = String::new(); reader.read_line(&mut line).expect("error reading line"); let cutoff: f32 = line.trim().parse().unwrap();

    println!("cutoff:            {}", cutoff);
    println!("old pts list size: {}", pts  .len());

    let front = trim_front(&mut pts, cutoff);

    println!("front   list size: {}", front.len());
    println!("new pts list size: {}", pts  .len());
}
