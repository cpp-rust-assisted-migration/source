# How to use these files/programs

1. Compile the test generator:

```C
g++ -Wall -D__SAFE_COMPUTATION__ -D__VERBOSE__ trim-front-generate.cc -c -o trim-front-generate.o
g++ -Wall -D__SAFE_COMPUTATION__ -D__VERBOSE__ functions.cc -c -o functions.o
g++ -Wall -D__SAFE_COMPUTATION__ -D__VERBOSE__ trim-front-generate.o functions.o -o trim-front-generate
```

2. Compile the trim-front programs:

```C
g++ -Wall -D__SAFE_COMPUTATION__ -D__VERBOSE__ trim-front.cc -c -o trim-front.o
g++ -Wall -D__SAFE_COMPUTATION__ -D__VERBOSE__ functions.cc -c -o functions.o
g++ -Wall -D__SAFE_COMPUTATION__ -D__VERBOSE__ trim-front.o functions.o -o trim-front-cc

rustc --edition 2021 trim-front.rs -o trim-front-rs
```

3. Run the test generator

```C
./trim-front-generate -h
./trim-front-generate 100000
```

4. Run the trim-front programs:

time ./trim-front-cc
time ./trim-front-rs

To get interesting running times, you likely need to choose a larger sample size when running `./trim-front-generate` in Step 3, perhaps as much as 10,000,000 (running trim-front takes 10-15sec for this size, on my machine). Repeat Steps 3 and 4.

5. Make clean

rm -f ./trim-front-{generate,cc,rs} ./trim-front-example.txt *.o *~
