#include <iostream>
#include <fstream>
#include <cmath>
#include <list>

struct Point {
  double x,y;
  double Distance(const Point & p) {
    auto a = this->x - p.x;
    auto b = this->y - p.y;
    return std::sqrt(a * a + b * b);
  }
  Point PointAlongSegment(const Point & p1, double distance) {
    return Point{this->x + distance * (p1.x - this->x), this->y + distance * (p1.y - this->y)};
  }
};

std::list<Point> trim_front(std::list<Point>& pts, float dist) {
  if (pts.size() < 2)
    return {};

  std::list<Point> result;
  result.push_back(pts.front());
  double d = 0.0f;
  for (auto p1 = pts.begin(), p2 = std::next(pts.begin()); p2 != pts.end(); ++p1, ++p2) {
    Point& next_point = *p2;
    double segdist = p1->Distance(next_point);
    if ((d + segdist) > dist) {
      double frac = (dist - d) / segdist;
      auto midpoint = p1->PointAlongSegment(next_point, frac);
      result.push_back(midpoint);

      pts.erase(pts.begin(), p1);
      pts.front() = midpoint;
      return result;
    } else {
      d += segdist;
      result.push_back(*p2);
    }
  }

  pts.clear();
  return result;
}

int main(const int argc, const char * const * const argv) {

  if (argc >= 2 && std::string(argv[1]) == "-h") {
    std::cout << argv[0] << ": usage: no args" << std::endl
	 << "Run the test suite currently stored in file 'trim-front-example.txt'" << std::endl;
    exit(1);
  }

  std::ifstream in("trim-front-example.txt");

  ulong sample_size; in >> sample_size;

  std::list<Point> pts;

  for (ulong i = 0; i < sample_size; ++i) {
    double x, y;
    in >> x >> y;
    pts.push_back(Point { x , y });
  }

  float cutoff;
  in >> cutoff;

  std::cout << "cutoff:            " << cutoff       << std::endl;
  std::cout << "old pts list size: " << pts.size()   << std::endl;

  std::list<Point> front = trim_front(pts, cutoff);

  std::cout << "front   list size: " << front.size() << std::endl;
  std::cout << "new pts list size: " << pts.size()   << std::endl;

  return 0;
}
