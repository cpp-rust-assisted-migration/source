#define __CRAM__CLONE(x) x
#define __CRAM__MOVE(x) x
#define __CRAM__REF(x) x
#define __CRAM__CREF(x) x
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

 struct Point {
 double x,y;
  double Distance(const Point & p) {
    const double a = this->x - p.x;
    const double b = this->y - p.y;
    return std::sqrt(a * a + b * b);
  }
  Point PointAlongSegment(const Point & p1, const double distance) {
    return Point{this->x + distance * (p1.x - this->x), this->y + distance * (p1.y - this->y)};
  }
};

 std::vector<Point> trim_front(std::vector<Point>& pts, const float dist) {
   if (pts.size() < 2)
     return {};

   std::vector<Point> result;
   result.push_back(pts.front());
   double d = static_cast<double>(0.0f);
   for (std::vector<Point>::iterator p1 = pts.begin(), p2 = std::next(pts.begin()); p2 != pts.end(); ++p1, ++p2) {
     const Point& next_point = *p2;
     const double segdist = p1->Distance(__CRAM__CREF(next_point));
     if ((d + segdist) > static_cast<double>(dist)) {
       const double frac = (static_cast<double>(dist) - d) / segdist;
       const Point midpoint = p1->PointAlongSegment(__CRAM__CREF(next_point), frac);
       result.push_back(__CRAM__CLONE(midpoint));

       pts.erase(pts.begin(), __CRAM__MOVE(p1));
       pts.front() = __CRAM__MOVE(midpoint);
       return result;
     } else {
       d += segdist;
       result.push_back(__CRAM__CLONE(*p2));
     }
   }

   pts.clear();
   return result;
 }

 int main(const int argc, const char * const * const argv) {

   if (argc >= 2 && std::string(argv[1]) == "-h") {
     std::cout << argv[0] << ": usage: no args" << std::endl
               << "Run the test suite currently stored in file 'trim-front-example.txt'" << std::endl;
     exit(1);
   }

   std::ifstream in("trim-front-example.txt");

   ulong sample_size; in >> __CRAM__CLONE(sample_size);

   std::vector<Point> pts;

   for (ulong i = 0; i < __CRAM__MOVE(sample_size); ++i) {
     double x, y;
     in >> x >> y;
     pts.push_back(Point { x , y });
   }

   float cutoff;
   __CRAM__MOVE(in) >> cutoff;

   std::cout << "cutoff:            " << cutoff       << std::endl;
   std::cout << "old pts list size: " << pts.size()   << std::endl;

   std::vector<Point> front = trim_front(__CRAM__REF(pts), cutoff);

   std::cout << "front   list size: " << front.size() << std::endl;
   std::cout << "new pts list size: " << pts.size()   << std::endl;

   return 0;
 }
