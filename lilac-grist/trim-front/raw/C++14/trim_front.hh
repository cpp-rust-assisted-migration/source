#ifndef __TRIM_FRONT_HH__
#define __TRIM_FRONT_HH__

#include<list>

struct Point {
  double x,y;
  double Distance(const Point& p) const;
  Point PointAlongSegment(const Point& p1, double distance) const;
};

std::list<Point> trim_front(std::list<Point>& pts, float dist);

void print_point(const Point& p);
void print_list_point(const std::list<Point>& pts);

#endif
