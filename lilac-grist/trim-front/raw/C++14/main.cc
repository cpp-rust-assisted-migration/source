#include<iostream>
#include<list>

#include "trim_front.hh"

int main() {
  // for testing
  Point p1 { 0.0, 0.0 };
  Point p2 { 3.0, 4.0 }; // segdist sqrt(3^2 + 4^2) = 5
  Point p3 { 7.0, 7.0 }; // segdist sqrt(4^2 + 3^2) = 5, total dist 10

  std::list<Point> pts = { p1, p2, p3 };
  std::cout << "pts:" << std::endl;
  print_list_point(pts);
  std::cout << std::endl;

  std::list<Point> result = trim_front(pts, 7.5);

  // expect: result = (0,0) -> (3,4) -> (5, 5.5) ; pts = (5, 5.5) -> (7,7) (the rest)
  std::cout << "result:"  << std::endl;
  print_list_point(result);
  std::cout << std::endl;
  std::cout << "new pts:" << std::endl;
  print_list_point(pts);
  std::cout << std::endl;

  return 0;
}
