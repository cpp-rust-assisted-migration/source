module;
#include <iostream>
#include <cmath>
#include <list>

module trim_front:impl_part;
import :interface_part;

double Point::Distance(const Point & p) const {
  auto a = this->x - p.x;
  auto b = this->y - p.y;
  return std::sqrt(a * a + b * b);
}

Point Point::PointAlongSegment(const Point & p1, double distance) const {
  return Point{this->x + distance * (p1.x - this->x), this->y + distance * (p1.y - this->y)};
}

std::list<Point> trim_front(std::list<Point>& pts, float dist) {
  if (pts.size() < 2)
    return {};

  std::list<Point> result;
  result.push_back(pts.front());
  double d = 0.0f;
  for (auto p1 = pts.begin(), p2 = std::next(pts.begin()); p2 != pts.end(); ++p1, ++p2) {
    Point& next_point = *p2;
    double segdist = p1->Distance(next_point);
    if ((d + segdist) > dist) {
      double frac = (dist - d) / segdist;
      auto midpoint = p1->PointAlongSegment(next_point, frac);
      result.push_back(midpoint);

      pts.erase(pts.begin(), p1);
      pts.front() = midpoint;
      return result;
    } else {
      d += segdist;
      result.push_back(*p2);
    }
  }

  pts.clear();
  return result;
}

void print_point(const Point& p) {
  std::cout << "(" << p.x << ", " << p.y << ")";
}

void print_list_point(const std::list<Point>& pts) {
  for (std::list<Point>::const_iterator pp = pts.begin(); pp != pts.end(); ++pp) {
    print_point(*pp);
    std::cout << std::endl;
  }
}
