module;
#include <list>

export module trim_front:interface_part;

export struct Point {
  double x,y;
  double Distance(const Point& p) const;
  Point PointAlongSegment(const Point& p1, double distance) const;
};

export std::list<Point> trim_front(std::list<Point>& pts, float dist);

export void print_point(const Point& p);
export void print_list_point(const std::list<Point>& pts);
