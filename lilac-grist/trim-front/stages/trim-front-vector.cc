#include<iostream>
#include<cmath>
#include<vector>

struct Point {
  double x,y;
  double Distance(const Point & p) {
    auto a = this->x - p.x;
    auto b = this->y - p.y;
    return std::sqrt(a * a + b * b);
  }
  Point PointAlongSegment(const Point & p1, double distance) {
    return Point{this->x + distance * (p1.x - this->x), this->y + distance * (p1.y - this->y)};
  }
};

std::vector<Point> trim_front(std::vector<Point>& pts, float dist) {
  if (pts.size() < 2)
    return {};

  std::vector<Point> result;
  result.push_back(pts.front());
  double d = 0.0f;
  for (auto p1 = pts.begin(), p2 = std::next(pts.begin()); p2 != pts.end(); ++p1, ++p2) {
    Point& next_point = *p2;
    double segdist = p1->Distance(next_point);
    if ((d + segdist) > dist) {
      double frac = (dist - d) / segdist;
      auto midpoint = p1->PointAlongSegment(next_point, frac);
      result.push_back(midpoint);

      pts.erase(pts.begin(), p1);
      pts.front() = midpoint;
      return result;
    } else {
      d += segdist;
      result.push_back(*p2);
    }
  }

  pts.clear();
  return result;
}

// the rest is for testing and was added later. To keep it simple, the code is not particularly idiomatic

void print_point(const Point& p) { std::cout << "(" << p.x << ", " << p.y << ")"; }

void print_list_point(const std::vector<Point>& pts) {
  for (std::vector<Point>::const_iterator pp = pts.begin(); pp != pts.end(); ++pp) {
    print_point(*pp);
    std::cout << std::endl;
  }
}

int main() {
  // for testing
  Point p1 { 0.0, 0.0 };
  Point p2 { 3.0, 4.0 }; // segdist sqrt(3^2 + 4^2) = 5
  Point p3 { 7.0, 7.0 }; // segdist sqrt(4^2 + 3^2) = 5, total dist 10

  std::vector<Point> pts = { p1, p2, p3 };
  std::cout << "pts:" << std::endl;
  print_list_point(pts);
  std::cout << std::endl;

  std::vector<Point> result = trim_front(pts, 7.5);

  // expect: result = (0,0) -> (3,4) -> (5, 5.5) ; pts = (5, 5.5) -> (7,7) (the rest)
  std::cout << "result:"  << std::endl;
  print_list_point(result);
  std::cout << std::endl;
  std::cout << "new pts:" << std::endl;
  print_list_point(pts);
  std::cout << std::endl;

  return 0;
}
