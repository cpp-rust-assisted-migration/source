#include <iostream>

template <typename T>
struct Point {
  T x,y;
  Point<T> operator-(const Point<T>& p) const {
    return Point<T>{x - p.x, y - p.y};
  }
};

int main() {
  Point<double> p1 = Point<double>{3.0, 2.0};
  Point<double> p2 = Point<double>{1.0, 1.0};
  Point<double> p3 = p1 - p2;
  std::cout << p3.x << "," << p3.y << std::endl;
  return 0;
}
