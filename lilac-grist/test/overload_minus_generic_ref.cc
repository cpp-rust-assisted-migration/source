#define __CRAM__MOVE(x) x
#include <iostream>

template <typename T>
struct Point {
  T x,y;
  Point<T> operator-(const Point<T>& p) const {
    return Point<T>{this->x - p.x, this->y - p.y};
  }
};

int main() {
  const Point<double> p1 = Point<double>{3.0, 2.0};
  const Point<double> p2 = Point<double>{1.0, 1.0};
  const Point<double> p3 = __CRAM__MOVE(p1) - __CRAM__MOVE(p2);
  std::cout << p3.x << "," << p3.y << std::endl;
  return 0;
}
