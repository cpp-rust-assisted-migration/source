#include <iostream>

struct Triple {
  int x, y, z;
  Triple(int x, int y, int z): x(x), y(y), z(z) {}
};

std::ostream& operator<<(std::ostream& os, const Triple& t) {
  return ( os << "(" << t.x << "," << t.y << "," << t.z << ")" );
}

void f(Triple& t) {
  t.y = 999; std::cout << "After f: " << t << std::endl;
}

void g(Triple& t) {
  t.z = 111; std::cout << "After g: " << t << std::endl;
}

int main() {
  Triple t(3,4,5);
  Triple* const t0 = &t;
  // Triple& t1 = *t0; // eliminated as redundant borrow

  f(*t0);
  g(*t0);

  return 0;
}
