#define __CRAM__CLONE(x) x
#define __CRAM__MOVE(x) x
#define __CRAM__DEREF(x) x

#include <iostream>

struct Triple {
  int x, y, z;
  Triple(const int x, const int y, const int z): x(x), y(y), z(z) {}
  void print() const { std::cout << "(" << this->x << "," << this->y << "," << this->z << ")" << std::endl; }
};

void dummy(const Triple t) { std::cout << "A dummy function that copies (not references) a triple" << std::endl; t.print(); }

int main() {
  const Triple t{1, 2, 3};

  const Triple& u = t;
  u.print();

  Triple v = __CRAM__CLONE(u); v.y = 17;
  v.print();

  dummy(__CRAM__CLONE(t));
  v.z = 87;
  v.print();
  t.print();

  Triple w = __CRAM__MOVE(t); w.x = -45;
  w.print();

  return 0;
}
