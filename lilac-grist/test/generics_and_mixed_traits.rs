use std::fmt;

pub trait PairParam {
    fn add(&self, _: &Self) -> Self;
}

#[derive(Clone)]
struct Pair<T> {
    l: T,
    r: T,
}

impl<T: PairParam> Pair<T> {
    fn sum(&self) -> T {
        self.l.add(&self.r)
    }
}

impl<T: fmt::Display> fmt::Display for Pair<T> {
    fn fmt(&self, os: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(os, "Left: {}", self.l).unwrap();
        return write!(os, "Right: {}", self.r);
    }
}

#[derive(PartialEq, Copy, Clone)]
enum Unit {
    IN,
    CM,
}

#[derive(Clone)]
struct Distance {
    pub x: f32,
    pub unit: Unit,
}

// 9. `T` is instantiated in `Pair` using the class type `Distance`. We implement all methods of the `PairParam` trait for `Distance`:

impl PairParam for Distance {
    fn add(&self, d: &Distance) -> Distance {
        if self.unit == d.unit {
            let result: Distance = Distance {
                x: self.x + d.x,
                unit: self.unit,
            };
            return result;
        } else {
            if self.unit == Unit::IN {
                let result: Distance = Distance {
                    x: self.x + ((((d.x) as f64) / 2.54) as f32),
                    unit: self.unit,
                }; // in + cm
                return result;
            } else {
                let result: Distance = Distance {
                    x: self.x + ((((d.x) as f64) * 2.54) as f32),
                    unit: self.unit,
                };
                return result;
            };
        };
    }
}

impl fmt::Display for Distance {
    fn fmt(&self, os: &mut fmt::Formatter<'_>) -> fmt::Result {
        return write!(
            os,
            "{}{}",
            self.x,
            (if self.unit == Unit::IN { "in" } else { "cm" })
        );
    }
}

fn main() {
    let l: Distance = Distance {
        x: 1.0,
        unit: Unit::IN,
    };
    let r: Distance = Distance {
        x: 5.08,
        unit: Unit::CM,
    }; // number (2 * 2.54) chosen in order to avoid rounding errors

    let p1: Pair<Distance> = Pair::<Distance> {
        l: l.clone(),
        r: r.clone(),
    };
    println!("{}", p1);
    let d1: Distance = p1.sum();
    println!("Sum: {}", d1);

    println!();

    let p2: Pair<Distance> = Pair::<Distance> { l: r, r: l };
    println!("{}", p2);
    let d2: Distance = p2.sum();
    println!("Sum: {}", d2);
}
