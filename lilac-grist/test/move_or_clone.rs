#[derive(Clone)]
struct Triple {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl Triple {
    fn print(&self) -> () {
        println!("({},{},{})", self.x, self.y, self.z);
    }
}

fn dummy(t: Triple) -> () {
    println!("A dummy function that copies (not references) a triple");
    t.print();
}

fn main() {
    let t: Triple = Triple { x: 1, y: 2, z: 3 };

    let u: &Triple = &t; // normal reference creation (move/clone is irrelevant)
    u.print();

    let mut v: Triple = u.clone();
    v.y = 17; // without 'clone' this should be an error: cannot move out of borrowed variable u
    v.print();

    dummy(t.clone()); // clone: t is still used
                      // dummy(t); // error: ownership is moved into the function; t becomes invalid (but is used below)
    v.z = 87;
    v.print();
    t.print();

    let mut w: Triple = t;
    w.x = -45; // move t into w
    w.print();
    // t.print(); // error: t is OOS
}
