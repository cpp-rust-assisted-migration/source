#define __CRAM__CLONE(x) x
#define __CRAM__MOVE(x) x
#define __CRAM__DEREF(x) x
#define __CRAM__CREF(x) x
// A type-parameterized struct `Pair` that calls methods on instances of the type parameter `T`, e.g. `l.print()`, which is only possible for other structs as instances, not scalar types.

#include <iostream>

template <typename T>
class Pair { // a "homogeneous" pair
private:
  T l;
  T r;
public:
  // construct a pair from l, r
  Pair(const T& l, const T& r): l(__CRAM__DEREF(l)), r(__CRAM__DEREF(r)) {}

  // print a pair to stdout
  void print() const {
    std::cout << "Left:  "; this->l.print(); std::cout << std::endl;
    std::cout << "Right: "; this->r.print(); std::cout << std::endl;
  }

  // add l, r to get a single T
  T sum() const { return this->l.add(__CRAM__CREF(this->r)); }
};

enum class Unit { IN, CM };

// This is essentially a wrapper around a float, adorned with a unit specification
struct Distance {
  float x;
  Unit unit;
  Distance(): x(0), unit(Unit::IN) {}
  Distance(const float& x, const Unit& unit): x(__CRAM__DEREF(x)), unit(__CRAM__DEREF(unit)) {}
  void print() const { std::cout << this->x << ( this->unit == Unit::IN ? "in" : "cm" ); }
  Distance add(const Distance& d) const;
};

Distance Distance::add(const Distance& d) const {
  if (this->unit == d.unit) {
    const Distance result{this->x + d.x, this->unit};
    return result; }
  else if (this->unit == Unit::IN) { // in + cm
    const Distance result{this->x + static_cast<float>(static_cast<double>(d.x) / 2.54), this->unit};
    return result; }
  else {                       // cm + in
    const Distance result{this->x + static_cast<float>(static_cast<double>(d.x) * 2.54), this->unit};
    return result; }
}

int main() {
  const Distance l{1.0, Unit::IN};
  const Distance r{5.08, Unit::CM}; // number (2 * 2.54) chosen in order to avoid rounding errors

  const Pair<Distance> p1={__CRAM__CLONE(l), __CRAM__CLONE(r)};
  p1.print();
  const Distance d1 = p1.sum();
  std::cout << "Sum: "; d1.print(); std::cout << std::endl;

  std::cout << std::endl;

  const Pair<Distance> p2={__CRAM__MOVE(r), __CRAM__MOVE(l)};
  p2.print();
  const Distance d2 = p2.sum();
  std::cout << "Sum: "; d2.print(); std::cout << std::endl;

  return 0;
}
