#include <functional>
#include <iostream>

template<typename T>
struct Point {
  T x, y;
  const T& get_x() const {
    return std::cref(this->x);
  }
};

int main () {
  const Point<float> p = Point<float>{1.0, 2.0};
  std::cout << p.get_x() << std::endl;
  return 0;
}
