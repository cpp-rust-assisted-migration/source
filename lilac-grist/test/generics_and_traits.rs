// The following is overspecified: we don't need `area` and `circ` to return `f32`. They only need to return something *printable*. There should really be another trait, like
//   trait Printable { fn println!(... &self ...); }
// but I don't know yet how to do that. Given the above, we could probably write more generically:
//   trait Area { fn area<P: Printable>(&self) -> P; }
//   trait Circ { fn circ<P: Printable>(&self) -> P; }
// For now I cheat: I determine P from the *uses* of function `print_area`. It is applied to a `Rectangle`, and `Rectangle`s have `area` and `circ` functions that both return `f32`, so we can use that. A lame solution!

trait Area {
    fn area(&self) -> f32;
}
trait Circ {
    fn circ(&self) -> f32;
}

// A "template" function to print the area and circumference of a generic geometric object, and a template instance Rectangle.
// For this to work, we need to have two things:
// - assumption: the signature of print_size must state that T must implement traits that provide methods `area` and `circ`.
//   This could be a single trait, or it could be one trait per feature/method, or something in between.
//   We present here a solution where the two methods come from two different traits.
// - guarantee: the Rectangle struct must actually implement the two traits.

// assumption: `T: Area + Circ`
fn print_size<T: Area + Circ>(object: &T) -> () {
    println!(
        "The area of the given geometric object is {}.",
        object.area()
    );
    println!(
        "The circumference of the given geometric object is {}.",
        object.circ()
    );
}
#[derive(Clone)]
struct Rectangle {
    x: f32,
    y: f32,
}

// guarantee: Rectangle implements both traits.
// Perhaps it is a little odd that the Rectangle struct is stand-alone but defines two methods that implement the two traits.
// Could also make the struct a template.
impl Area for Rectangle {
    fn area(&self) -> f32 {
        self.x * self.y
    }
}
impl Circ for Rectangle {
    fn circ(&self) -> f32 {
        2.0 * (self.x + self.y)
    }
}

fn main() {
    let r: Rectangle = Rectangle { x: 12.5, y: 4.5 };
    print_size(&r);
}
