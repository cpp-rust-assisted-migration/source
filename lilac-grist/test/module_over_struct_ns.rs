mod my_ns {
    pub static X: i64 = 21;
    pub struct MyClass {
        pub y: i64
    }
}

fn main() {
    let my = Box::new(my_ns::MyClass{y: 21});
    let sum: i64 = my_ns::X + my.y;
    println!("{}", sum);
}
