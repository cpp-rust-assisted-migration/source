#include <iostream>

struct Triple {
  int x, y, z;
  Triple(const int& x, const int& y, const int& z): x(x), y(y), z(z) {}
};

std::ostream& operator<<(std::ostream& os, const Triple& t) { return ( os << "(" << t.x << "," << t.y << "," << t.z << ")" ); }

void dummy(Triple t) {
  std::cout << "A dummy function that copies (not references) a triple" << std::endl
            << t << std::endl;
}

int main() {
  const Triple t(1, 2, 3);

  const Triple& u = t;
  std::cout << u << std::endl;

  Triple v = u; v.y = 17;
  std::cout << v << std::endl;

  dummy(t);
  v.z = 87;
  std::cout << v << std::endl;
  std::cout << t << std::endl;

  Triple w = t; w.x = -45;
  std::cout << w << std::endl;

  return 0;
}
