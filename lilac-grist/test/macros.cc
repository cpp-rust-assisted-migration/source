#include <iostream>

#define X

#ifdef X
#include <stdint.h>
#endif

#define LOG(MESSAGE) std::cout << MESSAGE << std::endl

#define Y 1

#define FUN(a, b) (a + b)

int main () {
    int z = Y;
    int a = FUN(2, 3);
    LOG(" " << z << a);
    return 0;
}
