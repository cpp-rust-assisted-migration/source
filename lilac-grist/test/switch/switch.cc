#include <iostream>

void printer(int i) {
    std::string output = "";

    switch (i) {
        case 0:
        case 2:
        case 4:
            output = "even";
            break;
        case 1:
        case 3:
        case 5:
            output = "odd";
            break;
        case -1:
            output = "negative";
            break;
        default:
            output = "?";
            break;
    }

    std::cout << output << std::endl;
}

void printer_no_default(int i) {
    std::string output = "";

    switch (i) {
        case 0:
            output = "0";
            break;
    }

    std::cout << output << std::endl;
}

void printer_bool(const bool &b) {
  switch (b) {
      case false:
        std::cout << "false" << std::endl;
        break;
      case true:
        std::cout << "true" << std::endl;
        break;
  }
}

void printer_return(int i) {
    std::string output = "";

    switch (i) {
        case 0:
            output = "0";
            break;
        case 1:
            std::cout << "1" << std::endl;
            return;
        default:
            break;
    }

    std::cout << output << std::endl;
}

void printer_leading_default(int i) {
    std::string output = "";

    switch (i) {
        default:
        case 0:
            output = "0";
            break;
        case 1:
            std::cout << "1" << std::endl;
            return;
    }

    std::cout << output << std::endl;
}


/* TODO: confirm that there is in fact switch fall through with previous
 *       execution. */

int main() {
    /* printer */
    printer(0);
    printer(2);
    printer(4);

    printer(1);
    printer(3);
    printer(5);

    printer(-1);

    printer(100);

    /* printer_no_default */
    printer_no_default(0);
    printer_no_default(1);

    /* printer_bool */
    printer_bool(false);
    printer_bool(true);

    /* printer_return */
    printer_return(0);
    printer_return(1);

    /* printer_leading_default */
    printer_leading_default(0);
    printer_leading_default(1);
    printer_leading_default(2);

    return 0;
}
