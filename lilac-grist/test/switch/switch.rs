fn printer(i: i32) -> () {
    let mut output: String = String::from("");

    match i {
        0 | 2 | 4 => {
            output = String::from("even");
        }
        1 | 3 | 5 => {
            output = String::from("odd");
        }
        -1 => {
            output = String::from("negative");
        }
        _ => {
            output = String::from("?");
        }
    };

    println!("{}", output);
}

fn printer_no_default(i: i32) -> () {
    let mut output: String = String::from("");

    match i {
        0 => {
            output = String::from("0");
        }
        _ => {}
    };

    println!("{}", output);
}

fn printer_bool(b: &bool) -> () {
    match b {
        false => {
            println!("false");
        }
        true => {
            println!("true");
        }
        _ => {}
    };
}

fn printer_return(i: i32) -> () {
    let mut output: String = String::from("");

    match i {
        0 => {
            output = String::from("0");
        }
        1 => {
            println!("1");
            return;
        }
        _ => {}
    };

    println!("{}", output);
}

fn printer_leading_default(i: i32) -> () {
    let mut output: String = String::from("");

    match i {
        1 => {
            println!("1");
            return;
        }
        0 | _ => {
            output = String::from("0");
        }
    };

    println!("{}", output);
}

fn main() {
    printer(0);
    printer(2);
    printer(4);

    printer(1);
    printer(3);
    printer(5);

    printer(-1);

    printer(100);

    printer_no_default(0);
    printer_no_default(1);

    printer_bool(&false);
    printer_bool(&true);

    printer_return(0);
    printer_return(1);

    printer_leading_default(0);
    printer_leading_default(1);
    printer_leading_default(2);
}
