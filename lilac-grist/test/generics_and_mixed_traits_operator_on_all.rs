// 0. The template-parameterized class definition is that of `Pair`.
// 1. The template-type variables are `l` and `r`.
// 2. There are no  method  requirements for `T`.
// 3. There are no function requirements for `T`.
// 4. There are two operator requirements:
//    - `ostream& operator<<(ostream&, const T&)`
//    - `T operator+(const T&, const T&)`
// 5. We define a new trait, `PairParam`
// 6. Since there are no method or function requirements, there is nothing to do in this step.

// 7. The traits associated with the two operators taking a `T` as argument are:
use std::fmt::Display;
use std::ops::Add;

// 8. We migrate the definition of `Pair` to Rust. The implementation of `Pair` relies on the traits we have identified as necessary, plus the `Clone` trait:

struct Pair<T> {
    l: T,
    r: T,
}

impl<T: Display + Add<Output = T> + Clone> Pair<T> {
    fn print(&self) {
        println!("Left:  {}", self.l);
        print!  ("Right: {}", self.r);
    }

    fn add(&self) -> T {
        self.l.clone() + self.r.clone()
    } // Rust allows a scalar type to be "cloned"
}

// 9. `T` is instantiated using `f32`. We double-check that `f32` implements the Display, Add and Clone traits:
fn main() {
    let l: f32 = 1.00;
    let r: f32 = 5.08;

    let p1 = Pair { l: l, r: r };
    p1.print(); println!();
    let d1 = p1.add();
    println!("Sum: {}", d1);

    println!();

    let p2 = Pair { l: r, r: l };
    p2.print(); println!();
    let d2 = p2.add();
    println!("Sum: {}", d2);
}
