#define __CRAM__CLONE(x) x
#define __CRAM__MOVE(x) x
#define __CRAM__DEREF(x) x
#include <iostream>

struct Triple {
  int x, y, z;
  Triple(const int& x, const int& y, const int& z): x(__CRAM__DEREF(x)), y(__CRAM__DEREF(y)), z(__CRAM__DEREF(z)) {}
};

std::ostream& operator<<(std::ostream& os, const Triple& t) { return ( __CRAM__DEREF(os) << "(" << t.x << "," << t.y << "," << t.z << ")" ); }

void dummy(const Triple t) {
  std::cout << "A dummy function that copies (not references) a triple" << std::endl
            << __CRAM__MOVE(t) << std::endl;
}

int main() {
  const Triple t{1, 2, 3};

  const Triple& u = t;
  std::cout << __CRAM__CLONE(u) << std::endl;

  Triple v = __CRAM__CLONE(u); v.y = 17;
  std::cout << __CRAM__CLONE(v) << std::endl;

  dummy(__CRAM__CLONE(t));
  v.z = 87;
  std::cout << __CRAM__MOVE(v) << std::endl;
  std::cout << __CRAM__CLONE(t) << std::endl;

  Triple w = __CRAM__MOVE(t); w.x = -45;
  std::cout << __CRAM__MOVE(w) << std::endl;

  return 0;
}
