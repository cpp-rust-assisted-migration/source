{
    let mut i:i32 = 0;
    while end_test() {
        if a[i] < 0 {
            step();
            continue;
        };
        for j in 0..i {
            if a[j] < 0 {
                continue;
            };
            handle_positive(j);
        };
        step();
    };
};
