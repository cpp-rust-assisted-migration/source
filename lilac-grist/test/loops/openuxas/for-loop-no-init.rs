let iWaypointIndexStart:i32 = 0;
let mut iWaypointIndexAfter:i32 = iWaypointIndexStart + 1;
let mut itWaypoint = 0 + iWaypointIndexStart;
let mut itWaypointNext = itWaypoint.clone() + 1;
while itWaypointNext != vway_get_waypoints().len() {
    let posChangeFromInitalWaypoint:CPosition = (((vway_get_waypoints()[itWaypointNext]) as CPosition) - ((vway_get_waypoints()[itWaypoint]) as CPosition)) * dFractionOfSegment.clone() + ((vway_get_waypoints()[itWaypoint]) as CPosition);
    itWaypoint += 1;
    itWaypointNext += 1;
    iWaypointIndexAfter += 1;
};
