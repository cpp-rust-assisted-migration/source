const int iWaypointIndexStart = 0;
int iWaypointIndexAfter = iWaypointIndexStart + 1;
auto itWaypoint = vwayGetWaypoints().begin() + iWaypointIndexStart;
auto itWaypointNext = itWaypoint + 1;
for (; itWaypointNext != vwayGetWaypoints().end();
     itWaypoint++, itWaypointNext++, iWaypointIndexAfter++) {
  CPosition posChangeFromInitalWaypoint =
      ((CPosition)(*itWaypointNext) - (CPosition)(*itWaypoint)) *
          dFractionOfSegment +
      (CPosition)(*itWaypoint);
}
