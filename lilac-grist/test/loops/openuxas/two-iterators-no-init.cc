auto itPointFirst = vxyTrueRoad.begin();
auto itPointSecond = vxyTrueRoad.begin() + 1;
for (;
     (itPointFirst != vxyTrueRoad.end() && itPointSecond != vxyTrueRoad.end());
     itPointFirst++, itPointSecond++) {
  dLengthRoad += itPointSecond->dist(*itPointFirst);
}
