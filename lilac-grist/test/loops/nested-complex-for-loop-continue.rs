{
    let mut i:i32 = 0;
    while end_test1() {
        if a[i] < 0 {
            step1(i);
            continue;
        };
        {
            let mut j:i32 = 0;
            while end_test2(j) {
                if a[j] < 0 {
                    step2(j);
                    continue;
                };
                handle_positive(j);
                step2(j);
            };
        };
        step1(i);
    };
};
