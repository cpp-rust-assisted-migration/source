std::vector<int> v = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
auto it = v.rbegin();
for (; it != v.rend(); ++it) {
  std::cout << *it << std::endl;
}
