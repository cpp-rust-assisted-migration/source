for (int i = 0; end_test1(); step1(i)) {
  if (a[i] < 0) {
    continue;
  }
  for (int j = 0; end_test2(j); step2(j)) {
    if (a[j] < 0) {
      continue;
    }
    handle_positive(j);
  }
}
