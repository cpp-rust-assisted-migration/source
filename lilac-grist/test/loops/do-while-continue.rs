loop {
    if inner_test() {
        if !(test()) {
            break;
        };
        continue;
    };
    do_something();
    if !(test()) {
        break;
    };
} ;
