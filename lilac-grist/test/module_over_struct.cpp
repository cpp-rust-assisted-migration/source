// When a C++ program uses a class as a namespace, we may need to
// translate it as a module with a struct declaration inside,
// qualifying uses of the name of the class with the new module.

#include <iostream>
#include <memory>

class MyClass {
public:
  static const int x = 21;
  int y;

  MyClass(int y) : y(y){};
};

int main() {
  auto my = std::make_unique<MyClass>(21);
  int sum = MyClass::x + my->y;
  std::cout << sum << std::endl;
  return 0;
}
