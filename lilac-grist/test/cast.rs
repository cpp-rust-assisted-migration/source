fn main() {
    let x: f32 = 10.0;
    let y: f64 = 20.0;
    let z0: f32 = ((x as f64) + y) as f32;
    let z1: f64 = (x as f64) * y;

    println!("{}", z0);
    println!("{}", z1);

    let a: i16 = 17;
    let b: i64 = 5;
    let c: i16 = a / (b as i16);
    println!("{}", c);
}
