// 0. The template-parameterized class definition is that of `Pair`.
// 1. The template-type variables are `l` and `r`.
// 2. The method requirements for `T` are
//      `void T.print()`
//      `T add(T,T)`
// 3. There are no function requirements.
// 4. There are no operator requirements.
// 5. We define a new trait, `PairParam`
// 6. We translate all method requirements into Rust declarations and include them in the trait:
pub trait PairParam {
    type Add1;
    type AddOutput;
    fn print(&self) -> ();
    fn add(&self, _: &Self::Add1) -> Self::AddOutput;
}
// The following is interesting about the add method: the required signature of this function should really be
//   fn add(&self, d: &PairParam) -> PairParam
// i.e. a method operating on a PairParam that takes another PairParam and returns a PairParam.
// But I didn't get Rust to accept that. So I have *generalized* this, adding a type parameter to the trait: PairParam<T>.
// 7. Since there are no operator requirements, nothing to do here.
// 8. We migrate the definition of `Pair` in Rust. The impl of `Pair` relies on the `PairParam` trait:
#[derive(Clone)]
struct Pair<T> {
    l: T,
    r: T,
}

impl<T: PairParam<Add1 = T, AddOutput = T>> Pair<T> {
    fn print(&self) -> () {
        print!("Left:  ");
        self.l.print();
        println!();
        print!("Right: ");
        self.r.print();
        println!();
    }

    fn sum(&self) -> T {
        self.l.add(&self.r)
    }
}

#[derive(PartialEq, Copy, Clone)]
enum Unit {
    IN,
    CM,
}

#[derive(Clone)]
struct Distance {
    pub x: f32,
    pub unit: Unit,
}

// 9. `T` is instantiated in `Pair` using the class type `Distance`. We implement all methods of the `PairParam` trait for `Distance`:

impl PairParam for Distance {
    type Add1 = Distance;
    type AddOutput = Distance;

    fn print(&self) -> () {
        print!(
            "{}{}",
            self.x,
            (if self.unit == Unit::IN { "in" } else { "cm" })
        );
    }
    fn add(&self, d: &Distance) -> Distance {
        if self.unit == d.unit {
            let result: Distance = Distance {
                x: self.x + d.x,
                unit: self.unit,
            };
            return result;
        } else {
            if self.unit == Unit::IN {
                let result: Distance = Distance {
                    x: self.x + ((((d.x) as f64) / 2.54) as f32),
                    unit: self.unit,
                }; // in + cm
                return result;
            } else {
                let result: Distance = Distance {
                    x: self.x + ((((d.x) as f64) * 2.54) as f32),
                    unit: self.unit,
                };
                return result;
            };
        };
    }
}

fn main() {
    let l: Distance = Distance {
        x: 1.0,
        unit: Unit::IN,
    };
    let r: Distance = Distance {
        x: 5.08,
        unit: Unit::CM,
    }; // number (2 * 2.54) chosen in order to avoid rounding errors

    let p1: Pair<Distance> = Pair::<Distance> {
        l: l.clone(),
        r: r.clone(),
    };
    p1.print();
    let d1: Distance = p1.sum();
    print!("Sum: ");
    d1.print();
    println!();

    println!();

    let p2: Pair<Distance> = Pair::<Distance> { l: r, r: l };
    p2.print();
    let d2: Distance = p2.sum();
    print!("Sum: ");
    d2.print();
    println!();
}
