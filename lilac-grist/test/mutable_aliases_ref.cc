#define __CRAM__DEREF(x) x
void f(int) {}
void g(int) {}

int main () {
  const int pl = 1;
  const int& r = pl;
  {
    const int& p = __CRAM__DEREF(r); // scope S begins here. We have A_S = {r,*p}
    f(__CRAM__DEREF(r));        // assume void f(int);
    g(__CRAM__DEREF(p));       // assume void g(int);
  }              // scope S ends here
  g(__CRAM__DEREF(r));          // this is a different scope. It has no illegal alias nests.
}
