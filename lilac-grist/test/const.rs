#[derive(Clone)]
struct Triple {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

fn f() -> Triple {
    let mut a: i32 = 20; // a, b must both be mutable since *b is modified
    let b: &mut i32 = &mut a;
    *b += 1;

    let c: i32 = 42;
    let d: &i32 = &c; // we elevate the C++ pointer d to an i32. The important thing is that it is immutable

    let e: i32 = *d / a;

    let t: Triple = Triple { x: *d, y: a, z: e };
    t // a move. Local variable transfers ownership to caller
}

fn main() {
    let t: Triple = f();
    println!("{} / {} = {}", t.x, t.y, t.z);
}
