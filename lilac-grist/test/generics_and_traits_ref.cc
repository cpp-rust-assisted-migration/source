#define __CRAM__MOVE(x) x
#define __CRAM__DEREF(x) x
#define __CRAM__CREF(x) x
#include <iostream>

// the following template function definition relies on the availability of methods `area` and `circ` for an `object`,
// but -- unlike in Rust -- the availability is not checked/enforced until instantiation time.
template <typename T>
void print_size(const T& object) {
  std::cout << "The area of the given geometric object is " << object.area() << "." << std::endl;
  std::cout << "The circumference of the given geometric object is " << object.circ() << "." << std::endl;
}

struct Rectangle {
  Rectangle(const double& x, const double& y): x(__CRAM__DEREF(x)), y(__CRAM__DEREF(y)) {}
  float x, y;
  float area() const { return this->x * this->y; }
  float circ() const { return 2.0 * (this->x + this->y); }
};

int main() {
  const Rectangle r { 12.5, 4.5 };
  print_size(__CRAM__CREF(__CRAM__MOVE(r)));

  return 0;
}
