// A type-parameterized struct `Pair` that uses instances of type `T` only as operator arguments.

#include <iostream>

template <typename T>
class Pair { // a "homogeneous" pair
private:
  T l;
  T r;
public:
  // construct a pair from l, r
  Pair(const T& l, const T& r): l(l), r(r) {}

  // sum l, r to get a single T
  T sum() const { return l + r; }

  // print a pair to stdout
  template <typename TT> friend std::ostream& operator<<(std::ostream&, const Pair<TT>&);
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const Pair<T>& p) {
  os << "Left:  " << p.l << std::endl;
  os << "Right: " << p.r;
  return os;
}

int main() {
  float l = 1.00;
  float r = 5.08;

  Pair<float> p1(l, r);
  std::cout << p1 << std::endl;
  auto d1 = p1.sum();
  std::cout << "Sum: " << d1 << std::endl;

  std::cout << std::endl;

  Pair<float> p2(r, l);
  std::cout << p2 << std::endl;
  auto d2 = p2.sum();
  std::cout << "Sum: " << d2 << std::endl;

  return 0;
}
