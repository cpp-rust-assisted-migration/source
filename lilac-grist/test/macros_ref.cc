#define __CRAM__CLONE(x) x
#include <iostream>

#define X

#ifdef X
#include <stdint.h>
#endif

#define LOG(MESSAGE) std::cout << MESSAGE << std::endl

const constexpr int Y = 1;

constexpr auto FUN(const auto a, const auto b) { return  (__CRAM__CLONE(a) + __CRAM__CLONE(b)); }

int main () {
    const int z = Y;
    const int a = static_cast<int>(FUN(2, 3));
    std::cout << " " << z << a << std::endl;
    return 0;
}
