#include <iostream>

struct Point {
  double x,y;
  Point operator-(const Point& p) const {
    return Point{x - p.x, y - p.y};
  }
};

int main() {
  Point p1 = Point{3.0, 2.0};
  Point p2 = Point{1.0, 1.0};
  Point p3 = p1 - p2;
  std::cout << p3.x << "," << p3.y << std::endl;
  return 0;
}
