// A type-parameterized struct `Pair` that calls methods on instances of the type parameter `T`, e.g. `l.print()`, which is only possible for other structs as instances, not scalar types.

#include <iostream>

template <typename T>
class Pair { // a "homogeneous" pair
private:
  T l;
  T r;
public:
  // construct a pair from l, r
  Pair(const T& l, const T& r): l(l), r(r) {}

  // print a pair to stdout
  void print() const {
    std::cout << "Left:  "; l.print(); std::cout << std::endl;
    std::cout << "Right: "; r.print(); std::cout << std::endl;
  }

  // add l, r to get a single T
  T sum() const { return l.add(r); }
};

enum class Unit { IN, CM };

// This is essentially a wrapper around a float, adorned with a unit specification
struct Distance {
  float x;
  Unit unit;
  Distance(): x(0), unit(Unit::IN) {}
  Distance(const float& x, const Unit& unit): x(x), unit(unit) {}
  void print() const { std::cout << x << ( unit == Unit::IN ? "in" : "cm" ); }
  Distance add(const Distance& d) const;
};

Distance Distance::add(const Distance& d) const {
  if (unit == d.unit) {
    Distance result(x + d.x, unit);
    return result; }
  else if (unit == Unit::IN) { // in + cm
    Distance result(x + d.x / 2.54, unit);
    return result; }
  else {                       // cm + in
    Distance result(x + d.x * 2.54, unit);
    return result; }
}

int main() {
  Distance l(1.00, Unit::IN);
  Distance r(5.08, Unit::CM); // number (2 * 2.54) chosen in order to avoid rounding errors

  Pair<Distance> p1(l, r);
  p1.print();
  Distance d1 = p1.sum();
  std::cout << "Sum: "; d1.print(); std::cout << std::endl;

  std::cout << std::endl;

  Pair<Distance> p2(r, l);
  p2.print();
  Distance d2 = p2.sum();
  std::cout << "Sum: "; d2.print(); std::cout << std::endl;

  return 0;
}
