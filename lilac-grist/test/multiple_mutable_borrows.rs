use std::fmt;

#[derive(Clone)]
struct Triple {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl fmt::Display for Triple {
    fn fmt(&self, os: &mut fmt::Formatter<'_>) -> fmt::Result {
        return write!(os, "({},{},{})", self.x, self.y, self.z);
    }
}

fn f(t: &mut Triple) -> () {
    t.y = 999;
    println!("After f: {}", *t);
}
fn g(t: &mut Triple) -> () {
    t.z = 111;
    println!("After g: {}", *t);
}

fn main() {
    let mut t: Triple = Triple { x: 3, y: 4, z: 5 };
    let t0: &mut Triple = &mut t; // implement the C++ pointer t0 as a reference in Rust
                     // omitting t1, since it doesn't exist in the refactored file

    f(t0);
    g(t0);
}
