use std::ops::Sub;

#[derive(Clone)]
struct Point {
    pub x: f64,
    pub y: f64,
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, p: Self) -> Self::Output {
        Self {
            x: self.x - p.x,
            y: self.y - p.y,
        }
    }
}

fn main() {
    let p1: Point = Point { x: 3.0, y: 2.0 };
    let p2: Point = Point { x: 1.0, y: 1.0 };
    let p3: Point = p1 - p2;
    println!("{},{}", p3.x, p3.y);
}
