#[derive(Clone)]
struct Point<T> {
    pub x: T,
    pub y: T,
}

impl<T> Point<T> {
    fn get_x(&self) -> &T {
        &self.x
    }
}

fn main() {
    let p: Point<f32> = Point::<f32> { x: 1.0, y: 2.0 };
    println!("{}", p.get_x());
}
