#include <iostream>

struct Triple { int x, y, z; };

Triple f() {
  int  a = 20;
  int& b = a;
  ++b;

  int  c = 42;
  int* d = &c;

  int e = *d / a;

  Triple t{*d, a, e};

  return t;
}

int main() {
  Triple t = f();
  std::cout << t.x << " / " << t.y << " = " << t.z << std::endl;

  return 0;
}
