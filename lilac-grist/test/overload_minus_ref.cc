#define __CRAM__MOVE(x) x
#include <iostream>

struct Point {
  double x,y;
  Point operator-(const Point& p) const {
    return Point{this->x - p.x, this->y - p.y};
  }
};

int main() {
  const Point p1 = Point{3.0, 2.0};
  const Point p2 = Point{1.0, 1.0};
  const Point p3 = __CRAM__MOVE(p1) - __CRAM__MOVE(p2);
  std::cout << p3.x << "," << p3.y << std::endl;
  return 0;
}
