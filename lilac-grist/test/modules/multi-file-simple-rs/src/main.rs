#![allow(unused_imports)]

pub mod a;
use a::*;

pub mod b;
use b::*;

fn main() {
    let a: A = A { x: 10, y: 20 };
    a.print();
}
