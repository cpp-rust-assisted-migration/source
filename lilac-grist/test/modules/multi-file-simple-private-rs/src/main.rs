#![allow(unused_imports)]

pub mod a;
use a::*;

pub mod b;
use b::*; // not needed (not using b here) but ok

fn main() {
    let a = A::new(10, 20);
    a.print();
    // println!("Private component from 'main': {}", a.z); // this one doesn't compile
    println!("Public component from 'main': {}", a.x); // this one compiles and runs fine
}
