use super::interface_part::*;
use crate::b as b;

impl A {
    pub fn new(xx: i64, yy: i64) -> Self {
        A { x: xx, y: yy, z: 13 } // is there a nicer way than naming the constructor parameters xx, yy? `A { self.x: x, ... }` does not work
    }
    pub fn print(&self) {
        b::print(self.x, self.y);
        println!("Private: z = {}", self.z);
    }
}
