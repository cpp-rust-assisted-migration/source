#ifndef A_H
#define A_H

class A {
  public:
    int x, y;
    A(int, int);
    void print();
  private:
    int z;
};

#endif // A_H
