#include <iostream>

#include "a.h"
#include "b.h"

A::A(int x, int y): x(x), y(y), z(42) {}

void A::print() {
    B::print(x, y);
    std::cout << "Private: z = " << z << std::endl;
}
