// #include <iostream> // to confirm the below "no"

import a;

int main() {
    A a{10, 20};

    a.print();

    // std::cout << "Private: a.z = " << a.z << std::endl; // no

    return 0;
}
