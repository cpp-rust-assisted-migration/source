module;

export import <iostream>;

export import <vector>;

module cram_main:impl_part;
import :interface_part;

import cram_trim_front;
#define __CRAM__MOVE(x) x

#define __CRAM__REF(x) x

#define __CRAM__CREF(x) x

 export int main () {
  // for testing
  const Point p1 { 0.0, 0.0 };
  const Point p2 { 3.0, 4.0 }; // segdist sqrt(3^2 + 4^2) = 5
  const Point p3 { 7.0, 7.0 }; // segdist sqrt(4^2 + 3^2) = 5, total dist 10

  std::vector<Point> pts = { __CRAM__MOVE(p1), __CRAM__MOVE(p2), __CRAM__MOVE(p3) };
  std::cout << "pts:" << std::endl;
  print_list_point(__CRAM__CREF(pts));
  std::cout << std::endl;

  const std::vector<Point> result = trim_front(__CRAM__REF(pts), 7.5);

  // expect: result = (0,0) -> (3,4) -> (5, 5.5) ; pts = (5, 5.5) -> (7,7) (the rest)
  std::cout << "result:"  << std::endl;
  print_list_point(__CRAM__CREF(__CRAM__MOVE(result)));
  std::cout << std::endl;
  std::cout << "new pts:" << std::endl;
  print_list_point(__CRAM__CREF(__CRAM__MOVE(pts)));
  std::cout << std::endl;

  return 0;
}
