module;

export import <iostream>;

export import <cmath>;

export import <vector>;

module cram_trim_front:impl_part;
import :interface_part;
#define __CRAM__CLONE(x) x

#define __CRAM__MOVE(x) x

#define __CRAM__CREF(x) x

double Point::Distance(const Point &p) const {
  const double a = this->x - p.x;
  const double b = this->y - p.y;
  return std::sqrt(a * a + b * b);
}

Point Point::PointAlongSegment(const Point &p1, const double distance) const {
  return Point{this->x + distance * (p1.x - this->x),
               this->y + distance * (p1.y - this->y)};
}

std::vector<Point> trim_front(std::vector<Point> &pts, const float dist) {
  if (pts.size() < 2)
    return {};

  std::vector<Point> result;
  result.push_back(pts.front());
  double d = static_cast<double>(0.0f);
  for (std::vector<Point>::iterator p1 = pts.begin(),
                                    p2 = std::next(pts.begin());
       p2 != pts.end(); ++p1, ++p2) {
    const Point &next_point = *p2;
    const double segdist = p1->Distance(next_point);
    if ((d + segdist) > static_cast<double>(dist)) {
      const double frac = (static_cast<double>(dist) - d) / segdist;
      const Point midpoint = p1->PointAlongSegment(next_point, frac);
      result.push_back(__CRAM__CLONE(midpoint));

      pts.erase(pts.begin(), __CRAM__MOVE(p1));
      pts.front() = __CRAM__MOVE(midpoint);
      return result;
    } else {
      d += segdist;
      result.push_back(__CRAM__CLONE(*p2));
    }
  }

  pts.clear();
  return result;
}

void print_point(const Point &p) {
  std::cout << "(" << p.x << ", " << p.y << ")";
}

void print_list_point(const std::vector<Point> &pts) {
  for (std::vector<Point>::const_iterator pp = pts.begin(); pp != pts.end();
       ++pp) {
    print_point(__CRAM__CREF(*pp));
    std::cout << std::endl;
  }
}
