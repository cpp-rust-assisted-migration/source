module;

export import <vector>;

export module cram_trim_front:interface_part;
export {
  struct Point {
    double x, y;
    double Distance(const Point &p) const;
    Point PointAlongSegment(const Point &p1, const double distance) const;
  };
  std::vector<Point> trim_front(std::vector<Point> & pts, const float dist);
  void print_point(const Point &p);
  void print_list_point(const std::vector<Point> &pts);
}
