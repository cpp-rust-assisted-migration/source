use std::fmt::Display;

// two abstract types that enforce the existence of functions that produce something printable
trait Area<T: Display> {
    fn area(&self) -> T;
}
trait Circ<T: Display> {
    fn circ(&self) -> T;
}

// A "template" function to print the area and circumference of a generic geometric object, and a template instance Rectangle.
// For this to work, we need to have two things:
// - assumption: the signature of print_size must state that T must implement traits that provide methods `area` and `circ`, which produce something printable.
//   This could be a single trait, or it could be one trait per feature/method, or something in between.
//   We present here a solution where the two methods come from two different traits.
// - guarantee: the Rectangle struct must actually implement the two traits.

// assumption: for "printable" types X and Y, T implements Area<X> + Circ<Y>
fn print_size<X: Display, Y: Display, T: Area<X> + Circ<Y>>(object: &T) {
    println!(
        "The area of the given geometric object is {}.",
        object.area()
    );
    println!(
        "The circumference of the given geometric object is {}.",
        object.circ()
    );
}

struct Rectangle {
    x: f32,
    y: f32,
}

// guarantee: Rectangle implements both traits.
// Perhaps it is a little odd that the Rectangle struct is stand-alone but defines two methods that implement the two traits.
// Could also make the struct a template.
impl Area<f32> for Rectangle {
    fn area(&self) -> f32 {
        self.x * self.y
    }
}
impl Circ<f32> for Rectangle {
    fn circ(&self) -> f32 {
        2.0 * (self.x + self.y)
    }
}

fn main() {
    let r = Rectangle { x: 12.5, y: 4.5 };
    print_size(&r);
}
