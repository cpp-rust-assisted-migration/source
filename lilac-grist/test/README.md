This directory contains a number of small C++ files `*.cc`, a refactoring `*_ref.cc` of such a file (in some cases), and a somewhat canonical translation `*.rs` into Rust that tests various migration techniques. The basename indicates what refactoring or migration technique is being tested.

The Makefile can be used to compile the `*.cc` and `*.rs` files to executables, and to compare their output. Just "make" gives instructions. The idea is:
- the `*.rs` file should be similar to the Rust program that CRAM produces from the corresponding (unrefactored) `*.cc`
- the output produced by corresponding `*.cc` , `*_ref.cc` and `*.rs` executables must be identical. This is the case for the hand-translated Rust programs provided here.
