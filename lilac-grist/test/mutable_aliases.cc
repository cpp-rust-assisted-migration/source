void f(int) {}
void g(int) {}

int main () {
  int pl = 1;
  int& r = pl;
  {
    int* p = &r; // scope S begins here. We have A_S = {r,*p}
    f(r);        // assume void f(int);
    g(*p);       // assume void g(int);
  }              // scope S ends here
  g(r);          // this is a different scope. It has no illegal alias nests.
}
