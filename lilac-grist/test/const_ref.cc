#define __CRAM__DEREF(x) x

#include <iostream>

struct Triple { int x, y, z; };

Triple f() {
  int  a = 20;
  int& b = a;
  ++__CRAM__DEREF(b);

  const int  c = 42;
  const int& d = c;

  const int e = __CRAM__DEREF(d) / a;

  const Triple t{__CRAM__DEREF(d), a, e};

  return t;
}

int main() {
  const Triple t = f();
  std::cout << t.x << " / " << t.y << " = " << t.z << std::endl;

  return 0;
}
