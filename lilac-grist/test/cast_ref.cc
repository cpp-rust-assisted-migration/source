#include <iostream>

int main() {
  const float  x  = 10.0;
  const double y  = 20.0;
  const float  z0 = static_cast<float>(static_cast<double>(x) + y);
  const double z1 = static_cast<double>(x) * y;

  std::cout << z0 << std::endl;
  std::cout << z1 << std::endl;

  const short a = 17;
  const long  b =  5;
  const short c = a / static_cast<short>(b);
  std::cout << c << std::endl;

  return 0;
}
