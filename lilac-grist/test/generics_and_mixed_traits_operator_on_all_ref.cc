#define __CRAM__CLONE(x) x
#define __CRAM__DEREF(x) x
// A type-parameterized struct `Pair` that uses instances of type `T` only as operator arguments.

#include <iostream>

template <typename T>
class Pair { // a "homogeneous" pair
private:
  T l;
  T r;
public:
  // construct a pair from l, r
  Pair(const T& l, const T& r): l(__CRAM__DEREF(l)), r(__CRAM__DEREF(r)) {}

  // sum l, r to get a single T
  T sum() const { return this->l + this->r; }

  // print a pair to stdout
  template <typename TT> friend std::ostream& operator<<(std::ostream&, const Pair<TT>&);
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const Pair<T>& p) {
  __CRAM__DEREF(os) << "Left:  " << __CRAM__CLONE(p.l) << std::endl;
  __CRAM__DEREF(os) << "Right: " << __CRAM__CLONE(p.r);
  return os;
}

int main() {
  const float l = 1.0;
  const float r = 5.08;

  const Pair<float> p1={l, r};
  std::cout << __CRAM__CLONE(p1) << std::endl;
  const float d1 = p1.sum();
  std::cout << "Sum: " << d1 << std::endl;

  std::cout << std::endl;

  const Pair<float> p2={r, l};
  std::cout << __CRAM__CLONE(p2) << std::endl;
  const float d2 = p2.sum();
  std::cout << "Sum: " << d2 << std::endl;

  return 0;
}
