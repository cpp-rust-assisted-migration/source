#include <iostream>

int main() {
  float  x  = 10.0;
  double y  = 20.0;
  float  z0 = x + y;
  double z1 = x * y;

  std::cout << z0 << std::endl;
  std::cout << z1 << std::endl;

  short a = 17;
  long  b =  5;
  short c = a / b;
  std::cout << c << std::endl;

  return 0;
}
