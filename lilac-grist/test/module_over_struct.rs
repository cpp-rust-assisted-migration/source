mod my_class {
    pub static X: i64 = 21;
    pub struct MyClass {
        pub y: i64
    }
}

fn main() {
    let my = Box::new(my_class::MyClass{y: 21});
    let sum: i64 = my_class::X + my.y;
    println!("{}", sum);
}
