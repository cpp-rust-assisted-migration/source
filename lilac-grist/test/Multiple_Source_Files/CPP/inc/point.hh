#ifndef __POINT__
#define __POINT__

#include <iostream>
#include <ostream>

class Point {
private:
  double x, y;

public:
  Point(const double& x, const double& y);
  friend std::ostream& operator<<(std::ostream& out, const Point& p);
};

#endif
