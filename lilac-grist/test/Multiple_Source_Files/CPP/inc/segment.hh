#ifndef __SEGMENT__
#define __SEGMENT__

#include <iostream>
#include <ostream>

#include "point.hh"

class Segment {
private:
  Point p, q;
public:
  Segment(const double& px, const double& py, const double& qx, const double& qy);
  friend std::ostream& operator<<(std::ostream& out, const Segment& s);
};

#endif
