#include <iostream>

#include "point.hh"
#include "segment.hh"

using namespace std;

int main() {
  Point p(-2.3, 3.4);
  Point q(2.5, -0.7);
  Segment s(-2.3, 6.1, 2.9, -1.1);

  cout << "I created two points for you: " << p << " and " << q << "." << endl
       << "I also created a line segment for you: " << s << "." << endl;

  return 0;
}
