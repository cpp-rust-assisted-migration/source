#include <iostream>

#include "segment.hh"

using namespace std;

Segment::Segment(const double& px, const double& py, const double& qx, const double& qy):
  p(px, py), q(qx, qy) {}

std::ostream& operator<<(std::ostream& out, const Segment& s) {
  out << s.p << " -> " << s.q;
  return out;
}
