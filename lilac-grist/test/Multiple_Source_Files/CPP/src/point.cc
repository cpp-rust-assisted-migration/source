#include <iostream>

#include "point.hh"

using namespace std;

Point::Point(const double& x, const double& y): x(x), y(y) {}

ostream& operator<<(ostream& out, const Point& p) {
  out << "(" << p.x << "," << p.y << ")";
  return out;
}
