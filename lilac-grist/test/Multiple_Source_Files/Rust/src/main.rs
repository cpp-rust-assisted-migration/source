mod point; use point::Point; // this brings struct Point into the current scope
mod segment; use segment::Segment;

fn main() {
    let p = Point::make_point(-2.3,  3.4);
    let q = Point::make_point( 2.5, -0.7);
    let s = Segment::make_segment(-2.3, 6.1, 2.9, -1.1);

    print!("I created two points for you: "); p.print(); print!(" and "); q.print(); println!(".");
    print!("I also created a line segment for you: "); s.print(); println!(".");
}
