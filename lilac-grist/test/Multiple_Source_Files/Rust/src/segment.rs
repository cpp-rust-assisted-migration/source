use crate::Point;

pub struct Segment {
    p: Point,
    q: Point
}

impl Segment{
    pub fn make_segment(px: f64, py: f64, qx: f64, qy: f64) -> Segment {
        Segment { p: Point::make_point(px, py),
                  q: Point::make_point(qx, qy)
        }
    }

    pub fn print(&self) {
        self.p.print(); print!(" -> "); self.q.print();
    }
}

