pub struct Point {
    x: f64,
    y: f64
}

impl Point {
    pub fn make_point(xx: f64, yy: f64) -> Point {
        Point { x: xx, y: yy }
    }

    pub fn print(&self) {
        print!("({},{})", self.x, self.y);
    }
}
