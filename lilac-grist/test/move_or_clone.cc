#include <iostream>

struct Triple {
  int x, y, z;
  Triple(int x, int y, int z): x(x), y(y), z(z) {}
  void print() const { std::cout << "(" << x << "," << y << "," << z << ")" << std::endl; }
};

void dummy(Triple t) { std::cout << "A dummy function that copies (not references) a triple" << std::endl; t.print(); }

int main() {
  const Triple t(1, 2, 3);

  const Triple& u = t;
  u.print();

  Triple v = u; v.y = 17;
  v.print();

  dummy(t);
  v.z = 87;
  v.print();
  t.print();

  Triple w = t; w.x = -45;
  w.print();

  return 0;
}
