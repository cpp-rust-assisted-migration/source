use std::ops::Sub;

#[derive(Clone)]
struct Point<T> {
    pub x: T,
    pub y: T,
}

impl<T: Sub<Output = T>> Sub for Point<T> {
    type Output = Self;

    fn sub(self, p: Self) -> Self::Output {
        Self {
            x: self.x - p.x,
            y: self.y - p.y,
        }
    }
}

fn main() {
    let p1: Point<f64> = Point::<f64> { x: 3.0, y: 2.0 };
    let p2: Point<f64> = Point::<f64> { x: 1.0, y: 1.0 };
    let p3: Point<f64> = p1 - p2;
    println!("{},{}", p3.x, p3.y);
}
