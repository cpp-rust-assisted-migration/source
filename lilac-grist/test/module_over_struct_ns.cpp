// When a C++ program uses a class as a namespace, but that class is
// already inside a namespace, we will need to move definitions into
// the surrounding namespace.

#include <iostream>
#include <memory>

namespace MyNs {
class MyClass {
public:
  static const int x = 21;
  int y;

  MyClass(int y) : y(y){};
};
} // namespace MyNs

int main() {
  auto my = std::make_unique<MyNs::MyClass>(21);
  int sum = MyNs::MyClass::x + my->y;
  std::cout << sum << std::endl;
  return 0;
}
