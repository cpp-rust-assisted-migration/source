# makefile for comparing (the output of) Rust and C++ files

SHELL = /bin/bash

default: help

%_cc: %.cc
	g++ -Wall -o $@ $<

%_rs: %.rs
	rustc --edition 2021 -o $@ $<

help:
	@ echo "Usage examples:"
	@ echo "'make cast_cc cast_rs' compiles cast.cc and cast.rs to executables"
	@ echo "'make L=const_cc     R=const_ref_cc compare' (produces and) runs const_cc     and const_ref_cc (a refactoring), and returns any diff in their outputs (expected: empty)"
	@ echo "'make L=const_cc     R=const_rs     compare' (produces and) runs const_cc     and const_rs,                     and returns any diff in their outputs (expected: empty)"
	@ echo "'make L=const_ref_cc R=const_rs     compare' (produces and) runs const_ref_cc and const_rs,                     and returns any diff in their outputs (expected: empty)"

compare: $(L) $(R)
	diff <(./$(L)) <(./$(R))

clean:
	find -type f -executable \( -name \*_cc -or -name \*_rs \) -delete

distclean: clean
	rm -f *.out *.exe *~
