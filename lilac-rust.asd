(defsystem "lilac-rust"
  :name "C++ to Rust muse"
  :pathname "lilac-rust"
  :author "GrammaTech"
  :description "Muse that translates C++ to Rust"
  :license "MIT"
  :class :package-inferred-system
  :depends-on ("lilac-rust/muse")
  :in-order-to ((test-op (load-op "lilac-rust/test")))
  :perform (test-op (o c) (symbol-call :lilac-rust/test '#:run-batch)))

(register-system-packages "lsp-server" '(:lsp-server/protocol))
(register-system-packages "lsp-server" '(:lsp-server/protocol-util))
(register-system-packages "lsp-server" '(:lsp-server/lsp-server))
(register-system-packages "lsp-server" '(:lsp-server/logger))
(register-system-packages "lsp-server/utilities" '(:lsp-server/utilities/lsp-utilities))
(register-system-packages "trivia" '(:trivia.fail))
(register-system-packages "cl-abstract-classes" '(:org.tfeb.hax.abstract-classes))
(register-system-packages "abstract-classes" '(:org.tfeb.hax.abstract-classes))
