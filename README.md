# CRAM

This repository contains the public source code of CRAM: C++ to Rust Assisted Migration.

For a full project description, please see our website at <https://cpp-rust-assisted-migration.gitlab.io>.

All material contained in this directory and any sub-directories is based upon work supported by DARPA under Contracts no. HR001122C0025 and HR001123C0079. Any opinions, findings and conclusions or recommendations expressed in this material are those of the authors and do not necessarily reflect the views of DARPA.

Distribution Statement "A" (Approved for Public Release, Distribution Unlimited).
