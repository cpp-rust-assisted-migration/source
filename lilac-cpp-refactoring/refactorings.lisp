(defpackage :lilac-cpp/refactorings
  (:use
    :gt/full
    :software-evolution-library
    :software-evolution-library/software/parseable
    :software-evolution-library/software/tree-sitter
    :software-evolution-library/software/cpp
    :software-evolution-library/software/cpp-project
    :lilac-commons/commons
    :lilac-commons/refactoring)
  (:import-from
    :software-evolution-library/software/tree-sitter
    :namespace-qualifiers
    :unqualified-name)
  (:import-from
   :cmd
   :$cmd)
  (:import-from :trivia.fail
                :fail)
  (:import-from :lilac-commons/analysis)
  (:import-from :software-evolution-library/software/c-cpp-project
                :evolve-files/dependency-order
                :project-dependency-tree)
  (:local-nicknames
   (:analysis :lilac-commons/analysis)
   (:attrs :functional-trees/attrs)
   (:dir :software-evolution-library/software/directory)
   (:lsp :lsp-server/protocol)
   (:refactoring :lilac-commons/refactoring)
   (:sel :software-evolution-library)
   (:ts :software-evolution-library/software/tree-sitter))
  (:shadow :infer-type)
  (:shadowing-import-from :serapeum :~> :~>>)
  (:export
    :rewrite-lists-to-vectors
    :unmix-subexpression-types
    :get-calls-referencing
    :non-const-calls
    :assignments
    :var-can-const-p
    :decl-id-can-const-p
    :lookup-in-include
    :system-includes
    :find-std-include
    :variable-declarations
    :non-const-variable-declaration-ids
    :adjoin-const
    :insert-const
    :constify-can-const-vars
    :constify-can-const-pointers
    :constify-can-const-methods
    :infer-auto-types
    :declared-const?
    :slice-template
    :pointers-to-references
    :add-explicit-this
    :use-uniform-initialization-syntax
    :make-uniform
    :non-uniform-initialization?
    :use-static-cast
    :wrap-references
    :wrap-derefs
    :add-move-and-clone
    :evaluated?
    :pointee-declarations
    :uniquify-mutable-aliases
    :const-method?
    :rewrite-definitions-to-constexprs
    :rewrite-preprocs-to-constexpr-funs
    :inline-preprocessor-macros
    :of-templated-type?
    :cpp-refactoring
    :rewrite-preprocs-to-constexpr-funs
    :modularize-project
    :*refactoring-pipeline*
    :refactoring-pipeline
    :modularize-project
    :add-brackets
    :enforce-braces
    :rewrite-switches))
(in-package :lilac-cpp/refactorings)

(def +list-only-methods+
  '("push_front"
    "emplace_front"
    "pop_front")
  "List methods that can be handled by std::deque.")

(def +algorithmic-list-methods+
  '("merge"
    "remove"
    "remove_if"
    "reverse"
    "unique"
    "sort")
  "std::list methods that can be handled by <algorithm>.")

(def +unsupported-list-methods+
  '("slice")
  "Methods that only make sense for linked lists.")


;;; Utilities.

(defun infer-type (arg)
  (infer-type* arg))

(defclass cpp-refactoring (refactoring)
  ())


;;; Pipeline

(defparameter *refactoring-pipeline*
  '(rewrite-definitions-to-constexprs
    rewrite-preprocs-to-constexpr-funs
    inline-preprocessor-macros
    enforce-braces
    use-uniform-initialization-syntax
    use-static-cast
    rewrite-lists-to-vectors
    constify-can-const-pointers
    constify-can-const-vars
    infer-auto-types
    add-explicit-this
    constify-can-const-methods
    unmix-subexpression-types
    pointers-to-references
    uniquify-mutable-aliases
    rewrite-switches
    wrap-references
    wrap-derefs
    add-move-and-clone
    modularize-project))

(defun refactoring-pipeline (&key (refactorings *refactoring-pipeline*))
  (assert (every (op (subtypep _ 'refactoring)) refactorings))
  (coerce (sort-refactorings refactorings) 'list))


;;; List to vector

(defclass rewrite-lists-to-vectors (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Refactor std::list to std::vector"
   :name :lilac.list2vec))

(defmethod change-table ((self rewrite-lists-to-vectors)
                         (project t)
                         (target t))
  (maphash-into (make-hash-table)
                (lambda (target)
                  (values target (copy target :text "vector")))
                (filter-map (op (decl-target target _))
                            (get-list-declarations target))))

(defmethod apply-changes ((self rewrite-lists-to-vectors)
                          (project t)
                          (target t)
                          (change-table t))
  (rewrite-list-import (call-next-method)))

(defun rewrite-lists-to-vectors (obj)
  (apply-refactoring 'rewrite-lists-to-vectors
                     (attrs-root*)
                     (list obj)))

(defgeneric rewrite-list-import (obj)
  (:method ((software software))
    (let* ((genome (genome software))
           (new-genome (rewrite-list-import genome)))
      (if (eql genome new-genome)
          software
          (copy software
                :genome new-genome))))
  (:method ((genome ast))
    (let ((list-import
            (find-if (lambda (ast)
                       (match ast
                         ((cpp-system-lib-string :text "<list>") t)))
                     genome)))
      (if list-import
          (with genome list-import
                (copy list-import
                      :text "<vector>"))
          genome))))

(defgeneric decl-target (obj decl)
  (:method ((obj t) (decl-path list))
    (decl-target obj (lookup obj decl-path)))
  (:method ((obj t) (decl ast))
    (find-if
     (lambda (ast)
       (and (typep ast 'cpp-type-identifier)
            (equal "list" (source-text ast))))
     (resolve-declaration-type decl nil))))

(defgeneric get-list-declarations (obj)
  (:documentation "Return a list of identifiers and a set of declarations.")
  (:method ((obj ast))
    (filter (op (string^= "std::list"
                          (trim-whitespace
                           (source-text (infer-type _)))))
            (collect-if (of-type 'declaration-ast)
                        obj)))
  (:method ((obj software))
    (get-list-declarations (genome obj))))


;;; Casting expression subtypes

(defclass unmix-subexpression-types (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Insert casts in mixed-type expressions"
   :name :lilac.unmix))

(defgeneric wrap-static-cast (expression type)
  (:method (expression type)
    (copy-with-surrounding-text
     (cpp* "static_cast<$TYPE>($EXPRESSION)"
           :expression expression
           :type (source-text type))
     expression))
  (:method ((expr cpp-call-expression) type)
    (match expr
      ((cpp* "static_cast<$OLDTYPE>($SUBEXPR)"
             :oldtype _
             :subexpr subexpr)
       (wrap-static-cast subexpr type))
      (otherwise (call-next-method))))
  (:method ((ast cpp-number-literal) type)
    "Don't wrap a cast around a number with no suffix."
    (flet ((num (text) (make 'cpp-number-literal :text text)))
      (with-accessors ((text text)) ast
        (cond-let result
          ((or (scan "[lLuUzZ]$" text)
               ;; If it just ends in F or f, it might just be hex.
               (and (string-suffix-p "f" text)
                    (if (string-prefix-p "0x" text)
                        ;; It's a hex float with exponent marker.
                        (position #\p text)
                        t)))
           (call-next-method))
          ;; Convert between literal floats and integers.
          ((ignore-errors
            (parse-integer text))
           (string-case (source-text type)
             ("float"
              (num (fmt "~f" (coerce result 'single-float))))
             ("double"
              (num (fmt "~f" (coerce result 'double-float))))
             (t ast)))
          ((ignore-errors
            (parse-float text))
           (case-using (flip #'string$=) (source-text type)
             (("int" "long" "short"
                     "int8_t" "int16_t" "int32_t" "int64_t" "uint8_t"
                     "uint16_t" "uint32_t" "uint64_t")
              ;; TODO How does C++ round when casting?
              (num (fmt "~f" (truncate result))))
             ("double"
              (num (fmt "~f" (coerce result 'double-float))))
             ("float"
              (num (fmt "~f" (coerce result 'single-float))))
             (t ast)))
          (t
           ast))))))

(defgeneric expression-type-list (ast)
  (:documentation "If AST is a potentially mixed-type binary
  expression, return a list of the overall type and the types of the
  left and right hand sides (if binary) or the individual AST.")
  (:method ((ast ast)) nil)
  (:method :around ((ast cpp-ast) &aux (root *root*))
    (match (get-parent-asts* root ast)
      ;; Cast an expression in an initializer list to its intended
      ;; type.
      ((or (list*
            (and init-list (cpp-initializer-list))
            (cpp-compound-literal-expression
             (cpp-type type))
            _)
           (list*
            (and init-list (cpp-initializer-list))
            (cpp-init-declarator)
            (cpp-declaration (cpp-type type))
            _))
       (let ((position (position ast (children init-list))))
         (assert position)
         (when-let (definition (get-declaration-ast :type type))
           (when-let* ((field-table (ts::field-table definition))
                       (variables (@ field-table :variable))
                       ;; TODO Getting the members in order should be
                       ;; factored out.
                       (members
                        (stable-sort
                         (apply #'append (convert 'list (range variables)))
                         (complement
                          (op (path-later-p root
                                            (ast-path root _)
                                            (ast-path root _))))))
                       (member
                        (nth position members))
                       (member-type (infer-type member))
                       (member-type
                        ;; TODO If the member type is a template type, specify it from the template.
                        (if (member member-type '("T" "U") :test #'source-text=)
                            nil
                            member-type)))
             (assert member)
             (if member-type
                 (ematch (call-next-method)
                   ((list)
                    (list member-type (infer-type ast)))
                   ((list _ left-type right-type)
                    (list member-type left-type right-type))
                   ((list _ expr-type)
                    (list member-type expr-type)))
                 member-type)))))
      (otherwise
       (call-next-method))))
  (:method ((ast cpp-binary-expression))
    (list (infer-type ast)
          (infer-type (lhs ast))
          (infer-type (rhs ast))))
  (:method ((ast cpp-init-declarator))
    (let ((var-type (infer-type (lhs ast))))
      (list var-type
            var-type
            (infer-type (rhs ast)))))
  (:method ((ast cpp-assignment-expression))
    (when (typep (lhs ast) 'identifier-ast)
      (let ((var-type (infer-type (lhs ast))))
        (list var-type
              var-type
              (infer-type (rhs ast)))))))

(defun mixed-type-expressions (obj)
  (iter (for ast in-tree (genome obj))
        (when-let (types (expression-type-list ast))
          (unless (or (some #'null types)
                      (same #'source-text types :test #'equal))
            (collect (cons ast (mapcar #'source-text types)))))))

(defmethod change-table ((r unmix-subexpression-types)
                         (project t)
                         (obj ast))
  (let* ((type-lists
           (sort
            (mapcar (op (cons (ast-path obj (car _1))
                              (cdr _1)))
                    (mixed-type-expressions obj))
            #'length> :key #'car)))
    (alist-hash-table
     (with-collectors (collect)
       (dolist (type-list type-lists)
         (flet ((static-cast-collect (expr type)
                  (let ((wrapped (wrap-static-cast expr type)))
                    (unless (eql wrapped expr)
                      (collect (cons expr wrapped))))))
           (ematch type-list
             ((list))
             ;; Binary mismatch.
             ((list path expr-type left-type right-type)
              (let ((ast (lookup obj path)))
                (cond ((not (equal expr-type left-type))
                       (static-cast-collect (lhs ast) expr-type))
                      ((not (equal expr-type right-type))
                       (static-cast-collect (rhs ast) expr-type))
                      (t
                       (assert (or (equal expr-type left-type)
                                   (equal expr-type right-type)))))))
             ;; Unary mismatch.
             ((list path correct-type expr-type)
              (let ((ast (lookup obj path)))
                (unless (equal correct-type expr-type)
                  (static-cast-collect ast correct-type)))))))))))

(defun unmix-subexpression-types (obj)
  "Insert static_casts to avoid implicit type casts."
  (apply-refactoring* 'unmix-subexpression-types obj))


;;; Inserting const

(defclass constify-can-const-vars (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Declare variables const"
   :after '(constify-can-const-methods)
   :name :lilac.constify))

(defparameter *non-const-types*
  '("std::ostream"
    "std::ostream&"))

(defun non-const-type? (ast)
  (member (source-text (infer-type ast))
          *non-const-types*
          :test #'equal))

(defun var-can-const-p (ast)
  "Is AST a variable that can const?"
  (when-let (id (get-declaration-id :variable ast))
    (decl-id-can-const-p ast)))

(defun initialized? (id &key (root (attrs-root*)))
  "Is ID an initialized declaration?"
  (find-enclosing '(or cpp-init-declarator parameter-ast)
                  root
                  id))

(defun id-has-writes? (id &key (root (attrs-root*)))
  (labels ((id-has-writes? (id)
             ;; A variable can be declared const if (1) it isn't
             ;; assigned to, (2) it isn't passed as a argument to a
             ;; non-const parameter and it's not an object with a
             ;; non-const method being called on it, and (3) it has no
             ;; aliases that can't be declared const.
             (or (assignments id)
                 (non-const-calls root id)
                 (non-can-const-aliases root id)
                 (non-const-type? id))))
    (if-let (params (declaration-definition-parameters id))
      ;; If this is a declaration parameter, we only care about how
      ;; the variable is used in definitions.
      (some #'id-has-writes? params)
        (some #'id-has-writes?
              (cons id (alias-set id))))))

(defun function-declaration-definitions (decl &key (root (attrs-root*)))
  "Get the function(s) that define DECL."
  (iter (for ast in-tree (genome root))
        (when (typep ast 'cpp-function-definition)
          (when (eql (get-declaration-ast :function ast)
                     decl)
            (collect ast)))))

(defun declaration-definition-parameters (id &key (root (attrs-root*)))
  "If ID is a parameter in a declaration, collect the corresponding
definition parameters."
  (when-let (param (find-enclosing 'parameter-ast root id))
    (match (get-parent-asts* root param)
      ((list* (and params (cpp-parameter-list))
              (cpp-function-declarator)
              (and fn-decl
                   (or (cpp-declaration)
                       (cpp-field-declaration)))
              _)
       (let* ((offset (assure array-index (position param (children params))))
              (definitions
                (function-declaration-definitions fn-decl)))
         (iter (for d in definitions)
               (appending
                (filter (op (source-text= id _))
                        (parameter-names
                         (elt (function-parameters d)
                              offset))))))))))

(defun decl-id-can-const-p (id)
  "Is ID a declaration ID that can const?"
  (and (initialized? id)
       (not (id-has-writes? id))))

(defun declared-const? (ns ast)
  (when-let (decl (get-declaration-ast ns ast))
    (const-declaration? decl)))

(defun variable-declaration-ids/root (root)
  (filter (op (ast-path root _))
          (variable-declaration-ids root)))

(defgeneric non-const-variable-declaration-ids (software ast)
  (:documentation "Collect declaration IDs from SW that are not currently declared constant.")
  (:method (sw file)
    (remove-if (lambda (id)
                 (when (ast-path file id)
                   (const-declaration?
                    (find-enclosing 'variable-declaration-ast file id))))
               (variable-declaration-ids/root (genome sw)))))

(defgeneric non-can-const-aliases (software ast)
  (:documentation "Collect aliases in SOFTWARE that point to AST and cannot be declared constant.")
  (:method (sw (id identifier-ast))
    (remove-if #'var-can-const-p (alias-set id))))

(defparameter *known-const-functions*
  (fset:set "static_cast"
            "const_cast"
            "dynamic_cast"
            "reinterpret_cast"
            "size")
  "Functions known to not affect their arguments.")

(defun known-const-function? (name)
  (typecase name
    (cpp-template-function
     (known-const-function? (cpp-name name)))
    (cpp-qualified-identifier
     (when (source-text= (cpp-scope name) "std")
       (known-const-function? (cpp-name name))))
    (cpp-ast
     (contains? *known-const-functions* (source-text name)))))

(defun get-overloads (fn)
  (get-declaration-asts :function fn))

(defun non-const-calls (sw id &aux (*root* sw))
  "Collect function calls where VARIABLE is passed as a non-constant reference.
VAR should be the declaration ID (not a variable use).

Note this is called on every alias of ID; it does not need to handle
aliases internally."
  (check-type id identifier-ast)
  (nest
   (labels ((relevant-arg? (arg)
              "Is ARG equal to ID, or a derefence of ID?"
              (or (and (typep arg 'identifier-ast)
                       (eql id (get-declaration-id :variable arg)))
                  (and (typep arg 'cpp-pointer-expression)
                       (source-text= (cpp-operator arg) "*")
                       (relevant-arg? (cpp-argument arg)))))))
   (iter outer
         (for call in (nub (collect-arg-uses sw id))))
   (with-accessors ((call-function call-function)) call)
   (unless (known-const-function? call-function))
   ;; We need access to overloads.
   (let ((fns (get-overloads call-function))))
   (econd
     ((no fns)
      ;; In conservative mode, if we can't find the
      ;; declaration, assume mutation.
      (when t #+(or) conservative
            (collect call)))
     ((every (of-type
              ;; TODO There doesn't seem to be a consistent choice
              ;; subclass for fields with function declarations.
              '(or function-declaration-ast
                cpp-field-declaration
                cpp-declaration))
             fns)
      (match (call-function call)
        ;; If a variable has a method invoked on it, and there are no
        ;; const overloads for that method, we consider it a non-const
        ;; call.
        ((cpp-field-expression
          (cpp-argument arg))
         (unless (same-place-p arg id)
           (fail))
         (unless (some #'const-declaration? fns)
           (collect call)))
        (otherwise
         ;; Look through the arguments and parameters for each method.
         ;; If the parameter corresponding to the argument we have is
         ;; declared const, the result is const.
         (let ((args (call-arguments call)))
           (unless (some (lambda (overload)
                           (let ((params (collect-if (of-type 'parameter-ast) overload)))
                             (and (length= params args)
                                  (notany (lambda (arg param)
                                            (and (relevant-arg? arg)
                                                 (not (const-declaration? param))
                                                 ;; TODO Should this be taken care of by
                                                 ;; canonicalize-type?
                                                 (find-if
                                                  (of-type '(or c/cpp-pointer-declarator
                                                             c/cpp-abstract-pointer-declarator
                                                             cpp-reference-declarator
                                                             cpp-abstract-reference-declarator))
                                                  param)))
                                          args
                                          params))))
                         fns)
             (in outer (collect call))))))))))

(defun make-const ()
  "Construct a `const' AST node."
  (make 'cpp-type-qualifier :text "const" :after-text " "))

(defun adjoin-const (ast)
  "Return a copy of AST with a const pre-specifier."
  (copy ast
        :cpp-pre-specifiers
        (adjoin (make-const)
                (cpp-pre-specifiers ast)
                :test #'source-text=)))

(defgeneric insert-const (sw ast)
  (:documentation "Insert const as a pre-specifier for AST in SW.")
  (:method ((obj software) ast)
    (copy obj :genome (insert-const (genome obj) ast)))
  (:method ((obj ast) ast)
    (with obj ast (adjoin-const ast))))

(defun has-pre-specifiers? (ast)
  "Does AST have a cpp-pre-specifiers slot?"
  (slot-exists-p ast 'cpp-pre-specifiers))

(defun constify-can-const-ids-changes (ids)
  "Build a change table to add const specifiers to appropriate IDS in OBJ."
  (~>>
   ids
   (filter #'decl-id-can-const-p)
   (filter-map (op (find-enclosing #'has-pre-specifiers? (attrs-root*) _)))
   (maphash-into (make-hash-table)
                 (op (values _1 (adjoin-const _1))))))

(defmethod change-table ((r constify-can-const-vars)
                         (project t)
                         (obj ast))
  (mvlet* ((non-const-ids (non-const-variable-declaration-ids obj obj))
           (alias-ids plain-ids (partition #'aliasee non-const-ids)))
    (merge-tables (constify-can-const-ids-changes alias-ids)
                  (constify-can-const-ids-changes plain-ids))))

(defun constify-can-const-vars (obj)
  "Return a copy of SW adding const declarations for variables that could have them, but don't."
  (apply-refactoring (make 'constify-can-const-vars)
                     (attrs-root*)
                     (list obj)))


;;; Constifying pointers

(defclass constify-can-const-pointers (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Declare pointers const"
   :name :lilac.constify.pointers))

(defun adjoin-pointer-const (ast)
  (mapcar
   (lambda (ast)
     (match ast
       ((cpp-pointer-declarator
         (cpp-declarator
          (and id (cpp-identifier))))
        (unless (pointer-assignments id)
          (copy ast
                :children
                (adjoin (make 'cpp-type-qualifier
                              :text "const"
                              :before-text " ")
                        (direct-children ast)
                        :test #'source-text=))))))
   ast))

(defun pointer-already-const? (ast)
  (find-if (lambda (ast)
             (match ast
               ((cpp-type-qualifier :text "const"))))
           (direct-children ast)))

(defun const-pointer-ids (obj)
  (nest
   (remove-if #'pointer-assignments)
   (mapcar #'cpp-declarator)
   (collect-if (of-type 'cpp-pointer-declarator))
   obj))

(defmethod change-table ((self constify-can-const-pointers)
                         (project software)
                         (obj ast))
  (~>> (const-pointer-ids obj)
       (mapcar (op (find-enclosing 'cpp-pointer-declarator obj _)))
       (remove-if #'pointer-already-const?)
       (maphash-into (make-hash-table)
                     (op (values _1 (adjoin-pointer-const _1))))))

(defun constify-can-const-pointers (obj)
  "Return a copy of SW adding const declarations for pointer variables."
  (apply-refactoring* 'constify-can-const-pointers obj))


;;; Resolving auto types

(defclass infer-auto-types (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Infer auto types"
   :name :lilac.unauto))

(defmethod change-table ((r infer-auto-types)
                         (project t)
                         (sw ast))
  (alist-hash-table
   (filter-map (lambda (id)
                 (and-let* ((declared-type
                             (declaration-type
                              (get-declaration-ast :variable id)))
                            ((placeholder-type-p declared-type))
                            (type (infer-type id))
                            ((not (source-text= type declared-type))))
                   (cons declared-type (tree-copy type))))
               (variable-declaration-ids sw))))

(defun infer-auto-types (sw)
  (apply-refactoring* 'infer-auto-types sw))


;;; Rewrite pointers to references.

(defclass pointers-to-references (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Convert pointers to references"
   :name :lilac.pointers2refs))

(-> pointer-declarations (ast)
  (values (soft-list-of cpp-pointer-declarator) &optional))
(defun pointer-declarations (sw)
  "Collect pointer declarations from SW."
  (collect-if (of-type 'cpp-pointer-declarator)
              sw))

(-> dereference? (ast) (values (or null ast) &optional))
(defun dereference? (ast)
  "If AST is a dereference, return the value being dereferenced."
  (match ast
    ((cpp-pointer-expression
      (cpp-operator (cpp-*))
      (cpp-argument arg))
     arg)
    ((cpp-field-expression
      (cpp-operator (cpp-->))
      (cpp-argument arg))
     arg)))

(defun can-be-reference? (sw pointer-decl)
  "Return T if POINTER-DECL could be rewritten to a reference.

The rules are that a pointer decl can be rewritten if:

1. The pointer is constant in itself; e.g. `int* const x' is
acceptable but `const int* x' is not.

2. The pointer is initialized.

3. The pointer is not initialized to `0' or `nullptr'.

4. Every use of the variable is dereferenced (e.g. `*x` or
`x->field`)."

  (assert (typep pointer-decl 'cpp-pointer-declarator))
  ;; TODO params too?
  (nest
   (and-let*
       (((not
          (find-if (of-type 'parameter-ast)
                   (get-parent-asts* (attrs-root*)
                                     pointer-decl))))
        (id
         ;; There must be an ID. TODO: Should this be a method on
         ;; c/cpp-init-declarator?
         (ematch pointer-decl
           ((cpp-pointer-declarator
             (cpp-declarator
              (and id (identifier-ast))))
            id)
           ((cpp-pointer-declarator
             (cpp-declarator
              (cpp-function-declarator)))
            nil)))
        ;; The pointer itself must be const. TODO Should
        ;; canonicalize-type handle this?
        ((member "const"
                 (children pointer-decl)
                 :test #'source-text=))
        (init-decl
         ;; There must be a surrounding init-declarator. References
         ;; can't be uninitialized.
         (find-enclosing 'cpp-init-declarator sw pointer-decl))
        ((not (source-text= (rhs init-decl) "0")))
        ((not (source-text= (rhs init-decl) "nullptr")))))
   (let ((uses (collect-var-uses sw id))))
   ;; Note that this will also return T if there are no uses, which is
   ;; what we want.
   (when (every (op (eql _1 (dereference? (get-parent-ast sw _1)))) uses)
     (cons id uses))))

(-> rewrite-pointer-to-reference (ast identifier-ast &rest ast)
  (soft-alist-of ast ast))
(defun rewrite-pointer-to-reference (sw id &rest uses)
  "Given ID and USES, a list of uses of ID, return an alist of
substitutions to make in SOFTWARE to convert ID from a pointer to a
declaration."
  (cons
   (let* ((pointer-decl
            (assure ast
              (find-enclosing 'cpp-pointer-declarator sw id)))
          (init-declarator
            (assure ast
              (find-enclosing 'cpp-init-declarator sw pointer-decl)))
          (new
            (with init-declarator
                  pointer-decl
                  (make 'cpp-reference-declarator
                        :cpp-valueness (make 'cpp-&)
                        :children (list
                                   (assure ast
                                     (cpp-declarator pointer-decl))))))
          (new
            (match (rhs new)
              ((and rhs
                    (cpp-pointer-expression
                     (cpp-operator (cpp-&))
                     (cpp-argument arg)))
               (with new rhs arg))
              (otherwise new))))
     (cons init-declarator new))
   (mapcar (lambda (parent)
             (cons parent
                   (ematch parent
                     ((cpp-pointer-expression)
                      (dereference? parent))
                     ((and expr (cpp-field-expression))
                      (copy expr :cpp-operator (make 'cpp-.))))))
           (mapcar (op (get-parent-ast sw _))
                   uses))))

(-> rewrite-pointers-to-references
  (ast (soft-alist-of ast (soft-list-of ast)))
  (values ast &optional))
(defun rewrite-pointers-to-references (sw pointer-data-alist)
  (mapcar (hash-table-function
           (alist-hash-table
            (apply #'append
                   (mapply (op (rewrite-pointer-to-reference sw _*))
                           pointer-data-alist))))
          (genome sw)))

(defmethod change-table ((r pointers-to-references)
                         (project t)
                         (sw ast))
  (let ((pointer-data-alist
          (filter-map (op (can-be-reference? sw _))
                 (pointer-declarations sw))))
    (alist-hash-table
     (apply #'append
            (mapply (op (rewrite-pointer-to-reference sw _*))
                    pointer-data-alist)))))

(defun pointers-to-references (sw)
  "Refactor pointers in SW into references, when possible."
  (apply-refactoring* 'pointers-to-references sw))


;;; Insert "this" into methods.

(defclass add-explicit-this (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Add explicit this to member access"
   :name :lilac.this))

(defun non-member-function? (fn)
  (match fn
    ((cpp-function-definition
      (cpp-declarator
       (cpp-function-declarator
        (cpp-declarator
         (and id (cpp-qualified-identifier))))))
     id)))

(defun bare-members (ast &aux (root *root*))
  "Scan AST for the use of unqualified members (without `this') in
method definitions in type definitions."
  (labels ((variable-ids (ast)
             "Extract only variable identifiers from AST. "
             (~>> (identifiers ast)
                  (remove-if (of-type 'cpp-field-identifier))
                  (keep 'variable-declaration-ast _
                        :key #'relevant-declaration-type)))
           (bare-member? (spec var)
             "Is VAR a bare member of SPEC?"
             (let ((decl (get-declaration-ast :variable var)))
               (cond ((typep decl 'cpp-field-declaration)
                      (and
                       (match (get-parent-ast root var)
                         ((cpp-field-expression
                           (cpp-field (eql var)))
                          nil)
                         (otherwise t))
                       (not (find-enclosing
                             'cpp-field-initializer root var))
                       (ancestor-of-p root decl spec)))
                     ;; TODO Shadowing from global variable?
                     ((and (no decl)
                           (when-let* ((table (ts::field-table spec))
                                       (var-table (@ table :variable)))
                             (lookup var-table (source-text var))))))))
           (extract-bare-members (spec ast)
             (filter (op (bare-member? spec _))
                     (variable-ids ast))))
    (iter outer
          (for node in-tree (genome ast))
          (match node
            ((and spec (c/cpp-classoid-specifier))
             (and-let* ((table (ts::field-table spec))
                        (methods (@ table :function))
                        ((@ table :variable)))
               (iter (for method-list in-set (range methods))
                     (iter (for method-id in method-list)
                           (for method =
                                (find-enclosing-declaration
                                 :function root method-id))
                           (in outer
                               (appending (extract-bare-members spec method)))))))
            ((and fn (cpp-function-definition))
             (match (non-member-function? fn)
               ((cpp-qualified-identifier
                 (cpp-scope class-name))
                (when-let ((spec (get-declaration-ast :type class-name)))
                  (in outer
                      (appending (extract-bare-members spec fn)))))))))))

(defun wrap-with-this (id)
  (declare (identifier-ast id))
  (cpp* "this->$ID" :id (make 'cpp-field-identifier :text (text id))))

(defmethod change-table ((r add-explicit-this)
                         (project t)
                         (sw ast))
  (let* ((bare-members (bare-members sw))
          (wrapped (mapcar #'wrap-with-this bare-members)))
     (if bare-members
         (pairhash bare-members wrapped)
         (dict))))

(defun add-explicit-this (sw)
  "Make `this' explicit in methods."
  (apply-refactoring* 'add-explicit-this sw))


;;; Add const to methods

(defclass constify-can-const-methods (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :after '(add-explicit-this)
   :description "Declare methods const"
   :name :lilac.constify.methods))

(defun extract-methods (obj &optional (root (genome obj)))
  (let ((these (collect-if (of-type 'cpp-this) root)))
    (nub
     (remove nil
             (mapcar (op (find-enclosing 'cpp-function-definition root _))
                     these)))))

(defun const-method? (method)
  (when (typep method 'cpp-function-definition)
    (when-let ((declarator
                (find-if (of-type 'cpp-function-declarator)
                         (cpp-declarator method))))
      (find-if (lambda (ast)
                 (match ast
                   ((cpp-type-qualifier :text "const")
                    t)))
               (direct-children declarator)))))

(defun constify-method (method)
  (or (let ((declarator (find-if (of-type 'cpp-function-declarator)
                                 method)))
        (with method
              declarator
              (tree-copy
               (copy declarator
                     :children
                     (cons (make 'cpp-type-qualifier
                                 :text "const"
                                 :before-text " ")
                           (direct-children method))))))
      method))

(defun method-can-const? (method)
  (and
   (let ((assignments (collect-if (of-type 'assignment-ast) method)))
     (notany (op (find-if (of-type 'cpp-this) (assignee _)))
             assignments))
   ;; TODO Also check if passed as mutable arg, or invoked with
   ;; mutable method. Fixpoint?
   ))

(defmethod change-table ((r constify-can-const-methods)
                         (project t)
                         (obj ast))
  (let* ((methods (extract-methods obj))
         (non-const-methods (remove-if #'const-method? methods))
         (can-const-methods (filter #'method-can-const? non-const-methods)))
    (maphash-into
     (make-hash-table)
     (op (values _1 (constify-method _1)))
     can-const-methods)))

(defun constify-can-const-methods (obj)
  (apply-refactoring* 'constify-can-const-methods obj))


;;; Use uniform initialization syntax.

(defclass use-uniform-initialization-syntax (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Use brace initialization"
   :name :lilac.uniform))

(-> non-uniform-initialization? (ast)
  (values (or null cpp-argument-list)))
(defun non-uniform-initialization? (ast)
  "If AST is a non-uniform initialization, return the argument list."
  (match ast
    ((cpp-declaration
      ;; Not a primitive type.
      (cpp-type (or (cpp-type-identifier) (cpp-template-type)))
      (cpp-declarator
       (list
        (cpp-init-declarator
         (cpp-declarator (cpp-identifier))
         (cpp-value (and args (cpp-argument-list)))))))
     args)))

(defun non-uniform-initializations (ast)
  "Collect non-uniform initializations in AST."
  (collect-if #'non-uniform-initialization? ast))

(defun make-uniform (init)
  "Rewrite INIT to use uniform initialization syntax."
  (let ((args (non-uniform-initialization? init)))
    (assert (typep args 'arguments-ast))
    (with init
          args
          (make 'cpp-initializer-list
                :before-text (before-text args)
                :before-asts (before-asts args)
                :after-text (after-text args)
                :after-asts (after-asts args)
                :children
                (children args)))))

(defun disambiguate-init (ast)
  (when (typep ast 'cpp-function-declarator)
    (let ((ast (contextualize-ast *root* ast)))
      (when (typep ast 'cpp-init-declarator)
        ast))))

(defun disambiguate-inits (root)
  (mapcar #'disambiguate-init root))

(defmethod change-table ((r use-uniform-initialization-syntax)
                         (project t)
                         (sw ast))
  (iter (for orig-ast in-tree sw)
        (when (typep orig-ast 'cpp-declaration)
          (let* ((ast (disambiguate-inits orig-ast)))
            (when (non-uniform-initialization? ast)
              (collecting
                (cons orig-ast (make-uniform ast))))))))

(defun use-uniform-initialization-syntax (sw)
  "Refactor SW to use uniform initialization syntax."
  (apply-refactoring* 'use-uniform-initialization-syntax sw))


;;; Use static (not C-style) casts.

(defclass use-static-cast (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Disambiguate casts"
   :name :lilac.static-cast))

(defmethod change-table ((r use-static-cast)
                         (project t)
                         (root t))
  (alist-hash-table
   (iter (for node in-tree root)
         (when (typep node '(or cpp-binary-expression cpp-call-expression))
           (when-let (cast (contextualize-ast root node))
             (when (typep cast 'cpp-cast-expression)
               (collecting
                 (cons node
                       (wrap-static-cast (cpp-value cast)
                                         (cpp-type (cpp-type cast)))))))))))

(defun use-static-cast (root)
  (apply-refactoring* 'use-static-cast root))


;;; Insert std::ref and std::cref

(defclass wrap-references (cpp-refactoring two-phase-refactoring annotation-refactoring)
  ()
  (:default-initargs
   :description "Make implicit references explicit"
   :name :lilac.ref))

(defmethod change-table ((r wrap-references) (project t) (root ast))
  (let ((*db* (analysis:analyze-templates root :db *db*)))
    (alist-hash-table
     (iter (for ast in-tree root)
           (match ast
             ((and call (cpp-call-expression))
              (when-let (rewrite
                         (rewrite-call-with-reference-wrappers call))
                (collect (cons call rewrite)))))))))

(defmethod annotation-preprocs append ((r wrap-references))
  (list
   (cpp* "#define __CRAM__REF(x) x")
   (cpp* "#define __CRAM__CREF(x) x")
   #+(or) (cpp* "#include<functional>")))

(defun wrap-references (root)
  (apply-refactoring* 'wrap-references root))

(deftype ref-decl-kind ()
  '(member nil :const :mut))

(defun passed-as-reference? (ast &key (argument-parameters (ast-argument-parameters ast)))
  "Is AST an argument being passed as a reference?"
  (labels ((collect-template-params (gen-decl pos)
             (let* ((constraints (templated-type-constraints *db* gen-decl))
                    (fn (call-function (find-enclosing 'call-ast *root* ast))))
               (iter (for constraint in constraints)
                     (when (and (typep constraint 'class-ast)
                                (typep fn 'field-ast))
                       ;; Having found the class, get the parameter from the method.
                       (when-let* ((class constraint)
                                   (field-table (ts::field-table class))
                                   (method-map (@ field-table :function))
                                   (method-name (car (@ method-map (source-text (cpp-field fn)))))
                                   (method
                                    (find-enclosing 'cpp-function-declarator
                                                    class
                                                    method-name))
                                   (param (elt (function-parameters method) pos)))
                         (collect param)))))))
    (when-let* ((alist argument-parameters)
                (pos (position ast alist :key #'car))
                (params
                 (if-let (gen-decl (of-templated-type? ast))
                   ;; Determine if a templated type is a reference, based on usage.
                   (collect-template-params gen-decl pos)
                   (cdr (elt alist pos)))))
      (let ((kinds (mapcar #'ref-decl-kind params)))
        (assure ref-decl-kind
          (cond ((no kinds) nil)
                ((every (eqls :const) kinds) :const)
                ((every (eqls :mut) kinds) :mut)
                (t nil)))))))

(defun rewrite-call-with-reference-wrappers (call)
  (match call
    ((cpp-call-expression
      (cpp-function function-name)
      (cpp-arguments
       (and args-ast (cpp-argument-list
                      (children args)))))
     (unless args
       (fail))
     (when-let* ((alist (argument-parameters function-name args))
                 (new-args
                  (iter (for (arg . nil) in alist)
                        (collecting
                          (match arg
                            ((or (cpp* "__CRAM__MOVE($1)" _)
                                 (cpp* "std::move($1)" _)
                                 (cpp* "std::cref($1)" _)
                                 (cpp* "__CRAM__CREF($1)" _)
                                 (cpp* "std::ref($1)" _)
                                 (cpp* "__CRAM__REF($1)" _))
                             arg)
                            (otherwise
                             (if (declared-as-reference? arg) arg
                                 (ecase-of ref-decl-kind
                                     (passed-as-reference?
                                      arg
                                      :argument-parameters alist)
                                   (:const
                                    (cpp* "__CRAM__CREF($1)" arg))
                                   (:mut
                                    (cpp* "__CRAM__REF($1)" arg))
                                   ((nil)
                                    arg)))))))))
       (unless (equal args new-args)
         (copy call
               :cpp-arguments
               (copy args-ast
                     :children new-args)))))))

(defun ref-decl-kind (decl)
  (assure ref-decl-kind
    (when (typep decl '(and declaration-ast
                        (not cpp-enumerator)))
      (let ((type (canonicalize-type decl :software (attrs-root*))))
        (and (find :reference (flatten (declarator type)))
             (if (const-declaration? decl)
                 :const
                 :mut))))))

(defun declared-as-reference? (id)
  (match id
    ((cpp-reference-declarator) t)
    ((cpp-init-declarator
      (cpp-declarator (cpp-reference-declarator)))
     t)
    (otherwise
     (when-let (decl (get-declaration-ast :variable id))
       (ref-decl-kind decl)))))

(defun ast-argument-parameters (arg)
  "Invoke ARGUMENT-PARAMETERS on the arglist ARG is part of."
  (when-let* ((call-expr (find-enclosing 'cpp-call-expression *root* arg))
              (arglist (find-enclosing 'cpp-argument-list call-expr arg))
              (pos (position arg (children arglist)))
              (function-name (cpp-function call-expr))
              (alist (argument-parameters function-name (children arglist))))
    alist))

(defun argument-parameters (function arguments)
  "Return an alist from each argument in ARGS to a list of its
  matching parameters in FUNCTION's overloads."
  (if (no arguments) nil
      (let* ((ids (get-declaration-ids :function function))
             (declarators
               (lret* ((root (attrs-root*))
                       (decls
                        (mapcar (op (find-enclosing 'c/cpp-function-declarator root _))
                                ids)))
                 (every (of-type 'c/cpp-function-declarator) decls)))
             (parameter-lists
               (filter (op (length= arguments _))
                       (mapcar #'function-parameters declarators))))
        (iter (for arg in arguments)
              (for i from 0)
              (collecting
                (cons arg
                      (mapcar (op (first (drop i _)))
                              parameter-lists)))))))


;;; Add move and clone annotations

(defclass add-move-and-clone (cpp-refactoring two-phase-refactoring annotation-refactoring)
  ()
  (:default-initargs
   :description "Add move and clone annotations"
   :name :lilac.move))

(defmethod change-table ((r add-move-and-clone) (project t) (root ast))
  (let ((*db* (analysis:analyze-templates root :db *db*)))
    (iter (for ast in-tree (genome root))
          (when-let (replacement (move-or-clone-args ast))
            (unless (source-text= (cdr replacement) ast)
              (collect replacement))))))

(defmethod annotation-preprocs append ((r add-move-and-clone))
  (list
   (cpp* "#define __CRAM__CLONE(x) x")
   (cpp* "#define __CRAM__MOVE(x) x")))

(defun add-move-and-clone (root)
  (apply-refactoring* 'add-move-and-clone root))

(defun can-copy? (ast)
  ;; Note that std::endl will be removed during translation anyway.
  ;; This just reduces noise.
  (or (source-text= "std::endl" ast)
      (source-text= "endl" ast)
      (declared-as-reference? ast)
      (and-let* ((decl (of-templated-type? ast))
                 (constraints
                  (templated-type-constraints *db* decl)))
        (every #'can-copy-type? constraints))
      (can-copy-type? (infer-type ast))))

(defgeneric can-copy-definition? (definition)
  (:documentation "Does DEFINITION define a type that can be copied?")
  (:method ((type-def t))
    nil)
  (:method ((type-def cpp-enum-specifier))
    t)
  (:method ((type-def cpp-enumerator))
    t))

(defgeneric can-copy-type? (type)
  (:documentation "Can TYPE be copied automatically (not cloned)?")
  ;; TODO Look at definitions for copy methods.
  (:method ((type t))
    nil)
  (:method ((type cpp-qualified-identifier))
    (cond
      ;; Note that these can be copied, but they will be removed.
      ((source-text= "std::endl" type))
      ((source-text= "std::ostream" type))
      (t (can-copy-definition? (get-declaration-ast :type type)))))
  (:method ((type cpp-type-identifier))
    (match type
      ((source-text= "ostream") t)
      (otherwise
       (can-copy-definition? (get-declaration-ast :type type)))))
  (:method ((type cpp-primitive-type))
    t)
  (:method ((type cpp-sized-type-specifier))
    t)
  (:method ((type cpp-type-descriptor))
    (can-copy-type? (cpp-type type))))

(defun moveable? (ast &aux (root *root*))
  "Can we apply move to AST?"
  (labels ((moveable? (ast)
             (and
              ;; Check if AST itself can move or copy.
              (match ast
                ;; Already a move.
                ((cpp* "__CRAM__MOVE($1)" _) nil)
                ((cpp* "std::move($1)" _) nil)
                ((cpp* "std::forward<$1>($2)" _ _) nil)
                ((or (identifier-ast)
                     (cpp-field-expression))
                 ;; Can't move out of a reference.
                 (if (needs-deref? ast)
                     nil
                     ;; Is it the last usage?
                     (last-usage-in-scope? root ast)))
                ((cpp-subscript-expression
                  (cpp-argument arg))
                 (moveable? arg))
                ;; ((cpp-field-expression
                ;;   (cpp-argument arg))
                ;;  (moveable project arg :ok t))
                (otherwise nil))
              ;; Check if the parent exempts AST from move/copy.
              (nlet rec ((ast ast))
                (match (get-parent-ast root ast)
                  ((cpp* "std::static_cast<$1>($2)" _ _) nil)
                  ((cpp* "static_cast<$1>($2)" _ _) nil)
                  ((and arglist (cpp-argument-list))
                   (rec arglist))
                  (otherwise t))))))
    (if (can-copy? ast)
        nil
        (moveable? ast))))

(defgeneric last-usage-in-scope? (software ast)
  (:documentation "Is AST the last usage in scope (and not returned)?")
  (:method (root (id identifier-ast))
    ;; An identifier does not need to be cloned if it is the last
    ;; usage in scope and if it is not returned.
    (and-let* ((decl (get-declaration-id :variable id))
               (scope (find-enclosing #'scope-ast-p (attrs-root*) decl))
               (uses (collect-var-uses scope decl))
               ((eql id (lastcar uses)))
               ((not (find-enclosing 'return-ast root id))))))
  (:method ((root t) (id cpp-field-expression))
    (ematch id
      ((cpp-field-expression
        (cpp-argument obj))
       (and-let* ((field-decl (get-declaration-id :variable id))
                  (obj-decl (get-declaration-id :variable obj))
                  (scope (find-enclosing #'scope-ast-p (attrs-root*) obj-decl))
                  (uses (collect-var-uses scope field-decl))
                  (uses (mapcar (lambda (use)
                                  (typecase use
                                    (cpp-field-identifier
                                     (find-enclosing 'cpp-field-expression
                                                     (attrs-root*)
                                                     use))
                                    (cpp-field-expression use)))
                                uses))
                  ((eql id (lastcar uses)))))))))

(defun wrap-move (ast)
  "Wrap AST with a move annotation."
  (copy-with-surrounding-text
   (cpp* "__CRAM__MOVE($1)" ast)
   ast))

(defun move-or-clone (arg)
  (cond ((can-copy? arg) arg)
        ((moveable? arg)
         (wrap-move arg))
        (t
         (maybe-wrap-clone arg))))

(defgeneric move-or-clone-args (ast)
  (:method ((ast ast)) nil)
  (:method ((ast assignment-ast))
    (when (source-text= (operator ast) "=")
      (unless (declared-as-reference? (lhs ast))
        (let* ((old-rhs (rhs ast))
               (new-rhs (move-or-clone old-rhs)))
          (unless (eql old-rhs new-rhs)
            (cons ast
                  (copy ast :rhs new-rhs)))))))
  (:method ((ast cpp-init-declarator))
    (unless (declared-as-reference? (lhs ast))
      (let* ((old-rhs (rhs ast))
             (new-rhs (move-or-clone old-rhs)))
        (unless (eql old-rhs new-rhs)
          (cons ast
                (copy ast :rhs new-rhs))))))
  (:method ((ast call-ast))
    (match ast
      ((cpp* "__CRAM__DEREF($1)" expr)
       (unless (can-copy? expr)
         (let ((new-expr (maybe-wrap-clone expr)))
           (unless (eql expr new-expr)
             (cons ast new-expr)))))
      ((or (cpp* "__CRAM__MOVE($1)" _)
           (cpp* "__CRAM__CLONE($1)" _))
       nil)
      (otherwise
       (let* ((args (call-arguments ast))
              (new-args (mapcar #'move-or-clone args)))
         (unless (equal args new-args)
           (cons ast
                 (copy ast
                       :arguments
                       (copy (cpp-arguments ast)
                             :children new-args))))))))
  (:method ((ast cpp-initializer-list))
    (let* ((children (children ast))
           (new-children
             (mapcar #'move-or-clone children)))
      (unless (equal children new-children)
        (cons ast
              (copy ast
                    :children new-children)))))
  (:method ((ast cpp-unary-expression))
    (let* ((old-arg (cpp-argument ast))
           (new-arg (move-or-clone old-arg)))
      (unless (eql old-arg new-arg)
        (cons ast
              (copy ast :cpp-argument new-arg)))))
  (:method ((ast cpp-binary-expression))
    (let* ((old-lhs (lhs ast))
           (new-lhs (move-or-clone old-lhs))
           (old-rhs (rhs ast))
           (new-rhs (move-or-clone old-rhs)))
      (unless (and (eql old-lhs new-lhs)
                   (eql old-rhs new-rhs))
        (cons ast
              (copy ast
                    :lhs new-lhs
                    :rhs new-rhs))))))

(defun clone-excluding-call? (ast)
  (match ast
    ((or (cpp* "__CRAM__MOVE($1)" _)
         (cpp* "std::move($1)" _)
         (cpp* "std::static_cast<$1>($2)" _ _)
         (cpp* "static_cast<$1>($2)" _ _)
         (cpp* "__CRAM__CLONE($1)" _)
         (cpp* "std::ref($1)" _)
         (cpp* "__CRAM__REF($1)" _)
         (cpp* "std::cref($1)" _)
         (cpp* "__CRAM__CREF($1)" _)
         (cpp* "$1 == $2" _ _)
         (cpp* "$1 != $2" _ _))
     t)
    ((cpp-argument-list)
     (clone-excluding-call? (get-parent-ast *root* ast)))))

(defun operator? (x)
  (string-case (source-text x)
    ;; NB No && and || by design.
    (("+" "+=" "&" "&=" "|" "=" "^" "^="
          "*x" "/" "/=" "()" "[]" "*" "*=" "-"
          "!" "%" "%=" "<<" "<<=" ">>" ">>="
          "-=")
     t)))

(defun wrap-clone (ast)
  "Actually wrap AST with a call to clone."
  (match ast
    ((cpp* "__CRAM__CLONE($1)" _) ast)
    ((cpp* "__CRAM__DEREF($1)" inner)
     (copy-with-surrounding-text
      (cpp* "__CRAM__CLONE($1)" inner)
      ast))
    (otherwise
     (copy-with-surrounding-text
      (cpp* "__CRAM__CLONE($1)" ast)
      ast))))

(-> maybe-wrap-clone (ast) (values ast &optional))
(defun maybe-wrap-clone (ast)
  "Maybe wrap AST with a clone call (as `$AST.clone()`)."
  (cond
    ((typep ast 'parenthesized-expression-ast) ast)
    ((clone-excluding-call? ast) ast)
    ((clone-excluding-call? (get-parent-ast *root* ast))
     ast)
    ((not (evaluated? ast))
     ast)
    ((can-copy? ast) ast)
    ((wrap-clone? ast)
     (wrap-clone ast))
    (t ast)))

(defun of-templated-type? (id &aux (root (attrs-root*)))
  "ID is of a templated type if its type is one of:
- A type parameter to a template.
- `auto' and it is bound by a parameter to a generic lambda
  or a constexpr function."
  (flet ((handle-auto-param (decl id)
           (let ((var-decl (get-declaration-ast :variable id)))
             (and (member var-decl (function-parameters decl))
                  (source-text= (cpp-type var-decl) "auto")
                  var-decl))))
    (when (typep id '(or identifier-ast field-ast))
      (cond-let decl
        ((find-enclosing 'cpp-template-declaration root id)
         (when-let (type (infer-type id))
           (let ((type-def (get-declaration-ast :type type)))
             (when (typep type-def 'cpp-type-parameter-declaration)
               type-def))))
        ((find-enclosing 'cpp-function-definition root id)
         (when (member "constexpr" (cpp-pre-specifiers decl) :test #'source-text=)
           (handle-auto-param decl id)))
        ((find-enclosing 'cpp-lambda-expression root id)
         (handle-auto-param decl id))))))

(defun canonicalize-primitive (ast)
  "If AST is a primitive type, canonicalize it."
  ;; TODO Handle sized type specifiers as well?
  (ignore-errors
   (let ((prim
           (ignore-errors
            (find-if (of-type 'cpp-primitive-type)
                     (cpp* "$TYPE x;" :type (source-text ast))))))
     (and (source-text= prim ast)
          prim))))

(defun templated-type-constraints (db param)
  "Get the types that PARAM, a template parameter, is specialized to."
  (when-let (template (find-enclosing 'cpp-template-declaration (attrs-root*)
                                      param))
    (when-let* ((pos (position param (children (cpp-parameters template))))
                (types
                 (mapcar (op (aref (car _) pos))
                         (lilac-commons/database:select-results
                          db
                          'analysis:template-specializations
                          '(:type-args)
                          :template template))))
      (~>> types
           (filter-map
            (lambda (type)
              ;; TODO Why do we get type descriptors instead of
              ;; primitive types in template specializations?
              (or (canonicalize-primitive type)
                  (get-declaration-ast :type type))))
           ;; TODO How to handle nested templates?
           (remove-if (of-type 'cpp-type-parameter-declaration))
           remove-duplicates))))

(defgeneric wrap-clone? (ast)
  (:method ((ast t)) nil)
  (:method ((ast cpp-new-expression)) nil)
  (:method :around ((ast literal-ast))  ;Precedence.
    nil)
  (:method ((ast arguments-ast))
    nil)
  (:method ((ast call-ast))
    (if-let (fn (get-declaration-ast :function ast))
      ;; TODO If a function returns an object, not a reference,
      ;; we shouldn't need to clone it.
      (typep (cpp-declarator fn)
             '(or cpp-reference-declarator cpp-pointer-declarator))
      (match ast
        ((cpp* "__CRAM__DEREF($X)" :x _)
         t))))
  (:method ((ast cpp-initializer-list))
    nil)
  (:method ((ast cpp-compound-literal-expression))
    nil)
  (:method ((ast subscript-ast))
    t)
  (:method ((ast binary-ast))
    (let ((o (operator ast)))
      (and (not (operator? o))
           (not (source-text= "==" o))
           (call-next-method))))
  (:method ((ast expression-ast))
    (or (needs-deref? ast)
        (not (can-copy? ast)))))


;;; Wrap explicit derefs.

(defclass wrap-derefs (cpp-refactoring
                       two-phase-refactoring
                       annotation-refactoring)
  ()
  (:default-initargs
   :description "Make dereferences explicit"
   :name :lilac.deref))

(defmethod change-table ((r wrap-derefs) (project t) (file ast))
  (let ((*db* (analysis:analyze-templates file :db *db*)))
    (iter (for ast in-tree (sel:genome file))
          (let ((deref (wrap-deref ast)))
            (unless (eql ast deref)
              (collect (cons ast deref)))))))

(defmethod annotation-preprocs append ((r wrap-derefs))
  (list (cpp* "#define __CRAM__DEREF(x) x")))

(defun wrap-derefs (root)
  (apply-refactoring* 'wrap-derefs root))

;;; TODO How (why) is this different from declared-as-reference?
(defun needs-deref? (ast)
  (if (declared-as-reference? ast)
      (not (passed-as-reference? ast))
      (match ast
        ((cpp* "__CRAM__DEREF($1)" _) nil)
        (otherwise
         (when-let* ((id (get-declaration-id :variable ast))
                     (decl (find-enclosing 'variable-declaration-ast *root* id)))
           (unless (find ast decl)
             (match (get-parent-ast decl id)
               ((cpp-reference-declarator (cpp-valueness (cpp-&)))
                t))))))))

(defun evaluated? (ast)
  "Try to infer from its context whether AST is evaluated (not an lvalue).

E.g., AST is evaluated if:
- Its parent is an expression.
- Its parent is an init declarator, and it is the RHS.
- Its parent is an argument list or an initializer list."
  (match (get-parent-ast (attrs-root*) ast)
    ((cpp-argument-list) t)
    ((cpp-initializer-list) t)
    ((cpp-assignment-expression
      (lhs (and (cpp-identifier)
                (eql ast))))
     t)
    ((cpp-assignment-expression
      (lhs (eql ast)))
     nil)
    ((and (expression-ast)
          (not (or (cpp-field-expression)
                   (cpp-pointer-expression))))
     t)
    ((cpp-init-declarator
      (rhs (eql ast)))
     t)))

(defun wrap-deref (ast &aux (root *root*))
  (or (and (typep ast 'cpp-identifier)
           (let ((parent (get-parent-ast root ast)))
             (and
              (not (typep parent 'cpp-qualified-identifier))
              (needs-deref? ast)
              (nlet rec ((parent parent))
                (match parent
                  ((cpp-argument-list)
                   (rec (get-parent-ast root parent)))
                  ((cpp* "__CRAM__CLONE($1)" _) nil)
                  ((cpp* "std::cref($1)" _) nil)
                  ((cpp* "__CRAM__CREF($1)" _) nil)
                  ((cpp* "std::ref($1)" _) nil)
                  ((cpp* "__CRAM__REF($1)" _) nil)
                  ((cpp* "__CRAM__DEREF($1)" _) nil)
                  ((cpp-field-expression) nil)
                  (otherwise t)))
              (evaluated? ast)
              (match ast
                ;; TODO Could double-deref ever be necessary (given the input
                ;; is a reference, not a pointer)?.
                ((cpp* "__CRAM__DEREF($1)" _) nil)
                (otherwise
                 (copy-with-surrounding-text
                  (cpp* "__CRAM__DEREF($1)" ast)
                  ast))))))
      ast))


;;; Expand update expressions.

;;; The concern is that a statement like `b++;` may need the
;;; expression and the assignment to be handled separately.

;; (defun rewrite-update-statement (expr)
;;   (let ((proto
;;           (ematch expr
;;             ((cpp-update-expression-postfix
;;               (cpp-argument arg))
;;              (match (infer-type arg)
;;                ((cpp-type-descriptor
;;                  (cpp-declarator
;;                   (cpp-abstract-pointer-declarator)
;;                   (cpp-type type)))
;;                 (cpp* "$TYPE * $TEMP = *$VAR;
;; *$VAR =
;; *$VAR = *$VAR + 1"))
;;                (otherwise
;;                 (cpp* "$VAR = $VAR + 1")))
;;              )
;;             ((cpp-update-expression-prefix
;;               (cpp-argument arg))
;;              (match (infer-type arg)
;;                ((cpp-type-descriptor
;;                  (cpp-declarator
;;                   (cpp-abstract-pointer-declarator)))
;;                 (cpp* "*$VAR = *$VAR + 1"))
;;                (otherwise
;;                 (cpp* "$VAR = $VAR + 1")))))))))

;; (defgeneric expand-update-expressions (root)
;;   (:method ((root software))
;;     (let ((new-genome (expand-update-expressions (genome root))))
;;       (if (eql new-genome (genome root)) root
;;           (copy root :genome new-genome))))
;;   (:method ((root ast))
;;     (if-let ((update-exprs (collect-if (of-type 'cpp-update-expression) root)))
;;       (mapcar
;;        (hash-table-function
;;         (maphash-into
;;          (make-hash-table)
;;          (lambda (expr)
;;            (values expr
;;                    (rewrite-update-expression expr)))
;;          update-exprs))
;;        root)
;;       root)))


;;; Break up illegal alias nests

(defclass uniquify-mutable-aliases (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Get rid of mutable alias nests"
   :name :lilac.unnest))

(defun declaration-variables (decl)
  (multiple-value-bind (decls namespaces)
      (sel/sw/ts::outer-declarations decl)
    (iter (for decl in decls)
          (for ns in namespaces)
          (when (or (no ns) (eql ns :variable))
            (collect decl)))))

(defun plain-vars (ast)
  (mappend (lambda (decl.vars)
             (destructuring-bind (decl . vars) decl.vars
               (filter (lambda (var)
                         (not (alias-declaration? decl var)))
                       vars)))
           (mapcar (op (cons _1 (declaration-variables _1)))
                   (collect-if (of-type 'variable-declaration-ast) ast))))

(defgeneric alias-declaration? (ast var)
  (:documentation
   "Return NIL if AST is not an alias declaration, otherwise :MUT or
:CONST as appropriate.

Note that even if the declarator is :CONST, that does not mean the
declaration is. Both the pointer and pointee must be const.")
  (:method ((ast cpp-type-parameter-declaration) (var t))
    nil)
  (:method ((ast cpp-ast) var)
    (ecase (alias-declarator? (cpp-declarator ast) var)
      ((nil) nil)
      (:mut :mut)
      (:const
       (if (find-if (op (source-text= "const" _))
                    (cpp-pre-specifiers ast))
           :const
           :mut))))
  (:method ((ast cpp-init-declarator) var)
    (and (alias-declarator? (cpp-declarator ast) var)
         (alias-declaration?
          (find-enclosing 'cpp-declaration
                          (attrs-root*)
                          ast)
          var))))

(defgeneric alias-declarator? (declarator var)
  (:documentation "Return NIL if DECLARATOR is not a declarator,
  otherwise :MUT or :CONST as appropriate.")
  (:method ((ast cpp-ast) (var t)) nil)
  (:method ((ast cpp-pointer-declarator) (var t))
    (if (find-if (op (source-text= "const" _))
                 (children ast))
        :const
        :mut))
  (:method ((ast cpp-reference-declarator) (var t))
    :const)
  ;; (:method ((ast cpp-array-declarator)) t)
  (:method ((ast cpp-init-declarator) var)
    (alias-declarator? (cpp-declarator ast) var))
  (:method ((asts list) var)
    (if-let (d (find-if (op (find var _)) asts))
      (alias-declarator? d var)
      (error "No declarator for ~a among ~a" var asts))))

;;; TODO Multiple vars per declaration?

(defun mutable-alias? (var)
  (eql :mut
       (alias-declaration?
        (get-declaration-ast :variable var)
        var)))

(defun mutable-aliases (var)
  ;; TODO Sort? What's the predicate? Or is the alias set always
  ;; sorted?
  (partition #'mutable-alias? (alias-set var)))

(defun uniquify-mutable-aliases/changes (root)
  "Compute the changes necessary to get rid of mutable alias nests in ROOT.

Returns two values:
1. A table of changes to make.
2. A list of declarations to remove."
  (labels ((parent-deref? (ast)
             (match (get-parent-ast root ast)
               ((and parent (cpp* "*$1" _))
                parent)
               (otherwise nil))))
    (lret ((change-table (make-hash-table)))
      (iter (for var in (plain-vars root))
            (match (mutable-aliases var)
              ;; At least two aliases.
              ((list* alias1 alias2 more-aliases)
               (let ((more-aliases (cons alias2 more-aliases))
                     (reference?
                       ;; Is alias1 declared as a reference?
                       (find-enclosing 'cpp-reference-declarator
                                       root
                                       alias1))
                     (pointer?
                       (find-enclosing 'cpp-pointer-declarator
                                       root
                                       alias1)))
                 (assert (nand reference? pointer?))
                 (dolist (alias more-aliases)
                   (let ((alias-decl (find-outermost 'variable-declaration-ast
                                                     root
                                                     alias)))
                     (setf (href change-table alias-decl) :remove)))
                 (dolist (alias more-aliases)
                   (let ((uses (collect-var-uses root alias)))
                     (dolist (use uses)
                       (let ((real-use
                               ;; If the canonical alias is a
                               ;; reference, and the reference being
                               ;; replaced is a pointer, get rid of
                               ;; the dereference operator.
                               (or (and reference?
                                        (parent-deref? use))
                                   use))
                             (alias-use
                               ;; If the canonical alias is a pointer,
                               ;; and alias being replaced is a
                               ;; reference, add a dereference
                               ;; operator.
                               (or (and pointer?
                                        (not (parent-deref? use))
                                        (cpp* "*$1" alias1))
                                   alias1)))
                         (setf (gethash real-use change-table)
                               (tree-copy alias-use)))))))))))))

(defmethod change-table ((r uniquify-mutable-aliases)
                         (project t)
                         (target t))
  (uniquify-mutable-aliases/changes target))

(defun uniquify-mutable-aliases (root)
  "Refactoring to get rid of mutable alias nests."
  (apply-refactoring (make 'uniquify-mutable-aliases)
                     (attrs-root*)
                     (list root)))


;;; Preproc def to constexpr

(defclass constexprify (cpp-refactoring two-phase-refactoring)
  ((predicate :type function :initform (error "No predicate"))
   (rewriter :type function :initform (error "No rewriter"))))

(defclass rewrite-definitions-to-constexprs (constexprify)
  ((predicate :initform #'rewritable-preproc-def-p)
   (rewriter :initform #'rewrite-preproc-def))
  (:default-initargs
   :description "Rewrite macro definitions"
   :name :lilac.constify.preproc-def))

(defclass rewrite-preprocs-to-constexpr-funs (constexprify)
  ((predicate :initform #'rewritable-preproc-func-p)
   (rewriter :initform #'rewrite-preproc-func))
  (:default-initargs
   :description "Rewrite macro functions"
   :name :lilac.constify.preproc-func))

(defmethod change-table ((r constexprify) (project t) (obj ast))
  (with-slots (rewriter predicate) r
    (fbind (rewriter predicate)
      (maphash-into
       (make-hash-table)
       (op (values _1 (rewriter obj _1)))
       (collect-if
        (op (predicate obj _1))
        obj)))))

(defun rewrite-definitions-to-constexprs (obj)
  (apply-refactoring* 'rewrite-definitions-to-constexprs obj))

;;; TODO: need a function that finds every reference to the function and
;;;       determines if any of them change the precedence of the macro?
(defun rewrite-preprocs-to-constexpr-funs (obj)
  (apply-refactoring* 'rewrite-preprocs-to-constexpr-funs obj))

(deftype cpp-literal ()
  '(or cpp-char-literal cpp-number-literal cpp-string-literal))

(deftype cpp-conditional-preproc ()
  '(or cpp-preproc-if cpp-preproc-elif cpp-preproc-ifdef))

(defun preproc-parameters (preproc-func)
  (collect-if (of-type 'cpp-identifier)
              (cpp-parameters preproc-func)))

(defun preproc-condition (ast)
  "Return the condition portion of the preprocessor conditional represented by
AST."
  (etypecase ast
    (cpp-preproc-ifdef (cpp-name ast))
    ((or cpp-preproc-if cpp-preproc-elif) (cpp-condition ast))))

(defun identifier-used-by-preprocessor (root identifier)
  "Return T if IDENTIFIER is part of a preprocessor form and is used in a
preprocessor conditional."
  (labels ((in-preproc (root preproc ast)
             (parent-ast-p root (preproc-condition preproc) ast)))
    (some (op (in-preproc root _ identifier))
          (remove-if-not
           (of-type 'cpp-conditional-preproc)
           (get-parent-asts* root identifier)))))

(defun preproc-def-used-by-preprocessor (obj preproc-def)
  "Return T if PREPROC-DEF is used by the PREPROCESSOR in ROOT."
  ;; TODO: collect all references to the identifier.
  ;;       NOTE that we'll start with only collecting from the header file, but
  ;;       we'll need to expand to everything that includes the header file
  ;;       directly or indirectly.
  (labels ((collect-identifiers (ast name)
             (when-let ((condition (preproc-condition ast)))
               (collect-if (lambda (child)
                             (and (typep child 'cpp-identifier)
                                  (source-text= child name)))
                           condition))))
    ;; TODO: assumes that an implicit value means it is used by the preprocessor.
    ;;       This may not be strictly true or all inclusive, so it will likely
    ;;       need to be changed in the future.
    (or (not (cpp-value preproc-def))
        (let ((name (cpp-name preproc-def)))
          (some
           (op (identifier-used-by-preprocessor obj _))
           (mappend
            (op (collect-identifiers _ name))
            (collect-if (of-type 'cpp-conditional-preproc)
                        (find-if (of-type 'root-ast)
                                 (get-parent-asts obj preproc-def)))))))))

(defun expression-literal-p (obj preproc-def value)
  "Return T if VALUE is an expression which only uses literal values.
This currently doesn't check to see if any referenced identifiers
are macro literals, constexpr variables, or constexpr functions."
  ;; TODO: Expand this further to work with more identifiers, such as constexpr
  ;;       and macro functions.
  ;; NOTE: if this doesn't work as expected, we may need to whitelist
  ;;       the relevant classes. Though with a cursory look at the
  ;;       grammar, this should work.
  (labels ((macro-identifier-literal-p (identifier-ast)
             "Return T if AST is a macro identifier which expands to a literal."
             (when-let* ((identifier
                          (find-in-symbol-table
                           preproc-def :macro (source-text identifier-ast)))
                         (definition (find-if (of-type 'cpp-preproc-def)
                                              (get-parent-asts obj (car identifier))))
                         (rhs (reparse-preproc-value definition)))
               (expression-literal-p obj definition rhs))))
    (match value
      ((type cpp-literal) t)
      ((cpp-parenthesized-expression)
       (every
        (lambda (target)
          (match target
            ((ast (children (not nil))) t)
            ;; TODO: handle constexpr function calls.
            ((cpp-call-expression) nil)
            ((cpp-identifier) (macro-identifier-literal-p target))
            ((or (not (computed-text))
                 (type cpp-literal))
             t)))
        value))
      ((cpp-identifier)
       (macro-identifier-literal-p value)))))

(defun reparse-preproc-value (preproc-def)
  "Reparse the value of PREPROC-DEF as the right hand side of an assignment
statement and return it if it parses correctly. This is done because the value
of a preprocessor definition is parsed as a string and not as an AST."
  (let ((reparsed-tree
          (cpp* "x = $1"
                ;; NOTE: remove any single line comments as they can cause
                ;;       issues. Note that this doesn't work with strings
                ;;       containing //. This is likely not a common issue.
                ;; TODO: Trime-whitespace as whitespace seems to cause some
                ;;       issues for some reason?
                (trim-whitespace (car (split "//" (source-text (cpp-value preproc-def))))))))
    (and (typep reparsed-tree 'cpp-assignment-expression)
         (not (find-if (of-type 'variation-point) reparsed-tree))
         (rhs reparsed-tree))))

(defun rewritable-preproc-def-p (obj ast &key reparsed-value)
  (and (typep ast 'cpp-preproc-def)
       (not (preproc-def-used-by-preprocessor obj ast))
       (expression-literal-p obj ast (or reparsed-value
                                         (reparse-preproc-value ast)))))

(defun rewritable-preproc-func-p (obj ast)
  ;; TODO: since parameters are textual replacement, order of operations can
  ;;       change. It will make sense to confirm that the order doesn't change
  ;;       at call sites. This could be difficult to do in places where it
  ;;       doesn't parse correctly, but simply combing through the source text
  ;;       for places where it occurs and refeeding them into the tree-sitter
  ;;       parser could be a start.
  ;; TODO: confirm that every identifier is in the symbol table at this point.
  ;;       Note that it is slightly more complicated than this. Actually need
  ;;       to check that it is in the symbol and the same reference is at every
  ;;       usage of the macro as well. This will confirm that it isn't being
  ;;       shadowed.
  (and (typep ast 'cpp-preproc-function-def)
       (not (preproc-def-used-by-preprocessor obj ast))
       (typep (reparse-preproc-value ast) 'cpp-parenthesized-expression)))

(defun rewrite-preproc-def (obj preproc)
  (labels ((rewrite/auto (name value)
             (cpp (fmt "constexpr auto $1 = $2;~%")
                   name
                   value))
           (rewrite/array (type name value)
             (cpp (fmt "constexpr $1 $2$3 = $4;~%")
                   (source-text (copy type :cpp-declarator nil))
                   name
                   (source-text (cpp-declarator type))
                   value))
           (rewrite/non-array (type name value)
             (cpp (fmt "constexpr $1 $2 = $3;~%")
                   (source-text type)
                   name
                   value))
           (rewrite (preproc reparsed-value)
             (let ((type (expression-type reparsed-value))
                   (name (source-text (cpp-name preproc))))
               (cond
                 ((typep type 'cpp-type-descriptor)
                  (rewrite/array type name reparsed-value))
                 (type
                  (rewrite/non-array type name reparsed-value))
                 ((expression-literal-p obj preproc reparsed-value)
                  (rewrite/auto name reparsed-value))))))
    (when-let ((reparsed-value (reparse-preproc-value preproc)))
      (rewrite preproc reparsed-value))))

(defun rewrite-preproc-func (obj preproc)
  (declare (ignore obj))
  (labels ((rewrite/auto (name parameters body)
             (cpp
              (fmt "constexpr auto $1(~{auto ~a~^, ~}) { return $2; }~%"
                   parameters)
              name
              body)))
    (let* ((name (source-text (cpp-name preproc)))
           (parameters (mapcar #'source-text (preproc-parameters preproc)))
           (body (source-text (cpp-value preproc)))
           (constexpr-function (rewrite/auto name parameters body)))
      (if (find-if (of-type 'parse-error-ast) constexpr-function)
          preproc
          constexpr-function))))


;;; Inline preprocs

(defclass inline-preprocessor-macros (cpp-refactoring one-shot-refactoring)
  ()
  (:default-initargs
   :description "Inline macro uses"
   :name :lilac.inline
   :after '(rewrite-definitions-to-constexprs
            rewrite-preprocs-to-constexpr-funs)
   :kind lsp:|CodeActionKind.RefactorInline|))

(defmethod refactor-target ((self inline-preprocessor-macros) (project t) obj)
  (when-let ((macros
              (~>> obj
                   (collect-if (of-type '(or cpp-preproc-def cpp-preproc-function-def)))
                   ;; Ignore CRAM annotation macros.
                   (remove-if (op (search "CRAM__" (source-text _)))))))
    (let ((new (inline-macros-with-m4 obj macros)))
      (unless (equal (trim-whitespace (source-text new))
                     (trim-whitespace (source-text obj)))
        new))))

(defun inline-preprocessor-macros (obj)
  "Inline every preprocessor invocation in OBJ that isn't used in conditional
compilation."
  (apply-refactoring* 'inline-preprocessor-macros obj))

(defun construct-m4-macro (preproc-ast)
  "Construct an m4 query for inlining PREPROC-AST."
  ;; NOTE: by default, m4 will ignore anything that occurs after a #.
  ;;       This should ignore the expansion of any macro usage in preprocessor
  ;;       conditionals.
  (labels ((map-args-to-new-args (args &aux (i 0))
             "Create conses mapping the arg to its corresponding m4 arg number."
             (mapcar (op (list _ (incf i))) args))
           (replace-arg (rhs arg i)
             "Replace ARG in RHS with a new argument based on I."
             ;; TODO: currently assumes that strings won't include anything that
             ;;       could be expanded by the C or M4 preprocessor.
             (regex-replace (fmt "(\\W)~a(\\W|$)" arg)
                            rhs
                            (fmt "\\{1}$~a\\{2}" i)))
           (replace-args (rhs args)
             (reduce (op (apply #'replace-arg _ _))
                     (map-args-to-new-args args)
                     :initial-value rhs)))
    (let ((name (source-text (cpp-name preproc-ast)))
          (args (typecase preproc-ast
                  (cpp-preproc-function-def
                   (mapcar #'source-text
                           (children
                            (cpp-parameters preproc-ast))))))
          (rhs (source-text (cpp-value preproc-ast))))
      (unless (equal rhs "")
        (fmt "-D~a='~a'"
             name
             (replace-args
             ;; NOTE: remove leading space
              (subseq rhs 1)
              args))))))

(defun inline-macros-with-m4 (obj preproc-asts)
  "Inline the macros defined by PREPROC-ASTS in OBJ."
  ;; TODO: this doesn't currently propagate to included
  ;;       files. This won't break anything since the preprocs aren't removed,
  ;;       but this should be addressed at some point.
  (let ((m4-macros (mapcar #'construct-m4-macro preproc-asts)))
    ;; TODO: this isn't the most efficient. It would better to try and
    ;;       localize where these changes are made--the closest compound
    ;;       statement may be the best bet aside from top-level forms?
    (convert 'cpp-ast
             (apply #'$cmd "m4"
                    (append m4-macros

                            (list :<<< (string+
                                        (fmt "~%changequote(,)dnl~%")
                                        (source-text (genome obj))
                                                ;; NOTE: add newline to prevent
                                                ;;       M4 from throwing an
                                                ;;       error on unterminated
                                                ;;       comments.
                                                (fmt "~%"))))))))


;;; Modularize project

(defclass modularize-project (cpp-refactoring one-shot-refactoring)
  ()
  (:default-initargs
   :description "Modularize project"
   :name :lilac.modularize-project
   :needs-confirmation nil
   :kind lsp:|CodeActionKind.Source|))

(defmethod apply-refactoring ((self modularize-project) (project cpp-project) targets)
  (declare (ignore targets))
  (nest
   ;; Don't try to modularize a single-file project.
   (if (single (dir:evolve-files project))
       project)
   ;; Don't try to modularize an already modularized project.
   (if (some (op (module-path? (car _)))
             (dir:evolve-files project))
       project)
   (labels ((to-software (file-ast)
              ;; NOTE: convert the file-ast to a CPP software object so that
              ;;       #'with works with it.
              ;; TODO: add functionality to #'with for file-asts.
              (make 'cpp
                    :genome (car (dir:contents file-ast))
                    :original-path (assure pathname (dir:full-pathname file-ast))))))
   (attrs:with-attr-session (project :shadow t))
   ;; TODO: the values of these maps may be in reverse order.
   (let* ((namespace->asts (namespace-map project))
          (namespace->dependencies (namespace-dependency-map project))
          (new-project (remove-evolve-objects project)))
      (setf (dir:evolve-files new-project) nil)
      (iter
        (for (namespace nil) in-map namespace->asts)
        (mapcar
         (lambda (file-ast)
           (assert (not (equalp ".cppm" (dir:full-pathname file-ast))))
           (withf new-project
                  (namestring (dir:full-pathname file-ast))
                  (to-software file-ast)))
         ;; TODO: don't use values for the return.
         (multiple-value-list
          (generate-module-files
           namespace namespace->asts namespace->dependencies)))
        (finally (return (refactor-main-module-to-file new-project)))))))

(defun modularize-project (obj)
  (apply-refactoring* 'modularize-project obj))

;;; Targets which can have other targets in their children.
(deftype nestable-transformation-target ()
  '(or cpp-class-specifier
    ;; NOTE: the grammar reuses these for type identifiers and definitions;
    ;;       they may require some extra analysis. Also note that it's likely
    ;;       this would occur in the children of a transformation-target, so
    ;;       it's probably not an issue?
    cpp-struct-specifier
    cpp-enum-specifier))

(deftype cram-preproc ()
  '(and cpp-preproc-function-def
    (satisfies cram-preproc?)))

(defun cram-preproc? (x)
  (and (typep x 'cpp-preproc-function-def)
       (search "__CRAM__" (source-text x))))

;;; NOTE: isn't using preprocs.
(deftype transformation-target ()
  '(and
    ;; NOTE: _top_level_item--not exposed in lisp interface.
    (or
     cram-preproc
     cpp-function-definition
     cpp-linkage-specification
     cpp-declaration
     cpp--statement
     cpp-attributed-statement
     cpp-type-definition
     cpp--type-specifier
     cpp-empty-statement
     nestable-transformation-target)
    (not (or
          cpp-namespace-definition cpp-namespace-alias-definition
          ;; NOTE: go a level down into the classes.
          cpp-declaration-list
          cpp-field-declaration-list))))

(deftype namespace-creator ()
  '(or cpp-namespace-definition))

(defun transformation-target-p (ast)
  (match ast
    ((type transformation-target) t)
    ((cpp-preproc-include
      (cpp-path (cpp-system-lib-string)))
     t)))

(defun filename-namespace (filename &key ignore-prefix)
  "Return a valid namespace for FILENAME."
  ;; NOTE: remove the file extension and assume that header and implementation
  ;;       files will match easily.
  (apply #'string+
         (list (and (not ignore-prefix) "cram_")
               (regex-replace-all
                "[|\\./-]"
                (regex-replace "\\..*$" filename "") "_"))))

(defgeneric namespace-map (ast &key)
  (:documentation "Construct a map of namespaces to their corresponding ASTs."))

;;; TODO: assuming skipping preprocessor directives is fine for now.
(defmethod namespace-map ((ast cpp-ast)
                          &key (file-path "|NO-FILEPATH|") headerp
                          &aux unhandled-asts
                            (file-namespace (filename-namespace file-path)))
  ;; TODO: does it make sense to put the #includes in here? It probably does.
  ;;       The question is at what scope something like "using" applies to.
  ;;       Worth checking, but maybe we just assuming that using isn't used
  ;;       at a local scope, i.e., not encompassing the whole namespace?
  (labels ((skip-ast-p (ast)
             "Return T if AST shouldn't be added to the namespace map.
             This is strictly for
             AST types."
             (match ast
               ((cpp-expression-statement
                 (children (list)))
                t)
               ((cpp-class-specifier
                 (cpp-body (list)))
                t)))
           (append-to-namespace (map ast namespace)
             "Append AST to the value at NAMESPACE in MAP."
             (if (skip-ast-p ast)
                 map
                 (let ((current-value (lookup map namespace)))
                   (with map namespace
                         (if headerp
                             (list (cons ast (car current-value))
                                   (cadr current-value))
                             (list (car current-value)
                                   (cons ast (cadr current-value))))))))
           (update-map/children (map ast &rest rest &key &allow-other-keys)
             "Update MAP with the children of AST."
             (reduce (op (map-union _ _ #'merge-namespace-maps))
                     (children ast)
                     :key (op (apply #'update-map map _ rest))
                     :initial-value map))
           (update-map (map ast &rest rest &key namespace-prefix file-namespace)
             "Traverse AST updating MAP along the way."
             (let ((namespace-creator-p (typep ast 'namespace-creator))
                   (transformation-target-p (transformation-target-p ast)))
               (cond
                 ((typep ast '(or parse-error-ast source-text-fragment))
                  ;; NOTE: pushing this on as a special case of unhandled
                  ;;       trees. Will likely need to remove this later before
                  ;;       handling.
                  (push ast unhandled-asts)
                  (fset:map))
                 ((and (not namespace-prefix)
                       transformation-target-p)
                  ;; NOTE: The global namespace for a file will be a file path
                  ;;       to the file. This should allow us to constrict it to
                  ;;       the necessary scope based on compilation units.
                  (append-to-namespace map ast file-namespace))
                 ;; TODO: skip anonymous namespaces for now.
                 ((and namespace-creator-p
                       (emptyp (source-text (cpp-name ast))))
                  (fset:map))
                 (namespace-creator-p
                  (let* ((source-string (source-text (cpp-name ast)))
                         (new-namespace
                           (if (emptyp namespace-prefix)
                               source-string
                               (string+ namespace-prefix "::" source-string))))
                    (apply #'update-map/children map ast
                           :namespace-prefix new-namespace
                           rest)))
                 (transformation-target-p
                  (append-to-namespace map ast namespace-prefix))
                 (t
                  (apply #'update-map/children map ast rest))))))
    (values
     ;; TODO: the values for the map are all in reverse order. A function could
     ;;       be made to create a new map with reversed values.
     ;;       For now, they will be reversed on usage.
     (update-map (fset:map) ast :file-namespace file-namespace)
     unhandled-asts)))

(defmethod namespace-map ((project cpp-project) &key &aux unhandled-asts)
  (labels ((get-map (evolve-file-pair)
             (let* ((file-path (car evolve-file-pair))
                    (global-namespace (filename-namespace file-path))
                    (namespace-map (file-namespace-map
                                    (genome (cdr evolve-file-pair))
                                    file-path)))
               ;; NOTE: ensure global namespace exists for file.
               (if (nth-value 1 (lookup namespace-map global-namespace))
                   namespace-map
                   (with namespace-map global-namespace nil))))
           (file-namespace-map (ast file-path)
             (mvlet ((map unhandled
                          (namespace-map ast
                                         :file-path file-path
                                         :headerp (header-path? file-path))))
               (push (list file-path unhandled) unhandled-asts)
               map))
           (reverse-values (map &aux (new-map (fset:map)))
             (do-map (key value map)
               (withf new-map key (list (reverse (car value))
                                        (reverse (cadr value)))))
             new-map))
    (attrs:with-attr-table project
      (values
       ;; NOTE: values in the map are backwards, so reverse them.
       (reverse-values
        (reduce
         (op (map-union _ _ #'merge-namespace-maps))
         (mapcar #'get-map
                 (sel/sw/c-cpp-project:evolve-files/dependency-order project))))
       unhandled-asts))))

(defun merge-namespace-maps (map1 map2)
  ;; NOTE: interface dependencies car
  ;;       impl dependencies cdr
  (list (append (car map1) (car map2))
        (append (cadr map1) (cadr map2))))

;;; TODO: merge this with namespace map to reduce iteration overhead.
(defmethod namespace-dependency-map ((ast cpp-ast)
                                     &key filename header->namespaces
                                       include-table)
  "Returns as values a namespace dependency map and a header to namespaces map."
  (assert filename)
  (labels ((collect-namespaces/children (ast &rest rest &key &allow-other-keys)
             "Update MAP with the children of AST."
             (mappend (op (apply #'collect-namespaces _ rest)) (children ast)))
           (collect-namespaces (ast &rest rest
                                    &key namespace-prefix
                                    &allow-other-keys)
             "Collect all namespaces which occur in AST."
             (let ((namespace-creator-p (typep ast 'namespace-creator))
                   (transformation-target-p (transformation-target-p ast)))
               (cond
                 ;; TODO: skip anonymous namespaces for now.
                 ((and namespace-creator-p
                       (emptyp (source-text (cpp-name ast))))
                  nil)
                 (namespace-creator-p
                  (let* ((source-string (source-text (cpp-name ast)))
                         (new-namespace
                           (if (emptyp namespace-prefix)
                               source-string
                               (string+ namespace-prefix "::" source-string))))
                    (cons new-namespace
                          (apply #'collect-namespaces/children ast
                                 :namespace-prefix new-namespace
                                 rest))))
                 (transformation-target-p
                  nil)
                 (t
                  (apply #'collect-namespaces/children ast rest)))))
           (update-header->namespaces (filename ast header->namespaces)
             "Update HEADER->NAMESPACES to include an entry for FILENAME with
              the value set to NAMESPACES."
             (symbol-macrolet ((header-hash
                                 (gethash filename header->namespaces)))
               (assert (null header-hash))
               (setf header-hash
                     ;; NOTE: always include a global namespace even if nothing
                     ;;       is in it. This will allow for reexporting all the
                     ;;       global namespaces that came before it.
                     ;; NOTE: global namespace is the file path.
                     (remove-duplicates
                      (cons (filename-namespace filename)
                            (collect-namespaces ast))))))
           (dependencies-from-includes
            (filename include-table header->namespaces)
             "Return a list of namespace dependencies for INCLUDES."
             (mappend (op (gethash (car _) header->namespaces))
                      (gethash filename include-table)))
           (map-namespace-dependencies (filename namespaces dependencies
                                        &aux (headerp (header-path? filename))
                                          (dependencies
                                           ;; NOTE: append at end to maintain
                                           ;;       the correct order.
                                           (append1 dependencies
                                                    (filename-namespace filename))))
             "Return a map of namespaces to their depencencies"
             (if-let* ((dependencies (if headerp
                                         (list dependencies)
                                         (list nil dependencies)))
                       (dependency-maps
                        (mapcar (lambda (namespace)
                                  ;; NOTE: add global namespace of current file
                                  ;;       to the dependencies.
                                  (fset:map (namespace dependencies)))
                                namespaces)))
               (reduce #'map-union dependency-maps)
               (fset:map)))
           (remove-self-references (map)
             "Return a modified version of MAP that doesn't allow any key-value
             pair to have the key in its value."
             (let ((result (fset:map)))
               (do-map (key value map)
                 (withf result key (list (remove key (car value)
                                                 :test #'equal)
                                         (remove key (cadr value)
                                                 :test #'equal))))
               result)))
    (let* ((namespaces
             (update-header->namespaces filename ast header->namespaces))
           ;; NOTE: dependent on previous binding's side effects.
           (dependencies
             (dependencies-from-includes
              filename include-table header->namespaces)))
      (remove-self-references
       (map-namespace-dependencies filename namespaces dependencies)))))

(defmethod namespace-dependency-map ((project cpp-project) &key)
  "Return a map which maps a namespace to a associative list of impl dependencies
and interface dependencies."
  (labels ((get-dependency-tree ()
             (handler-bind
                 ((sel/sw/c-cpp-project:include-conflict-error
                    (lambda (c)
                      (declare (ignorable c))
                      (invoke-restart 'first))))
               (project-dependency-tree project :allow-headers t))))
    (let* ((dependency-tree (get-dependency-tree))
           (include-table (alist-hash-table dependency-tree :test #'equal))
           (header->namespaces (make-hash-table :test #'equal)))
      (reduce
       (op (map-union _ _ #'merge-namespace-maps))
       (mapcar (op (namespace-dependency-map (genome (cdr _1))
                                             :filename (car _1)
                                             :header->namespaces header->namespaces
                                             :include-table include-table))
               (evolve-files/dependency-order
                project :include-tree dependency-tree))))))

(defun generate-module-files
    (namespace namespace->asts namespace->dependencies
     ;; TODO: confirm that the dependencies need to be reversed.
     &aux (dependencies
           (mapcar #'reverse (lookup namespace->dependencies namespace)))
       (asts (mapcar #'reverse (lookup namespace->asts namespace))))
  "Return as values the translation units for the module, implementation, and
interface files."
  (labels ((partition-asts (existing-asts)
             "Partition EXISTING-ASTS into non-include ASTs and include ASTs and
              return them as values. Also creates a copy of 'main' if it is found
              which exports it."
             (iter
               (for ast in (mapcar #'tree-copy existing-asts))
               (match ast
                 ((cpp-preproc-include
                   (cpp-path (cpp-system-lib-string)))
                  (collect ast into includes))
                 ((cpp-function-definition
                   (cpp-declarator
                    (cpp-function-declarator
                     (cpp-declarator
                      (and (cpp-identifier) (source-text= "main"))))))
                  (collect
                      (copy ast :children (cons (make-instance
                                                 'cpp-export-specifier
                                                 :text "export"
                                                 :before-text " "
                                                 :after-text " ")
                                                (direct-children ast)))
                    into asts))
                 (otherwise
                  (collect ast into asts)))
               (finally (return (values asts includes)))))
           (get-filepath (namespace)
             (regex-replace-all "::" (string-left-trim '(#\:) namespace) "/"))
           (get-file-ast (name translation-unit)
             (let ((filepath (get-filepath name))
                   ;; Using patch-whitespace is necessary at least for
                   ;; macro definitions from the beginning of files
                   ;; that have no before-text.
                   (unit (patch-whitespace translation-unit)))
               (make 'dir:file-ast
                     :contents (list unit)
                     :name (fmt "~a.cppm" (file-namestring filepath))
                     :full-pathname (pathname (fmt "~a.cppm" filepath)))))
           (get-module-ast (name &key export)
             (cpp* (fmt "~%~:[~;export ~]module ~a;" export name)))
           (get-module-fragment-ast ()
             (cpp* (fmt  "module;~%")))
           (get-import-ast (dependency &key export newline)
             (cpp* (fmt "~%~:[~;export ~]import ~a;~:[~;~%~]"
                        export dependency newline)))
           (get-namespace-ast (namespace body-asts)
             "Return a list of a namespace AST with BODY-ASTS in the body if
              NAMESPACE isn't a file name."
             (if (string^= "cram_" namespace)
                 body-asts
                 (list (cpp* (fmt "~%namespace $1 {~%@2~%}")
                             namespace body-asts))))
           (get-import-include (include-ast)
             (ematch include-ast
               ((cpp-preproc-include
                 (cpp-path (and path (cpp-system-lib-string))))
                (get-import-ast (source-text path) :export t :newline t))))
           (implementation-file (namespace module-dependencies asts include-asts)
             (get-file-ast
              (fmt "~a-impl_part" namespace)
              (make 'cpp-translation-unit
                    :children
                    `(,(get-module-fragment-ast)
                      ,@(mapcar #'get-import-include include-asts)
                      ,(get-module-ast (fmt "~a:impl_part" namespace))
                      ,(get-import-ast ":interface_part" :newline t)
                      ,@(mapcar #'get-import-ast module-dependencies)
                      ;; NOTE: the project shouldn't have any of the
                      ;;       existing ASTs in it. There were memory
                      ;;       issues when trying to copy them.
                      ,@(get-namespace-ast namespace asts)))))
           (interface-file (namespace module-dependencies asts include-asts)
             (get-file-ast
              (fmt "~a-interface_part" namespace)
              (make 'cpp-translation-unit
                    :children
                    `(,(get-module-fragment-ast)
                      ,@(mapcar #'get-import-include include-asts)
                      ,(get-module-ast (fmt "~a:interface_part" namespace)
                                       :export t)
                      ,@(mapcar (op (get-import-ast _ :export t))
                                module-dependencies)
                      ;; NOTE: the project shouldn't have any of the existing
                      ;;       ASTs in it. There were memory issues when trying
                      ;;       to copy them.
                      ,(cpp* (fmt "~%export {~%@1~%}~%")
                             (get-namespace-ast namespace asts))))))
           (module-file (namespace include-asts)
             ;; TODO: NOTE: the handling of includes here isn't ideal as every
             ;;             system include is always being exported.
             (get-file-ast
              namespace
              (make 'cpp-translation-unit
                    :children `(,(get-module-ast namespace :export t)
                                ,@(mapcar #'get-import-include include-asts)
                                ,(get-import-ast ":interface_part" :export t)
                                ,(get-import-ast ":impl_part"))))))
    (mvlet ((impl-asts impl-include-asts (partition-asts (cadr asts)))
            (header-asts header-include-asts (partition-asts (car asts))))
      (values (module-file
               namespace (append header-include-asts impl-include-asts))
              (implementation-file
               namespace (cadr dependencies) impl-asts impl-include-asts)
              (interface-file
               namespace (car dependencies) header-asts header-include-asts)))))

(defun refactor-main-module-to-file (project)
  "Refactor the module which contains main into a main.cc file.
Note that this function assumes that main will occur in an implementation file."
  (labels ((find-file (dir pathname)
             (find-if (lambda (ast)
                        (and (typep ast 'dir:file-ast)
                             (equal pathname
                                    (namestring (dir:full-pathname ast)))))
                      dir))
           (pathname-prefix (file-ast)
             "Return the path of the file-ast without the extension and partition
             part."
             (let ((file-name (namestring (dir:full-pathname file-ast))))
               (subseq file-name 0 (position #\- file-name))))
           (update-main-module (project dir filename-prefix)
             "Return a new version of PROJECT with the impl import removed from
              the relevant file."
             (let ((module-file (find-file dir (fmt "~a.cppm" filename-prefix))))
               (less project
                     (find-if (lambda (ast)
                                (and (typep ast 'cpp-import-declaration)
                                     (equal ":impl_part"
                                            (source-text (cpp-name ast)))))
                              module-file))))
           (remove-ast-from-impl-p (ast)
             "Return T if AST should be removed from the implementation file."
             (match ast
               ((cpp-import-declaration
                 (cpp-name (source-text= ":interface_part")))
                t)
               ((cpp-module-fragment-declaration) t)
               ((cpp-module-declaration) t)
               ((cpp-export-specifier) t)))
           (translation-unit (file-ast)
             "Return the translation unit of FILE-AST."
             (car (dir:contents file-ast)))
           (add-old-namespace-import (translation-unit filename-prefix)
             "Return a copy of TRANSLATION-UNIT with an import of
              FILENAME-PREFIX."
             (copy translation-unit
                   :children
                   (cons
                    (cpp* (fmt "import ~a"
                               (filename-namespace filename-prefix
                                                   :ignore-prefix t)))
                    (direct-children translation-unit))))
           (rename-impl-to-main (project file new-translation-unit)
             "Return a copy of PROJECT with the previous file removed and a new
              one added as main.cc at the root of the project directory."
             (let ((filename #p"main.cc"))
               (with (less project file)
                     (namestring filename)
                     (make 'cpp
                           :genome new-translation-unit
                           :original-path (assure pathname filename)))))
           (refactor-impl-file (project dir filename-prefix)
             "Refactor the impl file and rename it to main.cc."
             (let* ((file
                      (find-file dir (fmt "~a-impl_part.cppm" filename-prefix)))
                    (new-translation-unit
                      (add-old-namespace-import
                       (remove-if #'remove-ast-from-impl-p
                                  (translation-unit file))
                       filename-prefix)))
               (rename-impl-to-main project file new-translation-unit)))
           (refactor-main (project main-ast)
             "Refactor the main module to have a main.cc file instead."
             (let* ((file-ast
                      (find-if-in-parents (of-type 'dir:file-ast)
                                          project main-ast))
                    (parent-dir-ast
                      (find-if-in-parents (of-type 'dir:directory-ast)
                                          project main-ast))
                    (filename-prefix (pathname-prefix file-ast)))
               (refactor-impl-file
                (update-main-module project parent-dir-ast filename-prefix)
                parent-dir-ast
                filename-prefix))))
    (if-let ((main-ast (find-if (lambda (ast)
                                  (and (typep ast 'cpp-function-definition)
                                       (equal "main" (function-name ast))))
                                project)))
      (refactor-main project main-ast)
      project)))

(defun remove-evolve-objects (project)
  (declare (optimize (speed 3)))
  (lret ((new-project (reduce (op (less _ (find-enclosing (of-type 'dir:file-ast)
                                                          project
                                                          (genome (cdr _)))))
                              (dir:evolve-files project)
                              :initial-value project)))))


;;; Add brackets.

(defclass enforce-braces (cpp-refactoring two-phase-refactoring)
  ()
  (:default-initargs
   :description "Enforce optional curly brackets"
   :name :lilac.brackets))

(defmethod change-table ((r enforce-braces)
                         (project t)
                         (target t))
  (iter (for ast in-tree target)
        (when-let (rewrite (add-brackets ast))
          (collecting (cons ast rewrite)))))

(defun enforce-braces (root)
  (apply-refactoring (make 'enforce-braces)
                     (attrs-root*)
                     (list root)))

(-> add-brackets (ast &key (:prettify t)) (values (or ast null) &optional))
(defun add-brackets (ast &key prettify)
  "If AST needs brackets but doesn't have them, return a copy with brackets.
Otherwise return nil."
  (labels ((wrap (body)
             "Wrap BODY as a compound statement."
             (cond ((no body) body)
                   ((typep body 'compound-ast) body)
                   ((typep body 'if-ast)
                    ;; Handle indenting else-if.
                    (copy (or (add-brackets body) body)
                          :indent-adjustment 0
                          :before-text " "))
                   (t
                    (let ((body (or (add-brackets body) body)))
                      (make 'cpp-compound-statement
                            :indent-children (or (indent-children ast) 0)
                            :indent-adjustment 0
                            :children (list (copy body :before-text nil)))))))
           (add-brackets (ast)
             (match ast
               ((or (cpp-do-statement :body (and body (not (compound-ast))))
                    (cpp-while-statement :body (and body (not (compound-ast))))
                    (cpp-for-statement :body (and body (not (compound-ast)))))
                (copy ast
                      :body (wrap body)
                      :indent-children nil))
               ;; Handle conditionals.
               ((cpp-if-statement
                 :alternative a
                 :consequence c)
                (let ((a2 (wrap a))
                      (c2 (wrap c)))
                  (unless (and (eql a2 a)
                               (eql c2 c))
                    (copy ast
                          :alternative a2
                          :consequence c2
                          :indent-children nil))))))
           (prettify (ast)
             (and ast
                  (patch-whitespace ast
                                    :prettify prettify))))
    (prettify (add-brackets ast))))


;;; Switch statements

;;; NOTE: this should eventually be hidden from the user as it's mainly
;;;       to make the translation to Rust easier.
(defclass rewrite-switches (cpp-refactoring two-phase-refactoring)
  ((predicate :initform (of-type 'cpp-switch-statement))
   (rewriter :initform #'rewrite-switch))
  (:default-initargs
   :description "Rewrite switch statements"
   :after '(inline-preprocessor-macros)
   :name :lilac.switch))

(defun rewrite-switches (obj)
  (apply-refactoring* 'rewrite-switches obj))

(defmethod change-table ((r rewrite-switches) (project t) (obj ast))
  (with-slots (rewriter predicate) r
    (fbind (rewriter)
      (maphash-into
       (make-hash-table)
       (op (values _1 (rewriter _1)))
       (collect-if predicate obj)))))

(-> get-fallthrough-sets (ast) (values list &optional))
(defun get-fallthrough-sets (switch-ast)
  "Return a list of lists where each inner list is a grouping of cases which
fallthrough to each other."
  ;; NOTE: assume that there aren't any macros around the case statements as
  ;;       they should be removed by an earlier refactoring.
  (match switch-ast
    ((cpp* "switch ($_) { @CASES }" :cases cases)
     (runs cases
           :compare-last t
           :test (lambda (x y)
                   (member y (exit-control-flow x)))))))

(-> rewrite-switch (cpp-switch-statement) cpp-switch-statement)
(defun rewrite-switch (switch-ast)
  "Return a new ast if SWITCH-AST should be rewritten. Otherwise return
SWITCH-AST.

The rewritten ast will have the following applied to it:
 - The fallthrough set which contains the default case will be moved to occur
   after everything else.
 - The fallthrough set which contains the default case will move the default
   label to occur after every other label.
 - If no default case exists, a default is added at the end which simply breaks."
  (labels ((contains-default-p (fallthrough-set)
             (find-if-not #'cpp-value fallthrough-set))
           (make-empty-default-case ()
             (find-if (of-type 'cpp-case-statement)
                      (convert 'cpp-ast "switch (1) {default: break;}")))
           (rewrite-default (fallthrough-set)
             "Rewrite FALLTHROUGH-SET if a default case exists and doesn't occur
             last."
             (let ((default-case (contains-default-p fallthrough-set))
                   (final-case (lastcar fallthrough-set)))
               (cond ((eq default-case final-case)
                      fallthrough-set)
                     ((no default-case) fallthrough-set)
                     (t
                      (append
                       (remove-if (lambda (ast)
                                    (or (eq ast default-case)
                                        (eq ast final-case)))
                                  fallthrough-set)
                       ;; Move the body of the final case to the default.
                       (list (copy final-case :cpp-statements nil)
                             (copy default-case
                                   :cpp-statements (cpp-statements final-case))))))))
           (rewrite-default-set (fallthrough-sets)
             "Return a new version of FALLTHROUGH-SETS if a default case exists"
             (if-let* ((default-set
                        (find-if #'contains-default-p fallthrough-sets))
                       (new-default-set (rewrite-default default-set)))
               (cond
                 ((not (eq default-set new-default-set))
                  (append1 (remove default-set fallthrough-sets)
                           new-default-set))
                 ((not (eq (lastcar fallthrough-sets) default-set))
                  (append1 (remove default-set fallthrough-sets)
                           default-set))
                 (t fallthrough-sets))
               ;; Add a default at the end if it doesn't exist.
               (append1 fallthrough-sets (make-empty-default-case)))))
    (let* ((switch-body (cpp-body switch-ast))
           (fallthrough-sets (get-fallthrough-sets switch-ast))
           (rewritten-sets (rewrite-default-set fallthrough-sets)))
      (if (eq fallthrough-sets rewritten-sets)
          switch-ast
          (with switch-ast switch-body
                (patch-whitespace
                 (copy switch-body
                       :children (flatten rewritten-sets))))))))
