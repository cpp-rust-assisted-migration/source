(defpackage :lilac-cpp/test
  (:use :gt/full
        :stefil+
        :lilac-commons/commons
        :lilac-cpp/test-utils
        :lilac-cpp/refactorings
        :lilac-commons/aggressive-analysis
        :lilac-commons/refactoring
        :software-evolution-library
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/project
        :software-evolution-library/software/cpp-project
        :cmd)
  (:shadowing-import-from :serapeum :~>)
  (:local-nicknames (:dir :software-evolution-library/software/directory)
                    (:refactor :lilac-cpp/refactorings)
                    (:ts :software-evolution-library/software/tree-sitter))
  (:import-from :lilac-cpp/refactorings
                :wrap-static-cast
                :namespace-map
                :namespace-dependency-map)
  (:export :test :run-batch
           :apply-refactoring-pipeline
           :refactor-raw-cpp
           :assert-source-text=)
  (:local-nicknames (:attrs :functional-trees/attrs)))
(in-package :lilac-cpp/test)
(in-readtable :curry-compose-reader-macros)

(defroot root)

(defsuite test-lilac-cpp "Tests for the LiLaC muse")

(def +test-project+
  (from-file 'cpp-project *trim-front-dir*))

;;; Define both at once to make sure they stay in sync.
(def (values +test-project+ +trim-front.cc+)
  (let ((p (from-file 'cpp-project (path-join *trim-front-dir* "raw/one-file/"))))
    (values p
            (assure software
              (aget "trim-front.cc"
                    (evolve-files p)
                    :test #'equal)))))

(defun test-refactoring (refactoring reference-file &key (vs +trim-front.cc+) genome)
  (let* ((reference-file-string
           (read-file-into-string (assure absolute-pathname reference-file)))
         (input (if genome (genome vs) vs))
         (new (funcall refactoring input))
         (source-text
           (typecase new
             (project (source-text (lookup new (ast-path (attrs-root*) input))))
             (parseable (source-text new))))
         (mismatch (mismatch reference-file-string source-text)))
    (is (null mismatch)
        "Mismatch at ~a:~2%Original: ~a~%Refactored: ~a"
        mismatch
        (collapse-whitespace (take 100 (drop mismatch reference-file-string)))
        (collapse-whitespace (take 100 (drop mismatch source-text))))
    (is (try-compile reference-file)
        "Compiling ~a failed" reference-file)))

(deftest test-pipeline ()
  (let ((refactorings (finishes (refactoring-pipeline))))
    (is (every (of-type 'refactoring) refactorings))))

(deftest test-list-to-vector ()
  (attrs:with-attr-table +test-project+
    (test-refactoring #'rewrite-lists-to-vectors
                      (path-join *trim-front-dir* "stages/trim-front-vector.cc"))))

(deftest test-unmix-subexpression-types ()
  (attrs:with-attr-table +test-project+
    (test-refactoring #'unmix-subexpression-types
                      (path-join *trim-front-dir* "stages/trim-front-unmixed.cc"))))

(deftest test-infer-auto-types ()
  (attrs:with-attr-table +test-project+
    (test-refactoring #'infer-auto-types
                      (path-join *trim-front-dir* "stages/trim-front-noauto.cc"))))

(defun find-var (name &key (software +trim-front.cc+)
                        (genome (genome software))
                        fn)
  (let ((genome
          (if fn
              (find fn
                    (collect-if (of-type 'function-declaration-ast)
                                genome)
                    :key #'definition-name
                    :test #'equal)
              genome)))
    (find name (identifiers genome)
          :test #'source-text=)))

(deftest test-non-const-calls ()
  (attrs:with-attr-table +test-project+
    (is (length= 0 (non-const-calls +trim-front.cc+ (find-var "dist"))))
    (is (length= 0 (non-const-calls +trim-front.cc+ (find-var "d"))))
    (is (length= 0 (non-const-calls +trim-front.cc+ (find-var "segdist"))))
    (is (length= 0 (non-const-calls +trim-front.cc+ (find-var "midpoint"))))
    (is (length= 3 (non-const-calls +trim-front.cc+ (find-var "result"))))

    (is (length= 0 (non-const-calls +trim-front.cc+ (find-var "next_point"))))
    (is (length= 0 (non-const-calls +trim-front.cc+ (find-var "frac"))))
    (is (length= 2 (non-const-calls +trim-front.cc+
                                    (find-var "p1" :fn "trim_front"))))
    (is (length= 0 (non-const-calls +trim-front.cc+ (find-var "p2"))))))

(deftest test-var-can-const-p ()
  (attrs:with-attr-table +test-project+
    (is (not (var-can-const-p (find-var "p1" :fn "trim_front"))))
    (is (not (var-can-const-p (find-var "p2"))))
    (is (not (var-can-const-p (find-var "result"))))
    (is (not (var-can-const-p (find-var "d"))))
    (is (not (var-can-const-p (find-var "pts"))))
    (is (var-can-const-p (find-var "segdist")))
    (is (var-can-const-p (find-var "midpoint")))

    (is (var-can-const-p (find-var "next_point")))
    (is (var-can-const-p (find-var "frac")))
    (is (var-can-const-p (find-var "dist")))))

(deftest test-non-const-variable-declaration-ids ()
  (attrs:with-attr-table +test-project+
    (is (set-equal '("pts" "result" "d" "p1" "p2" "dist"
                     "next_point" "segdist" "frac" "midpoint"
                     "p3" "a" "b" "distance" "result" "pp")
                   (non-const-variable-declaration-ids
                    +trim-front.cc+ (genome +trim-front.cc+))
                   :test #'source-text=))))

(deftest test-decl-id-can-const-p ()
  (attrs:with-attr-table +test-project+
    (is (set-equal '("dist" "next_point" "segdist" "frac" "midpoint"
                     "a" "b" "distance" "p1" "p2" "p3" "result")
                   (filter (op (decl-id-can-const-p _))
                           (non-const-variable-declaration-ids
                            +trim-front.cc+ (genome +trim-front.cc+)))
                   :test #'source-text=))))

(deftest test-adjoin-const ()
  (is (source-text= "const int x = 1;"
                    (adjoin-const (find-if (of-type 'cpp-declaration)
                                           (cpp "int x = 1;")))))
  (is (source-text= "const int x = 1;"
                    (adjoin-const (find-if (of-type 'cpp-declaration)
                                           (cpp "const int x = 1;"))))))

(deftest test-insert-const ()
  (let ((cpp (from-string 'cpp "int x = 1;")))
    (is (source-text= "const int x = 1;"
                      (genome
                       (insert-const cpp
                                     (find-if (of-type 'cpp-declaration)
                                              (genome cpp))))))))

(deftest test-constify-can-const-vars ()
  (attrs:with-attr-table +test-project+
    (test-refactoring #'constify-can-const-vars
                      (path-join *trim-front-dir* "stages/trim-front-const.cc")
                      :vs +trim-front.cc+)))

(defun refactor-project (project &key (refactorings (refactoring-pipeline)))
  "Successively appply the refactorings in REFACTORINGS to PROJECT.
Return the result as a project."
  (reduce (lambda (project refactoring)
            (attrs:with-attr-table project
              (with-aggressive-analysis ()
                (apply-refactoring refactoring project
                                   (get-compilation-units)))))
          refactorings
          :initial-value project))

(defun refactor-string (string &key (refactorings (refactoring-pipeline)))
  "Parse STRING into a project and pass it to `refactor-project'.
Return the result as an AST."
  (multiple-value-bind (project) (project+file string)
    (attrs:with-attr-table project
      (let ((new-project
              (refactor-project project :refactorings refactorings)))
        (genome (cdr (only-elt (evolve-files new-project))))))))

(deftest test-constify-can-const-vars/vector ()
  (let ((dir (path-join *trim-front-dir* #p"stages/")))
    (multiple-value-bind (project file)
        (project+file
         (read-file-into-string
          (path-join dir #p"trim-front-vector.cc")))
      (attrs:with-attr-table project
        (test-refactoring #'constify-can-const-vars
                          (path-join dir #p"trim-front-vector+const.cc")
                          :vs file)))))

(deftest test-auto-resolution ()
  (attrs:with-attr-table +test-project+
    (let* ((sw +trim-front.cc+)
           (midpoint (find-var "midpoint" :software sw))
           (p1 (find-var "p1" :software sw :fn "trim_front")))
      (is (source-text= "Point" (infer-type midpoint)))
      (is (some (op (source-text= "iterator" _))
                (infer-type p1))))))

(deftest test-auto-resolution-in-method ()
  (let* ((sw (from-string 'cpp-project "struct Point {
  double x,y;
  double Distance(const Point & p) {
    const auto a = this.x - p.y;
    const auto b = this.y - p.y;
    return std::sqrt(a * a + b * b);
  }
  Point PointAlongSegment(const Point & p1, const double distance) {
    return new Point(this.x + distance * (p1.x - this.x), this.y + distance * (p1.y - this.y));
  }
};")))
    (attrs:with-attr-table sw
      (let* ((file (is (cdar (evolve-files sw))))
             (inferred (infer-auto-types file))
             (source-text (source-text inferred)))
        (is (string*= "const double a" source-text))
        (is (string*= "const double b" source-text))))))

(deftest test-wrap-static-cast ()
  "Don't wrap static casts around number literals without suffixes.
In particular, distinguish hex literals from floats ending in f/F."
  (let ((cpp (cpp* "0xabcdef")))
    (is (equal? cpp (refactor::wrap-static-cast cpp "int"))))
  (let ((cpp (cpp* "0xabcdefL")))
    (is (source-text= (refactor::wrap-static-cast cpp "int")
                      "static_cast<int>(0xabcdefL)")))
  (let ((cpp (cpp* "0x1ffp10")))
    (is (source-text= (refactor::wrap-static-cast cpp "float")
                      cpp)))
  (let ((cpp (cpp* "0x1ffp10f")))
    (is (source-text= (refactor::wrap-static-cast cpp "double")
                      "static_cast<double>(0x1ffp10f)")))
  ;; TODO This is being parsed as a user-defined literal, not a number
  ;; literal.
  #+(or) (let ((cpp (cpp* "0X1FFP10F")))
           (is (source-text= (refactor::wrap-static-cast cpp "double")
                             "static_cast<double>(0X1FFP10F)")))
  (let ((cpp (cpp* "0.0f")))
    (is (source-text= (refactor::wrap-static-cast cpp "double")
                      "static_cast<double>(0.0f)")))
  (let ((cpp (cpp* "0.0F")))
    (is (source-text= (refactor::wrap-static-cast cpp "double")
                      "static_cast<double>(0.0F)"))))

(deftest test-unmix-nested-expressions ()
  (let ((cpp (from-string 'cpp "{float  x  = 10.0; double y  = 20.0; float z = x * y;}")))
    (attrs:with-attr-table cpp
      (is (search "static_cast<float>(static_cast<double>(x) * y)"
                  (source-text (unmix-subexpression-types cpp)))))))

(deftest test-unmix-literals ()
  (let ((cpp (from-string 'cpp "float x = 0; float y = 0; 2 * (x + y);")))
    (attrs:with-attr-table cpp
      (source-text= (unmix-subexpression-types cpp)
                    "float x = 0.0; float y = 0.0; 2.0 * (x + y);"))))

(deftest test-const-declaration-predicate ()
  (let ((cpp (cpp "void print() const {}")))
    (attrs:with-attr-table cpp
      (is (const-declaration? cpp)))))

(deftest test-of-templated-type/constexpr ()
  (let ((cpp (cpp "constexpr auto plus(auto x, int y) { return x + y; }")))
    (attrs:with-attr-table cpp
      (is (of-templated-type? (last-use cpp "x")))
      (is (not (of-templated-type? (last-use cpp "y")))))))

(deftest test-of-templated-type/lambda ()
  (let ((cpp (cpp* "[](auto a, auto&& b) { return a < b; }")))
    (attrs:with-attr-table cpp
      (symbol-table (last-use cpp "a"))
      (is (of-templated-type? (last-use cpp "a")))
      (is (of-templated-type? (last-use cpp "b"))))))

(deftest test-of-templated-type ()
  (let ((cpp (genome (from-string 'cpp "template <typename T>
class Pair {
private:
  T l;
  T r;
public:
  Pair(const T& l, const T& r): l(l), r(r) {}
  T sum() const { return l.add(r); }
};"))))
    (attrs:with-attr-table cpp
      (is (of-templated-type? (last-use cpp "l")))
      (is (of-templated-type? (last-use cpp "r"))))))

(deftest test-use-static-cast ()
  (let ((cpp (from-string 'cpp "struct Type {};
(Type) * variable;")))
    (is (string*= "static_cast"
                  (source-text
                   (attrs:with-attr-table cpp
                     (use-static-cast cpp))))))
  (let ((cpp (from-string 'cpp "struct Type {};
(Type)(*variable);")))
    (is (string*= "static_cast"
                  (source-text
                   (attrs:with-attr-table cpp
                     (use-static-cast cpp)))))))

(deftest rewrite-static-cast ()
  (is (source-text=
       "static_cast<double>(x)"
       (wrap-static-cast (cpp* "static_cast<float>(x)")
                         (make 'cpp-type-identifier :text "double")))))

(deftest test-add-explicit-this ()
  (let ((cpp
          (from-string 'cpp
                       "struct Pair {
  float l, r;
  float add() const { return l.add(r); }
};")))
    (attrs:with-attr-table cpp
      (is (string*= "this->l.add(this->r)"
                    (source-text (add-explicit-this cpp)))))))

(deftest test-add-explicit-this/non-member ()
  (let ((cpp
          (from-string 'cpp
                       "struct Distance {
float x;
Unit unit;
};

Distance Distance::add(const Distance& d) const {
if (unit == d.unit) {
    Distance result(x + d.x, unit);
    return result; }
  else if (unit == Unit::IN) { // in + cm
    Distance result(x + d.x / 2.54, unit);
    return result; }
  else {                       // cm + in
    Distance result(x + d.x * 2.54, unit);
    return result; }
}")))
    (attrs:with-attr-table cpp
      (let ((text (source-text (add-explicit-this cpp))))
        (is (= 3 (count-matches "this->x" text)))
        (is (= 5 (count-matches "this->unit" text)))))))

(deftest test-struct-expression-type ()
  (let ((cpp
          (from-string 'cpp "struct Pair {
   float l, r;
}

int main() {
    auto x = Pair { 1, 2 };
}")))
    (attrs:with-attr-table cpp
      (is (string*= "1.0, 2.0"
                    (source-text
                     (apply-refactoring-pipeline cpp
                                                 '(infer-auto-types
                                                   unmix-subexpression-types))))))))

(deftest test-struct-expression-type/init-declarator ()
  (let ((cpp
          (from-string 'cpp "struct Pair {
   float l, r;
}

int main() {
    Pair x{ 1, 2 };
}")))
    (attrs:with-attr-table cpp
      (is (string*= "1.0, 2.0"
                    (source-text
                     (~> cpp
                         infer-auto-types
                         unmix-subexpression-types)))))))

(deftest test-dont-clone-stream ()
  (let ((cpp (from-string 'cpp
                          "std::ostream& operator<<(std::ostream& os, const Date& dt)
{
    os << dt.mo << '/' << dt.da << '/' << dt.yr;
    return os;
}")))
    (attrs:with-attr-table cpp
      (is (source-text= (infer-type (lastcar (collect-if (op (source-text= "os" _))
                                                         cpp)))
                        "std::ostream")))
    (is (not (string*= "__CRAM__CLONE(os)"
                       (source-text
                        (apply-refactoring-pipeline cpp
                                                    '(wrap-derefs
                                                      add-move-and-clone))))))))

(deftest test-const-method-predicate ()
  (is (const-method? (cpp* "int Bar(int random_arg) const {}")))
  (is (not (const-method? (cpp* "int Bar(const int random_arg) {}"))))
  (is (const-method? (cpp* "T& get_x() const {}")))
  (is (const-method? (cpp* "T* get_x() const {}")))
  (is (not (const-method? (cpp* "T* get_x(const x) {}")))))

(deftest test-constify-can-const-methods ()
  (let ((cpp (from-string 'cpp "struct Point {
  double x,y;
  double Distance(const Point & p) {
    const auto a = this->x - p.x;
    const auto b = this->y - p.y;
    return std::sqrt(a * a + b * b);
  }
  Point PointAlongSegment(const Point & p1, const double distance) {
    return Point{this->x + distance * (p1.x - this->x), this->y + distance * (p1.y - this->y)};
  }
};")))
    (is (= 2 (count-matches
              "\\) const \\{"
              (source-text
               (attrs:with-attr-table cpp
                 (constify-can-const-methods cpp))))))))

(deftest test-constexprify-preproc-def ()
  (check-preproc-def-to-constexpr
   (fmt "#define VERSION \"1.2.3\"~%")
   (fmt "constexpr const char VERSION[6] = \"1.2.3\";~%"))
  (check-preproc-def-to-constexpr
   (fmt "#define VALUE 1~%")
   (fmt "constexpr int VALUE = 1;~%"))
  (check-preproc-def-to-constexpr
   (fmt "#define VALUE (1 + 2 (3 / 2))~%")
   (fmt "constexpr auto VALUE = (1 + 2 (3 / 2));~%"))
  (check-preproc-def-to-constexpr
   (fmt "#define X 1~%#define VALUE X~%")
   (fmt "constexpr int X = 1;~%constexpr auto VALUE = X;~%"))
  (check-preproc-def-to-constexpr
   (fmt "#define X 1~%#define Y X~%#define VALUE (X + Y)~%")
   (fmt "constexpr int X = 1;~%constexpr auto Y = X;~%constexpr auto VALUE = (X + Y);~%")))

(deftest test-constexprify-preproc-def-failure ()
  ;; NOTE: these are to demonstrate limitations of the current functionality.
  (check-preproc-def-to-constexpr-fails
   (fmt "#define VALUE 1 + 2~%"))
  (check-preproc-def-to-constexpr-fails
   (fmt "#define VALUE (1 + x())~%"))
  (check-preproc-def-to-constexpr-fails
   (fmt "#define VALUE (1 + X)~%"))
  (check-preproc-def-to-constexpr-fails
   (fmt "#define VALUE X~%")))

(deftest test-constexprify-preproc-fun ()
  (check-preproc-func-to-constexpr
   (fmt "#define X()(1)~%")
   (fmt "constexpr auto X() { return (1); }~%"))
  (check-preproc-func-to-constexpr
   (fmt "#define X(a, b) (a + b)~%")
   (fmt "constexpr auto X(auto a, auto b) { return  (a + b); }~%")))

(defun check-inline-preprocessor-macros (source expected)
  (let ((cpp (from-string 'cpp source)))
    (attrs:with-attr-table cpp
      (let ((refactored-cpp
              (source-text (inline-preprocessor-macros cpp))))
        (is (string= expected (source-text refactored-cpp)))))))

(deftest test-inline-preprocessor-macros ()
  (check-inline-preprocessor-macros
   "#define LOG(MESSAGE) std::cout << MESSAGE << std::endl;
LOG(1 << 2);"
   (fmt "~%#define LOG(MESSAGE) std::cout << MESSAGE << std::endl;
std::cout << 1 << 2 << std::endl;;"))
  ;; Doesn't inline in preprocessor conditionals.
  (check-inline-preprocessor-macros
   (fmt "#define X 1~%#ifdef X~%X~%#endif~%")
   (fmt "~%#define X 1~%#ifdef X~%1~%#endif~%")))

(defun check-namespaces (root namespace-alist
                         &rest rest
                         &key &allow-other-keys)
  "Check that NAMESPACE-ALIST matches the namespace map of SOURCE.
Keyword arguments are passed directly to #'namespace-map."
  (labels ((trim-map (map key expected-value)
             (let* ((value (cadr (lookup map key)))
                    (intersection (intersection value expected-value
                                                :test #'eq)))
               (is (= (length value)
                      (length expected-value)
                      (length intersection)))
               (less map key))))
    (is (empty? (reduce (op (trim-map _1 (car _2) (cdr _2)))
                        namespace-alist
                        :initial-value (apply #'namespace-map root rest))))))

(defun namespace-alist (root &rest namespace-aconses)
  (labels ((find-starts-with (source)
             (or (find-if (lambda (ast)
                            (and (not (typep ast 'cpp-translation-unit))
                                 (starts-with-subseq source (source-text ast))))
                          root)
                 (error "Invalid namespace alist--'~a' not found"
                        source)))
           (namespace-acons (acons)
             (list* (car acons)
                    (mapcar #'find-starts-with (cdr acons)))))
    (mapcar #'namespace-acons namespace-aconses)))

(defmacro check-namespaces/alist (source &rest namespace-alist-acons)
  "Check that NAMESPACE-ALIST-ACONS matches the namespace map of SOURCE.
The file path of the global namespace is ~/file-path."
  `(let ((root (convert 'cpp-ast ,source)))
     (check-namespaces
      root
      (apply #'namespace-alist root ',namespace-alist-acons)
      :file-path "file-path")))

(deftest test-namespace-map ()
  ;; Global namespace of the file is represented.
  (check-namespaces/alist
   "int x;"
   ("cram_file_path" "int x"))
  ;; Namespaces can map to multiple ASTs.
  (check-namespaces/alist
   "int x; int y;"
   ("cram_file_path" "int x" "int y"))
  ;; Maps nested namespaces.
  (check-namespaces/alist
   "int x; namespace x { int y; namespace y { int z; }}"
   ("cram_file_path" "int x")
   ("x" "int y")
   ("x::y" "int z")))

(deftest test-uniform-initialization-syntax ()
  (let ((cpp (from-string 'cpp "struct Point {
  double x,y;
  double Distance(const Point & p) {
    const auto a = this->x - p.x;
    const auto b = this->y - p.y;
    return std::sqrt(a * a + b * b);
  }
  Point PointAlongSegment(const Point & p1, const double distance) {
    Point p(this->x + distance * (p1.x - this->x), this->y + distance * (p1.y - this->y));
    return p;
  }
};")))
    (is (string*= "Point p{this->x + distance * (p1.x - this->x), this->y + distance * (p1.y - this->y)}"
                  (source-text
                   (attrs:with-attr-table cpp
                     (use-uniform-initialization-syntax cpp)))))))

(defun check-namespace-dependency-map
    (path expected-map &aux (project (from-file 'cpp-project path)))
  (labels ((check-mapping (namespace expected-map actual)
             (let ((expected (lookup expected-map namespace)))
               (is (null (set-exclusive-or (car expected) (car actual)
                                           :test #'equal))
                   "FOR ~a HEADER DEPENDENCIES EXPECTED ~a GOT ~a"
                   namespace (car expected) (car actual))
               (is (null (set-exclusive-or (cadr expected) (cadr actual)
                                           :test #'equal))
                   "FOR ~a IMPLEMENTATION DEPENDENCIES, EXPECTED ~a GOT ~a"
                   namespace (cadr expected) (cadr actual))))
           (check-size (map expected-map)
             (is (= (size map) (size expected-map)))))
    (attrs:with-attr-session (project)
      (let ((namespace-map (namespace-dependency-map project)))
        (check-size namespace-map expected-map)
        (do-map (namespace mapping expected-map)
          (check-mapping namespace expected-map mapping))))))

(deftest test-namespace-dependency-map ()
  ;; Properly maps namespace dependencies across files.
  ;; TODO: two files--header, implementation--with same namespace, one depends on
  ;;       the other.
  ;; Nested namespace dependencies are correctly handled.
  ;; TODO: single file with nested namespaces?
  (check-namespace-dependency-map
      (path-join *grist-dir* "test/modules/multi-file-simple/")
      (fset:map ("B" '(("cram_b") ("cram_b")))
                ("cram_a" '(nil ("cram_b" "B")))
                ("cram_b" '(nil nil))
                ("cram_main" '(nil ("cram_a")))))
  (check-namespace-dependency-map
      (path-join *grist-dir* "test/modules/dependency-map/")
      (fset:map ("a" '(("cram_b" "b" "cram_a") ("cram_a")))
                ("b" '(("cram_b") nil))
                ("cram_a" '(("cram_b" "b") nil))
                ("cram_b" '(nil nil))
                ("cram_c" '(("cram_d" "cram_b" "b") nil))
                ("cram_d" '(nil nil))
                ("cram_main" '(nil ("cram_a" "a"))))))

(deftest test-add-brackets ()
  (is (equal (fmt "~
do {
  y();
} while (x());")
             (source-text
              (add-brackets (cpp* (fmt "~
do
  y();
while (x())"))))))

  (is (equal (fmt "~
while (x) {
  y();
}")
             (source-text
              (add-brackets (cpp* (fmt "~
while (x)
  y();"))))))

  (is (equal (fmt "~
for (i; i++; i < j) {
  y();
}")
             (source-text
              (add-brackets (cpp* (fmt "~
for (i; i++; i<j)
  y();")))))))

(deftest test-enforce-braces/do-nothing ()
  "Test we don't change an already properly braced if statement."
  (is (equal (source-text
              (refactor-string (fmt "~
if (test1) {
  do_something();
} else if (test2) {
  do_something_else();
}")))
             (fmt "~
if (test1) {
  do_something();
} else if (test2) {
  do_something_else();
}"))))

(deftest test-enforce-braces/else ()
  "That that we add braces to both branches of an if-else."
  (is (equal (source-text
              (refactor-string (fmt "~
if (test1)
  do_something();
else
  do_something_else();")))
             (fmt "~
if (test1) {
  do_something();
} else {
  do_something_else();
}"))))

(deftest test-enforce-braces/else-if ()
  "Test that we add braces to both branches for else-if."
  (is (equal (source-text
              (refactor-string (fmt "~
if (test1)
  do_something();
else if (test2)
  do_something_else();")))
             (fmt "~
if (test1) {
  do_something();
} else if (test2) {
  do_something_else();
}"))))

(deftest test-enforce-braces/else/if ()
  "Test that we add braces to else-if even if indentation obscures it."
  (is (equal (source-text
              (refactor-string (fmt "~
if (test1)
  do_something();
else
  if (test2)
    do_something_else();")))
             (fmt "~
if (test1) {
  do_something();
} else if (test2) {
  do_something_else();
}"))))

(deftest test-enforce-braces/else-if/else ()
  "That we add braces to an else-if with else."
  (is (equal (source-text
              (refactor-string (fmt "~
if (test1)
  do_something();
else if (test2)
  do_something_else();
else
  give_up();")))
             (fmt "~
if (test1) {
  do_something();
} else if (test2) {
  do_something_else();
} else {
  give_up();
}"))))


;;; Pipeline tests.

(deftest (test-pipeline/cast :long-running) ()
  (try-pipeline*
   (path-join *grist-dir* "test/cast.cc")
   (path-join *grist-dir* "test/cast_ref.cc")))

(deftest (test-pipeline/const :long-running) ()
  (try-pipeline*
   (path-join *grist-dir* "test/const.cc")
   (path-join *grist-dir* "test/const_ref.cc")))

(deftest (test-pipeline/move-or-clone :long-running) ()
  (try-pipeline*
   (path-join *grist-dir* "test/move_or_clone.cc")
   (path-join *grist-dir* "test/move_or_clone_ref.cc")))

(deftest test-pipeline/uniquify-mutable-alias ()
  (let ((cpp (from-string 'cpp "int myfun () { int pl = 1;
int& r = pl;
{
  int* p = &r; // scope S begins here. We have A_S = {r,*p}
  f(r);        // assume void f(int);
  g(*p);       // assume void g(int);
}              // scope S ends here
g(r);          // this is a different scope. It has no illegal alias nests.
}")))
    (attrs:with-attr-table cpp
      (let ((text
              (source-text
               (remove-if (of-type 'comment-ast)
                          (uniquify-mutable-aliases cpp)))))
        (is (not (string*= "*p" text)))
        (is (string*= "g(r)" (remove-if #'whitespacep text)))))))

(deftest test-pipeline/uniquify-mutable-alias/const ()
  (let ((cpp (from-string 'cpp "int myfun () {
int pl = 1;
int& r = pl;
{
  const int* p = &r; // scope S begins here. We have A_S = {r,*p}
  f(r);              // assume void f(int);
  g(*p);             // assume void g(int);
}                    // scope S ends here
g(r);                // this is a different scope. It has no illegal alias nests.
}")))
    (attrs:with-attr-table cpp
      (is (not (string*= "*p"
                         (source-text
                          (remove-if (of-type 'comment-ast)
                                     (uniquify-mutable-aliases cpp)))))))))

;;; TODO Implement this by identifying multiple mutables aliases and
;;; retaining the one with the largest scope.
(deftest test-pipeline/multiple-mutable-borrows ()
  (let ((*refactoring-pipeline* '(uniquify-mutable-aliases constify-can-const-pointers)))
    (try-pipeline*
     (path-join *grist-dir* "test/multiple_mutable_borrows.cc")
     (path-join *grist-dir* "test/multiple_mutable_borrows_ref.cc"))))

(deftest test-pipeline/generics-and-traits ()
  (try-pipeline*
   (path-join *grist-dir* "test/generics_and_traits.cc")
   (path-join *grist-dir* "test/generics_and_traits_ref.cc")))

(deftest test-pipeline/generics-and-mixed-traits ()
  (try-pipeline*
   (path-join *grist-dir* "test/generics_and_mixed_traits.cc")
   (path-join *grist-dir* "test/generics_and_mixed_traits_ref.cc")))

(deftest test-pipeline/generics-and-mixed-traits-operator-on-all ()
  (try-pipeline*
   (path-join *grist-dir* "test/generics_and_mixed_traits_operator_on_all.cc")
   (path-join *grist-dir* "test/generics_and_mixed_traits_operator_on_all_ref.cc")))

(deftest test-pipeline/generics-and-mixed-traits-operator-on-generic ()
  (try-pipeline*
   (path-join *grist-dir* "test/generics_and_mixed_traits_operator_on_template.cc")
   (path-join *grist-dir* "test/generics_and_mixed_traits_operator_on_template_ref.cc")))

(deftest test-pipeline/generics-and-mixed-traits-print ()
  (try-pipeline*
   (path-join *grist-dir* "test/generics_and_mixed_traits_print.cc")
   (path-join *grist-dir* "test/generics_and_mixed_traits_print_ref.cc")))

(deftest test-pipeline/move-or-clone-operator ()
  (try-pipeline*
   (path-join *grist-dir* "test/move_or_clone_operator.cc")
   (path-join *grist-dir* "test/move_or_clone_operator_ref.cc")))

(deftest test-pipeline/mutable-aliases ()
  (try-pipeline*
   (path-join *grist-dir* "test/mutable_aliases.cc")
   (path-join *grist-dir* "test/mutable_aliases_ref.cc")))

(deftest test-pipeline/overload-minus ()
  (try-pipeline*
   (path-join *grist-dir* "test/overload_minus.cc")
   (path-join *grist-dir* "test/overload_minus_ref.cc")))

(deftest test-pipeline/overload-minus-generic ()
  (try-pipeline*
   (path-join *grist-dir* "test/overload_minus_generic.cc")
   (path-join *grist-dir* "test/overload_minus_generic_ref.cc")))

(deftest test-pipeline/simple-generic-method ()
  (try-pipeline*
   (path-join *grist-dir* "test/simple_generic_method.cc")
   (path-join *grist-dir* "test/simple_generic_method_ref.cc")))

(deftest test-pipeline/macro-rewriting ()
  (try-pipeline*
   (path-join *grist-dir* "test/macros.cc")
   (path-join *grist-dir* "test/macros_ref.cc")
   ;; TODO Use once clang is updated to support -std=c++20.
   :compile nil))

(deftest test-pipeline/switch-rewriting ()
  (let ((*refactoring-pipeline*
          (list 'rewrite-switches)))
    (try-pipeline*
     (path-join *grist-dir* "test/switch/switch.cc")
     (path-join *grist-dir* "test/switch/switch_ref.cc"))))



(defun non-header-files (project)
  (iter (for (path . sw) in (evolve-files project))
        (unless (header-path? path)
          (collect sw))))

(deftest test-refactor-trim-front-header ()
  (let* ((raw-path (path-join *grist-dir* "trim-front-header/raw"))
         (reference-path (path-join *grist-dir* "trim-front-header/refactored"))
         (raw-project (from-file 'cpp-project raw-path))
         (reference-project (from-file 'cpp-project reference-path))
         (refactored-project
           (refactor-project raw-project
                             :refactorings
                             (remove 'modularize-project (refactoring-pipeline)
                                     :key #'type-of)))
         (files1 (sort-new (evolve-files reference-project) #'string<= :key #'car))
         (files2 (sort-new (evolve-files refactored-project) #'string<= :key #'car)))
    ;; Same files, same order.
    (is (length= files1 files2))
    (is (every (op (equal (car _) (car _))) files1 files2))
    ;; Same text.
    (iter (for (nil . sw1) in-vector files1)
          (for (nil . sw2) in-vector files2)
          (assert-source-text= sw1 sw2 :header "Reference, Result"))))
