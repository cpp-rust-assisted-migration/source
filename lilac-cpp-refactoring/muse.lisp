(defpackage :lilac-cpp/muse
  (:use
    :gt/full
    :argot/argot
    :argot/readtable
    :lsp-server
    :lsp-server/protocol
    :argot-server/utility
    :argot-server/lsp-diff
    :argot-server/muses/muse
    :argot-server/muses/code-actor
    :argot-server/muses/project-ast-muse
    :software-evolution-library
    :software-evolution-library/software/parseable
    :software-evolution-library/software/tree-sitter
    :software-evolution-library/software/cpp
    :software-evolution-library/software/cpp-project
    :lilac-cpp/refactorings
    :lilac-commons/refactoring
    :lilac-commons/commons)
  (:import-from :lilac-commons/aggressive-analysis
                :with-aggressive-analysis)
  (:import-from :lilac-cpp/refactorings
                :refactoring-pipeline)
  (:local-nicknames
   (:attrs :functional-trees/attrs)
   (:dir :software-evolution-library/software/directory)
   (:lp :lparallel)
   (:lsp :lsp-server/utilities/lsp-utilities))
  (:shadowing-import-from :argot/readtable :argot-readtable)
  (:export :lilac-cpp-muse))
(in-package :lilac-cpp/muse)
(in-readtable argot-readtable)

(defclass lilac-cpp-muse (code-actor project-ast-muse)
  ()
  (:default-initargs
   :name "LiLaC C++ Refactoring"
   :lang 'cpp))



(defun toggle-scope ()
  (clear-refactoring-cache)
  (setf *scope*
        (ecase-of scope *scope*
          (:unit :project)
          (:project :unit))))

(defconst +toggle-scope-command-name+ :toggle.scope)

(defun toggle-scope-command-title ()
  "Title for the scope toggling code action (what the user will see)."
  (ecase-of scope *scope*
    (:unit "Apply refactorings to whole project")
    (:project "Apply refactorings to compilation unit")))

(defun toggle-scope-command-message ()
  "Message to send the user after toggling scope."
  (ecase-of scope *scope*
    (:unit "Refactorings apply to compilation unit")
    (:project "Refactorings apply to whole project")))

(defun toggle-scope-command ()
  (make 'Command
        :title (toggle-scope-command-title)
        :command (string +toggle-scope-command-name+)))

(defmethod apply-command ((muse lilac-cpp-muse)
                          (cmd (eql #.+toggle-scope-command-name+))
                          arguments)
  (declare (ignore arguments))
  (toggle-scope)
  (message (toggle-scope-command-message))
  (dict))



(defmethod apply-command ((muse lilac-cpp-muse)
                          (cmd symbol)
                          arguments)
  (unless (keywordp cmd)
    (return-from apply-command (call-next-method)))
  (assert (find cmd (refactoring-pipeline) :key #'refactoring-name))
  (let ((action (convert 'CodeAction (first arguments))))
    (ematch action
      ((CodeAction
        :edit (and edit (WorkspaceEdit
                         :document-changes changes)))
       ;; Ensure all the edited buffers are open.
       (with-no-code-actions ()
         (mapc #'await-uri-buffer (edited-uris changes)))
       (make 'ApplyWorkspaceEditParams
             :|label| (string cmd)
             :|edit| edit)))))

(defmethod muse-commands append ((muse lilac-cpp-muse))
  (cons +toggle-scope-command-name+
        (mapcar #'refactoring-name (refactoring-pipeline))))

(defun lilac-lsp-command (uri command name project new-project
                          &key (kind CodeActionKind.Refactor)
                            (needs-confirmation t))
  "Return an LSP command to update PROJECT to NEW-PROJECT."
  (unless (eql project new-project)
    (let ((changes (lsp-diff-projects uri project new-project)))
      (make 'Command
            :title name
            :command (string command)
            :arguments (list
                        (make 'CodeAction
                              :title name
                              :kind (make-keyword kind)
                              :edit
                              (annotate-workspace-edit
                               name
                               (make 'WorkspaceEdit
                                     :document-changes changes)
                               :needs-confirmation
                               needs-confirmation)))))))

(defun refactoring-command (text-document refactoring)
  "If REFACTORING applies to TEXT-DOCUMENT, return an LSP command."
  (with-thread-name (:name "Refactoring C++")
    (let ((uri (slot-value text-document '|uri|))
          (name (refactoring-name refactoring))
          (description (refactoring-description refactoring)))
      (declare (string uri description)
               (keyword name))
      (mvlet* ((project (ensure-project-ast uri))
               (old (lookup-project-uri uri))
               (new-project
                (with-thread-name (:name (fmt "~a" (type-of refactoring)))
                  (attrs:with-attr-table project
                    (with-aggressive-analysis ()
                      (apply-refactoring refactoring
                                         project
                                         (get-files-in-scope old)))))))
        (lilac-lsp-command uri
                           name
                           description
                           project
                           new-project
                           :needs-confirmation
                           (needs-confirmation? refactoring)
                           :kind
                           (refactoring-kind refactoring))))))

(defmethod applicable-commands ((muse lilac-cpp-muse) &key start end text-document)
  (declare (ignore start end))          ;TODO Region refactorings?
  (nest
   (cons (toggle-scope-command))
   (unless (inhibit-code-actions?))
   (let ((uri (slot-value text-document '|uri|))))
   (when (ensure-project-ast uri))
   (filter-map (op (refactoring-command text-document _)))
   (refactoring-pipeline)))

(defmethod muse-code-action-kinds append ((muse lilac-cpp-muse))
  (nest (mapcar #'string)
        (nub)
        (mapcar #'refactoring-kind)
        (refactoring-pipeline)))
