(defpackage :lilac-cpp/test-utils
  (:use :gt/full
        :stefil+
        :lilac-cpp/refactorings
        :lilac-commons/refactoring
        :lilac-commons/aggressive-analysis
        :software-evolution-library
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/project
        :software-evolution-library/software/cpp-project
        :cmd)
  (:local-nicknames (:dir :software-evolution-library/software/directory))
  (:import-from :lilac-cpp/refactorings :wrap-static-cast)
  (:local-nicknames (:attrs :functional-trees/attrs))
  (:export
    :*grist-dir*
    :*trim-front-dir*
    :try-compile
    :calls-referencing-name
    :check-preproc-def-to-constexpr
    :check-preproc-def-to-constexpr-fails
    :check-preproc-func-to-constexpr
    :apply-refactoring-pipeline
    :refactor-raw-cpp
    :assert-source-text=
    :try-pipeline
    :try-pipeline*
    :last-use))
(in-package :lilac-cpp/test-utils)
(in-readtable :curry-compose-reader-macros)

(defparameter *grist-dir*
  (asdf:system-relative-pathname :lilac-cpp "lilac-grist/"))

(defparameter *trim-front-dir*
  (asdf:system-relative-pathname :lilac-cpp "lilac-grist/trim-front/"))

(defun last-use (ast name)
  "Return the last identifier named NAME in AST."
  (let (result)
    (iter (for node in-tree ast)
          (when (and (typep node 'identifier-ast)
                     (source-text= node name))
            (setf result node)))
    result))

(defun try-compile (file)
  (let ((path (assure absolute-pathname file)))
    (labels ((get-std-option (compiler)
               "Return what, if any, options we need to pass the compiler to
                support auto constexpr arguments."
               (when (string$= "++" compiler)
                 (with-temporary-file (:pathname p :type "cc")
                   (write-string-into-file (fmt "int main() {}~%") p)
                   (some (lambda (std)
                           (and (cmd? compiler std p "-o" "/dev/null")
                                std))
                         '(""
                           "-std=c++20"
                           "-std=c++2a -fconcepts"
                           "-std=c++2a"
                           "-std=c++17")))))
             (try-compiler (compiler)
               (let ((stderr (make-string-output-stream)))
                 (unwind-protect
                      (lret ((result
                              (cmd? compiler
                                    path
                                    "-Wl,--unresolved-symbols=ignore-in-object-files"
                                    (get-std-option compiler)
                                    "-o /dev/null"
                                    :2> stderr)))
                        (unless result
                          (print (get-output-stream-string stderr))))
                   (close stderr)))))
      (assert
        (or (resolve-executable "g++")
            (resolve-executable "clang++")))
      (and
       (if (resolve-executable "g++")
           (try-compiler "g++")
           t)
       (if (resolve-executable "clang")
           (try-compiler "clang++")
           t)))))

(defun calls-referencing-name (sw name)
  (let* ((id (find name
                   (identifiers (genome sw))
                   :test #'equal
                   :key #'source-text)))
    (collect-arg-uses sw id)))

(defun check-preproc-def-to-constexpr (source expected)
  (let ((cpp (from-string 'cpp source)))
    (attrs:with-attr-table cpp
      (let ((refactored-cpp
              (source-text (rewrite-definitions-to-constexprs cpp))))
        (is (not (string*= "#define" (source-text refactored-cpp))))
        (is (string= expected (source-text refactored-cpp)))))))

(defun check-preproc-def-to-constexpr-fails (source)
  (let ((cpp (from-string 'cpp source)))
    (attrs:with-attr-table cpp
      (let ((refactored-cpp
              (source-text (rewrite-definitions-to-constexprs cpp))))
        (is (string*= "#define" (source-text refactored-cpp)))))))

(defun check-preproc-func-to-constexpr (source expected)
  (let ((cpp (from-string 'cpp source)))
    (attrs:with-attr-table cpp
      (let ((refactored-cpp
              (source-text (rewrite-preprocs-to-constexpr-funs cpp))))
        (is (not (string*= "#define" (source-text refactored-cpp))))
        (is (string= expected (source-text refactored-cpp)))))))


;;; Pipeline tests.

(defun apply-refactoring-pipeline (source pipeline &key verbose)
  (let* ((source
           (etypecase source
             (string source)
             (pathname (read-file-into-string source))
             (software (source-text source))))
         (project (from-string 'cpp-project source))
         (root (assure cpp-translation-unit
                 (first
                  (children
                   (find-if (of-type 'dir:file-ast)
                            project)))))
         (root-path
           (assure (not null)
             (ast-path project root))))
    (labels ((rec (project pipeline)
               (if (null pipeline) project
                   (let ((fn (car pipeline)))
                     (when verbose
                       (format *error-output* "~&Running ~a" fn))
                     (let* ((new-project (assure project
                                           (attrs:with-attr-table project
                                             (funcall fn
                                                      (assure ast
                                                        (lookup project root-path))))))
                            (new
                              (assure ast
                                (lookup new-project root-path))))
                       (if (eql new (lookup project root-path))
                           (rec project (rest pipeline))
                           (let ((new-project (with project root-path new)))
                             ;; (is (single (changed-evolve-files project new-project)))
                             (is (not (eql project new-project)))
                             (rec new-project (cdr pipeline)))))))))
      (source-text (lookup (rec project pipeline) root-path)))))

(defun refactor-raw-cpp (source &key verbose (pipeline *refactoring-pipeline*))
  (apply-refactoring-pipeline
   source
   pipeline
   :verbose verbose))

(defun assert-source-text= (x y &key (header ""))
  (flet ((normalize-whitespace (s)
           (string-join (remove-if
                         (lambda (line)
                           (or (blankp line)
                               (scan "^\\s+//" line)))
                         (lines (chomp s)))
                        #\Newline)))
    (let ((s1 (normalize-whitespace (source-text x)))
          (s2 (normalize-whitespace (source-text y))))
      (is (equal s1 s2)
          "~a"
          (string+ header
                   #\Newline
                   ($cmd "diff -y -W 200"
                         (psub-echo s1) (psub-echo s2)
                         :ignore-error-status t))))))

(defgeneric try-pipeline (source reference)
  (:documentation
   "Run SOURCE through the refactoring pipeline and assert that it
matches REFERENCE, reporting failures with a diff.")
  (:method-combination standard/context)
  (:method :context (source reference)
    (with-aggressive-analysis ()
      (call-next-method)))
  (:method ((source pathname) (reference string))
    (let ((auto (refactor-raw-cpp source)))
      (try-pipeline auto reference)))
  (:method ((source string) (reference string))
    (assert-source-text= reference source :header "Reference, Result"))
  (:method ((source pathname) (reference pathname))
    (try-pipeline source (read-file-into-string reference)))
  (:method ((source string) (reference pathname))
    (try-pipeline source (read-file-into-string reference))))

(defun try-pipeline* (source reference &key (compile t) modularize)
  (check-type reference pathname)
  (check-type source pathname)
  (assert (file-exists-p source))
  (assert (file-exists-p reference))
  (when compile
    (is (try-compile reference)))
  (let ((*refactoring-pipeline*
          (if modularize
              *refactoring-pipeline*
              (remove 'modularize-project *refactoring-pipeline*))))
    (is (try-pipeline source reference))))
