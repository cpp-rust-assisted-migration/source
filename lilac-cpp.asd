(defsystem "lilac-cpp"
  :name "LiLaC C++ refactoring muse"
  :pathname "lilac-cpp-refactoring"
  :author "GrammaTech"
  :license "MIT"
  :description "C++ refactoring muse for use with Mnemosyne"
  :class :package-inferred-system
  :depends-on ("lilac-cpp/muse")
  :in-order-to ((test-op (load-op "lilac-cpp/test")))
  :perform (test-op (o c) (symbol-call :lilac-cpp/test '#:run-batch)))

(register-system-packages "lsp-server" '(:lsp-server/protocol))
(register-system-packages "lsp-server" '(:lsp-server/protocol-util))
(register-system-packages "lsp-server" '(:lsp-server/lsp-server))
(register-system-packages "lsp-server" '(:lsp-server/logger))
(register-system-packages "lsp-server" '(:lsp-server/utilities/lsp-utilities))
(register-system-packages "trivia" '(:trivia.fail))
