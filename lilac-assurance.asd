(defsystem "lilac-assurance"
  :name "C++ assurance muse"
  :pathname "lilac-assurance"
  :author "GrammaTech"
  :license "MIT"
  :description "Muse that generates assurance for refactorings"
  :class :package-inferred-system
  :depends-on ("lilac-assurance/muse")
  :in-order-to ((test-op (load-op "lilac-assurance/test")))
  :perform (test-op (o c) (symbol-call :lilac-assurance/test '#:run-batch)))

(register-system-packages "lsp-server" '(:lsp-server/protocol))
(register-system-packages "lsp-server" '(:lsp-server/protocol-util))
(register-system-packages "lsp-server" '(:lsp-server/lsp-server))
(register-system-packages "lsp-server" '(:lsp-server/logger))
