(defpackage :lilac-commons/aggressive-analysis
  (:use :gt/full)
  (:local-nicknames
   (:ast :software-evolution-library/software/parseable)
   (:attrs :functional-trees/attrs)
   (:c/cpp-project :software-evolution-library/software/c-cpp-project)
   (:dbg :software-evolution-library/utility/debug)
   (:dir :software-evolution-library/software/directory)
   (:project :software-evolution-library/software/project)
   (:sel :software-evolution-library)
   (:tg :trivial-garbage)
   (:ts :software-evolution-library/software/tree-sitter))
  (:export
    :with-aggressive-analysis
    :*aggressive-analysis-flag*))
(in-package :lilac-commons/aggressive-analysis)

(defvar *aggressive-analysis-flag* t
  "Flag to control whether analysis is actually aggressive.")

(defun call/aggressive-analysis (fn &key more-restarts)
  (if (not *aggressive-analysis-flag*) (funcall fn)
      (let ((errors (queue)))
        (flet ((collect-error (e) (enq e errors)))
          (handler-bind ((c/cpp-project:include-conflict-error
                           (lambda (e)
                             (collect-error e)
                             (invoke-restart 'ignore)))
                         (ts:unqualifiable-ast-error
                           (lambda (e)
                             (collect-error e)
                             (continue e)))
                         (ts:unresolved-overloads-error
                           (lambda (e)
                             (collect-error e)
                             (continue e)))
                         (ts:no-enclosing-declaration-error
                           (lambda (e)
                             (collect-error e)
                             (continue e)))
                         (error
                           (lambda (e)
                             (when-let (r
                                        (or (find-restart 'ts:infer-type-as-nil)
                                            (some #'find-restart more-restarts)))
                               (collect-error e)
                               (invoke-restart r)))))
            (values-list (append1 (multiple-value-list (funcall fn))
                                  (qlist errors))))))))

(defmacro with-aggressive-analysis ((&rest kwargs &key &allow-other-keys)
                                    &body body)
  "Run BODY, collecting and handling analysis errors and returning
them as a value, after any values from BODY."
  (with-thunk (body)
    `(call/aggressive-analysis ,body ,@kwargs)))
