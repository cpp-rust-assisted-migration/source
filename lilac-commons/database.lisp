(defpackage :lilac-commons/database
  (:documentation "Helper common to lenses and translations.")
  (:use :gt/full)
  (:local-nicknames (:ns :in-nomine))
  (:export
    :database
    :empty-database
    :upsert
    :upsertf
    :define-table
    :compile-query
    :get-column-values
    :project-results
    :select-results
    :dump-table
    :dump-database
    :ensure-tables))
(in-package :lilac-commons/database)


;;; Globals

(defconst +max-columns+ 20
  "Max number of columns for a table.")


;;; Methods

;;; TODO Move to gt/fset.
(defmethod union ((r1 list-relation) (r2 list-relation) &key key test test-not)
  (declare (ignore key test test-not))
  (fset:union r1 r2))

;;; TODO Move to FSet.
(defmethod fset:union ((r1 list-relation) (r2 list-relation) &key)
  (assert (eql (fset:arity r1)
               (fset:arity r2)))
  (do-list-relation (tuple r2 r1)
    (setf r1 (with r1 tuple))))


;;; Columns

(defstruct-read-only column
  "Specification for a column of a table."
  (name :type keyword)
  (offset :type array-index)
  (type :type t)
  (predicate :type function))


;;; Tables

(defclass table-definition ()
  ((name :type symbol :initarg :name :reader table-name)
   (arity :initarg :arity :reader arity)
   (columns :initarg :columns
            :reader columns
            :type hash-table))
  (:default-initargs
   :name (required-argument :name)
   :arity (required-argument :arity))
  (:documentation "Definition of a table.
Specifies how to relate the columns of the table to the tuples of the
relation. Completely independent from the relation's storage.

NOte that table definitions occupy a global namespace."))

(ns:define-namespace table-definition table-definition)

(defmethod print-object ((self table-definition) stream)
  (print-unreadable-object (self stream :type t)
    (format stream "~a" (table-name self))))

(-> sorted-columns (table-definition) (values vector &optional))
(defun sorted-columns (tdef)
  "Return the columns specified by TDEF as a vector."
  (sort-new (hash-table-values (columns tdef))
            #'<
            :key #'column-offset))

(defmacro define-table (name &body (columns &rest options))
  "Define a relation named NAME.
ARGS define the relation's columns.

For example, to define a `widget' table with two columns, one
for brand and one for inventory count:

    (define-table widget
      ((:brand :type string)
       (:count :type (integer 0))))"
  (when-let (docs (assocadr :documentation options))
    (setf (documentation name 'table-definition) docs))
  (assert (length<= columns +max-columns+))
  (multiple-value-bind (column-names column-types)
      (iter (for arg in columns)
            (for (column-name . kwargs) = (ensure-list arg))
            (collect column-name into column-names)
            (destructuring-bind (&key (type t)) kwargs
              (collect type into column-types))
            (finally (return (values column-names column-types))))
    (assert (length= column-names column-types))
    (assert (every #'keywordp column-names))
    `(setf (symbol-table-definition ',name)
           (make 'table-definition
                 :name ',name
                 :arity ,(length column-names)
                 :columns
                 (dict 'eql
                       ,@(iter (for name in column-names)
                               (for type in column-types)
                               (for offset from 0)
                               (appending
                                `(',name (make-column :name ,name
                                                      :offset ,offset
                                                      :type ',type
                                                      :predicate (of-type ',type))))))))))

(defun find-table-definition (table)
  "Find the definition of TABLE."
  (etypecase table
    (table-definition table)
    (symbol (symbol-table-definition table))))

(defun get-column (name column-name)
  "Get the definition of COLUMN-NAME in NAME."
  (or (gethash column-name (columns (find-table-definition name)))
      (error "No such column as ~a in ~a" column-name name)))

(-> get-column-values (symbol
                       list &rest keyword)
  list)
(defun get-column-values (name tuple &rest names)
  "Project TUPLE to eliminate all columns except those in NAMES."
  (let* ((tdef (find-table-definition name))
         (columns (mapcar (op (get-column tdef _)) names))
         (vtuple (make-array +max-columns+)))
    (declare (dynamic-extent vtuple))
    (replace vtuple tuple)
    (with-collectors (collecting)
      (dolist (c columns)
        (collecting (aref vtuple (column-offset c)))))))

(defun project-results (name results &rest names)
  "Project RESULTS to eliminate all columns except those in NAMES."
  (mapcar (op (apply #'get-column-values name _ names))
          results))

(defun select-results (db table-name columns &rest plist &key &allow-other-keys)
  "Filter tuples in TABLE-NAME by PLIST, and project on COLUMNS.
For each key-value pair in PLIST, only rows where the column named by
the key has the provided value will be returned.

The result will be projected to eliminate all columns except those
named in COLUMNS. If COLUMNS is t, nil, or `*', then all columns are
returned."
  (let ((results (apply #'query-table db table-name plist)))
    (etypecase columns
      ((member nil t (eql *)) results)
      (list (apply #'project-results table-name results columns)))))

(defsubst take-as-list (n vector)
  "Take N elements from the start of VECTOR, as a list."
  (loop for i below n
        collect (aref vector i)))

(defun compile-query (table-name plist)
  "Compile PLIST into a pattern and metapattern for `fset:query'."
  (let* ((tdef (find-table-definition table-name))
         (arity (arity tdef))
         (pattern (make-array +max-columns+ :initial-element t))
         (metapattern (make-array +max-columns+ :initial-element nil)))
    (declare (dynamic-extent pattern metapattern))
    (doplist (column-name value plist)
      (let* ((column (get-column tdef column-name))
             (offset (column-offset column)))
        (check-column-value table-name column value)
        (setf (aref pattern offset) value
              (aref metapattern offset) t)))
    (values (take-as-list arity pattern)
            (take-as-list arity metapattern))))

(defun table-exists-p (db table-name)
  "Does DB have a table named NAME?"
  (lookup db table-name))

(defun ensure-tables (db &rest table-names)
  (reduce #'ensure-table table-names :initial-value db))

(defun ensure-table (db table-name)
  (if (lookup db table-name) db
      (with db
            table-name
            (empty-list-relation (arity (find-table-definition table-name))))))

(defun query-table (db name &rest plist &key &allow-other-keys)
  "Query TABLE-NAME in DB using PLIST."
  (let ((relation (get-relation db name)))
    (multiple-value-bind (pattern metapattern)
        (compile-query name plist)
      (convert 'list (query relation pattern metapattern)))))

(defun upsert-tuple (db table-name tuple)
  "Update/insert TUPLE in TABLE-NAME in DB, returning a new database."
  (with db
        table-name
        (with (lookup (ensure-table db table-name) table-name)
              tuple)))

(defun check-column-value (table-name column value)
  (unless (funcall (column-predicate column) value)
    (error "Value ~a for column ~a in relation ~a should be of type ~a"
           value (column-name column)
           table-name
           (column-type column))))

(defun upsert (db table-name &rest plist &key &allow-other-keys)
  "Update/insert PLIST in DB, returning a new database."
  (let* ((tdef (find-table-definition table-name))
         (arity (arity tdef))
         (vtuple (make-array +max-columns+ :initial-element 'unknown)))
    (declare (dynamic-extent vtuple))
    (doplist (k v plist)
      (let ((c (get-column tdef k)))
        (check-column-value table-name c v)
        (setf (aref vtuple (column-offset c)) v)))
    ;; TODO Allow for true upsert.
    (when (loop for i below arity
                thereis (eql 'unknown (aref vtuple i)))
      (error "Missing keys for ~a in ~a" table-name plist))
    (let ((tuple (take-as-list arity vtuple)))
      (upsert-tuple db table-name tuple))))

(define-modify-macro upsertf (table-name &rest plist) upsert)


;;; Database

(defclass database ()
  ((relations :type fset:map
              :initform (empty-map)
              :initarg :relations
              :reader database-relations))
  (:documentation "A database, that is, a map from names to relations."))

(defun empty-database ()
  "Return an empty database."
  (make 'database))

(defmethod with ((db database) (name symbol) &optional relation)
  "Return DB with a new relation named NAME."
  (assert (typep relation 'relation))
  (make 'database
        :relations (with (database-relations db) name relation)))

(defmethod less ((db database) (name symbol) &optional value2)
  "Drop table NAME from DB."
  (declare (ignore value2))
  (make 'database
        :relations (less (database-relations db) name)))

(defmethod lookup ((db database) (name symbol))
  "Find table NAME in DB."
  (lookup (database-relations db) name))

(defun get-relation (db name)
  "Return the relation named NAME in DB, or signal an error."
  (or (lookup db name)
      (error "No such relation as ~a in ~a" name db)))

(defun dump-table (db table)
  "Dump TABLE as a list of tuples."
  (when-let ((rel (lookup db table)))
    (let* ((column-names
             (map 'list #'column-name
                  (sorted-columns
                   (find-table-definition table)))))
      (with-collectors (collecting)
        (do-list-relation (tuple rel)
          (collecting
            (mappend #'list column-names tuple)))))))

(defun dump-database (db)
  "Dump DB as an alist from relations to tuples."
  (with-collectors (collecting)
    (dolist (rel (convert 'list (domain (database-relations db))))
      (collecting (cons rel (dump-table db rel))))))
