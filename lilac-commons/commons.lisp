(defpackage :lilac-commons/commons
  (:use :gt/full
        :software-evolution-library
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/cpp
        :software-evolution-library/software/c-cpp-project
        :software-evolution-library/software/cpp-project
        :software-evolution-library/software/rust
        :software-evolution-library/software/rust-project)
  (:import-from :atomics)
  (:import-from :mustache)
  (:local-nicknames
   (:dir :software-evolution-library/software/directory)
   (:file :software-evolution-library/components/file)
   (:project :software-evolution-library/software/project))
  (:export :const-declaration?
           :project+file
           :infer-type*
           :strip-surrounding-text
           :get-compilation-unit
           :header-path?
           :translated-path
           :generate-cargo.toml
           :guess-project-name
           :module-path?
           :get-compilation-units
           :*scope*
           :scope
           :get-files-in-scope
           :with-no-code-actions
           :inhibit-code-actions?))
(in-package :lilac-commons/commons)

(defvar *inhibit-code-actions* (cons 0 nil)
  "Count of tasks in flight that should inhibit computing code actions.
Represented as a cons for use with atomic operations.")

(defun inhibit-code-actions? ()
  (> (car *inhibit-code-actions*) 0))

(defmacro with-no-code-actions ((&key) &body body)
  (with-thunk (body)
    `(call/no-code-actions ,body)))

(defun call/no-code-actions (fn)
  (atomics:atomic-incf (car *inhibit-code-actions*))
  (unwind-protect
       (funcall fn)
    (atomics:atomic-decf (car *inhibit-code-actions*))))

(deftype scope ()
  '(member :unit :project))
(declaim (type scope *scope*))
(defvar *scope* :project
  "Variable to control whether we refactor compilation units or whole projects.")

(defun strip-surrounding-text (ast)
  "Strip the before and after text from AST."
  (if (not (typep ast 'structured-text)) ast
      (if (and (emptyp (before-text ast))
               (emptyp (after-text ast)))
          ast
          (copy ast :before-text nil :after-text nil))))

(defun const-declaration? (decl)
  "Is AST a const declaration? This returns T for pointers to const but
not for const pointers to non-const."
  (or
   ;; Enum variants can't be mutated.
   (typep decl 'cpp-enumerator)
   (member "const"
           (specifier
            (canonicalize-type decl :software (attrs-root*)))
           :test #'source-text=)
   (match decl
     ((cpp-function-definition
       (cpp-declarator
        (cpp-function-declarator
         (children (list (cpp-type-qualifier (text "const")))))))
      t)
     ((cpp-declaration
       (cpp-declarator
        (cpp-function-declarator
         (children (list (cpp-type-qualifier (text "const")))))))
      t))))

(defun project+file (string)
  "Parse STRING as a file embedded in a project."
  (let* ((project (sel:from-string 'cpp-project string))
         (file
           (only-elt
            (children
             (find-if (of-type 'dir:file-ast) (genome project))))))
    (values project file)))

(defun infer-type* (arg)
  "Infer the type of ARG, handling CRAM-specific annotations."
  ;; TODO How to handle this in a more principled way?
  (match arg
    ((or
      ;; TODO ref and cref aren't CRAM-specific.
      (cpp* "std::ref($1)" arg)
      (cpp* "std::cref($1)" arg)
      (cpp* "__CRAM__REF($1)" arg)
      (cpp* "__CRAM__CREF($1)" arg)
      (cpp* "__CRAM__MOVE($1)" arg)
      (cpp* "__CRAM__CLONE($1)" arg)
      (cpp* "__CRAM__DEREF($1)" arg))
     (infer-type* arg))
    (otherwise (infer-type arg))))

(defun get-compilation-unit (software &key (project (attrs-root*)))
  "Get all the project files in SOFTWARE's compilation unit."
  (let ((root (attrs-root*)))
    (cons software
          (~>> (file-dependency-tree project software)
               (flatten)
               (filter #'stringp)
               (filter-map (op (lookup root _)))
               (mapcar #'genome)
               (mapcar (op (find-if (of-type 'cpp-translation-unit) _)))
               ;; Get the software objects for the ASTs.
               (mapcar (op (cdr (rassoc _ (dir:evolve-files root) :key #'genome))))))))

(defun get-compilation-units (&key (project (attrs-root*)))
  (let ((units
          (remove-if #'header-path?
                     (dir:evolve-files project)
                     :key #'car)))
    (nub (mappend #'get-compilation-unit
                  (mapcar #'cdr units)))))

(defun get-files-in-scope (sw)
  (ecase-of scope *scope*
    (:unit (get-compilation-unit sw))
    (:project (get-compilation-units))))

(defconst +header-path-extensions+ '("h" "hh" "hpp" "hxx")
  "Possible C++ header extensions.")

(defconst +cpp-module-extensions+ '("ixx" "cppm" "ccm" "cxxm" "c++m")
  "Possible C++ header extensions.")

(defun header-path? (path)
  "Return T if PATH is a path with a header extension."
  (typecase path
    (pathname
     (string-case (pathname-type path)
       (#.+header-path-extensions+
        t)))
    (string
     (some (op (string$= _ path))
           +header-path-extensions+))))

(defun module-path? (path)
  "Is PATH a module path (does it have a module extension)?"
  (when (typep path '(or pathname string))
    (member (pathname-type path)
            +cpp-module-extensions+
            :test #'equal)))

(defun translated-path (path &key (header-suffix "_header"))
  "Return the name for the Rust translation of PATH.

If PATH is a header, HEADER-SUFFIX is appended to its translated
name.

Rust files are emitted in a rust/src subdirectory. Module partitions
get a further directory prefix for the enclosing module."
  (labels ((split-partition-file-name (path)
             "Split the file name of PATH, using the convention of separating
              the partition name from the module name with a dash."
             (and (module-path? path)
                  (when-let* ((name (pathname-name path))
                              (pos (position #\- name)))
                    (values (subseq name 0 pos)
                            (subseq name (1+ pos)))))))
    (mvlet* ((path (pathname path))
             (partition-dir partition-name
              (split-partition-file-name path))
             (dir-suffix
              `("rust" "src" ,@(and partition-dir (list partition-dir)))))
      (make-pathname
       :directory
       (let ((dir (pathname-directory path)))
         (if (no dir)
             `(:relative ,@dir-suffix)
             (append dir dir-suffix)))
       :name
       (let ((name (or partition-name
                       (pathname-name path))))
         (if (header-path? path)
             (string+ name header-suffix)
             name))
       :type "rs"
       :defaults path))))

(def cargo.toml-template
  (mustache:compile-template
   (asdf:system-relative-pathname
    :lilac-rust
    "lilac-rust/Cargo.toml.template"))
  "Template for generating a Cargo.toml file.")

(-> sanitize-project-name (string) string)
(defun sanitize-project-name (name)
  (substitute-if #\_
                 (lambda (c)
                   (nor (alphanumericp c)
                        (find c "-_")))
                 name))

(-> generate-cargo.toml (string &key (:entry-point (or string null)))
    string)
(defun generate-cargo.toml (project-name &key entry-point)
  "Generate a Cargo.toml name embedding PROJECT_NAME."
  (let ((project-name (sanitize-project-name project-name)))
    (with-output-to-string (s)
      (funcall cargo.toml-template
               `((:name . ,project-name)
                 (:entry-point . ,(drop-prefix "rust/" entry-point)))
               s))))

(-> guess-project-name (project:project) string)
(defun guess-project-name (project)
  "Infer the name of PROJECT from its directory."
  (or (lastcar (pathname-directory (project:project-dir project)))
      "Unknown"))
