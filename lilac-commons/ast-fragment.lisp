(defpackage :lilac-commons/ast-fragment
  (:use :gt/full
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter)
  (:export :ast-fragment
           :fragment-children))
(in-package :lilac-commons/ast-fragment)

(defclass ast-fragment (functional-tree-ast)
  ((child-slots
    :initform '((fragment-children . 0))
    :allocation :class)
   (original
    :initarg :original
    :type (or null ast)
    :reader ast-fragment-original)
   (fragment-children
    :initarg :children
    :accessor children))
  (:default-initargs
   :original nil
   :children nil))

(defgeneric fragment-children (ast)
  (:method ((ast ast-fragment))
    (children ast))
  (:method ((list list))
    (let ((q (queue)))
      (dolist (item list (qlist q))
        ;; Can happen with conflict ASTs.
        (if (and (listp item)
                 (not (proper-list-p item)))
            (enq item q)
            (qconc q (fragment-children item))))))
  (:method ((ast ast))
    (list ast)))

(defmethod initialize-instance :after ((self ast-fragment) &key children)
  (setf (children self)
        (fragment-children children)))

(defmethod (setf children) :around ((value t) (self ast-fragment))
  (call-next-method (fragment-children value) self))

(defgeneric ast-fragment (ast)
  (:documentation "Convert AST into an AST fragment.")
  (:method ((ast root-ast))
    (make 'ast-fragment
          :children (children ast)
          :original ast))
  (:method ((ast compound-ast))
    (make 'ast-fragment
          :children (children ast)
          :original ast))
  (:method ((ast ast))
    (make 'ast-fragment
          :children (list ast)
          :original ast))
  (:method ((asts list))
    (make 'ast-fragment
          :children asts)))

;;; TODO This is the wrong semantics.
(defmethod source-text ((fragment ast-fragment) &rest args &key)
  (mapc (lambda (child)
          (apply #'source-text child args))
        (fragment-children fragment)))

(defmethod with :around ((root tree-sitter-ast)
                         (path t)
                         &optional value)
  (if (typep value 'ast-fragment)
      (let ((children (reverse (children value))))
        (if children
            (reduce (lambda (root child)
                      (insert root path child))
                    (rest children)
                    :initial-value (with root path (first children)))
            root))
      (call-next-method)))

(defmethod initialize-instance :around ((self tree-sitter-ast)
                                        &rest args &key children)
  (cond ((typep children 'ast-fragment)
         (apply #'call-next-method self
                :children (children children)
                args))
        ((typep children 'list)
         (apply #'call-next-method self
                :children
                (if (some (of-type 'ast-fragment) children)
                    (mappend #'fragment-children children)
                    children)
                args))
        (t (call-next-method))))
