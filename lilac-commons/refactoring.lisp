(defpackage :lilac-commons/refactoring
  (:use :gt/full
        :argot-server/lsp-diff
        :argot/argot
        :argot/readtable
        :lsp-server
        :lsp-server/protocol
        :argot-server/muses/project-ast-muse
        :argot-server/utility
        :software-evolution-library
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :lilac-commons/commons
        :functional-trees/attrs)
  (:shadowing-import-from :argot/readtable :argot-readtable)
  (:local-nicknames
   (:db :lilac-commons/database)
   (:fset :fset)
   (:project :software-evolution-library/software/project)
   (:sel :software-evolution-library))
  (:export
    :refactoring
    :change-table
    :apply-changes
    :apply-refactoring
    :changed-evolve-files
    :*root*
    :*db*
    :with-object-or-ast
    :apply-refactoring*
    :annotation-preprocs
    :add-preproc-list
    :sort-refactorings
    :one-shot-refactoring
    :refactor-target
    :two-phase-refactoring
    :refactoring-description
    :refactoring-name
    :refactoring-constraints
    :add-preprocs
    :annotation-refactoring
    :lsp-diff-projects
    :edited-uris
    :project-relative-uri
    :*use-refactoring-cache*
    :*refactoring-cache*
    :clear-refactoring-cache
    :needs-confirmation?
    :refactoring-kind))
(in-package :lilac-commons/refactoring)
(in-readtable argot-readtable)

(defvar-unbound *root* "The current object being refactored.")

(defvar *db* (db:empty-database)
  "The current database.")

(defvar *use-refactoring-cache* t)

(defvar *refactoring-cache*
  (tg:make-weak-hash-table :weakness-matters t)
  "Cache for refactorings.
Editors may request that refactorings be recomputed much more often
than the project actually changes.")

(defvar *cache-hit-count* 0)
(defvar *cache-miss-count* 0)

(define-reset-hook clear-refactoring-cache ()
  (synchronized (*refactoring-cache*)
    (clrhash *refactoring-cache*)
    (setf *cache-hit-count* 0)
    (setf *cache-miss-count* 0)))

(defun ensure-cached (fn root refactoring targets)
  (declare (function fn)
           ((or software ast) root)
           ((or symbol class refactoring) refactoring)
           (list targets))
  (nest
   (if (not *use-refactoring-cache*)
       (funcall fn))
   (let* ((refactoring
            (etypecase refactoring
              (symbol refactoring)
              (class (class-name refactoring))
              (refactoring (type-of refactoring))))
          (cache-key (cons refactoring targets)))
     (declare (symbol refactoring)))
   (symbol-macrolet ((cache (gethash root *refactoring-cache* (dict)))))
   (or
    ;; Note the result might be absent, or it might already be
    ;; garbage-collected.
    (synchronized (*refactoring-cache*)
      (when-let (result (href cache cache-key))
        (incf *cache-hit-count*)
        (if (eql result :same) root
            (tg:weak-pointer-value result))))
    (lret ((result
            (with-thread-name (:name "Recomputing")
              (funcall fn))))
      (synchronized (*refactoring-cache*)
        (incf *cache-miss-count*)
        (setf cache
              (dict* cache
                     cache-key
                     (if (eql result root) :same
                         (tg:make-weak-pointer result)))))))))

(defun call/object-or-ast (obj fn)
  "Helper function for `with-object-or-ast'."
  (labels ((rec (obj)
             (etypecase obj
               (software
                (let ((genome (genome obj))
                      (new-genome (rec (genome obj))))
                  (if (eql genome new-genome)
                      obj
                      (copy obj :genome new-genome))))
               (ast
                (let ((*root* obj))
                  (funcall fn obj))))))
    (rec obj)))

(defmacro with-object-or-ast ((obj) &body body)
  "Handle OBJ as an AST, even if it's a software object."
  (with-thunk (body obj)
    `(call/object-or-ast ,obj ,body)))

(defclass refactoring ()
  ((description
    :reader refactoring-description
    :type string
    :initarg :description
    :documentation "Description for muse to present.")
   (name
    :reader refactoring-name
    :type keyword
    :initarg :name
    :documentation "Refactoring command name for LSP.")
   (before
    :initarg :before
    :reader refactoring-before
    :documentation "Refactorings this refactoring should precede.")
   (after
    :initarg :after
    :reader refactoring-after
    :documentation "Refactorings this refactoring should follow.")
   (needs-confirmation
    :initarg :needs-confirmation
    :reader needs-confirmation?
    :reader refactoring-needs-confirmation?
    :type boolean
    :documentation "Whether changes sent to the editor need confirming.")
   (kind
    :initarg :kind
    :reader refactoring-kind
    ;;  Code action kinds are defined as an open set.
    :type (or string keyword)
    :documentation "The code action kind."))
  (:default-initargs
   :before nil
   :after nil
   :needs-confirmation t
   :kind CodeActionKind.RefactorRewrite)
  (:documentation "A single user-facing refactoring."))

(defgeneric refactoring-constraints (refactoring)
  (:documentation "Topological constraints for sorting available refactorings.")
  (:method ((self refactoring))
    (let ((class-name (type-of self)))
      (append
       ;; The refactorings this refactoring should come before.
       (mapcar (op (list class-name _))
               (refactoring-before self))
       ;; The refactorings this refactoring should come after.
       (mapcar (op (list _ class-name))
               (refactoring-after self))))))

(defclass two-phase-refactoring (refactoring)
  ()
  (:documentation "A refactoring where changes are applied in a separate phase."))

(defclass one-shot-refactoring (refactoring)
  ()
  (:documentation "A refactoring without separate phases."))

(defmethod convert ((to (eql 'refactoring))
                    (x refactoring) &key)
  x)

(defmethod convert ((to (eql 'refactoring))
                    (x symbol) &key)
  (if (subtypep x 'refactoring)
      (make x)
      (call-next-method)))

(defmethod convert ((to (eql 'refactoring))
                    (x class) &key)
  (if (subtypep x 'refactoring)
      (make x)
      (call-next-method)))

(defun sort-refactorings (refactorings)
  "Do a topological sort over REFACTORINGS."
  (let* ((refactorings (mapcar (op (convert 'refactoring _)) refactorings))
         (constraints (mappend #'refactoring-constraints refactorings)))
    (stable-sort-new refactorings (toposort constraints))))

(defgeneric change-table (refactoring project target)
  (:documentation "Return a change table for REFACTORING on TARGET.
A change table is a hash table where nodes are mapped to either a new
node to replace them, or the keyword `:remove' if they should be
removed from the tree.

Change tables can also be FSet maps or alists.")
  (:method-combination standard/context)
  (:method :context ((self two-phase-refactoring) (project software) (target ast))
    (with-attr-session (project :inherit t :shadow nil)
      (call-next-method)))
  (:method ((self one-shot-refactoring) (project software) (target ast))
    (load-time-value (make-hash-table) t)))

(defgeneric refactor-target (refactoring project target)
  (:documentation "Perform a one-shot refactoring of TARGET.
If TARGET does not need refactoring, methods may return nil instead of
TARGET.")
  (:method-combination standard/context)
  (:method :context (r p target)
    (declare (ignore r p))
    (or (call-next-method)
        target)))

(defgeneric apply-changes (self project target change-table)
  (:documentation "Apply the changes in CHANGE-TABLE to TARGET, returning the new
version of TARGET.")
  (:method ((self one-shot-refactoring) (project software) (target ast) (table t))
    (with-attr-session (project :inherit t :shadow nil)
      (refactor-target self project target)))
  (:method ((self two-phase-refactoring) (project software) (target ast) (table function))
    (fbind (table
            (distinct? (distinct)))
      (do-tree (node target :rebuild t)
        (let ((result (table node)))
          (cond ((eql result :remove) nil)
                ((and result
                      ;; If the node has been wrapped, we don't want
                      ;; to recurse on it.
                      (distinct? result))
                 (copy-with-surrounding-text result node))
                (t node))))))
  (:method ((self two-phase-refactoring) (project software) (target ast) (table hash-table))
    (if (zerop (hash-table-count table)) target
        (apply-changes self project target (hash-table-function table :read-only t))))
  (:method ((self two-phase-refactoring) (project software) (target ast) (table fset:map))
    (if (zerop (fset:size table)) target
        (apply-changes self project target (op (lookup table _)))))
  (:method ((self two-phase-refactoring) (project software) (target ast) (table list))
    (if (emptyp table) target
        (apply-changes self project target (alist-hash-table table)))))

(defun apply-refactoring* (refactoring target/s)
  (apply-refactoring refactoring
                     (attrs-root*)
                     (ensure-list target/s)))

(define-compiler-macro apply-refactoring* (&whole call refactoring target/s)
  (match refactoring
    ((list 'quote (and sym (type symbol)))
     `(apply-refactoring* (find-class ',sym) ,target/s))
    (otherwise call)))

(defun update-project/alist (project alist)
  "Update PROJECT to replace the cars of ALIST with the cdrs."
  (if (no alist) project
      (let ((root (convert 'node project))
            (target-paths
              (mapcar (op (ast-path project (car _)))
                      alist))
            (result project))
        (iter (for path in target-paths)
              (for (target . new-target) in alist)
              (assert (or (eql target root)
                          (not (null path))))
              (unless (eql target new-target)
                (withf result path new-target)))
        result)))

(defgeneric apply-refactoring (refactoring project targets)
  (:method-combination standard/context)
  (:method :context (class project targets)
    (ensure-cached (lambda () (call-next-method))
                   project
                   class
                   targets))
  (:method (class project targets)
    (apply-refactoring (convert 'refactoring class) project targets))
  (:method ((self one-shot-refactoring) project (targets list))
    (let* ((targets (mapcar (op (convert 'node _)) targets))
           (new-targets
             (mapcar (lambda (*root*)
                       (refactor-target self project *root*))
                     targets)))
      (update-project/alist project (pairlis targets new-targets))))
  (:method ((self two-phase-refactoring) project (targets list))
    (let* ((targets (mapcar (op (convert 'node _)) targets))
           (change-tables
             (mapcar (lambda (*root*)
                       (change-table self project *root*))
                     targets))
           (new-targets
             (mapcar (lambda (*root* table)
                       (apply-changes self project *root* table))
                     targets
                     change-tables)))
      (update-project/alist project (pairlis targets new-targets)))))

(defun relevant-files (project)
  "The relevant files of PROJECT (to check for changes).
Currently the evolve files and the Cargo.toml file, if there is one."
  (append1 (project:evolve-files project)
           (assoc "rust/Cargo.toml"
                  (project:other-files project)
                  :test #'equal)))

(defun changed-evolve-files (project1 project2)
  "Return a list of changed files between PROJECT1 and PROJECT2.
Return three-element lists: the file path, the original version, and
the modified version.

Versions can be null if the file was removed or added."
  (let* ((files1 (relevant-files project1))
         (files2 (relevant-files project2))
         (ht1 (alist-hash-table files1 :test 'equal))
         (ht2 (alist-hash-table files2 :test 'equal)))
    (unless (eql files1 files2)
      (with-collectors (collect*)
        ;; Collect removed and changed files.
        (iter (for (k . v1) in files1)
              (let ((v2 (href ht2 k)))
                (unless (eql v1 v2)
                  (collect* (list k v1 v2)))))
        ;; Collect new files.
        (iter (for (k . v2) in files2)
              (let ((v1 (href ht1 k)))
                (unless v1
                  (collect* (list k nil v2)))))))))

(defun edited-uris (edits)
  "Return edited URIs (less URIs being created)."
  (iter (for edit in edits)
        (match edit
          ((TextDocumentEdit :text-document (TextDocumentIdentifier :uri uri))
           (collect uri into edited))
          ((CreateFile :uri uri)
           (collect uri into created)))
        (finally
         (return (stable-set-difference edited created :test #'equal)))))

(defun project-relative-uri (uri &key (root (project-root-for uri)))
  (string+ root "/" (string-left-trim "/" uri)))

(defvar *dev-shm*
  (when-let (p (directory-exists-p #p"/dev/shm"))
    (namestring p)))

(defun lsp-diff-projects (uri project1 project2 &key (root (project-root-for uri)))
  "Return a list of LSP document changes objects between PROJECT1 and PROJECT2.
URI is only used to get the root of the project."
  (nest
   ;; When available, use a tmpfs for the temporary files.
   (let ((*temp-dir* (or *dev-shm* *temp-dir*))))
   (iter (for (k v1 v2) in-vector
              ;; Stable sort for transcript testing.
              (stable-sort-new (changed-evolve-files project1 project2)
                               #'string<=
                               :key #'car)))
   (let ((uri (project-relative-uri k :root root))))
   (cond
     ((and v1 v2)
      (nest
       (let* ((old-text (source-text v1))
              (new-text (source-text v2))))
       (unless (equal old-text new-text))
       (when-let ((edits (lsp-edit-diff-strings old-text new-text))))
       (collecting)
       (make 'TextDocumentEdit
             :text-document
             (make 'OptionalVersionedTextDocumentIdentifier
                   :uri uri
                   :version (if-let (b (uri-buffer uri))
                              (buffer-version b)
                              nil))
             :edits edits)))
     (v1
      (collecting (make 'DeleteFile :uri uri)))
     ;; Created.
     (v2
      (appending
       (list
        (make 'CreateFile
              :uri uri
              :options (make 'CreateFileOptions
                             :overwrite t))
        (make 'TextDocumentEdit
              :text-document
              (make 'OptionalVersionedTextDocumentIdentifier
                    :uri uri
                    :version nil)
              :edits
              (list
               (make 'TextEdit
                     :new-text
                     (source-text v2)
                     :range
                     (make 'Range
                           :start (make 'Position
                                        :line 0
                                        :character 0)
                           :end (make 'Position
                                      :line 0
                                      :character 0)))))))))))

(defclass annotation-refactoring ()
  ()
  (:documentation "Mixin for a refactoring that only contributes annotations."))

(defmethod refactoring-description :around ((self annotation-refactoring))
  (string+ "Annotation: " (call-next-method)))

(defgeneric annotation-preprocs (refactoring)
  (:documentation "List of new preprocessor definitions from REFACTORING.")
  (:method-combination append)
  (:method append ((self annotation-refactoring)) nil))

(defun add-preproc-list (root preproc-defs)
  "Add PREPROC-DEFS, a list of preprocessor definition ASTs, to ROOT."
  (assert (every (of-type '(or cpp-preproc-function-def cpp-preproc-include))
                 preproc-defs))
  (let* ((text (source-text root))
         (old-pds (collect-if (of-type 'cpp-preproc-function-def) root))
         (old-includes (collect-if (of-type 'cpp-preproc-include) root))
         (preproc-defs
           ;; Only insert the preproc defs that occur textually.
           (filter (lambda (pp)
                     (ematch pp
                       ((cpp-preproc-function-def
                         (cpp-name name))
                        (and (not (find-if (op (source-text= name (cpp-name _)))
                                           old-pds))
                             (string*= (source-text name) text)))
                       ((cpp-preproc-include
                         (cpp-path path))
                        (not (find-if (op (source-text= path (cpp-path _)))
                                      old-includes)))))
                   preproc-defs)))
    (with-object-or-ast (root)
      (if (no preproc-defs) root
          (copy root
                :children
                (append (mapcar #'tree-copy preproc-defs)
                        (direct-children root)))))))

(defmethod apply-changes :around ((self annotation-refactoring) project target change-table)
  (declare (ignore project target change-table))
  (add-preproc-list (call-next-method)
                    (annotation-preprocs self)))
