(defpackage :lilac-commons/analysis
  (:use :gt/full
        :lilac-commons/database)
  (:import-from :lilac-commons/aggressive-analysis
                :with-aggressive-analysis)
  (:shadowing-import-from :serapeum :~>)
  (:import-from :lilac-commons/commons
                :const-declaration?)
  (:import-from :trivia.fail :fail)
  (:import-from :software-evolution-library/software/rust)
  (:local-nicknames
   (:ast :software-evolution-library/software/parseable)
   (:attrs :functional-trees/attrs)
   (:cpp :software-evolution-library/software/cpp-project)
   (:dbg :software-evolution-library/utility/debug)
   (:dir :software-evolution-library/software/directory)
   (:project :software-evolution-library/software/project)
   (:sel :software-evolution-library)
   (:tg :trivial-garbage)
   (:ts :software-evolution-library/software/tree-sitter))
  (:export
    :param-ids
    :invoked-methods+operators
    :analyze-templates
    :with-aggressive-analysis
    :analyze-template-params
    :get-type-ids
    :collect-template-methods
    :collect-type-constraints
    :derive-traits
    :collect-template-methods
    :trait
    :op-trait
    :find-trait
    :trait-method-name
    :trait-type-id
    :trait-wants-reference?
    :trait-wants-mutable?
    :operator-traits
    :operator-trait
    :trait.name
    :trait.self-ref
    :trait.self-mut
    :trait.other-ref
    :trait.arity
    :analyze-template-params/attr
    :group-methods-by-trait
    :extract-type-methods
    :invoked-methods
    :invoked-operators
    :template-invocations
    :template-specializations))
(in-package :lilac-commons/analysis)

(deftype offset ()
  '(or (integer 0) (member :this :return)))

(define-table file-templates
  ((:file :type dir:file-ast)
   (:template :type ts:cpp-ast)))

(define-table invoked-methods
  ((:param :type ts:cpp-ast)
   (:method :type ts:cpp-ast)
   (:offset :type
            (or (member :return :this)
                (integer 0)))))

(define-table invoked-operators
  ((:param :type ts:cpp-ast)
   (:op :type ts:cpp-ast)
   (:side :type (member :lhs :rhs))))

(define-table template-specializations
  ((:template :type ts:cpp-template-declaration)
   (:location :type ts:cpp-ast)
   (:type-args :type vector))
  (:documentation
   "Table from template functions to types they are instantiated with."))

;;; Not a table from params to type constraints, but a table from
;;; /ids/ of the type of the parameter to type constraints.
(define-table param-ids
  ((:template)
   (:id)
   (:param-position)
   (:definition)
   (:type)
   (:param :type ts:cpp-ast)
   (:param-type)
   (:param-type-string :type string)))

(defun empty-analysis ()
  (ensure-tables (empty-database)
                 'file-templates
                 'invoked-methods
                 'invoked-operators
                 'param-ids))

;;; Trait information.

(defstruct-read-only (trait (:conc-name trait.)
                            (:constructor trait))
  "Properties of a trait."
  (name (error "No name") :type string)
  (self-ref nil :type boolean)
  (self-mut nil :type boolean)
  (other-ref nil :type boolean)
  ;; Arity 1 includes the self parameter.
  (arity 1 :type (integer 1 *)))

(defstruct-read-only (op-trait (:conc-name trait.)
                               (:include trait)
                               (:constructor op-trait))
  "Properties of a operator trait."
  (method-name nil :type (or null string)))

(def +trait-methods+
  (dictq
   "Display" ("fmt")
   "Sub" ("sub")
   "Add" ("add")))

(def +traits-by-name+
  (lret ((traits
          (list
           (op-trait :name "Add" :arity 2)
           (op-trait :name "AddAssign" :arity 2 :self-ref t :self-mut t)
           (op-trait :name "BitAnd" :arity 2)
           (op-trait :name "BitAndAssign" :arity 2 :self-ref t :self-mut t)
           (op-trait :name "BitOr" :arity 2)
           (op-trait :name "BitOrAssign" :arity 2 :self-ref t :self-mut t)
           (op-trait :name "BitXorAssign" :arity 2 :self-ref t :self-mut t)
           (op-trait :name "Deref" :self-ref t :self-mut nil)
           (op-trait :name "DerefMut" :self-ref t :self-mut t)
           (op-trait :name "Div" :arity 2)
           (op-trait :name "Fn" :arity 1 :self-ref t :self-mut t)
           (op-trait :name "Fn" :arity 1 :self-ref t)
           (op-trait :name "Mul" :arity 2)
           (op-trait :name "Shl" :arity 2)
           (op-trait :name "ShlAssign" :arity 2 :self-ref t :self-mut t)
           (op-trait :name "Shr" :arity 2)
           (op-trait :name "ShrAssign" :arity 2 :self-ref t :self-mut t)
           (op-trait :name "Sub" :arity 2)
           (op-trait :name "SubAssign" :arity 2 :self-ref t :self-mut t)))
         (table (make-hash-table :test 'equalp)))
    (dolist (trait traits)
      (setf (gethash (trait.name trait) table) trait)))
  "Property lists for traits.
Unless specified, ARITY is assumed to be 1, and the self parameter is
assumed to be immutable and not a reference.")

(defgeneric find-trait (trait)
  (:method ((trait-name string))
    (href +traits-by-name+ trait-name))
  (:method ((ast ast:ast))
    (find-trait (ast:source-text ast)))
  (:method ((trait trait))
    trait))

(defun trait-method-name (trait)
  (when-let (trait (find-trait trait))
    (or (trait.method-name trait)
        (string-downcase (trait.name trait)))))

(defun trait-type-id (trait)
  (make 'ts:rust-type-identifier :text (trait.name trait)))

(defgeneric trait-wants-reference? (trait &key rhs)
  (:method ((trait-name string) &key rhs)
    (trait-wants-reference? (href +traits-by-name+ trait-name) :rhs rhs))
  (:method ((ast ast:ast) &key rhs)
    (trait-wants-reference? (ast:source-text ast) :rhs rhs))
  (:method ((trait null) &key rhs)
    (declare (ignore rhs))
    t)
  (:method ((trait trait) &key rhs)
    (if rhs
        (trait.other-ref trait)
        (trait.self-ref trait))))

(defgeneric trait-wants-mutable? (trait)
  (:method ((trait-name string))
    (trait-wants-mutable? (href +traits-by-name+ trait-name)))
  (:method ((ast ast:ast))
    (trait-wants-mutable? (ast:source-text ast)))
  (:method ((trait trait))
    (trait.self-mut trait))
  (:method ((trait null))
    nil))

;;; TODO Obtain programmatically (from source or docs?).
(def +operator-traits+
  (dictq
   ;; NB No && and || by design.
   "+" "Add"
   "+=" "AddAssign"
   "&" "BitAnd"
   "&=" "BitAndAssign"
   "|" "BitOr"
   "=" "BitOrAssign"
   "^" "BitXor"
   "^=" "BitXorAssign"
   "*x" ("Deref" "DerefMut")
   "/"	"Div"
   "/="	"DivAssign"
   ;; TODO drop
   "()" ("Fn" "FnMut" "FnOnce")
   "[]" ("Index" "IndexMut")
   "*" "Mul"
   "*=" "MulAssign"
   "-" ("Sub" "Neg")
   "!" "Not"
   ;; TODO RangeBounds
   "%" "Rem"
   "%=" "RemAssign"
   ;; NB << and >> get special handling for type inference; not the
   ;; same as x.shl or x.shr.
   "<<" "Shl"
   "<<=" "ShlAssign"
   ">>" "Shr"
   ">>=" "ShrAssign"
   "-" "Sub"
   "-=" "SubAssign")
  "Rust operator traits as exported by std::ops (or the prelude).
See <https://doc.rust-lang.org/std/ops/index.html>.")

(defun operator-traits (operator)
  (gethash (drop-prefix "operator" (ast:source-text operator))
           +operator-traits+))

(defun operator-trait (ast fn)
  (assure ts:rust-ast
    (ematch (operator-traits ast)
      ((list (and sub "Sub") (and neg "Neg"))
       (let ((params (find-if (of-type 'ts:parameters-ast) fn)))
         (ecase (length (children params))
           (0 (make 'ts:rust-type-identifier :text neg))
           (1 (make 'ts:rust-type-identifier :text sub)))))
      ;; TODO Is FnOnce actually possible?
      ((list (and fn-trait "Fn") (and fn-mut-trait "FnMut") "FnOnce")
       (make 'ts:rust-type-identifier
             :text
             (if (const-declaration? fn)
                 fn-trait fn-mut-trait)))
      ((and trait (type string))
       (make 'ts:rust-type-identifier :text trait)))))


;;; Template analysis.

(defun same-type? (ast1 ast2)
  (or (ast:source-text= ast1 ast2)
      (ignore-errors
       (let ((root (attrs:attrs-root*)))
         (eql (ts:find-enclosing-declaration :type root ast1)
              (ts:find-enclosing-declaration :type root ast2))))))

(defun ast-typep (ast type)
  (when-let* ((type (ts:get-declaration-id :type type))
              (itype (ts:infer-type ast)))
    (same-type? type itype)))

(defun get-type-ids (type ast)
  "Find every binding of TYPE in AST."
  (nub
   (filter-map (op (ts:get-declaration-id :variable _))
               (filter (op (ast-typep _ type))
                       (ast:collect-if (of-type 'ts:identifier-ast) ast)))))

(defun get-type-invoked-methods+operators (type ast)
  "Get the methods and operators invoked on every binding of TYPE in AST."
  (let ((all-methods (queue))
        (all-operators (queue)))
    (dolist (id (get-type-ids type ast))
      (multiple-value-bind (methods operators)
          (invoked-methods+operators id ast)
        (qappend all-methods methods)
        (qappend all-operators operators)))
    (values (qlist all-methods)
            (qlist all-operators))))

(-> extract-type-methods (ts:type-declaration-ast)
  (values (soft-list-of ast:ast) &optional))
(defun extract-type-methods (type-decl)
  "Extract method names from TYPE-DECL."
  (let* ((table (ts::field-table type-decl))
         (function-table
           (@ table :function))
         (range (and function-table (fset:range function-table))))
    (apply #'append (convert 'list range))))

(defun collect-type-constraints (ast)
  "Collect a plist of type definitions from AST.

METHOD-INVOCATIONS is a list of methods invoked on variable of the
type in AST."
  (iter (for type-decl in (ast:collect-if (of-type 'ts:type-declaration-ast) ast))
        (collecting
          ;; TODO Handle operators as well.
          (mvlet* ((type-name (ts:definition-name-ast type-decl))
                   (invoked-methods operators
                    (get-type-invoked-methods+operators type-name ast)))
            (list :type type-name
                  :type-decl type-decl
                  :method-invocations
                  (mapcar
                   (lambda-ematch
                     ((cons method offset)
                      (list
                       method
                       (ts:find-enclosing 'ts:call-ast
                                          (ts:attrs-root*)
                                          method)
                       offset)))
                   invoked-methods)
                  :operators operators
                  :methods (extract-type-methods type-decl))))))


(-> collect-template-methods (list list)
  (cons ts:type-declaration-ast
        (soft-list-of ast:ast)))
(defun collect-template-methods (constraint templates-analysis)
  "Collect an alist from type definitions to the methods invoked on them in templates."
  (destructuring-bind
      (&key type type-decl method-invocations methods &allow-other-keys)
      constraint
    (declare (ignorable type methods))
    (cons type-decl
          (iter outer
                (for (method-name nil offset) in method-invocations)
                (when-let* ((fdef (ts:get-declaration-id :function method-name))
                            (template (ts:find-enclosing
                                       'ts:cpp-template-declaration
                                       (attrs:attrs-root*)
                                       fdef))
                            (template-analysis
                             (find template templates-analysis
                                   :key (op (getf _ :template))))
                            (params (getf template-analysis :params)))
                  (etypecase-of offset offset
                    ((eql :this)
                     (collecting
                       (let ((text (ast:source-text method-name)))
                         (find text methods :test #'ast:source-text=))))
                    ((eql :return))
                    ((integer 0)
                     (iter (for param in params)
                           (for param-offset from 0)
                           (when (eql param-offset offset)
                             ;; TODO Collect operators too.
                             (let* ((invoked-methods
                                      (~> (getf param :ids)
                                          (mappend (op (getf _ :invoked-methods)) _)
                                          (keep :this _ :key #'cdr)
                                          nub
                                          (mapcar #'car _)))
                                    (hash-set
                                      (set-hash-table invoked-methods
                                                      :key #'ast:source-text
                                                      :test #'equal)))
                               (in outer (appending
                                          (filter
                                           (lambda (method)
                                             (gethash
                                              (ast:source-text method)
                                              hash-set))
                                           methods)))))))))))))

(-> group-methods-by-trait
  ((soft-list-of ast:ast))
  (values (soft-alist-of string (soft-list-of ast:ast)) &optional))
(defun group-methods-by-trait (methods)
  ;; TODO Trivial implementation.
  (let ((dict (dict)))
    (iter (for method in methods)
          (let ((name (string-capitalize (ast:source-text method))))
            (pushnew method (gethash name dict))))
    (reverse
     (sort (hash-table-alist dict)
           (ordering (mapcar #'ast:source-text methods)
                     :test #'equal)
           :key #'car))))

(defun types-to-traits (ast &key template-analysis)
  "Return an alist from the types defined in AST to the methods that
are invoked on them in templates (for which they may need to
specialize traits)."
  (let* ((template-analysis (or template-analysis (analyze-templates ast)))
         (type-constraints (collect-type-constraints ast)))
    (iter (for constraint in type-constraints)
          (destructuring-bind (type . methods)
              (collect-template-methods constraint template-analysis)
            (collect (cons type (group-methods-by-trait methods)))))))

(defun template-param-traits (ast &key
                                    (template-analysis (analyze-templates ast))
                                    (types-to-traits
                                     (types-to-traits ast :template-analysis template-analysis)))
  (let ((methods-to-traits
          (lret ((dict (dict)))
            (iter (for (nil . traits-to-methods) in types-to-traits)
                  (iter (for (trait . trait-methods) in traits-to-methods)
                        (iter (for method in trait-methods)
                              (pushnew trait
                                       (gethash (ast:source-text method) dict)
                                       :test #'equal)))))))
    (iter (for plist in template-analysis)
          (destructuring-bind (&key template params &allow-other-keys)
              plist
            (collecting
              (cons template
                    (iter (for param in params)
                          (let ((methods
                                  (destructuring-bind (&key ids &allow-other-keys) param
                                    (mapcar #'car
                                            (mappend (op (getf _ :invoked-methods))
                                                     ids)))))
                            (collecting
                              (iter (for method in methods)
                                    (appending (gethash (ast:source-text method)
                                                        methods-to-traits))))))))))))


(defun derive-traits (ast)
  (when (find-if (of-type 'ts:cpp-template-declaration) ast)
    (with-aggressive-analysis ()
      (let* ((template-analysis
               (analyze-templates ast))
             (types-to-traits
               (types-to-traits ast :template-analysis template-analysis))
             (template-param-traits
               (template-param-traits ast
                                      :template-analysis template-analysis
                                      :types-to-traits types-to-traits)))
        (values types-to-traits
                template-param-traits)))))

(defun invoked-methods+operators (id ast)
  "Get all methods and operators invoked on ID in AST.
ID should be the identifier from an initialization."
  (assert (ts:find-enclosing-declaration 'ts:variable-declaration-ast
                                         (attrs:attrs-root*)
                                         id))
  (iter methods-loop
        (for node in-tree ast)
        (match node
          ((and (ts:call-ast)
                (access #'ts:call-arguments args)
                (access #'ts:call-function fn))
           (when (string^= "__CRAM__" (ast:source-text fn))
             (fail))
           ;; This is a method call and the object is of the right type.
           (match fn
             ((ts:cpp-field-expression
               (ts:cpp-argument arg)
               (ts:cpp-field field))
              (when (eql id (ts:get-declaration-id :variable arg))
                (collect (cons field :this) into methods))))
           ;; Collect arguments of the right type.
           (iter (for offset from 0)
                 (for arg in args)
                 (when (eql id (ts:get-declaration-id :variable arg))
                   (in methods-loop
                       (collect (cons fn offset) into methods))))
           ;; Collect the return type.
           (when-let (return-type (ts:infer-type node))
             (when (eql return-type
                        (ts:get-declaration-id
                         :variable return-type))
               (collect (cons fn :return) into methods))))
          ((and binop
                (ts:binary-ast)
                (access #'ts:lhs lhs)
                (access #'ts:rhs rhs))
           (when (typep lhs 'ts:identifier-ast)
             (when (eql id (ts:get-declaration-id :variable lhs))
               (collect (cons (ts:operator binop) :lhs) into binops)))
           (when (typep rhs 'ts:identifier-ast)
             (when (eql id (ts:get-declaration-id :variable rhs))
               (collect (cons (ts:operator binop) :rhs) into binops)))
           (when-let (return-type (ts:infer-type binop))
             (when (eql return-type
                        (ts:get-declaration-id
                         :variable return-type))
               (collect (cons (ts:operator binop) :return) into binops)))))
        (finally (return-from methods-loop (values methods binops)))))

(defun analyze-template-params (template
                                &key verbose (stream t) (db (empty-analysis)))
  "Analyze a single template.
Return a database with tables for parameters, methods invoked on
parameters in the template, and operators invoked on parameters in the
template."
  (when verbose
    (format stream "  Analyzing template \"~a\""
            (first (lines (ast:source-text template)))))
  (let ((params (children (ts:cpp-parameters template))))
    (when (every (of-type 'ts:cpp-type-parameter-declaration) params)
      (let ((ids (filter (op (ts:find-enclosing 'ts:expression-ast template _))
                         (ts:identifiers template)))
            (definition (find-if (of-type 'ts:definition-ast)
                                 (children template))))
        (iter (for param in params)
              (for pos from 0)
              (let ((param-type (only-elt (ts:parameter-names param))))
                (iter (for id in ids)
                      (when-let ((id-type (ts:infer-type id)))
                        (when (ast:source-text= id-type param-type)
                          (when-let ((decl-id (ts:get-declaration-id :variable id)))
                            (unless (find decl-id seen-ids)
                              (multiple-value-bind (methods operators)
                                  (invoked-methods+operators decl-id template)
                                (iter (for (method . offset) in methods)
                                      (upsertf db
                                               'invoked-methods
                                               :param param
                                               :method method
                                               :offset offset))
                                (iter (for (op . pos) in operators)
                                      (upsertf db
                                               'invoked-operators
                                               :param param
                                               :op op
                                               :pos pos))
                                (collect id into seen-ids)
                                (upsertf db 'param-ids
                                         :template template
                                         :id decl-id
                                         :param-position pos
                                         :definition definition
                                         :type id-type
                                         :param param
                                         :param-type param-type
                                         :param-type-string
                                         (ast:source-text param-type)))))))))))))
  db)

(attrs:def-attr-fun analyze-template-params/attr ()
  (:method ((template ts:cpp-template-declaration))
    (analyze-template-params template)))

(defun analyze-template-specializations (root &key (db (empty-database)))
  "Augment DB with a table that analyzes how templates are specialized
ROOT."
  (let ((db (ensure-tables db 'template-specializations)))
    (labels ((find-enclosing-template (ast)
               (ts:find-enclosing 'ts:cpp-template-declaration
                                  (attrs:attrs-root*)
                                  ast)))
      (iter (for node in-tree root)
            (typecase node
              ;; Collect specializations from types.
              (ts:cpp-template-type
               (let* ((type (ts:cpp-name node))
                      (arguments (ts:cpp-arguments node)))
                 (when-let* ((type-args (children arguments))
                             (type-def (ts:get-declaration-id :type type))
                             (template (find-enclosing-template type-def)))
                   (upsertf db 'template-specializations
                            :template template
                            :location node
                            :type-args (coerce type-args 'vector)))))
              ;; Collect specializations from invocations.
              (ts:call-ast
               (when (typep node 'ts:call-ast)
                 (let* ((call node)
                        (fn (ts:call-function call))
                        (args (ts:call-arguments call)))
                   (typecase fn
                     (ts:cpp-field-expression)
                     (ts:cpp-template-function
                      (let ((name (ts:cpp-name fn))
                            (type-args (ts:cpp-arguments fn)))
                        (when-let* ((id (ts:get-declaration-id :function name))
                                    (template (find-enclosing-template id)))
                          (upsertf db 'template-specializations
                                   :template template
                                   :location node
                                   :type-args (coerce type-args 'vector)))))
                     (ts:identifier-ast
                      (unless (ts:get-declaration-id :macro fn)
                        (when-let* ((id (ts:get-declaration-id :function fn))
                                    (template (find-enclosing-template id)))
                          (upsertf db 'template-specializations
                                   :template template
                                   :location node
                                   :type-args
                                   (map 'vector #'ts:infer-type args))))))))))))
    db))

(defgeneric analyze-templates (source &key verbose stream project db file)
  (:documentation "Analyze templates in FILE")
  (:method ((source dir:file-ast)
            &rest kwargs
            &key verbose stream project (db (empty-analysis)) file)
    (declare (ignore project file))
    (when verbose
      (format stream "~&Analyzing file ~a~%"
              (dir:full-pathname source)))
    (apply #'analyze-templates (first (dir:contents source))
           :file source
           :db db
           kwargs))
  (:method ((source sel:software)
            &rest kwargs
              &key verbose stream project (db (empty-analysis)) file)
    (declare (ignore verbose stream project db file))
    (apply #'analyze-templates (sel:genome source) kwargs))
  (:method ((root ast:ast)
            &key verbose (stream t) project (db (empty-analysis)) file)
    (when project
      (assert (ast:ast-path project root)))
    (setf db (analyze-template-specializations root :db db))
    (when-let ((templates
                (ast:collect-if (of-type 'ts:cpp-template-declaration)
                                root)))
      (iter (for template in templates)
            (when file
              (upsertf db 'file-templates
                       :file file
                       :template template))
            (restart-case
                (setf db
                      (analyze-template-params template
                                               :stream stream
                                               :verbose verbose
                                               :db db))
              (continue ()
                :report "Skip template"
                (format stream "~&Skipped ~a~%" template)
                (next-iteration)))))
    db)
  (:method ((project dir:directory-project)
            &key verbose (stream t) only ((:project project*) nil)
              (db (empty-analysis))
              file)
    (declare (ignore project* file))
    (attrs:with-attr-table project
      (let* ((files (ast:collect-if (of-type 'dir:file-ast) (sel:genome project)))
             (files (if (not only)
                        files
                        (keep only files
                              :key (compose #'namestring #'dir:full-pathname)
                              :test #'equal)))
             (skip (empty-set)))
        (unless files
          (return-from analyze-templates nil))
        (when verbose
          (format stream "~&Analyzing ~a file~:p"
                  (length files)))
        (iter (for file-ast in files)
              (for file = (first (dir:contents file-ast)))
              (unless (contains? skip file)
                (tagbody :retry         ; Iterate can't parse nlet.
                   (restart-case
                       (setf db
                             (analyze-templates file :project project
                                                     :verbose verbose
                                                     :stream stream
                                                     :file file-ast
                                                     :db db))
                     (continue ()
                       :report "Skip file"
                       (withf skip file)
                       (next-iteration))
                     (retry ()
                       :report "Retry file"
                       (go :retry))))))
        db))))
